﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageErrorControl.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.MessageErrorControl" %>
<table>
    <tr>
        <td align="center" valign="middle">
            <asp:Image ID="imgMessage" runat="server" Width="20px" Height="20px" />
        </td>
        <td align="center" valign="middle">
            <asp:Label ID="lblMessage" runat="server" Text="Label" Font-Bold="True" Font-Names="Arial"
                Font-Size="10pt"></asp:Label>
        </td>
    </tr>
</table>
