﻿using System;
using System.Web;
using ITC.Library.Classes;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess
{
    public partial class HomePage : ItcBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["ModuleId"] = ModuleConfiguration.ModuleID;
            if (HttpContext.Current.User==null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect(Intranet.Configuration.Settings.PortalSettings.PortalPath);
            }
            if (!IsPostBack)
            {
                this.Page.Title = "سامانه مدیریتی دسترسی ها";

                ItcMenu.LoadMenuItc();
              //  CustomItcMenuWithRowAccess1.LoadMenuItc("[acs].[v_PubItcMenu]");
            }
        }

        #region Procedure:
        private void AddPageView()
        {
            var pageView = new RadPageView() { ID = ViewState["MenuPageName"].ToString().Replace(@"\", "") };
            radmultipageMenu.PageViews.Add(pageView);
        }

        //public string GetSingleFilrtNameAndLastnameuser()
        //{
        //    DataSet ds = Classes.User.GetSingleUserFirstNameAndLastName(Classes.Variables.GetUserId());
        //    string s = "";
        //    s = s + ds.Tables[0].Rows[0]["FirstName"].ToString();
        //    s = s + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
        //    return s;

        //}

        #endregion

        protected void radmultipageMenu_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            string userControlName = @"DeskTopModules\" + ViewState["MenuPageName"] + ".ascx";
            var userControl = (Page.LoadControl(userControlName));
            if (userControl != null)
            {
                userControl.ID = ViewState["MenuPageName"].ToString().Replace(@"\", "") + "_userControl";
            }
            e.PageView.Controls.Add(userControl);
            e.PageView.Selected = true;

        }

        protected void CustomItcMenu1_ItemClick(object sender, RadPanelBarEventArgs e)
        {
            var ClassItcMenu = ITC.Library.Classes.JsonExecutor.DeserializationCustomData(e.Item.Value, new ITC.Library.Classes.ItcMenuParameter());
            var ClassItcMenuParameter = ((ITC.Library.Classes.ItcMenuParameter)(ClassItcMenu));
            this.Page.Title = e.Item.Text;
            if (ClassItcMenuParameter.IsLoadControl == true)
            {
                if (!string.IsNullOrEmpty(ClassItcMenuParameter.PageName))
                {
                    ViewState["MenuPageName"] = ClassItcMenuParameter.PageName;
                    radmultipageMenu.PageViews.Clear();
                    AddPageView();
                 //   ((radmultipageMenu.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl).InitControl();
                    lblSubControlTitle.Text = e.Item.Text;
                }
            }
            else
            {
                string PageUrl;
                Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" +
                ModuleConfiguration.ModuleID + "&page=" + ClassItcMenuParameter.PageName));
            }
        }

    }
}