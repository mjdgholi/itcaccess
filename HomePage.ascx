﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePage.ascx.cs" Inherits="Intranet.DesktopModules.ItcAccess.HomePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<script type="text/javascript">
    function conditionalPostback(sender, args) {
        //alert(args.get_eventTarget());
        try {
            if (args.get_eventTarget().indexOf("WithUploadFile") >= 0) {
                args.set_enableAjax(false);
            }
        } catch (err) {

        }


    }
</script>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <style>
        .title
        {
            background-image: url("<%=Intranet.Configuration.Settings.PortalSettings.PortalPath +"/DesktopModules/AssessmentProject/Assessment/Images/Tile.png"%>");
            border-style: none;
        }
    </style>
</telerik:RadScriptBlock>
<table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" valign="middle" class="title" height="30">
            <asp:Label ID="lblSubControlTitle" runat="server" Font-Bold="True" Font-Names="B Titr"
                Font-Size="12pt" ForeColor="White">سامانه مدیریت دسترسی ها</asp:Label>
        </td>
    </tr>
</table>
<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Skin="Office2007"
    Height="5000px" BackColor="White">
    <telerik:RadPane ID="RadPane1" runat="server" Width="22px" Scrolling="None" Skin="Office2007"
        Height="5000px">
        <telerik:RadSlidingZone ID="RadSlidingZone1" runat="server" Width="22px" Skin="Office2007"
            Height="5000px">
            <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server" Skin="Office2007" Title="مـنـوی اصلی"
                Width="265" Height="5000px">
                
                <cc1:CustomItcMenu ID="ItcMenu" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    Skin="Office2007" MenuImageUrl="Training\Images" OnItemClick="CustomItcMenu1_ItemClick"
                    CausesValidation="False" ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True">
                </cc1:CustomItcMenu>
                
              <%--   <cc1:CustomItcMenuWithRowAccess ID="CustomItcMenuWithRowAccess1" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    MenuImageUrl="Training\Images" OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True" 
                    Skin="Office2007">
                </cc1:CustomItcMenuWithRowAccess>--%>

            </telerik:RadSlidingPane>
        </telerik:RadSlidingZone>
    </telerik:RadPane>
    <telerik:RadPane ID="RadPane2" runat="server" Height="5000px" Scrolling="None">
        <telerik:RadMultiPage ID="radmultipageMenu" runat="server" Width="100%" OnPageViewCreated="radmultipageMenu_PageViewCreated"
            BackColor="White" Height="5000px">
        </telerik:RadMultiPage>
    </telerik:RadPane>
</telerik:RadSplitter>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ItcMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="ItcMenu" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="ItcMenu" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
<cc1:ResourceJavaScript ID="ResourceJavaScript1" runat="server" />
<telerik:RadTreeView ID="RadTreeView1" runat="server" Skin="Office2007">
    <ContextMenus>
        <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
            <Items>
                <telerik:RadMenuItem ImageUrl="../Images/Edit.bmp" Text="ویرایش" Value="edit">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem ImageUrl="../Images/Delete.bmp" Text="حذف" Value="remove">
                </telerik:RadMenuItem>
            </Items>
            <CollapseAnimation Type="OutQuint" />
        </telerik:RadTreeViewContextMenu>
    </ContextMenus>
</telerik:RadTreeView>
