﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsDveloperControl"

    /// <summary>
    /// 
    /// </summary>
    public class AcsDveloperControl : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int DveloperControlId { get; set; }
        public string DveloperControlTitle { get; set; }

        public string DeveloperControlEnglishTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsDveloperControl()
        {
        }

        public AcsDveloperControl(int _DveloperControlId, string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            DveloperControlId = _DveloperControlId;
            this.DveloperControlTitle = DveloperControlTitle;
            this.DeveloperControlEnglishTitle = DeveloperControlEnglishTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :       

        #region Static Methods :

        public static void Add(string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
             AcsDveloperControlDB.AddAcsDveloperControlDB(DveloperControlTitle, DeveloperControlEnglishTitle, IsActive, CreationDate, ModificationDate,out _AddedId);
            
        }

        public static void Update(int _DveloperControlId, string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsDveloperControlDB.UpdateAcsDveloperControlDB(_DveloperControlId, DveloperControlTitle, DeveloperControlEnglishTitle, IsActive, CreationDate, ModificationDate);
            
        }

        public static void Delete(int DveloperControlId)
        {
             AcsDveloperControlDB.DeleteAcsDveloperControlDB(DveloperControlId);
           
        }

        public static AcsDveloperControl GetSingleById(int _DveloperControlId)
        {
            AcsDveloperControl o = GetAcsDveloperControlFromAcsDveloperControlDB(
            AcsDveloperControlDB.GetSingleAcsDveloperControlDB(_DveloperControlId));

            return o;
        }

        public static List<AcsDveloperControl> GetAll()
        {
            List<AcsDveloperControl> lst = new List<AcsDveloperControl>();
            //string key = "AcsDveloperControl_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsDveloperControl>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsDveloperControlCollectionFromAcsDveloperControlDBList(
            AcsDveloperControlDB.GetAllAcsDveloperControlDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsDveloperControl> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsDveloperControl_List_Page_" + currentPage ;
            //string countKey = "AcsDveloperControl_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsDveloperControl> lst = new List<AcsDveloperControl>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsDveloperControl>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsDveloperControlCollectionFromAcsDveloperControlDBList(
            AcsDveloperControlDB.GetPageAcsDveloperControlDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsDveloperControl GetAcsDveloperControlFromAcsDveloperControlDB(AcsDveloperControlDB o)
        {
            if (o == null)
                return null;
            AcsDveloperControl ret = new AcsDveloperControl(o.DveloperControlId, o.DveloperControlTitle, o.DeveloperControlEnglishTitle, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsDveloperControl> GetAcsDveloperControlCollectionFromAcsDveloperControlDBList(List<AcsDveloperControlDB> lst)
        {
            List<AcsDveloperControl> RetLst = new List<AcsDveloperControl>();
            foreach (AcsDveloperControlDB o in lst)
            {
                RetLst.Add(GetAcsDveloperControlFromAcsDveloperControlDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}