﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class PortalUser
    {
        public static DataSet PortalUserGetAll()
        {
            return PortalUserDB.PortalUserGetAll();
        }
    }
}