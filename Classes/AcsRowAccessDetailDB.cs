﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsRowAccessDetailDB"

    public class AcsRowAccessDetailDB
    {

        #region Properties :

        public int RowAccessDetailId { get; set; }
        public int RowAccessId { get; set; }

        public int? ObjectFieldId { get; set; }

        public int? OperatorId { get; set; }

        public int? FunctionId { get; set; }

        public int? ConditionId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int? OwnerId { get; set; }

        public string ValuesOrFunction { get; set; }

        public int? SystemRowAccessDetailOwnerId { get; set; }

        #endregion

        #region Constrauctors :

        public AcsRowAccessDetailDB()
        {
        }

        public AcsRowAccessDetailDB(int _RowAccessDetailId, int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, string ValuesOrFunction, int? SystemRowAccessDetailOwnerId)
        {
            RowAccessDetailId = _RowAccessDetailId;
            this.RowAccessId = RowAccessId;
            this.ObjectFieldId = ObjectFieldId;
            this.OperatorId = OperatorId;
            this.FunctionId = FunctionId;
            this.ConditionId = ConditionId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.OwnerId = OwnerId;
            this.ValuesOrFunction = ValuesOrFunction;
            this.SystemRowAccessDetailOwnerId = SystemRowAccessDetailOwnerId;
        }

        #endregion

        #region Methods :

        // -------------- start own function ---------------

        public static DataSet GetSqlDataResultForRowAccess(int objectId, string userAndGroupRoleAccesstr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@UserAndGroupRoleAccesstr", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = userAndGroupRoleAccesstr;
            var dataset = _intranetDB.RunProcedureDS("acs.GetSqlDataResultForRowAccess", parameters);
            if (dataset != null) return dataset;

            return new DataSet();
        }

        public static DataSet GetSqlDataResultForRowAccessFromRowAccessStr(int objectId, string rowAccessStr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@RowAccessStr", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = rowAccessStr;
            var dataset = _intranetDB.RunProcedureDS("acs.[GetSqlDataResultForRowAccessFromRowAccessStr]", parameters);
            if (dataset != null) return dataset;

            return new DataSet();
        }

        public static DataSet RunSqlCommand(int rowAccessId, string strSql)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@StrSql", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = rowAccessId;
            parameters[1].Value = strSql;
            return _intranetDB.RunProcedureDS("acs.RunSqlCommand", parameters);
        }

        public static DataSet RowAccessDetailGetAll(int rowAccessId, bool loadUserData)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@UserData", SqlDbType.Bit),
                };
            parameters[0].Value = rowAccessId;
            parameters[1].Value = loadUserData;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            var dataset = new DataSet();
            using (var sqlCommand = new SqlCommand("acs.[p_RowAccessDetailGetAll]", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                SqlDataAdapter sqlDataAdapter=new SqlDataAdapter(sqlCommand);
                
                sqlDataAdapter.Fill(dataset);
                sqlConnection.Close();
            }
            return dataset;
            //return _intranetDB.RunProcedureDS("acs.p_RowAccessDetailGetAll", parameters);
        }

        public static void p_AcsRowAccessDetailAddInTree(int RowAccessId, int? UserObjectFieldId, int? OperatorId, int? FunctionId, string Values, string userValue, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@UserObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@OperatorId", SqlDbType.Int),
                    new SqlParameter("@FunctionId", SqlDbType.Int),
                    new SqlParameter("@Values", SqlDbType.NVarChar, -1),
                    new SqlParameter("@UserValue", SqlDbType.NVarChar, -1),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@OwnerId", SqlDbType.Int),
                    new SqlParameter("@RowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RowAccessId;
            parameters[1].Value = (UserObjectFieldId != -1 ? (object) UserObjectFieldId : DBNull.Value);
            parameters[2].Value = (OperatorId != -1 ? (object) OperatorId : DBNull.Value);
            parameters[3].Value = (FunctionId != -1 ? (object) FunctionId : DBNull.Value);
            parameters[4].Value = GeneralDB.ConvertTo256(Values);
            parameters[5].Value = GeneralDB.ConvertTo256(userValue);
            parameters[6].Value = (ConditionId != -1 ? (object) ConditionId : DBNull.Value);
            parameters[7].Value = CreationDate;
            parameters[8].Value = ModificationDate;
            parameters[9].Value = (OwnerId != -1 ? (object) OwnerId : DBNull.Value);
            parameters[10].Direction = ParameterDirection.Output;
            parameters[11].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection=Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.[p_AcsRowAccessDetailAddInTree]", sqlConnection))
            {
                sqlCommand.CommandType=CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            //_intranetDB.RunProcedure("acs.[p_AcsRowAccessDetailAddInTree]", parameters);
            if (parameters[10].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[10].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void AcsRowAccessDetailUpdateInTree(int RowAccessDetailId, int? userObjectFieldId, int? OperatorId, int? FunctionId, string values, string userValues, int? ConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@UserObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@OperatorId", SqlDbType.Int),
                    new SqlParameter("@FunctionId", SqlDbType.Int),
                    new SqlParameter("@Values", SqlDbType.NVarChar, -1),
                    new SqlParameter("@UserValue", SqlDbType.NVarChar, -1),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = RowAccessDetailId;
            parameters[1].Value = (userObjectFieldId != -1 ? (object) userObjectFieldId : DBNull.Value);
            parameters[2].Value = (OperatorId != -1 ? (object) OperatorId : DBNull.Value);
            parameters[3].Value = (FunctionId != -1 ? (object) FunctionId : DBNull.Value);
            parameters[4].Value = GeneralDB.ConvertTo256(values);
            parameters[5].Value = GeneralDB.ConvertTo256(userValues);
            parameters[6].Value = (ConditionId != -1 ? (object) ConditionId : DBNull.Value);
            parameters[7].Direction = ParameterDirection.Output;
            //_intranetDB.RunProcedure("acs.[p_AcsRowAccessDetailUpdateInTree]", parameters);

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.[p_AcsRowAccessDetailUpdateInTree]", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsRowAccessDetailFromTree(int userRowAccessDetailId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserRowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = userRowAccessDetailId;
            parameters[1].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.[p_AcsRowAccessDetailDelFromTree]", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
         
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static DataSet RowAccessGetChildTree(int RowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int, 4),
                };
            parameters[0].Value = RowAccessId;
            return _intranetDB.RunProcedureDS("acs.[p_ReportChildTreeForRowAccess]", parameters);
        }


        // -------------- end own function -----------------

        public static void AddAcsRowAccessDetailDB(int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@ObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@OperatorId", SqlDbType.Int),
                    new SqlParameter("@FunctionId", SqlDbType.Int),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@OwnerId", SqlDbType.Int),
                    new SqlParameter("@RowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RowAccessId;
            parameters[1].Value = ObjectFieldId;
            parameters[2].Value = (OperatorId != -1 ? (object) OperatorId : DBNull.Value);
            parameters[3].Value = (FunctionId != -1 ? (object) FunctionId : DBNull.Value);
            parameters[4].Value = (ConditionId != -1 ? (object) ConditionId : DBNull.Value);
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Value = (OwnerId != -1 ? (object) OwnerId : DBNull.Value);
            parameters[8].Direction = ParameterDirection.Output;
            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRowAccessDetailAdd", parameters);
            if (parameters[8].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[8].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsRowAccessDetailDB(int RowAccessDetailId, int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@ObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@OperatorId", SqlDbType.Int),
                    new SqlParameter("@FunctionId", SqlDbType.Int),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@OwnerId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = RowAccessDetailId;

            parameters[1].Value = RowAccessId;
            parameters[2].Value = (ObjectFieldId != -1 ? (object) ObjectFieldId : DBNull.Value);
            parameters[3].Value = (OperatorId != -1 ? (object) OperatorId : DBNull.Value);
            parameters[4].Value = (FunctionId != -1 ? (object) FunctionId : DBNull.Value);
            parameters[5].Value = (ConditionId != -1 ? (object) ConditionId : DBNull.Value);
            parameters[6].Value = CreationDate;
            parameters[7].Value = ModificationDate;
            parameters[8].Value = (OwnerId != -1 ? (object) OwnerId : DBNull.Value);
            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRowAccessDetailUpdate", parameters);
            string messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsRowAccessDetailDB(int RowAccessDetailId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessDetailId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RowAccessDetailId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRowAccessDetailDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsRowAccessDetailDB GetSingleAcsRowAccessDetailDB(int RowAccessDetailId)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@RowAccessDetailId", DataTypes.integer, 4, RowAccessDetailId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsRowAccessDetailGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsRowAccessDetailDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsRowAccessDetailDB> GetAllAcsRowAccessDetailDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsRowAccessDetailDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsRowAccessDetailGetAll", new IDataParameter[] {}));
        }

        public static List<AcsRowAccessDetailDB> GetPageAcsRowAccessDetailDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsRowAccessDetail";
            parameters[5].Value = "RowAccessDetailId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsRowAccessDetailDBCollectionFromDataSet(ds, out count);
        }

        public static AcsRowAccessDetailDB GetAcsRowAccessDetailDBFromDataReader(IDataReader reader)
        {
            return new AcsRowAccessDetailDB(int.Parse(reader["RowAccessDetailId"].ToString()),
                                            int.Parse(reader["RowAccessId"].ToString()),
                                            Convert.IsDBNull(reader["ObjectFieldId"]) ? null : (int?) reader["ObjectFieldId"],
                                            Convert.IsDBNull(reader["OperatorId"]) ? null : (int?) reader["OperatorId"],
                                            Convert.IsDBNull(reader["FunctionId"]) ? null : (int?) reader["FunctionId"],
                                            Convert.IsDBNull(reader["ConditionId"]) ? null : (int?) reader["ConditionId"],
                                            reader["CreationDate"].ToString(),
                                            reader["ModificationDate"].ToString(),
                                            Convert.IsDBNull(reader["OwnerId"]) ? null : (int?) reader["OwnerId"],
                                            reader["ValuesOrFunction"].ToString(),
                                            Convert.IsDBNull(reader["SystemRowAccessDetailOwnerId"]) ? null : (int?) reader["SystemRowAccessDetailOwnerId"]);
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsRowAccessDetailDB> lst = new List<AcsRowAccessDetailDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsRowAccessDetailDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsRowAccessDetailDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsConditionDB(int? ConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ConditionId", SqlDbType.Int);
            parameter.Value = ConditionId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsCondition", new[] {parameter}));
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsFunctionDB(int? FunctionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FunctionId", SqlDbType.Int);
            parameter.Value = FunctionId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsFunction", new[] {parameter}));
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsObjectFieldDB(int? ObjectFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectFieldId", SqlDbType.Int);
            parameter.Value = ObjectFieldId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsObjectField", new[] {parameter}));
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsOperatorDB(int? OperatorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OperatorId", SqlDbType.Int);
            parameter.Value = OperatorId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsOperator", new[] {parameter}));
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsRowAccessDB(int RowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RowAccessId", SqlDbType.Int);
            parameter.Value = RowAccessId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsRowAccess", new[] {parameter}));
        }

        public static List<AcsRowAccessDetailDB> GetAcsRowAccessDetailDBCollectionByAcsRowAccessDetailDB(int? OwnerId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OwnerId", SqlDbType.Int);
            parameter.Value = OwnerId;
            return GetAcsRowAccessDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRowAccessDetailGetByAcsRowAccessDetail", new[] {parameter}));
        }

        #endregion
    }

    #endregion
}