﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------
    #region Class "AcsConditionDB"

    public class AcsConditionDB
    {

        #region Properties :

        public int ConditionId { get; set; }
        public string ConditionTitle { get; set; }

        public string SqlCondition { get; set; }

        public int PatternId { get; set; }

        public bool IsMultiValue { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsConditionDB()
        {
        }

        //------------------- start own function --------------
        public static DataSet GetAllAcsCondition()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.p_AcsConditionGetAll", new IDataParameter[] {});
        }

        //------------------- end own function ----------------

        public AcsConditionDB(int _ConditionId, string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate)
        {
            ConditionId = _ConditionId;
            this.ConditionTitle = GeneralDB.ConvertTo256(ConditionTitle);
            this.SqlCondition = SqlCondition;
            this.PatternId = PatternId;
            this.IsMultiValue = IsMultiValue;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsConditionDB(string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ConditionTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SqlCondition", SqlDbType.NVarChar,200) ,
                                              new SqlParameter("@PatternId",SqlDbType.Int),
                                              new SqlParameter("@IsMultiValue",SqlDbType.Bit), 
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
                                                new SqlParameter("@ConditionId", SqlDbType.Int, 4),
                                               new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
							};
            parameters[0].Value = GeneralDB.ConvertTo256(ConditionTitle);
            parameters[1].Value = SqlCondition;
            parameters[2].Value = PatternId;
            parameters[3].Value = IsMultiValue;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            parameters[8].Direction = ParameterDirection.Output;
           
             _intranetDB.RunProcedure("acs.p_AcsConditionAdd", parameters);
             if (parameters[7].Value.ToString() != "")
             {
                 _AddedId = Int32.Parse(parameters[7].Value.ToString());
             }
             else
             {
                 _AddedId = -1;
             }
             var messageError = parameters[8].Value.ToString();
             if (messageError != "")
             {
                 throw new Exception(MessageError.GetMessage(messageError));
             }
        }

        public static void UpdateAcsConditionDB(int ConditionId, string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ConditionId", SqlDbType.Int, 4),
											 new SqlParameter("@ConditionTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SqlCondition", SqlDbType.NVarChar,200) ,
                                              new SqlParameter("@PatternId",SqlDbType.Int), 
                                              new SqlParameter("@IsMultiValue",SqlDbType.Bit), 
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
                                              new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
										};

            parameters[0].Value = ConditionId;

            parameters[1].Value = ConditionTitle;
            parameters[2].Value = SqlCondition;
            parameters[3].Value = PatternId;
            parameters[4].Value = IsMultiValue;
            parameters[5].Value = IsActive;
            parameters[6].Value = CreationDate;
            parameters[7].Value = ModificationDate;
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsConditionUpdate", parameters);
            string messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsConditionDB(int ConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ConditionId", SqlDbType.Int, 4),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
										};
            parameters[0].Value = ConditionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsConditionDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsConditionDB GetSingleAcsConditionDB(int ConditionId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@ConditionId", DataTypes.integer, 4, ConditionId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsConditionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsConditionDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsConditionDB> GetAllAcsConditionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsConditionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("acs.p_AcsConditionGetAll", new IDataParameter[] { }));
        }

        public static List<AcsConditionDB> GetPageAcsConditionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsCondition";
            parameters[5].Value = "ConditionId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsConditionDBCollectionFromDataSet(ds, out count);
        }

        public static AcsConditionDB GetAcsConditionDBFromDataReader(IDataReader reader)
        {
            return new AcsConditionDB(int.Parse(reader["ConditionId"].ToString()),
                                    reader["ConditionTitle"].ToString(),
                                    reader["SqlCondition"].ToString(),
                                    Int32.Parse(reader["PatternId"].ToString()),
                                    bool.Parse(reader["IsMultiValue"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public static List<AcsConditionDB> GetAcsConditionDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsConditionDB> lst = new List<AcsConditionDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsConditionDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsConditionDB> GetAcsConditionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsConditionDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}