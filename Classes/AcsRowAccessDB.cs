﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsRowAccessDB"

    public class AcsRowAccessDB
    {

        #region Properties :

        public int RowAccessId { get; set; }
        public int ObjectId { get; set; }
        public string AccessTitle { get; set; }

        public string ConditionalExpression { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsRowAccessDB()
        {
        }

        public AcsRowAccessDB(int _RowAccessId,int objectId, string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate)
        {
            RowAccessId = _RowAccessId;
            this.ObjectId = objectId;
            this.AccessTitle = AccessTitle;
            this.ConditionalExpression = ConditionalExpression;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //------------------- start own function ----------------
        public static void UpdateRowAccessConditionalExpression(int rowAccessId, string ConditionalExpression)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@rowAccessId", SqlDbType.Int),
                    new SqlParameter("@ConditionalExpression", SqlDbType.NVarChar, -1),

                };
            parameters[0].Value = rowAccessId;
            parameters[1].Value = ConditionalExpression;

            _intranetDB.RunProcedure("acs.p_AcsRowAccessUpdateConditionalExpression", parameters);
        }

        public static DataTable GetRowAccessPerObject(int objectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int)
                };
            parameters[0].Value = objectId;

            return _intranetDB.RunProcedureDS("acs.p_AcsGetRowAceessPerObjectId", parameters).Tables[0];
        }

        public static DataTable GetRowAccessPerObjectRoleHasNotIt(int objectId, int roleId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@RoleId", SqlDbType.Int)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = roleId;
            return _intranetDB.RunProcedureDS("acs.p_AcsGetRowAceessPerObjectIdRoleHasNotIt", parameters).Tables[0];
        }

        public static DataTable GetRowAccessPerObjectUserHasNotIt(int objectId, int userId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@UserId", SqlDbType.Int)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = userId;
            return _intranetDB.RunProcedureDS("acs.p_AcsGetRowAceessPerObjectIdRoleUserNotIt", parameters).Tables[0];
        }

        public static int GetObjectPerRowAccess(int rowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int),                   
                };
            parameters[0].Value = rowAccessId;          

             var dt=_intranetDB.RunProcedureDS("acs.p_AcsGetObjectPerRowAccess", parameters).Tables[0];
            if (dt.Rows.Count>0)
            {
                return Int32.Parse(dt.Rows[0][0].ToString());
            }
            return -1;

        }

        //------------------- end own function ------------------

        public static void AddAcsRowAccessDB(int objectId,string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@AccessTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@ConditionalExpression", SqlDbType.NVarChar, -1),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@RowAccessId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = GeneralDB.ConvertTo256(AccessTitle);
            parameters[2].Value = ConditionalExpression;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRowAccessAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsRowAccessDB(int RowAccessId,int objectId, string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectId", SqlDbType.Int),
                    new SqlParameter("@AccessTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@ConditionalExpression", SqlDbType.NVarChar, -1),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = RowAccessId;
            parameters[1].Value = objectId;
            parameters[2].Value = GeneralDB.ConvertTo256(AccessTitle);
            parameters[3].Value = ConditionalExpression;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsRowAccessUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsRowAccessDB(int RowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RowAccessId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRowAccessDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsRowAccessDB GetSingleAcsRowAccessDB(int RowAccessId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@RowAccessId", DataTypes.integer, 4, RowAccessId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsRowAccessGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsRowAccessDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsRowAccessDB> GetAllAcsRowAccessDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsRowAccessDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsRowAccessGetAll", new IDataParameter[] {}));
        }

        public static List<AcsRowAccessDB> GetPageAcsRowAccessDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsRowAccess";
            parameters[5].Value = "RowAccessId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsRowAccessDBCollectionFromDataSet(ds, out count);
        }

        public static AcsRowAccessDB GetAcsRowAccessDBFromDataReader(IDataReader reader)
        {
            return new AcsRowAccessDB(int.Parse(reader["RowAccessId"].ToString()),
                                      int.Parse(reader["ObjectId"].ToString()),
                                      reader["AccessTitle"].ToString(),
                                      reader["ConditionalExpression"].ToString(),
                                      bool.Parse(reader["IsActive"].ToString()),
                                      reader["CreationDate"].ToString(),
                                      reader["ModificationDate"].ToString());
        }

        public static List<AcsRowAccessDB> GetAcsRowAccessDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsRowAccessDB> lst = new List<AcsRowAccessDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsRowAccessDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsRowAccessDB> GetAcsRowAccessDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsRowAccessDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}