﻿using System;
using System.Text;
using System.Web;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Intranet.Common;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Telerik.Web.UI.Editor.Rtf.Converter.Html;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class GeneralDB
    {        

        public static int GetRowNumberOfRecord(string TableName, string PrimaryKey, string SortField, string SortType, string WhereClause, string WhereClauseMaster)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 100),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 100),
                    new SqlParameter("@SortField", SqlDbType.NVarChar, 100),
                    new SqlParameter("@SortType", SqlDbType.NVarChar, 100),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1),
                    new SqlParameter("@WhereClauseMaster", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = TableName;
            parameters[1].Value = PrimaryKey;
            parameters[2].Value = SortField;
            parameters[3].Value = SortType;
            parameters[4].Value = GeneralDB.ConvertTo256(WhereClause);
            parameters[5].Value = GeneralDB.ConvertTo256(WhereClauseMaster);
            DataRow dr = _intranetDB.RunProcedureDR("[acs].p_getRowNumberOfRecord", parameters);
            return Int32.Parse(dr["RowNumber"].ToString());
        }

        public static bool CheckDateCorrect(string myDate)
        {
            if (myDate == "" || DConvert.ShamsiToMiladi(myDate) != "-1")
            {
                return true;
            }
            return false;
        }

        public static string Shamsi2Miladi(string myDate)
        {
            if (myDate == "")
            {
                return "";
            }
            return DConvert.ShamsiToMiladi(myDate);
        }

        public static string Miladi2Shamsi(string strDate)
        {
            if (strDate == "")
            {
                return "";
            }
            var myDate = new DateTime(Int32.Parse(strDate.Substring(6, 4)),
                                      Int32.Parse(strDate.Substring(0, 2)),
                                      Int32.Parse(strDate.Substring(3, 2)));
            return DConvert.MiladiToShamsi(myDate);
        }

        public static string AddDateSlash8Digit(string MyDate)
        {
            string str = "-";
            if (MyDate.Trim().Length != 0)
            {
                str = "";
                for (int i = 0; i < 8; i++)
                {
                    str += MyDate[i];
                    if (i == 3 || i == 5)
                        str += "/";
                }
            }
            if (str == "-") str = "";
            return str;
        }

        public static string AddTimeColon(string myTime)
        {
            if (myTime.Trim().Length == 0) return "";
            var str = "";
            for (var i = 0; i < 4; i++)
            {
                str += myTime[i];
                if (i == 1)
                    str += ":";
            }
            return str;
        }

        public static DataSet GetPageDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, string TableName, string PrimaryKey, out int count, string sortType)
        {
            var intranetDb = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, -1),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 200),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 200),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 200),
                    new SqlParameter("@SortType", SqlDbType.VarChar, 200),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = GeneralDB.ConvertTo256(WhereClause);
            parameters[3].Value = OrderBy;
            parameters[4].Value = TableName;
            parameters[5].Value = PrimaryKey;
            parameters[6].Value = sortType;
            var ds = intranetDb.RunProcedureDS("[acs].[p_TablesGetPage]", parameters);
            count = Int32.Parse(ds.Tables[1].Rows[0][0].ToString());
            return ds;
        }

        public static void accessDenied()
        {
            HttpContext.Current.Response.Redirect(IntranetUI.BuildUrl("~/accessdenied.aspx"));
        }       

        public static bool checkUserHasRole(int userId, string roleName)
        {
            DataSet ds = Intranet.Security.UserDB.GetUserRoles(userId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //    if (dr["RoleName"].ToString() == roleName) 
                if (dr["RoleName"].ToString().Contains(roleName)) //  after site upgrade 1390-04-01
                {
                    return true;
                }
            }
            return false;
        }

        public static string ConvertTo256(string strValue)
        {
            return Intranet.Common.IntranetUI.ConvertTo1256(strValue);
        }
        
        public static DataSet GetUserRoles(string RoleName)
        {
            int userID = Int32.Parse(HttpContext.Current.User.Identity.Name);
            DataSet ds = new DataSet();
            ds = Intranet.Security.UserDB.GetUserRoles(userID);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
            }

            return ds;
        }     
    }
}