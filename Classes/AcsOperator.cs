﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsOperator"

    /// <summary>
    /// 
    /// </summary>
    public class AcsOperator : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int OperatorId { get; set; }
        public string OperatorTitle { get; set; }

        public string SqlOperator { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        // ----------- start own Function ---------------
        public static DataSet GetAllOperator()
        {
            return AcsOperatorDB.GetAllOperator();
        }

        public AcsOperator()
        {
        }

        public AcsOperator(int _OperatorId, string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate)
        {
            OperatorId = _OperatorId;
            this.OperatorTitle = OperatorTitle;
            this.SqlOperator = SqlOperator;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

       #region Mehtods :

        #region Static Methods :

        public static void Add(string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
        AcsOperatorDB.AddAcsOperatorDB(OperatorTitle, SqlOperator, IsActive, CreationDate, ModificationDate,out _AddedId);
        
        }

        public static void Update(int _OperatorId, string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsOperatorDB.UpdateAcsOperatorDB(_OperatorId, OperatorTitle, SqlOperator, IsActive, CreationDate, ModificationDate);
            
        }

        public static void Delete(int OperatorId)
        {
            AcsOperatorDB.DeleteAcsOperatorDB(OperatorId);
            
        }

        public static AcsOperator GetSingleById(int _OperatorId)
        {
            AcsOperator o = GetAcsOperatorFromAcsOperatorDB(
            AcsOperatorDB.GetSingleAcsOperatorDB(_OperatorId));

            return o;
        }

        public static List<AcsOperator> GetAll()
        {
            List<AcsOperator> lst = new List<AcsOperator>();
            //string key = "AcsOperator_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsOperator>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsOperatorCollectionFromAcsOperatorDBList(
            AcsOperatorDB.GetAllAcsOperatorDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsOperator> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsOperator_List_Page_" + currentPage ;
            //string countKey = "AcsOperator_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsOperator> lst = new List<AcsOperator>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsOperator>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsOperatorCollectionFromAcsOperatorDBList(
            AcsOperatorDB.GetPageAcsOperatorDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsOperator GetAcsOperatorFromAcsOperatorDB(AcsOperatorDB o)
        {
            if (o == null)
                return null;
            AcsOperator ret = new AcsOperator(o.OperatorId, o.OperatorTitle, o.SqlOperator, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsOperator> GetAcsOperatorCollectionFromAcsOperatorDBList(List<AcsOperatorDB> lst)
        {
            List<AcsOperator> RetLst = new List<AcsOperator>();
            foreach (AcsOperatorDB o in lst)
            {
                RetLst.Add(GetAcsOperatorFromAcsOperatorDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}