﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsSystemDB"

    public class AcsSystemDB
    {

        #region Properties :

        public int SystemId { get; set; }
        public string SystemTitle { get; set; }
        public string SustemEnglishTitle { get; set; }
        public string SystemSchema { get; set; }
        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsSystemDB()
        {
        }

        public AcsSystemDB(int _SystemId, string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate)
        {
            SystemId = _SystemId;
            this.SystemTitle = SystemTitle;
            this.SustemEnglishTitle = SustemEnglishTitle;
            this.SystemSchema = SystemSchema;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsSystemDB(string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@SystemTitle", SqlDbType.NVarChar, 300),
                    new SqlParameter("@SustemEnglishTitle", SqlDbType.NVarChar, 300),
                    new SqlParameter("@SystemSchema", SqlDbType.NVarChar, 50),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@SystemId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(SystemTitle);
            parameters[1].Value = GeneralDB.ConvertTo256(SustemEnglishTitle);
            parameters[2].Value = SystemSchema;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsSystemAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsSystemDB(int SystemId, string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@SystemId", SqlDbType.Int, 4),
                    new SqlParameter("@SystemTitle", SqlDbType.NVarChar, 300),
                    new SqlParameter("@SustemEnglishTitle", SqlDbType.NVarChar, 300),
                    new SqlParameter("@SystemSchema", SqlDbType.NVarChar, 50),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };

            parameters[0].Value = SystemId;
            parameters[1].Value = GeneralDB.ConvertTo256(SystemTitle);
            parameters[2].Value = GeneralDB.ConvertTo256(SustemEnglishTitle);
            parameters[3].Value = SystemSchema;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsSystemUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsSystemDB(int SystemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@SystemId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = SystemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsSystemDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsSystemDB GetSingleAcsSystemDB(int SystemId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@SystemId", DataTypes.integer, 4, SystemId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsSystemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsSystemDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return null;
        }

        public static List<AcsSystemDB> GetAllAcsSystemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsSystemDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsSystemGetAll", new IDataParameter[] {}));
        }

        public static List<AcsSystemDB> GetPageAcsSystemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsSystem";
            parameters[5].Value = "SystemId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsSystemDBCollectionFromDataSet(ds, out count);
        }

        public static AcsSystemDB GetAcsSystemDBFromDataReader(IDataReader reader)
        {
            return new AcsSystemDB(int.Parse(reader["SystemId"].ToString()),
                                   reader["SystemTitle"].ToString(),
                                   reader["SustemEnglishTitle"].ToString(),
                                   reader["SystemSchema"].ToString(),
                                   bool.Parse(reader["IsActive"].ToString()),
                                   reader["CreationDate"].ToString(),
                                   reader["ModificationDate"].ToString());
        }

        public static List<AcsSystemDB> GetAcsSystemDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsSystemDB> lst = new List<AcsSystemDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsSystemDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsSystemDB> GetAcsSystemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsSystemDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}