﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsCondition"

    /// <summary>
    /// 
    /// </summary>
    public class AcsCondition : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int ConditionId { get; set; }
        public string ConditionTitle { get; set; }

        public string SqlCondition { get; set; }

        public int PatternId { get; set; }

        public bool IsMultiValue { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsCondition()
        {
        }

        //------------------- start own function --------------
        public static DataSet GetAllAcsCondition()
        {
            return AcsConditionDB.GetAllAcsCondition();
        }

        public AcsCondition(int _ConditionId, string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate)
        {
            ConditionId = _ConditionId;
            this.ConditionTitle = ConditionTitle;
            this.SqlCondition = SqlCondition;
            this.PatternId = PatternId;
            this.IsMultiValue = IsMultiValue;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :        

        #region Static Methods :

        public static void Add(string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
             AcsConditionDB.AddAcsConditionDB(ConditionTitle, SqlCondition,  PatternId,  IsMultiValue, IsActive, CreationDate, ModificationDate,out _AddedId);            
        }

        public static void Update(int _ConditionId, string ConditionTitle, string SqlCondition, int PatternId, bool IsMultiValue, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsConditionDB.UpdateAcsConditionDB(_ConditionId, ConditionTitle, SqlCondition,  PatternId,  IsMultiValue, IsActive, CreationDate, ModificationDate);           
        }

        public static void Delete(int ConditionId)
        {
            AcsConditionDB.DeleteAcsConditionDB(ConditionId);          
        }

        public static AcsCondition GetSingleById(int _ConditionId)
        {
            AcsCondition o = GetAcsConditionFromAcsConditionDB(
            AcsConditionDB.GetSingleAcsConditionDB(_ConditionId));

            return o;
        }

        public static List<AcsCondition> GetAll()
        {
            List<AcsCondition> lst = new List<AcsCondition>();
            //string key = "AcsCondition_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsConditionCollectionFromAcsConditionDBList(
            AcsConditionDB.GetAllAcsConditionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsCondition> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsCondition_List_Page_" + currentPage ;
            //string countKey = "AcsCondition_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsCondition> lst = new List<AcsCondition>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsConditionCollectionFromAcsConditionDBList(
            AcsConditionDB.GetPageAcsConditionDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsCondition GetAcsConditionFromAcsConditionDB(AcsConditionDB o)
        {
            if (o == null)
                return null;
            AcsCondition ret = new AcsCondition(o.ConditionId, o.ConditionTitle, o.SqlCondition,o.PatternId,o.IsMultiValue, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsCondition> GetAcsConditionCollectionFromAcsConditionDBList(List<AcsConditionDB> lst)
        {
            List<AcsCondition> RetLst = new List<AcsCondition>();
            foreach (AcsConditionDB o in lst)
            {
                RetLst.Add(GetAcsConditionFromAcsConditionDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}