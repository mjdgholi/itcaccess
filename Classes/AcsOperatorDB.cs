﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsOperatorDB"

    public class AcsOperatorDB
    {

        #region Properties :

        public int OperatorId { get; set; }
        public string OperatorTitle { get; set; }

        public string SqlOperator { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        // ----------- start own Function ---------------
        public static DataSet GetAllOperator()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.p_AcsOperatorGetAll", new IDataParameter[] {});
        }

        // ----------- end own function -----------------

        public AcsOperatorDB()
        {
        }

        public AcsOperatorDB(int _OperatorId, string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate)
        {
            OperatorId = _OperatorId;
            this.OperatorTitle = OperatorTitle;
            this.SqlOperator = SqlOperator;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsOperatorDB(string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@OperatorTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@SqlOperator", SqlDbType.NVarChar, 200),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(OperatorTitle);
            parameters[1].Value = SqlOperator;
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsOperatorAdd", parameters);
            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsOperatorDB(int OperatorId, string OperatorTitle, string SqlOperator, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                    new SqlParameter("@OperatorTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@SqlOperator", SqlDbType.NVarChar, 200),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = OperatorId;

            parameters[1].Value = GeneralDB.ConvertTo256(OperatorTitle);
            parameters[2].Value = SqlOperator;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsOperatorUpdate", parameters);
            string messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsOperatorDB(int OperatorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@OperatorId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = OperatorId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsOperatorDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsOperatorDB GetSingleAcsOperatorDB(int OperatorId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@OperatorId", DataTypes.integer, 4, OperatorId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsOperatorGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsOperatorDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsOperatorDB> GetAllAcsOperatorDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsOperatorDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsOperatorGetAll", new IDataParameter[] {}));
        }

        public static List<AcsOperatorDB> GetPageAcsOperatorDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsOperator";
            parameters[5].Value = "OperatorId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsOperatorDBCollectionFromDataSet(ds, out count);
        }

        public static AcsOperatorDB GetAcsOperatorDBFromDataReader(IDataReader reader)
        {
            return new AcsOperatorDB(int.Parse(reader["OperatorId"].ToString()),
                                     reader["OperatorTitle"].ToString(),
                                     reader["SqlOperator"].ToString(),
                                     bool.Parse(reader["IsActive"].ToString()),
                                     reader["CreationDate"].ToString(),
                                     reader["ModificationDate"].ToString());
        }

        public static List<AcsOperatorDB> GetAcsOperatorDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsOperatorDB> lst = new List<AcsOperatorDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsOperatorDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsOperatorDB> GetAcsOperatorDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsOperatorDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}