﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsUserAndGroupDB"

    public class AcsUserAndGroupDB
    {

        #region Properties :

        public int UserAndGroupId { get; set; }
        public string UserAndGroupName { get; set; }

        public Guid UserAndGroupDomainId { get; set; }

        public bool IsUser { get; set; }

        public string UserPass { get; set; }

        public bool IsBuiltin { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int WebUserId { get; set; }


        #endregion

        #region Constrauctors :

        public AcsUserAndGroupDB()
        {
        }

        public AcsUserAndGroupDB(int _UserAndGroupId, string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId)
        {
            UserAndGroupId = _UserAndGroupId;
            this.UserAndGroupName = UserAndGroupName;
            this.UserAndGroupDomainId = UserAndGroupDomainId;
            this.IsUser = IsUser;
            this.UserPass = UserPass;
            this.IsBuiltin = IsBuiltin;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.WebUserId = WebUserId;
        }

        #endregion

        #region Methods :

        //------------ start own function --------------
        public static void AddAcsUserAndGroupWithOwnerDB(string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, int userAndGroupOwnerId, string CreationDate, string ModificationDate, int WebUserId, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupName", SqlDbType.NVarChar, 200),
                    new SqlParameter("@UserAndGroupDomainId", SqlDbType.UniqueIdentifier),
                    new SqlParameter("@IsUser", SqlDbType.Bit),
                    new SqlParameter("@UserPass", SqlDbType.NVarChar, 50),
                    new SqlParameter("@IsBuiltin", SqlDbType.Bit),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@UserAndGroupOwnerId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@WebUserId",SqlDbType.Int), 
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = GeneralDB.ConvertTo256(UserAndGroupName);
            parameters[1].Value = UserAndGroupDomainId;
            parameters[2].Value = IsUser;
            parameters[3].Value = GeneralDB.ConvertTo256(UserPass);
            parameters[4].Value = IsBuiltin;
            parameters[5].Value = IsActive;
            parameters[6].Value = (userAndGroupOwnerId != -1) ? (object) userAndGroupOwnerId : DBNull.Value;
            parameters[7].Value = CreationDate;
            parameters[8].Value = ModificationDate;
            parameters[9].Value = WebUserId;
            parameters[10].Direction = ParameterDirection.Output;
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupAddWithOwner", parameters);
            if (parameters[10].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[10].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        //------------ end own function ----------------

        public static void AddAcsUserAndGroupDB(string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupName", SqlDbType.NVarChar, 200),
                    new SqlParameter("@UserAndGroupDomainId", SqlDbType.UniqueIdentifier),
                    new SqlParameter("@IsUser", SqlDbType.Bit),
                    new SqlParameter("@UserPass", SqlDbType.NVarChar, 50),
                    new SqlParameter("@IsBuiltin", SqlDbType.Bit),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@WebUserId",SqlDbType.Int), 
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = GeneralDB.ConvertTo256(UserAndGroupName);
            parameters[1].Value = UserAndGroupDomainId;
            parameters[2].Value = IsUser;
            parameters[3].Value = GeneralDB.ConvertTo256(UserPass);
            parameters[4].Value = IsBuiltin;
            parameters[5].Value = IsActive;
            parameters[6].Value = CreationDate;
            parameters[7].Value = ModificationDate;
            parameters[8].Value = WebUserId;
            parameters[9].Direction = ParameterDirection.Output;
            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupAdd", parameters);

            if (parameters[9].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[9].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        public static void UpdateAcsUserAndGroupDB(int UserAndGroupId, string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@UserAndGroupName", SqlDbType.NVarChar, 200),
                    new SqlParameter("@UserAndGroupDomainId", SqlDbType.UniqueIdentifier),
                    new SqlParameter("@IsUser", SqlDbType.Bit),
                    new SqlParameter("@UserPass", SqlDbType.NVarChar, 50),
                    new SqlParameter("@IsBuiltin", SqlDbType.Bit),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@WebUserId",SqlDbType.Int), 
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };

            parameters[0].Value = UserAndGroupId;

            parameters[1].Value = GeneralDB.ConvertTo256(UserAndGroupName);
            parameters[2].Value = UserAndGroupDomainId;
            parameters[3].Value = IsUser;
            parameters[4].Value = UserPass;
            parameters[5].Value = IsBuiltin;
            parameters[6].Value = IsActive;
            parameters[7].Value = CreationDate;
            parameters[8].Value = ModificationDate;
            parameters[9].Value = WebUserId;
            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupUpdate", parameters);
            string messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsUserAndGroupDB(int UserAndGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = UserAndGroupId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsUserAndGroupDB GetSingleAcsUserAndGroupDB(int userAndGroupId)
        {
            IDataReader reader = null;
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            try
            {
                IDataParameter[] parameters =
                    {
                        intranetDB.GetParameter("@UserAndGroupId", DataTypes.integer, 4, userAndGroupId)
                    };

                reader = intranetDB.RunProcedureReader
                    ("[acs].p_AcsUserAndGroupGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsUserAndGroupDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsUserAndGroupDB> GetAllAcsUserAndGroupDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsUserAndGroupDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[acs].p_AcsUserAndGroupGetAll", new IDataParameter[] {}));
        }

        public static List<AcsUserAndGroupDB> GetPageAcsUserAndGroupDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsUserAndGroup";
            parameters[5].Value = "UserAndGroupId";
            DataSet ds = _intranetDB.RunProcedureDS("[acs].p_TablesGetPage", parameters);
            return GetAcsUserAndGroupDBCollectionFromDataSet(ds, out count);
        }

        public static AcsUserAndGroupDB GetAcsUserAndGroupDBFromDataReader(IDataReader reader)
        {
            return new AcsUserAndGroupDB(int.Parse(reader["UserAndGroupId"].ToString()),
                                         reader["UserAndGroupName"].ToString(),
                                         new Guid(reader["UserAndGroupDomainId"].ToString()),
                                         bool.Parse(reader["IsUser"].ToString()),
                                         reader["UserPass"].ToString(),
                                         bool.Parse(reader["IsBuiltin"].ToString()),
                                         bool.Parse(reader["IsActive"].ToString()),
                                         reader["CreationDate"].ToString(),
                                         reader["ModificationDate"].ToString(),
                                         Int32.Parse(reader["WebUserId"].ToString()));
        }

        public static List<AcsUserAndGroupDB> GetAcsUserAndGroupDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsUserAndGroupDB> lst = new List<AcsUserAndGroupDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsUserAndGroupDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsUserAndGroupDB> GetAcsUserAndGroupDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsUserAndGroupDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}