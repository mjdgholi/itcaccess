﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsUserAndGroupTreeDB"

    public class AcsUserAndGroupTreeDB
    {

        #region Properties :

        public int UserAndGroupTreeId { get; set; }
        public int UserAndGroupOwnerId { get; set; }

        public int UserAndGroupId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsUserAndGroupTreeDB()
        {
        }

        public AcsUserAndGroupTreeDB(int _UserAndGroupTreeId, int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroupTreeId = _UserAndGroupTreeId;
            this.UserAndGroupOwnerId = UserAndGroupOwnerId;
            this.UserAndGroupId = UserAndGroupId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //------------------------- start own function ----------------

        public static DataSet AcsUserAndGroupTreeGetGetChildNode(int userAndGroupId, int isUser)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("IsUser", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            parameter[1].Value = isUser;
            return intranetDb.RunProcedureDS("[acs].[p_AcsUserAndGroupTreeGetGetChildNode]", parameter);
        }

        public static void UpdateUserOrGroupName(int userAndGroupId, string userAndGroupName)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("UserAndGroupName", SqlDbType.NVarChar, 200),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameter[0].Value = userAndGroupId;
            parameter[1].Value = GeneralDB.ConvertTo256(userAndGroupName);
            parameter[2].Direction = ParameterDirection.Output;
            intranetDb.RunProcedure("[acs].[p_AcsUserAndGroupUpdateName]", parameter);
            var messageError = parameter[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        public static DataTable GetParentNodes(int userAndGroupId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_ReportParentTree]", parameter).Tables[0];
            return dataTable;
        }

        public static ArrayList GetParentNodesArrayList(int userAndGroupId)
        {
            var parentArraylist = new ArrayList();
            var parentDt = GetParentNodes(userAndGroupId);
            foreach (DataRow dataRow in parentDt.Rows)
            {
                parentArraylist.Add(dataRow["UserAndGroupId"]);
            }
            return parentArraylist;
        }

        public static DataTable GetChildNodes(int userAndGroupId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_ReportChildTree]", parameter).Tables[0];
            return dataTable;
        }

        public static ArrayList GetChildNodesArray(int userAndGroupId)
        {
            DataTable childDt = GetChildNodes(userAndGroupId);
            ArrayList childArray = new ArrayList();
            foreach (DataRow child in childDt.Rows)
            {
                if (!childArray.Contains(child["UserAndGroupId"].ToString()))
                {
                    childArray.Add(child["UserAndGroupId"].ToString());
                }
            }
            return childArray;
        }

        public static ArrayList GetDirectParentNodes(int userAndGroupId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_UserAndGroupGetDirectParentNodes]", parameter).Tables[0];
            ArrayList converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                converted.Add(row["UserAndGroupOwnerId"].ToString());
            }
            return converted;
        }

        public static DataSet GetDirectChildNodes(int userAndGroupId, int isUser)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("IsUser", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            parameter[1].Value = isUser;
            return intranetDb.RunProcedureDS("[acs].[p_UserAndGroupGetDirectChildNodes]", parameter);

        }

        public static ArrayList GetDirectChildNodesArray(int userAndGroupId, int isUser)
        {
            DataTable dataTable = GetDirectChildNodes(userAndGroupId, isUser).Tables[0];
            var converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                if (!converted.Contains(row["UserAndGroupId"].ToString()))
                {
                    converted.Add(row["UserAndGroupId"].ToString());
                }
            }
            return converted;
        }

        public static ArrayList GetAllNodeForUserAndGroup(int userAndGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int)
                };
            parameters[0].Value = userAndGroupId;
            var ds = _intranetDB.RunProcedureDS("[acs].p_GetAllNodeForUserAndGroup", parameters);
            ArrayList converted = new ArrayList(ds.Tables[0].Rows.Count);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                converted.Add(row["UserAndGroupTreeId"].ToString());
            }
            return converted;
        }

        public static DataSet GetAllNodeExceptDirectChild(int userAndGroupId, int isUser)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("IsUser", SqlDbType.Int)
                };
            parameter[0].Value = userAndGroupId;
            parameter[1].Value = isUser;
            return intranetDb.RunProcedureDS("[acs].[p_UserAndGroupGetAllNodeExceptDirectChild]", parameter);

        }

        public static ArrayList GetAllNodeExceptDirectChildArray(int userAndGroupId, int isUser)
        {
            DataTable dataTable = GetAllNodeExceptDirectChild(userAndGroupId, isUser).Tables[0];
            var converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                if (!converted.Contains(row["UserAndGroupId"].ToString()))
                {
                    converted.Add(row["UserAndGroupId"].ToString());
                }
            }
            return converted;
        }

        public static void ChangeUserAndGroupTreeOwner(int userAndGroupTreeId, int userAndGroupOwnerId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("UserAndGroupTreeId", SqlDbType.Int),
                    new SqlParameter("UserAndGroupOwnerId", SqlDbType.Int),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = userAndGroupTreeId;
            parameters[1].Value = (userAndGroupOwnerId == -1) ? DBNull.Value : (object) userAndGroupOwnerId;
            parameters[2].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_ChangeUserAndGroupTreeOwner", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static void UserOrGroupUpdateGroupMembership(int userAndGroupId, string groupMemberShip)
        {          
            IDataParameter[] parameters =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("GroupMemberShip", SqlDbType.NVarChar, -1),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = userAndGroupId;
            parameters[1].Value = groupMemberShip;
            parameters[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_UserOrGroupUpdateGroupMembership", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }           
            
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static void UserOrGroupUpdateGroupMembers(int userAndGroupId, string groupMembers, int isUser)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("GroupMembers", SqlDbType.NVarChar, -1),
                    new SqlParameter("IsUser", SqlDbType.Int),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = userAndGroupId;
            parameters[1].Value = groupMembers;
            parameters[2].Value = isUser;
            parameters[3].Direction = ParameterDirection.Output;


            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_UserOrGroupUpdateGroupMembers", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }                      
            
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception("خطا در ثبت"));
        }

        public static DataTable GetAllNodes(int isUser, string userAndGroupName)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("IsUser", SqlDbType.Int),
                    new SqlParameter("UserAndGroupName", SqlDbType.NVarChar, 300)
                };
            parameter[0].Value = isUser;
            parameter[1].Value = GeneralDB.ConvertTo256(userAndGroupName);
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_UserAndGroupGetAll]", parameter).Tables[0];
            return dataTable;
        }

        //------------------------- end own function ----------------

        public static void AddAcsUserAndGroupTreeDB(int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupOwnerId", SqlDbType.Int),
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@UserAndGroupTreeId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = UserAndGroupOwnerId;
            parameters[1].Value = UserAndGroupId;
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupTreeAdd", parameters);

            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static bool UpdateAcsUserAndGroupTreeDB(int UserAndGroupTreeId, int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupTreeId", SqlDbType.Int, 4),
                    new SqlParameter("@UserAndGroupOwnerId", SqlDbType.Int),
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime)
                };

            parameters[0].Value = UserAndGroupTreeId;

            parameters[1].Value = UserAndGroupOwnerId;
            parameters[2].Value = UserAndGroupId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;


            return _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupTreeUpdate", parameters);
        }

        public static void DeleteAcsUserAndGroupTreeDB(int UserAndGroupTreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupTreeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = UserAndGroupTreeId;
            parameters[1].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupTreeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static AcsUserAndGroupTreeDB GetSingleAcsUserAndGroupTreeDB(int UserAndGroupTreeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@UserAndGroupTreeId", DataTypes.integer, 4, UserAndGroupTreeId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("[acs].p_AcsUserAndGroupTreeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsUserAndGroupTreeDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsUserAndGroupTreeDB> GetAllAcsUserAndGroupTreeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsUserAndGroupTreeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[acs].p_AcsUserAndGroupTreeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsUserAndGroupTreeDB> GetPageAcsUserAndGroupTreeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsUserAndGroupTree";
            parameters[5].Value = "UserAndGroupTreeId";
            DataSet ds = _intranetDB.RunProcedureDS("[acs].p_TablesGetPage", parameters);
            return GetAcsUserAndGroupTreeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsUserAndGroupTreeDB GetAcsUserAndGroupTreeDBFromDataReader(IDataReader reader)
        {
            return new AcsUserAndGroupTreeDB(int.Parse(reader["UserAndGroupTreeId"].ToString()),
                                             (string.IsNullOrEmpty(reader["UserAndGroupOwnerId"].ToString())) ? -1 : int.Parse(reader["UserAndGroupOwnerId"].ToString()),
                                             int.Parse(reader["UserAndGroupId"].ToString()),
                                             bool.Parse(reader["IsActive"].ToString()),
                                             reader["CreationDate"].ToString(),
                                             reader["ModificationDate"].ToString());
        }

        public static List<AcsUserAndGroupTreeDB> GetAcsUserAndGroupTreeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsUserAndGroupTreeDB> lst = new List<AcsUserAndGroupTreeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsUserAndGroupTreeDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsUserAndGroupTreeDB> GetAcsUserAndGroupTreeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsUserAndGroupTreeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsUserAndGroupTreeDB> GetAcsUserAndGroupTreeDBCollectionByAcsUserAndGroupDB(int UserAndGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@UserAndGroupId", SqlDbType.Int);
            parameter.Value = UserAndGroupId;
            return GetAcsUserAndGroupTreeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[acs].p_AcsUserAndGroupTreeGetByAcsUserAndGroup", new[] {parameter}));
        }




        #endregion



    }

    #endregion
}