﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsObject"

    /// <summary>
    /// 
    /// </summary>
    public class AcsObject : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int ObjectId { get; set; }
        public string ObjectTitle { get; set; }

        public int SystemId { get; set; }
        public bool IsHierarchy { get; set; }

        public int ObjectTypeId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsObject()
        {
        }

        public AcsObject(int _ObjectId, string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate, string ModificationDate)
        {
            ObjectId = _ObjectId;
            this.ObjectTitle = ObjectTitle;
            this.SystemId = SystemId;
            this.IsHierarchy = IsHierarchy;
            this.ObjectTypeId = ObjectTypeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :   
     
        


        #region Static Methods :

        //---------------- start own function -------------
        public static DataSet GetAllSqlObject(string objectTitle,string schemaName)
        {
            return AcsObjectDB.GetAllSqlObject( objectTitle, schemaName);
        }

        public static DataSet GetSqlObjectColumn(string objectId)
        {
        return    AcsObjectDB.GetSqlObjectColumn(objectId);
        }

        public static DataSet GetAllDs()
        {
            return AcsObjectDB.GetAllDs();
        }

        public static DataSet GetAllPerSystemDs(int systemId)
        {
            return AcsObjectDB.GetAllPerSystemDs(systemId);
        }

        public static bool CheckObjectHasKey(int objectId)
        {
            return AcsObjectDB.CheckObjectHasKey(objectId);
        }

        //--------------- end own function ----------------

        public static void Add(string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsObjectDB.AddAcsObjectDB(ObjectTitle, SystemId, IsHierarchy, ObjectTypeId, CreationDate, ModificationDate, out _AddedId);
        
        }

        public static void Update(int _ObjectId, string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate, string ModificationDate)
        {
            AcsObjectDB.UpdateAcsObjectDB(_ObjectId, ObjectTitle, SystemId, IsHierarchy, ObjectTypeId, CreationDate, ModificationDate);
            
        }

        public static void Delete(int ObjectId)
        {
           AcsObjectDB.DeleteAcsObjectDB(ObjectId);
           
        }

        public static AcsObject GetSingleById(int _ObjectId)
        {
            AcsObject o = GetAcsObjectFromAcsObjectDB(
            AcsObjectDB.GetSingleAcsObjectDB(_ObjectId));

            return o;
        }

        public static List<AcsObject> GetAll()
        {
            List<AcsObject> lst = new List<AcsObject>();
            //string key = "AcsObject_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObject>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsObjectCollectionFromAcsObjectDBList(
            AcsObjectDB.GetAllAcsObjectDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsObject> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsObject_List_Page_" + currentPage ;
            //string countKey = "AcsObject_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsObject> lst = new List<AcsObject>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObject>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsObjectCollectionFromAcsObjectDBList(
            AcsObjectDB.GetPageAcsObjectDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsObject GetAcsObjectFromAcsObjectDB(AcsObjectDB o)
        {
            if (o == null)
                return null;
            AcsObject ret = new AcsObject(o.ObjectId, o.ObjectTitle, o.SystemId, o.IsHierarchy, o.ObjectTypeId, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsObject> GetAcsObjectCollectionFromAcsObjectDBList(List<AcsObjectDB> lst)
        {
            List<AcsObject> RetLst = new List<AcsObject>();
            foreach (AcsObjectDB o in lst)
            {
                RetLst.Add(GetAcsObjectFromAcsObjectDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}