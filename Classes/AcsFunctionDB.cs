﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsFunctionDB"

    public class AcsFunctionDB
    {

        #region Properties :

        public int FunctionId { get; set; }
        public int ObjectFieldId { get; set; }

        public string FunctionTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsFunctionDB()
        {
        }

        public AcsFunctionDB(int _FunctionId, int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            FunctionId = _FunctionId;
            this.ObjectFieldId = ObjectFieldId;
            this.FunctionTitle = FunctionTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //---------------- start own function -------------

        public static DataSet GetAllSqlFunction()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {

                };
            return _intranetDB.RunProcedureDS("acs.p_GelAllSqlFunction", parameters);
        }

        //---------------- end own function   -------------

        public static void AddAcsFunctionDB(int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@FunctionTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@FunctionId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = ObjectFieldId;
            parameters[1].Value = GeneralDB.ConvertTo256(FunctionTitle);
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsFunctionAdd", parameters);
            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsFunctionDB(int FunctionId, int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@FunctionId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectFieldId", SqlDbType.Int),
                    new SqlParameter("@FunctionTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = FunctionId;

            parameters[1].Value = ObjectFieldId;
            parameters[2].Value = GeneralDB.ConvertTo256(FunctionTitle);
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsFunctionUpdate", parameters);
            string messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsFunctionDB(int FunctionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@FunctionId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = FunctionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsFunctionDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsFunctionDB GetSingleAcsFunctionDB(int FunctionId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@FunctionId", DataTypes.integer, 4, FunctionId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsFunctionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsFunctionDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsFunctionDB> GetAllAcsFunctionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsFunctionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsFunctionGetAll", new IDataParameter[] {}));
        }

        public static List<AcsFunctionDB> GetPageAcsFunctionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsFunction";
            parameters[5].Value = "FunctionId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsFunctionDBCollectionFromDataSet(ds, out count);
        }

        public static AcsFunctionDB GetAcsFunctionDBFromDataReader(IDataReader reader)
        {
            return new AcsFunctionDB(int.Parse(reader["FunctionId"].ToString()),
                                     int.Parse(reader["ObjectFieldId"].ToString()),
                                     reader["FunctionTitle"].ToString(),
                                     bool.Parse(reader["IsActive"].ToString()),
                                     reader["CreationDate"].ToString(),
                                     reader["ModificationDate"].ToString());
        }

        public static List<AcsFunctionDB> GetAcsFunctionDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsFunctionDB> lst = new List<AcsFunctionDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsFunctionDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsFunctionDB> GetAcsFunctionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsFunctionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsFunctionDB> GetAcsFunctionDBCollectionByAcsObjectFieldDB(int ObjectFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectFieldId", SqlDbType.Int);
            parameter.Value = ObjectFieldId;
            return GetAcsFunctionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsFunctionGetByAcsObjectField", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}