﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsAccessType"

    /// <summary>
    /// 
    /// </summary>
    public class AcsAccessType : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int AccessTypeId { get; set; }
        public string AccessTypeTitle { get; set; }

        public bool HasAccess { get; set; }

        public bool IsDeniedAccess { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsAccessType()
        {
        }

        public AcsAccessType(int _AccessTypeId, string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate)
        {
            AccessTypeId = _AccessTypeId;
            this.AccessTypeTitle = AccessTypeTitle;
            this.HasAccess = HasAccess;
            this.IsDeniedAccess = IsDeniedAccess;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :

       
        #region Static Methods :

        public static void Add(string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
        AcsAccessTypeDB.AddAcsAccessTypeDB(AccessTypeTitle, HasAccess, IsDeniedAccess, IsActive, CreationDate, ModificationDate,out _AddedId);
        
        }

        public static void Update(int _AccessTypeId, string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsAccessTypeDB.UpdateAcsAccessTypeDB(_AccessTypeId, AccessTypeTitle, HasAccess, IsDeniedAccess, IsActive, CreationDate, ModificationDate);
            
        }

        public static void Delete(int AccessTypeId)
        {
            AcsAccessTypeDB.DeleteAcsAccessTypeDB(AccessTypeId);
            
        }

        public static AcsAccessType GetSingleById(int _AccessTypeId)
        {
            AcsAccessType o = GetAcsAccessTypeFromAcsAccessTypeDB(
            AcsAccessTypeDB.GetSingleAcsAccessTypeDB(_AccessTypeId));

            return o;
        }

        public static List<AcsAccessType> GetAll()
        {
            List<AcsAccessType> lst = new List<AcsAccessType>();
            //string key = "AcsAccessType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsAccessType>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsAccessTypeCollectionFromAcsAccessTypeDBList(
            AcsAccessTypeDB.GetAllAcsAccessTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsAccessType> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsAccessType_List_Page_" + currentPage ;
            //string countKey = "AcsAccessType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsAccessType> lst = new List<AcsAccessType>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsAccessType>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsAccessTypeCollectionFromAcsAccessTypeDBList(
            AcsAccessTypeDB.GetPageAcsAccessTypeDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsAccessType GetAcsAccessTypeFromAcsAccessTypeDB(AcsAccessTypeDB o)
        {
            if (o == null)
                return null;
            AcsAccessType ret = new AcsAccessType(o.AccessTypeId, o.AccessTypeTitle, o.HasAccess, o.IsDeniedAccess, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsAccessType> GetAcsAccessTypeCollectionFromAcsAccessTypeDBList(List<AcsAccessTypeDB> lst)
        {
            List<AcsAccessType> RetLst = new List<AcsAccessType>();
            foreach (AcsAccessTypeDB o in lst)
            {
                RetLst.Add(GetAcsAccessTypeFromAcsAccessTypeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}