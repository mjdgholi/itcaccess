﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsObjectDB"

    public class AcsObjectDB
    {

        #region Properties :

        public int ObjectId { get; set; }
        public string ObjectTitle { get; set; }
        public int SystemId { get; set; }

        public bool IsHierarchy { get; set; }
        public int ObjectTypeId { get; set; }
        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsObjectDB()
        {
        }

        public AcsObjectDB(int _ObjectId, string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate, string ModificationDate)
        {
            ObjectId = _ObjectId;
            this.ObjectTitle = ObjectTitle;
            this.SystemId = SystemId;
            this.IsHierarchy = IsHierarchy;
            this.ObjectTypeId = ObjectTypeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //---------------- start own function -------------
        public static DataSet GetAllSqlObject(string objectTitle,string schemaName)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@ObjectType",SqlDbType.NVarChar,50), 
                    new SqlParameter("@SchemaName",SqlDbType.NVarChar,300), 
                };
            parameters[0].Value = objectTitle;
            parameters[1].Value = schemaName;
            return _intranetDB.RunProcedureDS("acs.p_GelAllSqlObject", parameters);
        }

        public static DataSet GetAllDs()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {

                };
            return _intranetDB.RunProcedureDS("acs.[p_AcsObjectGetAll]", parameters);
        }

        public static DataSet GetAllPerSystemDs(int systemId)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@SystemId",SqlDbType.Int)                   
                };
            parameters[0].Value = systemId;
            return intranetDB.RunProcedureDS("acs.[p_AcsObjectGetAllPerSystemId]", parameters);
        }

        public static DataSet GetSqlObjectColumn(string objectName)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("ObjectName", SqlDbType.NVarChar, 200),
                };
            parameters[0].Value = objectName;
            return _intranetDB.RunProcedureDS("acs.p_GelSqlObjectField", parameters);
        }

        public static bool CheckObjectHasKey(int objectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@objectId", SqlDbType.Int),
                };
            parameters[0].Value = objectId;
            var ds = _intranetDB.RunProcedureDS("acs.p_CheckObjectHasKey", parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var keycount = Int32.Parse(ds.Tables[0].Rows[0]["keyCount"].ToString());
                if (keycount == 1)
                {
                    return true;
                }
                else if (keycount == 2)
                {
                    throw new Exception("تعداد کلیدهای جدول بیش از 1 میباشد");
                }
                else if (keycount == 0)
                {
                    throw new Exception("آبجکت فاقد فیلد کلید است");
                }
            }
            return false;
        }

        //---------------- end own function   -------------

        public static void AddAcsObjectDB(string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate, string ModificationDate,
                                          out int _AddedId)
        {

            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsHierarchy", SqlDbType.Bit),
                    new SqlParameter("@ObjectTypeId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(ObjectTitle);
            parameters[1].Value = SystemId;
            parameters[2].Value = IsHierarchy;
            parameters[3].Value = ObjectTypeId;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsObjectAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsObjectDB(int ObjectId, string ObjectTitle, int SystemId, bool IsHierarchy, int ObjectTypeId, string CreationDate,
                                             string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsHierarchy", SqlDbType.Bit),
                    new SqlParameter("@ObjectTypeId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = ObjectId;
            parameters[1].Value = GeneralDB.ConvertTo256(ObjectTitle);
            parameters[2].Value = SystemId;
            parameters[3].Value = IsHierarchy;
            parameters[4].Value = ObjectTypeId;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsObjectUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsObjectDB(int ObjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = ObjectId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsObjectDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsObjectDB GetSingleAcsObjectDB(int ObjectId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@ObjectId", DataTypes.integer, 4, ObjectId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsObjectGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsObjectDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsObjectDB> GetAllAcsObjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsObjectDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsObjectGetAll", new IDataParameter[] {}));
        }

        public static List<AcsObjectDB> GetPageAcsObjectDB(int PageSize, int CurrentPage, string WhereClause,
                                                           string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsObject";
            parameters[5].Value = "ObjectId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsObjectDBCollectionFromDataSet(ds, out count);
        }

        public static AcsObjectDB GetAcsObjectDBFromDataReader(IDataReader reader)
        {
            return new AcsObjectDB(int.Parse(reader["ObjectId"].ToString()),
                                   reader["ObjectTitle"].ToString(),
                                   Int32.Parse(reader["SystemId"].ToString()),
                                   bool.Parse(reader["IsHierarchy"].ToString()),
                                   Int32.Parse(reader["ObjectTypeId"].ToString()),
                                   reader["CreationDate"].ToString(),
                                   reader["ModificationDate"].ToString());
        }

        public static List<AcsObjectDB> GetAcsObjectDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsObjectDB> lst = new List<AcsObjectDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsObjectDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsObjectDB> GetAcsObjectDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsObjectDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}