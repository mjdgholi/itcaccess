﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsObjectType"

    /// <summary>
    /// 
    /// </summary>
    public class AcsObjectType : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int ObjectTypeId { get; set; }
        public string ObjectTypeTitle { get; set; }

        public string ObjectTypeEnglieshTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsObjectType()
        {
        }

        public AcsObjectType(int _ObjectTypeId, string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            ObjectTypeId = _ObjectTypeId;
            this.ObjectTypeTitle = ObjectTypeTitle;
            this.ObjectTypeEnglieshTitle = ObjectTypeEnglieshTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :

       

        #region Static Methods :

        public static void Add(string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
            AcsObjectTypeDB.AddAcsObjectTypeDB(ObjectTypeTitle, ObjectTypeEnglieshTitle, IsActive, CreationDate, ModificationDate,out _AddedId);            
        }

        public static void Update(int _ObjectTypeId, string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsObjectTypeDB.UpdateAcsObjectTypeDB(_ObjectTypeId, ObjectTypeTitle, ObjectTypeEnglieshTitle, IsActive, CreationDate, ModificationDate);
            
        }

        public static void Delete(int ObjectTypeId)
        {
            AcsObjectTypeDB.DeleteAcsObjectTypeDB(ObjectTypeId);
            
        }

        public static AcsObjectType GetSingleById(int _ObjectTypeId)
        {
            AcsObjectType o = GetAcsObjectTypeFromAcsObjectTypeDB(
            AcsObjectTypeDB.GetSingleAcsObjectTypeDB(_ObjectTypeId));

            return o;
        }

        public static List<AcsObjectType> GetAll()
        {
            List<AcsObjectType> lst = new List<AcsObjectType>();
            //string key = "AcsObjectType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObjectType>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsObjectTypeCollectionFromAcsObjectTypeDBList(
            AcsObjectTypeDB.GetAllAcsObjectTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsObjectType> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsObjectType_List_Page_" + currentPage ;
            //string countKey = "AcsObjectType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsObjectType> lst = new List<AcsObjectType>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObjectType>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsObjectTypeCollectionFromAcsObjectTypeDBList(
            AcsObjectTypeDB.GetPageAcsObjectTypeDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsObjectType GetAcsObjectTypeFromAcsObjectTypeDB(AcsObjectTypeDB o)
        {
            if (o == null)
                return null;
            AcsObjectType ret = new AcsObjectType(o.ObjectTypeId, o.ObjectTypeTitle, o.ObjectTypeEnglieshTitle, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsObjectType> GetAcsObjectTypeCollectionFromAcsObjectTypeDBList(List<AcsObjectTypeDB> lst)
        {
            List<AcsObjectType> RetLst = new List<AcsObjectType>();
            foreach (AcsObjectTypeDB o in lst)
            {
                RetLst.Add(GetAcsObjectTypeFromAcsObjectTypeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}