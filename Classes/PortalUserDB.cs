﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class PortalUserDB
    {
        //------------ start own function --------------
        public static DataSet PortalUserGetAll()
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {};
            return intranetDB.RunProcedureDS("acs.p_portalUserGetAll", parameters);
        }
    }
}