﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsRoleAndCategorizeDB"

    public class AcsRoleAndCategorizeDB
    {

        #region Properties :

        public int RoleAndCategorizeId { get; set; }
        public string RoleAndCategorizeTilte { get; set; }

        public bool IsRole { get; set; }

        public int SystemId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsRoleAndCategorizeDB()
        {
        }

        public AcsRoleAndCategorizeDB(int _RoleAndCategorizeId, string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate)
        {
            RoleAndCategorizeId = _RoleAndCategorizeId;
            this.RoleAndCategorizeTilte = GeneralDB.ConvertTo256(RoleAndCategorizeTilte);
            this.IsRole = IsRole;
            this.SystemId = SystemId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsRoleAndCategorizeDB(string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeTilte", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsRole", SqlDbType.Bit),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(RoleAndCategorizeTilte);
            parameters[1].Value = IsRole;
            parameters[2].Value = SystemId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsRoleAndCategorizeDB(int RoleAndCategorizeId, string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@RoleAndCategorizeTilte", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsRole", SqlDbType.Bit),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.SmallDateTime),
                };

            parameters[0].Value = RoleAndCategorizeId;

            parameters[1].Value = RoleAndCategorizeTilte;
            parameters[2].Value = IsRole;
            parameters[3].Value = SystemId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsRoleAndCategorizeDB(int RoleAndCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.SmallDateTime),
                };
            parameters[0].Value = RoleAndCategorizeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsRoleAndCategorizeDB GetSingleAcsRoleAndCategorizeDB(int RoleAndCategorizeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@RoleAndCategorizeId", DataTypes.integer, 4, RoleAndCategorizeId)
                    };

                using (reader = _intranetDB.RunProcedureReader("acs.p_AcsRoleAndCategorizeGetSingle", parameters))
                {
                    if (reader != null)
                        if (reader.Read())
                            return GetAcsRoleAndCategorizeDBFromDataReader(reader);
                }
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return null;
        }

        public static List<AcsRoleAndCategorizeDB> GetAllAcsRoleAndCategorizeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsRoleAndCategorizeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsRoleAndCategorizeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsRoleAndCategorizeDB> GetPageAcsRoleAndCategorizeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsRoleAndCategorize";
            parameters[5].Value = "RoleAndCategorizeId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsRoleAndCategorizeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsRoleAndCategorizeDB GetAcsRoleAndCategorizeDBFromDataReader(IDataReader reader)
        {
            return new AcsRoleAndCategorizeDB(int.Parse(reader["RoleAndCategorizeId"].ToString()),
                                              reader["RoleAndCategorizeTilte"].ToString(),
                                              bool.Parse(reader["IsRole"].ToString()),
                                              int.Parse(reader["SystemId"].ToString()),
                                              bool.Parse(reader["IsActive"].ToString()),
                                              reader["CreationDate"].ToString(),
                                              reader["ModificationDate"].ToString());
        }

        public static List<AcsRoleAndCategorizeDB> GetAcsRoleAndCategorizeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsRoleAndCategorizeDB> lst = new List<AcsRoleAndCategorizeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsRoleAndCategorizeDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsRoleAndCategorizeDB> GetAcsRoleAndCategorizeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsRoleAndCategorizeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsRoleAndCategorizeDB> GetAcsRoleAndCategorizeDBCollectionByAcsSystemDB(int SystemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SystemId", SqlDbType.Int);
            parameter.Value = SystemId;
            return GetAcsRoleAndCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRoleAndCategorizeGetByAcsSystem", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}