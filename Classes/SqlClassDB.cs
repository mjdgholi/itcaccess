﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class SqlClassDB
    {
        public static DataSet GetAllDataType()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.p_sqlDbTypeGetAll", new IDataParameter[] {});
        }

        public static DataSet ExecuteSqlQuery(string query)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@QueryString", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = query;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            var dataset = new DataSet();
            using (var sqlCommand = new SqlCommand("acs.[ExecuteSql]", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                sqlDataAdapter.Fill(dataset);
                sqlConnection.Close();
            }
            return dataset;
           // return _intranetDB.RunProcedureDS("[acs].ExecuteSql", parameters);
        }

        public static DataSet GetAllSqlSchema()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.[p_GelAllSqlSchema]", new IDataParameter[] { });
        }

        public static DataSet GetAllSqlObjectType()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.[p_GetAllSqlObjectType]", new IDataParameter[] { });
        }
    }
}