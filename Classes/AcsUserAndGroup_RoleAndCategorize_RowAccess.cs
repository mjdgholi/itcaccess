﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsUserAndGroup_RoleAndCategorize_RowAccess"

    /// <summary>
    /// 
    /// </summary>
    public class AcsUserAndGroup_RoleAndCategorize_RowAccess : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int UserAndGroup_Role_AccessId { get; set; }
        public int? UserAndGroupId { get; set; }

        public int? RoleAndCategorizeId { get; set; }

        public int AccessTypeId { get; set; }

        public int RowAccessId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsUserAndGroup_RoleAndCategorize_RowAccess()
        {
        }

        public AcsUserAndGroup_RoleAndCategorize_RowAccess(int _UserAndGroup_Role_AccessId, int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroup_Role_AccessId = _UserAndGroup_Role_AccessId;
            this.UserAndGroupId = UserAndGroupId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.AccessTypeId = AccessTypeId;
            this.RowAccessId = RowAccessId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :

        #region Static Methods :

        //-------------- start own function ---------------
        public static DataTable GetRowAccessObjectsPerRole(int roleId, int objectid)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetRowAccessObjectsPerRole(roleId,objectid);
        }

        public static DataTable GetRowAccessObjectsPerUser(int userId, int objectid)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetRowAccessObjectsPerUser(userId, objectid);
        }

        public static DataTable GetRowAccessObjectsPerRolePerObject(int roleId, int objectId)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetRowAccessObjectsPerRolePerObject(roleId, objectId);
        }

        public static DataTable GetRowAccessObjectsPerUserPerObject(int userId, int objectId)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetRowAccessObjectsPerUserPerObject(userId, objectId);
        }

        public static int GetObjectPerRowAccess(int rowAccessId)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetObjectPerRowAccess(rowAccessId);
        }

        public static void DeleteAcsUserAndGroup_RowAccessPerObjectPerRole(int objectId, int roleId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.DeleteAcsUserAndGroup_RowAccessPerObjectPerRole(objectId, roleId);
        }

        public static void DeleteAcsUserAndGroup_RowAccessPerObjectPerUser(int objectId, int userId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.DeleteAcsUserAndGroup_RowAccessPerObjectPerUser(objectId,userId);
        }

        public static void AddAcsUserAndGroup_RoleAndCategorize_RowAccessGroupDB(int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, string RowAccessId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.AddAcsUserAndGroup_RoleAndCategorize_RowAccessGroupDB(UserAndGroupId, RoleAndCategorizeId, AccessTypeId, RowAccessId, IsActive, CreationDate, ModificationDate,out _AddedId);
        }

        //-------------- end own function -----------------
        public static void Add(int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.AddAcsUserAndGroup_RoleAndCategorize_RowAccessDB(UserAndGroupId, RoleAndCategorizeId, AccessTypeId, RowAccessId, IsActive, CreationDate, ModificationDate, out _AddedId);
        }

        public static void Update(int _UserAndGroup_Role_AccessId, int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.UpdateAcsUserAndGroup_RoleAndCategorize_RowAccessDB(_UserAndGroup_Role_AccessId, UserAndGroupId, RoleAndCategorizeId, AccessTypeId, RowAccessId, IsActive, CreationDate, ModificationDate);

        }

        public static void Delete(int UserAndGroup_Role_AccessId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccessDB.DeleteAcsUserAndGroup_RoleAndCategorize_RowAccessDB(UserAndGroup_Role_AccessId);

        }

        public static AcsUserAndGroup_RoleAndCategorize_RowAccess GetSingleById(int _UserAndGroup_Role_AccessId)
        {
            AcsUserAndGroup_RoleAndCategorize_RowAccess o = GetAcsUserAndGroup_RoleAndCategorize_RowAccessFromAcsUserAndGroup_RoleAndCategorize_RowAccessDB(
                AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetSingleAcsUserAndGroup_RoleAndCategorize_RowAccessDB(_UserAndGroup_Role_AccessId));

            return o;
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAll()
        {
            List<AcsUserAndGroup_RoleAndCategorize_RowAccess> lst = new List<AcsUserAndGroup_RoleAndCategorize_RowAccess>();
            //string key = "AcsUserAndGroup_RoleAndCategorize_RowAccess_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup_RoleAndCategorize_RowAccess>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(
                AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetAllAcsUserAndGroup_RoleAndCategorize_RowAccessDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAllPaging(int currentPage, int pageSize,
                                                                                     string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsUserAndGroup_RoleAndCategorize_RowAccess_List_Page_" + currentPage ;
            //string countKey = "AcsUserAndGroup_RoleAndCategorize_RowAccess_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsUserAndGroup_RoleAndCategorize_RowAccess> lst = new List<AcsUserAndGroup_RoleAndCategorize_RowAccess>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup_RoleAndCategorize_RowAccess>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(
                AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetPageAcsUserAndGroup_RoleAndCategorize_RowAccessDB(pageSize, currentPage,
                                                                                                                   whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsUserAndGroup_RoleAndCategorize_RowAccess GetAcsUserAndGroup_RoleAndCategorize_RowAccessFromAcsUserAndGroup_RoleAndCategorize_RowAccessDB(AcsUserAndGroup_RoleAndCategorize_RowAccessDB o)
        {
            if (o == null)
                return null;
            AcsUserAndGroup_RoleAndCategorize_RowAccess ret = new AcsUserAndGroup_RoleAndCategorize_RowAccess(o.UserAndGroup_Role_AccessId, o.UserAndGroupId, o.RoleAndCategorizeId, o.AccessTypeId, o.RowAccessId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> lst)
        {
            List<AcsUserAndGroup_RoleAndCategorize_RowAccess> RetLst = new List<AcsUserAndGroup_RoleAndCategorize_RowAccess>();
            foreach (AcsUserAndGroup_RoleAndCategorize_RowAccessDB o in lst)
            {
                RetLst.Add(GetAcsUserAndGroup_RoleAndCategorize_RowAccessFromAcsUserAndGroup_RoleAndCategorize_RowAccessDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionByAcsAccessType(int AccessTypeId)
        {
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsAccessTypeDB(AccessTypeId));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionByAcsRoleAndCategorize(int? RoleAndCategorizeId)
        {
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsRoleAndCategorizeDB(RoleAndCategorizeId));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionByAcsRowAccess(int RowAccessId)
        {
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsRowAccessDB(RowAccessId));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccess> GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionByAcsUserAndGroup(int? UserAndGroupId)
        {
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessCollectionFromAcsUserAndGroup_RoleAndCategorize_RowAccessDBList(AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsUserAndGroupDB(UserAndGroupId));
        }


        #endregion



        #endregion


    }

    #endregion
}