﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsPatternGroup"

    /// <summary>
    /// 
    /// </summary>
    public class AcsPatternGroup : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int PatternGroupId { get; set; }
        public string PatternGroupTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsPatternGroup()
        {
        }

        public AcsPatternGroup(int _PatternGroupId, string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            PatternGroupId = _PatternGroupId;
            this.PatternGroupTitle = PatternGroupTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :       

        #region Static Methods :

        public static void Add(string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate,out int _addedId)
        {
           AcsPatternGroupDB.AddAcsPatternGroupDB(PatternGroupTitle, IsActive, CreationDate, ModificationDate,out _addedId);         
        }

        public static void Update(int _PatternGroupId, string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
           AcsPatternGroupDB.UpdateAcsPatternGroupDB(_PatternGroupId, PatternGroupTitle, IsActive, CreationDate, ModificationDate);           
        }

        public static void Delete(int PatternGroupId)
        {
           AcsPatternGroupDB.DeleteAcsPatternGroupDB(PatternGroupId);           
        }

        public static AcsPatternGroup GetSingleById(int _PatternGroupId)
        {
            AcsPatternGroup o = GetAcsPatternGroupFromAcsPatternGroupDB(
                AcsPatternGroupDB.GetSingleAcsPatternGroupDB(_PatternGroupId));

            return o;
        }

        public static List<AcsPatternGroup> GetAll()
        {
            List<AcsPatternGroup> lst = new List<AcsPatternGroup>();
            //string key = "AcsPatternGroup_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsPatternGroup>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsPatternGroupCollectionFromAcsPatternGroupDBList(
                AcsPatternGroupDB.GetAllAcsPatternGroupDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsPatternGroup> GetAllPaging(int currentPage, int pageSize,
                                                         string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsPatternGroup_List_Page_" + currentPage ;
            //string countKey = "AcsPatternGroup_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsPatternGroup> lst = new List<AcsPatternGroup>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsPatternGroup>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsPatternGroupCollectionFromAcsPatternGroupDBList(
                AcsPatternGroupDB.GetPageAcsPatternGroupDB(pageSize, currentPage,
                                                           whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsPatternGroup GetAcsPatternGroupFromAcsPatternGroupDB(AcsPatternGroupDB o)
        {
            if (o == null)
                return null;
            AcsPatternGroup ret = new AcsPatternGroup(o.PatternGroupId, o.PatternGroupTitle, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsPatternGroup> GetAcsPatternGroupCollectionFromAcsPatternGroupDBList(List<AcsPatternGroupDB> lst)
        {
            List<AcsPatternGroup> RetLst = new List<AcsPatternGroup>();
            foreach (AcsPatternGroupDB o in lst)
            {
                RetLst.Add(GetAcsPatternGroupFromAcsPatternGroupDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }

    #endregion
}