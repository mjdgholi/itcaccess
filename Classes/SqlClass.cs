﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class SqlClass
    {
        public static DataSet GetAllDataType()
        {
            return SqlClassDB.GetAllDataType();
        }

        public static DataSet ExecuteSqlQuery(string query)
        {
            return SqlClassDB.ExecuteSqlQuery(query);
        }

        public static DataSet GetAllSqlSchema()
        {
            return SqlClassDB.GetAllSqlSchema();
        }

        public static DataSet GetAllSqlObjectType()
        {
            return SqlClassDB.GetAllSqlObjectType();
        }
    }
}