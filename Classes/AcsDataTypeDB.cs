﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsDataTypeDB"

    public class AcsDataTypeDB
    {

        #region Properties :

        public int DataTypeId { get; set; }
        public string DataTypeTitle { get; set; }

        public string DataTypeSqlTitle { get; set; }

        public int DataTypeSqlId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsDataTypeDB()
        {
        }

        public AcsDataTypeDB(int _DataTypeId, string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            DataTypeId = _DataTypeId;
            this.DataTypeTitle = DataTypeTitle;
            this.DataTypeSqlTitle = DataTypeSqlTitle;
            this.DataTypeSqlId = DataTypeSqlId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //------------ start own function -------------
        public static DataSet GetAllDataTypeDs()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return _intranetDB.RunProcedureDS("acs.p_AcsDataTypeGetAllDs", new IDataParameter[] {});
        }

        //------------ end own function ---------------

        public static void AddAcsDataTypeDB(string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DataTypeTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@DataTypeSqlTitle", SqlDbType.NVarChar, 50),
                    new SqlParameter("@DataTypeSqlId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@DataTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(DataTypeTitle);
            parameters[1].Value = GeneralDB.ConvertTo256(DataTypeSqlTitle);
            parameters[2].Value = DataTypeSqlId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsDataTypeAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsDataTypeDB(int DataTypeId, string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DataTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@DataTypeTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@DataTypeSqlTitle", SqlDbType.NVarChar, 50),
                    new SqlParameter("@DataTypeSqlId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = DataTypeId;

            parameters[1].Value = GeneralDB.ConvertTo256(DataTypeTitle);
            parameters[2].Value = GeneralDB.ConvertTo256(DataTypeSqlTitle);
            parameters[3].Value = DataTypeSqlId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsDataTypeUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsDataTypeDB(int DataTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DataTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = DataTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsDataTypeDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsDataTypeDB GetSingleAcsDataTypeDB(int DataTypeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@DataTypeId", DataTypes.integer, 4, DataTypeId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsDataTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsDataTypeDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsDataTypeDB> GetAllAcsDataTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsDataTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsDataTypeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsDataTypeDB> GetPageAcsDataTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsDataType";
            parameters[5].Value = "DataTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsDataTypeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsDataTypeDB GetAcsDataTypeDBFromDataReader(IDataReader reader)
        {
            return new AcsDataTypeDB(int.Parse(reader["DataTypeId"].ToString()),
                                     reader["DataTypeTitle"].ToString(),
                                     reader["DataTypeSqlTitle"].ToString(),
                                     int.Parse(reader["DataTypeSqlId"].ToString()),
                                     bool.Parse(reader["IsActive"].ToString()),
                                     reader["CreationDate"].ToString(),
                                     reader["ModificationDate"].ToString());
        }

        public static List<AcsDataTypeDB> GetAcsDataTypeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsDataTypeDB> lst = new List<AcsDataTypeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsDataTypeDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsDataTypeDB> GetAcsDataTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsDataTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}