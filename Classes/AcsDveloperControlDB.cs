﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsDveloperControlDB"

    public class AcsDveloperControlDB
    {

        #region Properties :

        public int DveloperControlId { get; set; }
        public string DveloperControlTitle { get; set; }

        public string DeveloperControlEnglishTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsDveloperControlDB()
        {
        }

        public AcsDveloperControlDB(int _DveloperControlId, string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            DveloperControlId = _DveloperControlId;
            this.DveloperControlTitle = DveloperControlTitle;
            this.DeveloperControlEnglishTitle = DeveloperControlEnglishTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsDveloperControlDB(string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DveloperControlTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@DeveloperControlEnglishTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(DveloperControlTitle);
            parameters[1].Value = GeneralDB.ConvertTo256(DeveloperControlEnglishTitle);
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;


            _intranetDB.RunProcedure("acs.p_AcsDveloperControlAdd", parameters);
            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsDveloperControlDB(int DveloperControlId, string DveloperControlTitle, string DeveloperControlEnglishTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@DveloperControlTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@DeveloperControlEnglishTitle", SqlDbType.NVarChar, 200),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = DveloperControlId;

            parameters[1].Value = GeneralDB.ConvertTo256(DveloperControlTitle);
            parameters[2].Value = GeneralDB.ConvertTo256(DeveloperControlEnglishTitle);
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsDveloperControlUpdate", parameters);
            string messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsDveloperControlDB(int DveloperControlId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = DveloperControlId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsDveloperControlDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsDveloperControlDB GetSingleAcsDveloperControlDB(int DveloperControlId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@DveloperControlId", DataTypes.integer, 4, DveloperControlId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsDveloperControlGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsDveloperControlDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsDveloperControlDB> GetAllAcsDveloperControlDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsDveloperControlDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsDveloperControlGetAll", new IDataParameter[] {}));
        }

        public static List<AcsDveloperControlDB> GetPageAcsDveloperControlDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsDveloperControl";
            parameters[5].Value = "DveloperControlId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsDveloperControlDBCollectionFromDataSet(ds, out count);
        }

        public static AcsDveloperControlDB GetAcsDveloperControlDBFromDataReader(IDataReader reader)
        {
            return new AcsDveloperControlDB(int.Parse(reader["DveloperControlId"].ToString()),
                                            reader["DveloperControlTitle"].ToString(),
                                            reader["DeveloperControlEnglishTitle"].ToString(),
                                            bool.Parse(reader["IsActive"].ToString()),
                                            reader["CreationDate"].ToString(),
                                            reader["ModificationDate"].ToString());
        }

        public static List<AcsDveloperControlDB> GetAcsDveloperControlDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsDveloperControlDB> lst = new List<AcsDveloperControlDB>();

            try
            {
                while (reader.Read())
                    lst.Add(GetAcsDveloperControlDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }
            return lst;
        }

        public static List<AcsDveloperControlDB> GetAcsDveloperControlDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsDveloperControlDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}