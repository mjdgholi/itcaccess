﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsUserAndGroupTree"

    /// <summary>
    /// 
    /// </summary>
    public class AcsUserAndGroupTree : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int UserAndGroupTreeId { get; set; }
        public int UserAndGroupOwnerId { get; set; }

        public int UserAndGroupId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        //------------------------- start own function ----------------

        public static DataSet AcsUserAndGroupTreeGetGetChildNode(int UserAndGroupId, int isUser)
        {
            return AcsUserAndGroupTreeDB.AcsUserAndGroupTreeGetGetChildNode(UserAndGroupId, isUser);
        }

        public static void UpdateUserOrGroupName(int userAndGroupId, string userAndGroupName)
        {
            AcsUserAndGroupTreeDB.UpdateUserOrGroupName(userAndGroupId, userAndGroupName);
        }

        public static DataTable GetParentNodes(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetParentNodes(userAndGroupId);
        }

        public static ArrayList GetParentNodesArrayList(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetParentNodesArrayList(userAndGroupId);
        }

        public static DataTable GetChildNodes(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetChildNodes(userAndGroupId);
        }

        public static ArrayList GetChildNodesArray(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetChildNodesArray(userAndGroupId);
        }

        public static ArrayList GetDirectParentNodes(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetDirectParentNodes(userAndGroupId);
        }

        public static DataSet GetDirectChildNodesTable(int userAndGroupId, int isUser)
        {
            return AcsUserAndGroupTreeDB.GetDirectChildNodes(userAndGroupId, isUser);
        }

        public static ArrayList GetDirectChildNodesArray(int userAndGroupId, int isUser)
        {
            return AcsUserAndGroupTreeDB.GetDirectChildNodesArray(userAndGroupId, isUser);
        }

        public static DataSet GetAllNodeExceptDirectChild(int userAndGroupId, int isUser)
        {
            return AcsUserAndGroupTreeDB.GetAllNodeExceptDirectChild(userAndGroupId, isUser);

        }

        public static ArrayList GetAllNodeExceptDirectChildArray(int userAndGroupId, int isUser)
        {
            return AcsUserAndGroupTreeDB.GetAllNodeExceptDirectChildArray(userAndGroupId, isUser);
        }

        public static ArrayList GetAllNodeForUserAndGroup(int userAndGroupId)
        {
            return AcsUserAndGroupTreeDB.GetAllNodeForUserAndGroup(userAndGroupId);
        }

        public static void ChangeUserAndGroupTreeOwner(int userAndGroupTreeId, int userAndGroupOwnerId)
        {
            AcsUserAndGroupTreeDB.ChangeUserAndGroupTreeOwner(userAndGroupTreeId, userAndGroupOwnerId);
        }

        public static DataTable GetAllNodes(int isUser, string userAndGroupName)
        {
            return AcsUserAndGroupTreeDB.GetAllNodes(isUser, userAndGroupName);
        }

        public static void UserOrGroupUpdateGroupMembership(int userAndGroupId, string groupMemberShip)
        {
            AcsUserAndGroupTreeDB.UserOrGroupUpdateGroupMembership(userAndGroupId, groupMemberShip);
        }

        public static void UserOrGroupUpdateGroupMembers(int userAndGroupId, string groupMembers, int isUser)
        {
            AcsUserAndGroupTreeDB.UserOrGroupUpdateGroupMembers(userAndGroupId, groupMembers, isUser);
        }

        //------------------------- end own function ------------------

        public AcsUserAndGroupTree()
        {
        }

        public AcsUserAndGroupTree(int _UserAndGroupTreeId, int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroupTreeId = _UserAndGroupTreeId;
            this.UserAndGroupOwnerId = UserAndGroupOwnerId;
            this.UserAndGroupId = UserAndGroupId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :

        #region Instance Methods :

        public bool Update()
        {
            return Update(UserAndGroupTreeId, UserAndGroupOwnerId, UserAndGroupId, IsActive, CreationDate, ModificationDate);
        }

        #endregion

        #region Static Methods :

        public static void Add(int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsUserAndGroupTreeDB.AddAcsUserAndGroupTreeDB(UserAndGroupOwnerId, UserAndGroupId, IsActive, CreationDate, ModificationDate, out _AddedId);

        }

        public static bool Update(int _UserAndGroupTreeId, int UserAndGroupOwnerId, int UserAndGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            bool b = AcsUserAndGroupTreeDB.UpdateAcsUserAndGroupTreeDB(_UserAndGroupTreeId, UserAndGroupOwnerId, UserAndGroupId, IsActive, CreationDate, ModificationDate);
            //if(b)
            //PurgeCacheItem("AcsUserAndGroupTree_List");
            return b;
        }

        public static void Delete(int UserAndGroupTreeId)
        {
            AcsUserAndGroupTreeDB.DeleteAcsUserAndGroupTreeDB(UserAndGroupTreeId);
        }

        public static AcsUserAndGroupTree GetSingleById(int _UserAndGroupTreeId)
        {
            AcsUserAndGroupTree o = GetAcsUserAndGroupTreeFromAcsUserAndGroupTreeDB(
                AcsUserAndGroupTreeDB.GetSingleAcsUserAndGroupTreeDB(_UserAndGroupTreeId));

            return o;
        }

        public static List<AcsUserAndGroupTree> GetAll()
        {
            List<AcsUserAndGroupTree> lst = new List<AcsUserAndGroupTree>();
            //string key = "AcsUserAndGroupTree_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroupTree>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsUserAndGroupTreeCollectionFromAcsUserAndGroupTreeDBList(
                AcsUserAndGroupTreeDB.GetAllAcsUserAndGroupTreeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsUserAndGroupTree> GetAllPaging(int currentPage, int pageSize,
                                                             string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsUserAndGroupTree_List_Page_" + currentPage ;
            //string countKey = "AcsUserAndGroupTree_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsUserAndGroupTree> lst = new List<AcsUserAndGroupTree>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroupTree>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsUserAndGroupTreeCollectionFromAcsUserAndGroupTreeDBList(
                AcsUserAndGroupTreeDB.GetPageAcsUserAndGroupTreeDB(pageSize, currentPage,
                                                                   whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsUserAndGroupTree GetAcsUserAndGroupTreeFromAcsUserAndGroupTreeDB(AcsUserAndGroupTreeDB o)
        {
            if (o == null)
                return null;
            AcsUserAndGroupTree ret = new AcsUserAndGroupTree(o.UserAndGroupTreeId, o.UserAndGroupOwnerId, o.UserAndGroupId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsUserAndGroupTree> GetAcsUserAndGroupTreeCollectionFromAcsUserAndGroupTreeDBList(List<AcsUserAndGroupTreeDB> lst)
        {
            List<AcsUserAndGroupTree> RetLst = new List<AcsUserAndGroupTree>();
            foreach (AcsUserAndGroupTreeDB o in lst)
            {
                RetLst.Add(GetAcsUserAndGroupTreeFromAcsUserAndGroupTreeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsUserAndGroupTree> GetAcsUserAndGroupTreeCollectionByAcsUserAndGroup(int UserAndGroupId)
        {
            return GetAcsUserAndGroupTreeCollectionFromAcsUserAndGroupTreeDBList(AcsUserAndGroupTreeDB.GetAcsUserAndGroupTreeDBCollectionByAcsUserAndGroupDB(UserAndGroupId));
        }


        #endregion



        #endregion


    }

    #endregion
}