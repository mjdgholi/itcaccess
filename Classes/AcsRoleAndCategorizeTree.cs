﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsRoleAndCategorizeTree"

    /// <summary>
    /// 
    /// </summary>
    public class AcsRoleAndCategorizeTree : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int RoleAndCategorizeTreeId { get; set; }
        public int? RoleAndCategorizeOwnerId { get; set; }

        public int RoleAndCategorizeId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsRoleAndCategorizeTree()
        {
        }

        public AcsRoleAndCategorizeTree(int _RoleAndCategorizeTreeId, int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            RoleAndCategorizeTreeId = _RoleAndCategorizeTreeId;
            this.RoleAndCategorizeOwnerId = RoleAndCategorizeOwnerId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :


        #region Static Methods :

        //------------------------- start own function ----------------
        public static void RoleAndCategorizeAddWithOwner(string roleAndCategorizeTilte, bool isRole, int systemId, bool isActive, int roleAndCategorizeOwnerId
                                                         , string creationDate, string modificationDate, out int _AddedId)
        {
            AcsRoleAndCategorizeTreeDB.RoleAndCategorizeAddWithOwner(roleAndCategorizeTilte, isRole, systemId, isActive, roleAndCategorizeOwnerId
                                                                     , creationDate, modificationDate, out _AddedId);
        }


        public static void RoleAndCategorizeUpdateWithOwner(int roleAndCategorizeId, string roleAndCategorizeTilte, bool isRole, int systemId, bool isActive, int roleAndCategorizeOwnerId
                                                            , string creationDate, string modificationDate)
        {
            AcsRoleAndCategorizeTreeDB.RoleAndCategorizeUpdateWithOwner(roleAndCategorizeId, roleAndCategorizeTilte, isRole, systemId, isActive, roleAndCategorizeOwnerId
                                                                        , creationDate, modificationDate);
        }

        public static void DeleteWithAllChild(int roleAndCategorizeId)
        {
            AcsRoleAndCategorizeTreeDB.DeleteWithAllChild(roleAndCategorizeId);
        }

        public static DataSet AcsRoleAndCategorizeTreeGetGetChildNode(int RoleAndCategorizeId, int isRole)
        {
            return AcsRoleAndCategorizeTreeDB.AcsRoleAndCategorizeTreeGetGetChildNode(RoleAndCategorizeId, isRole);
        }

        public static void UpdateRoleOrCategorizeName(int roleAndCategorizeId, string RoleAndCategorizeTilte)
        {
            AcsRoleAndCategorizeTreeDB.UpdateRoleOrCategorizeName(roleAndCategorizeId, RoleAndCategorizeTilte);
        }

        public static DataTable GetParentNodes(int roleAndCategorizeId)
        {
            return AcsRoleAndCategorizeTreeDB.GetParentNodes(roleAndCategorizeId);
        }

        public static DataTable GetChildNodes(int roleAndCategorizeId)
        {
            return AcsRoleAndCategorizeTreeDB.GetChildNodes(roleAndCategorizeId);
        }

        public static ArrayList GetChildNodesArray(int roleAndCategorizeId)
        {
            return AcsRoleAndCategorizeTreeDB.GetChildNodesArray(roleAndCategorizeId);
        }

        public static ArrayList GetDirectParentNodes(int roleAndCategorizeId)
        {
            return AcsRoleAndCategorizeTreeDB.GetDirectParentNodes(roleAndCategorizeId);
        }

        public static DataTable GetDirectChildNodesTable(int roleAndCategorizeId, int isRole)
        {
            return AcsRoleAndCategorizeTreeDB.GetDirectChildNodes(roleAndCategorizeId, isRole);
        }

        public static ArrayList GetDirectChildNodesArray(int roleAndCategorizeId, int isRole)
        {
            return AcsRoleAndCategorizeTreeDB.GetDirectChildNodesArray(roleAndCategorizeId, isRole);
        }

        public static DataTable GetAllNodeExceptDirectChild(int roleAndCategorizeId, int isRole)
        {
            return AcsRoleAndCategorizeTreeDB.GetAllNodeExceptDirectChild(roleAndCategorizeId, isRole);

        }

        public static ArrayList GetAllNodeExceptDirectChildArray(int roleAndCategorizeId, int isRole)
        {
            return AcsRoleAndCategorizeTreeDB.GetAllNodeExceptDirectChildArray(roleAndCategorizeId, isRole);
        }

        public static ArrayList GetAllNodeForRoleAndCategorize(int roleAndCategorizeId)
        {
            return AcsRoleAndCategorizeTreeDB.GetAllNodeForRoleAndCategorize(roleAndCategorizeId);
        }

        public static void ChangeRoleCategorizeTreeOwner(int RoleAndCategorizeTreeId, int roleAndCategorizeOwnerId)
        {
            AcsRoleAndCategorizeTreeDB.ChangeRoleCategorizeTreeOwner(RoleAndCategorizeTreeId, roleAndCategorizeOwnerId);
        }

        public static DataTable GetAllNodes(int isRole, string roleAndCategorizeTilte, int systemId)
        {
            return AcsRoleAndCategorizeTreeDB.GetAllNodes(isRole, roleAndCategorizeTilte,systemId);
        }

        public static void RoleOrCategorizeUpdateCategorizeMembership(int roleAndCategorizeId, string CategorizeMemberShip)
        {
            AcsRoleAndCategorizeTreeDB.RoleOrCategorizeUpdateCategorizeMembership(roleAndCategorizeId, CategorizeMemberShip);
        }

        public static void RoleOrCategorizeUpdateCategorizeMembers(int roleAndCategorizeId, string CategorizeMembers, int isRole)
        {
            AcsRoleAndCategorizeTreeDB.RoleOrCategorizeUpdateCategorizeMembers(roleAndCategorizeId, CategorizeMembers, isRole);
        }

        public static ArrayList GetAllNodesArrayList(int isRole, string RoleAndCategorizeTilte, int systemId)
        {
            return AcsRoleAndCategorizeTreeDB.GetAllNodesArrayList(isRole, RoleAndCategorizeTilte, systemId);
        }

        //------------------------- end own function ------------------

        public static int Add(int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            int i = AcsRoleAndCategorizeTreeDB.AddAcsRoleAndCategorizeTreeDB(RoleAndCategorizeOwnerId, RoleAndCategorizeId, IsActive, CreationDate, ModificationDate);
            //if(i>0)
            //PurgeCacheItem("AcsRoleAndCategorizeTree_List");
            return i;
        }

        public static bool Update(int _RoleAndCategorizeTreeId, int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            bool b = AcsRoleAndCategorizeTreeDB.UpdateAcsRoleAndCategorizeTreeDB(_RoleAndCategorizeTreeId, RoleAndCategorizeOwnerId, RoleAndCategorizeId, IsActive, CreationDate, ModificationDate);
            //if(b)
            //PurgeCacheItem("AcsRoleAndCategorizeTree_List");
            return b;
        }

        public static bool Delete(int RoleAndCategorizeTreeId)
        {
            bool b = AcsRoleAndCategorizeTreeDB.DeleteAcsRoleAndCategorizeTreeDB(RoleAndCategorizeTreeId);
            //if(b)
            //PurgeCacheItem("AcsRoleAndCategorizeTree_List");
            return b;
        }

        public static AcsRoleAndCategorizeTree GetSingleById(int _RoleAndCategorizeTreeId)
        {
            AcsRoleAndCategorizeTree o = GetAcsRoleAndCategorizeTreeFromAcsRoleAndCategorizeTreeDB(
                AcsRoleAndCategorizeTreeDB.GetSingleAcsRoleAndCategorizeTreeDB(_RoleAndCategorizeTreeId));

            return o;
        }

        public static List<AcsRoleAndCategorizeTree> GetAll()
        {
            List<AcsRoleAndCategorizeTree> lst = new List<AcsRoleAndCategorizeTree>();
            //string key = "AcsRoleAndCategorizeTree_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRoleAndCategorizeTree>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsRoleAndCategorizeTreeCollectionFromAcsRoleAndCategorizeTreeDBList(
                AcsRoleAndCategorizeTreeDB.GetAllAcsRoleAndCategorizeTreeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsRoleAndCategorizeTree> GetAllPaging(int currentPage, int pageSize,
                                                                  string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsRoleAndCategorizeTree_List_Page_" + currentPage ;
            //string countKey = "AcsRoleAndCategorizeTree_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsRoleAndCategorizeTree> lst = new List<AcsRoleAndCategorizeTree>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRoleAndCategorizeTree>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsRoleAndCategorizeTreeCollectionFromAcsRoleAndCategorizeTreeDBList(
                AcsRoleAndCategorizeTreeDB.GetPageAcsRoleAndCategorizeTreeDB(pageSize, currentPage,
                                                                             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsRoleAndCategorizeTree GetAcsRoleAndCategorizeTreeFromAcsRoleAndCategorizeTreeDB(AcsRoleAndCategorizeTreeDB o)
        {
            if (o == null)
                return null;
            AcsRoleAndCategorizeTree ret = new AcsRoleAndCategorizeTree(o.RoleAndCategorizeTreeId, o.RoleAndCategorizeOwnerId, o.RoleAndCategorizeId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsRoleAndCategorizeTree> GetAcsRoleAndCategorizeTreeCollectionFromAcsRoleAndCategorizeTreeDBList(List<AcsRoleAndCategorizeTreeDB> lst)
        {
            List<AcsRoleAndCategorizeTree> RetLst = new List<AcsRoleAndCategorizeTree>();
            foreach (AcsRoleAndCategorizeTreeDB o in lst)
            {
                RetLst.Add(GetAcsRoleAndCategorizeTreeFromAcsRoleAndCategorizeTreeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsRoleAndCategorizeTree> GetAcsRoleAndCategorizeTreeCollectionByAcsRoleAndRoleCategorize(int? RoleAndCategorizeOwnerId)
        {
            return GetAcsRoleAndCategorizeTreeCollectionFromAcsRoleAndCategorizeTreeDBList(AcsRoleAndCategorizeTreeDB.GetAcsRoleAndCategorizeTreeDBCollectionByAcsRoleAndRoleCategorizeDB(RoleAndCategorizeOwnerId));
        }

        #endregion

        #endregion
    }

    #endregion
}