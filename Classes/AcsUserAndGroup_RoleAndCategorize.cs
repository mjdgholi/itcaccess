﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsUserAndGroup_RoleAndCategorize"

    /// <summary>
    /// 
    /// </summary>
    public class AcsUserAndGroup_RoleAndCategorize : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int UserAndGroup_RoleAndCategorizeId { get; set; }
        public int UserAndGroupId { get; set; }

        public int RoleAndCategorizeId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        //-------------- start own function ----------------
        public static DataSet GetMemberOfRole(int roleAndCategorizeId, int isUser, int userGroupId)
        {

            return AcsUserAndGroup_RoleAndCategorizeDB.GetMemberOfRole(roleAndCategorizeId, isUser, userGroupId);
        }
        public static DataSet GetNotMemberOfRole(int roleAndCategorizeId, int isUser, int userGroupId)
        {

            return AcsUserAndGroup_RoleAndCategorizeDB.GetNotMemberOfRole(roleAndCategorizeId, isUser, userGroupId);
        }

        public static void AddMembersForRoleAndCategorize(int roleAndCategorizeId, string userAndGroupIdStr)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.AddMembersForRoleAndCategorize(roleAndCategorizeId,userAndGroupIdStr);
        }

        public static void UpdateMembersForRoleAndCategorize(int userAndGroupId, string roleAndCategorizeIdStr)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.UpdateMembersForRoleAndCategorize(userAndGroupId,roleAndCategorizeIdStr);
        }

        public static void DeleteMembersForRoleAndCategorize(int roleAndCategorizeId, string userAndGroupIdStr)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.DeleteMembersForRoleAndCategorize(roleAndCategorizeId,userAndGroupIdStr);
        }

        //-------------- end own function ------------------

        public AcsUserAndGroup_RoleAndCategorize()
        {
        }

        public AcsUserAndGroup_RoleAndCategorize(int _UserAndGroup_RoleAndCategorizeId, int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroup_RoleAndCategorizeId = _UserAndGroup_RoleAndCategorizeId;
            this.UserAndGroupId = UserAndGroupId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :


        #region Static Methods :

        public static void Add(int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.AddAcsUserAndGroup_RoleAndCategorizeDB(UserAndGroupId, RoleAndCategorizeId, IsActive, CreationDate, ModificationDate, out _AddedId);
        }

        public static void Update(int _UserAndGroup_RoleAndCategorizeId, int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.UpdateAcsUserAndGroup_RoleAndCategorizeDB(_UserAndGroup_RoleAndCategorizeId, UserAndGroupId, RoleAndCategorizeId, IsActive, CreationDate, ModificationDate);

        }

        public static void Delete(int UserAndGroup_RoleAndCategorizeId)
        {
            AcsUserAndGroup_RoleAndCategorizeDB.DeleteAcsUserAndGroup_RoleAndCategorizeDB(UserAndGroup_RoleAndCategorizeId);

        }

        public static AcsUserAndGroup_RoleAndCategorize GetSingleById(int _UserAndGroup_RoleAndCategorizeId)
        {
            AcsUserAndGroup_RoleAndCategorize o = GetAcsUserAndGroup_RoleAndCategorizeFromAcsUserAndGroup_RoleAndCategorizeDB(
                AcsUserAndGroup_RoleAndCategorizeDB.GetSingleAcsUserAndGroup_RoleAndCategorizeDB(_UserAndGroup_RoleAndCategorizeId));

            return o;
        }

        public static List<AcsUserAndGroup_RoleAndCategorize> GetAll()
        {
            List<AcsUserAndGroup_RoleAndCategorize> lst = new List<AcsUserAndGroup_RoleAndCategorize>();
            //string key = "AcsUserAndGroup_RoleAndCategorize_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup_RoleAndCategorize>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsUserAndGroup_RoleAndCategorizeCollectionFromAcsUserAndGroup_RoleAndCategorizeDBList(
                AcsUserAndGroup_RoleAndCategorizeDB.GetAllAcsUserAndGroup_RoleAndCategorizeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsUserAndGroup_RoleAndCategorize> GetAllPaging(int currentPage, int pageSize,
                                                                           string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsUserAndGroup_RoleAndCategorize_List_Page_" + currentPage ;
            //string countKey = "AcsUserAndGroup_RoleAndCategorize_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsUserAndGroup_RoleAndCategorize> lst = new List<AcsUserAndGroup_RoleAndCategorize>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup_RoleAndCategorize>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsUserAndGroup_RoleAndCategorizeCollectionFromAcsUserAndGroup_RoleAndCategorizeDBList(
                AcsUserAndGroup_RoleAndCategorizeDB.GetPageAcsUserAndGroup_RoleAndCategorizeDB(pageSize, currentPage,
                                                                                               whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsUserAndGroup_RoleAndCategorize GetAcsUserAndGroup_RoleAndCategorizeFromAcsUserAndGroup_RoleAndCategorizeDB(AcsUserAndGroup_RoleAndCategorizeDB o)
        {
            if (o == null)
                return null;
            AcsUserAndGroup_RoleAndCategorize ret = new AcsUserAndGroup_RoleAndCategorize(o.UserAndGroup_RoleAndCategorizeId, o.UserAndGroupId, o.RoleAndCategorizeId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsUserAndGroup_RoleAndCategorize> GetAcsUserAndGroup_RoleAndCategorizeCollectionFromAcsUserAndGroup_RoleAndCategorizeDBList(List<AcsUserAndGroup_RoleAndCategorizeDB> lst)
        {
            List<AcsUserAndGroup_RoleAndCategorize> RetLst = new List<AcsUserAndGroup_RoleAndCategorize>();
            foreach (AcsUserAndGroup_RoleAndCategorizeDB o in lst)
            {
                RetLst.Add(GetAcsUserAndGroup_RoleAndCategorizeFromAcsUserAndGroup_RoleAndCategorizeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsUserAndGroup_RoleAndCategorize> GetAcsUserAndGroup_RoleAndCategorizeCollectionByAcsRoleAndCategorize(int RoleAndCategorizeId)
        {
            return GetAcsUserAndGroup_RoleAndCategorizeCollectionFromAcsUserAndGroup_RoleAndCategorizeDBList(AcsUserAndGroup_RoleAndCategorizeDB.GetAcsUserAndGroup_RoleAndCategorizeDBCollectionByAcsRoleAndCategorizeDB(RoleAndCategorizeId));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize> GetAcsUserAndGroup_RoleAndCategorizeCollectionByAcsUserAndGroup(int UserAndGroupId)
        {
            return GetAcsUserAndGroup_RoleAndCategorizeCollectionFromAcsUserAndGroup_RoleAndCategorizeDBList(AcsUserAndGroup_RoleAndCategorizeDB.GetAcsUserAndGroup_RoleAndCategorizeDBCollectionByAcsUserAndGroupDB(UserAndGroupId));
        }


        #endregion



        #endregion


    }

    #endregion
}