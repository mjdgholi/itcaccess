﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsPatternGroupDB"

    public class AcsPatternGroupDB
    {

        #region Properties :

        public int PatternGroupId { get; set; }
        public string PatternGroupTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        #endregion

        #region Constrauctors :

        public AcsPatternGroupDB()
        {
        }

        public AcsPatternGroupDB(int _PatternGroupId, string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            PatternGroupId = _PatternGroupId;
            this.PatternGroupTitle = PatternGroupTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsPatternGroupDB(string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternGroupTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@PatternGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(PatternGroupTitle);
            parameters[1].Value = IsActive;
            parameters[2].Value = CreationDate;
            parameters[3].Value = ModificationDate;
            parameters[4].Direction = ParameterDirection.Output;
            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsPatternGroupAdd", parameters);
            if (parameters[4].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[4].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsPatternGroupDB(int PatternGroupId, string PatternGroupTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@PatternGroupTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = PatternGroupId;

            parameters[1].Value = GeneralDB.ConvertTo256(PatternGroupTitle);
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsPatternGroupUpdate", parameters);
            string messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsPatternGroupDB(int PatternGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternGroupId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = PatternGroupId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsPatternGroupDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsPatternGroupDB GetSingleAcsPatternGroupDB(int PatternGroupId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@PatternGroupId", DataTypes.integer, 4, PatternGroupId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsPatternGroupGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsPatternGroupDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsPatternGroupDB> GetAllAcsPatternGroupDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsPatternGroupDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsPatternGroupGetAll", new IDataParameter[] {}));
        }

        public static List<AcsPatternGroupDB> GetPageAcsPatternGroupDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsPatternGroup";
            parameters[5].Value = "PatternGroupId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsPatternGroupDBCollectionFromDataSet(ds, out count);
        }

        public static AcsPatternGroupDB GetAcsPatternGroupDBFromDataReader(IDataReader reader)
        {
            return new AcsPatternGroupDB(int.Parse(reader["PatternGroupId"].ToString()),
                                         reader["PatternGroupTitle"].ToString(),
                                         bool.Parse(reader["IsActive"].ToString()),
                                         reader["CreationDate"].ToString(),
                                         reader["ModificationDate"].ToString());
        }

        public static List<AcsPatternGroupDB> GetAcsPatternGroupDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsPatternGroupDB> lst = new List<AcsPatternGroupDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsPatternGroupDBFromDataReader(reader));


            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsPatternGroupDB> GetAcsPatternGroupDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsPatternGroupDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}