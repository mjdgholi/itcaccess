﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsCondition_Datatype_PatternDB"

    public class AcsCondition_Datatype_PatternDB
    {

        #region Properties :

        public int Condition_Datatype_PatternId { get; set; }
        public int ConditionId { get; set; }

        public int DataTypeId { get; set; }

        public int PatternId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsCondition_Datatype_PatternDB()
        {
        }

        public AcsCondition_Datatype_PatternDB(int _Condition_Datatype_PatternId, int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate)
        {
            Condition_Datatype_PatternId = _Condition_Datatype_PatternId;
            this.ConditionId = ConditionId;
            this.DataTypeId = DataTypeId;
            this.PatternId = PatternId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsCondition_Datatype_PatternDB(int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@DataTypeId", SqlDbType.Int),
                    new SqlParameter("@PatternId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@Condition_Datatype_PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = ConditionId;
            parameters[1].Value = DataTypeId;
            parameters[2].Value = PatternId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_PatternAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsCondition_Datatype_PatternDB(int Condition_Datatype_PatternId, int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@Condition_Datatype_PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@DataTypeId", SqlDbType.Int),
                    new SqlParameter("@PatternId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                     new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = Condition_Datatype_PatternId;

            parameters[1].Value = ConditionId;
            parameters[2].Value = DataTypeId;
            parameters[3].Value = PatternId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_PatternUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsCondition_Datatype_PatternDB(int Condition_Datatype_PatternId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@Condition_Datatype_PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = Condition_Datatype_PatternId;
            parameters[1].Direction = ParameterDirection.Output;
             _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_PatternDel", parameters);
             string messageError = parameters[1].Value.ToString();
             if (messageError != "")
                 throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsCondition_Datatype_PatternDB GetSingleAcsCondition_Datatype_PatternDB(int Condition_Datatype_PatternId)
        {
            IDataReader reader = null;

            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@Condition_Datatype_PatternId", DataTypes.integer, 4, Condition_Datatype_PatternId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsCondition_Datatype_PatternGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsCondition_Datatype_PatternDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAllAcsCondition_Datatype_PatternDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsCondition_Datatype_PatternGetAll", new IDataParameter[] {}));
        }

        public static List<AcsCondition_Datatype_PatternDB> GetPageAcsCondition_Datatype_PatternDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsCondition_Datatype_Pattern";
            parameters[5].Value = "Condition_Datatype_PatternId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataSet(ds, out count);
        }

        public static AcsCondition_Datatype_PatternDB GetAcsCondition_Datatype_PatternDBFromDataReader(IDataReader reader)
        {
            return new AcsCondition_Datatype_PatternDB(int.Parse(reader["Condition_Datatype_PatternId"].ToString()),
                                                       int.Parse(reader["ConditionId"].ToString()),
                                                       int.Parse(reader["DataTypeId"].ToString()),
                                                       int.Parse(reader["PatternId"].ToString()),
                                                       bool.Parse(reader["IsActive"].ToString()),
                                                       reader["CreationDate"].ToString(),
                                                       reader["ModificationDate"].ToString());
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsCondition_Datatype_PatternDB> lst = new List<AcsCondition_Datatype_PatternDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsCondition_Datatype_PatternDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAcsCondition_Datatype_PatternDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAcsCondition_Datatype_PatternDBCollectionByAcsConditionDB(int ConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ConditionId", SqlDbType.Int);
            parameter.Value = ConditionId;
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_PatternGetByAcsCondition", new[] {parameter}));
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAcsCondition_Datatype_PatternDBCollectionByAcsDataTypeDB(int DataTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DataTypeId", SqlDbType.Int);
            parameter.Value = DataTypeId;
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_PatternGetByAcsDataType", new[] {parameter}));
        }

        public static List<AcsCondition_Datatype_PatternDB> GetAcsCondition_Datatype_PatternDBCollectionByAcsPatternDB(int PatternId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PatternId", SqlDbType.Int);
            parameter.Value = PatternId;
            return GetAcsCondition_Datatype_PatternDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_PatternGetByAcsPattern", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}