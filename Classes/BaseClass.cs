﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration.Settings;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class BaseClass : ModuleControls
    {
        public  BaseClass()
        {
            string className = GetType().FullName;
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                HttpContext.Current.Response.Redirect(IntranetUI.BuildUrl("~/accessdenied.aspx"));
            }
           
        }
    }
}