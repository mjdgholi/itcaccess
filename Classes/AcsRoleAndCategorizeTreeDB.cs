﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsRoleAndCategorizeTreeDB"

    public class AcsRoleAndCategorizeTreeDB
    {

        #region Properties :

        public int RoleAndCategorizeTreeId { get; set; }
        public int? RoleAndCategorizeOwnerId { get; set; }

        public int RoleAndCategorizeId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsRoleAndCategorizeTreeDB()
        {
        }

        public AcsRoleAndCategorizeTreeDB(int _RoleAndCategorizeTreeId, int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            RoleAndCategorizeTreeId = _RoleAndCategorizeTreeId;
            this.RoleAndCategorizeOwnerId = RoleAndCategorizeOwnerId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //------------------------- start own function ----------------
        public static void RoleAndCategorizeAddWithOwner(string roleAndCategorizeTilte, bool isRole, int systemId, bool isActive, int roleAndCategorizeOwnerId
                                                         , string creationDate, string modificationDate, out int _AddedId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeTilte", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsRole", SqlDbType.Bit),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@RoleAndCategorizeOwnerId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@RoleAndCategorizeTreeId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameter[0].Value = roleAndCategorizeTilte;
            parameter[1].Value = isRole;
            parameter[2].Value = systemId;
            parameter[3].Value = isActive;
            parameter[4].Value = (roleAndCategorizeOwnerId==-1)?(object)DBNull.Value:roleAndCategorizeOwnerId;
            parameter[5].Value = creationDate;
            parameter[6].Value = modificationDate;
            parameter[7].Direction = ParameterDirection.Output;
            parameter[8].Direction = ParameterDirection.Output;


            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsRoleAndCategorizeAddWithOwner", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameter);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }          
          
            if (parameter[7].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameter[7].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameter[8].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void RoleAndCategorizeUpdateWithOwner(int roleAndCategorizeId, string roleAndCategorizeTilte, bool isRole, int systemId, bool isActive, int roleAndCategorizeOwnerId
                                                            , string creationDate, string modificationDate)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeTilte", SqlDbType.NVarChar, 350),
                    new SqlParameter("@IsRole", SqlDbType.Bit),
                    new SqlParameter("@SystemId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@RoleAndCategorizeOwnerId", SqlDbType.Int),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = roleAndCategorizeTilte;
            parameter[2].Value = isRole;
            parameter[3].Value = systemId;
            parameter[4].Value = isActive;
            parameter[5].Value = (roleAndCategorizeOwnerId == -1) ? (object)DBNull.Value : roleAndCategorizeOwnerId;
            parameter[6].Value = creationDate;
            parameter[7].Value = modificationDate;
            parameter[8].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsRoleAndCategorizeEditWithOwner", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameter);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }          
                    
            var messageError = parameter[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        public static void DeleteWithAllChild(int roleAndCategorizeId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = roleAndCategorizeId;
            parameters[1].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsRoleAndCategorizeAddDeleteWithAllChild", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }          
           
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static DataSet AcsRoleAndCategorizeTreeGetGetChildNode(int roleAndCategorizeId, int isRole)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("IsRole", SqlDbType.Int)
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = isRole;
            return intranetDb.RunProcedureDS("[acs].[p_AcsRoleAndCategorizeTreeGetChildNode]", parameter);
        }

        public static void UpdateRoleOrCategorizeName(int roleAndCategorizeId, string RoleAndCategorizeTilte)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("RoleAndCategorizeTilte", SqlDbType.NVarChar, 200),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = GeneralDB.ConvertTo256(RoleAndCategorizeTilte);
            parameter[2].Direction = ParameterDirection.Output;
            intranetDb.RunProcedure("[acs].[p_AcsRoleAndCategorizeTilte]", parameter);
            var messageError = parameter[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        public static DataTable GetParentNodes(int RoleAndCategorizeId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int)
                };
            parameter[0].Value = RoleAndCategorizeId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_ReportParentTreeForRole]", parameter).Tables[0];
            return dataTable;
        }

        public static DataTable GetChildNodes(int RoleAndCategorizeId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int)
                };
            parameter[0].Value = RoleAndCategorizeId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_ReportChildTreeForRole]", parameter).Tables[0];
            return dataTable;
        }

        public static ArrayList GetChildNodesArray(int RoleAndCategorizeId)
        {
            DataTable childDt = GetChildNodes(RoleAndCategorizeId);
            ArrayList childArray = new ArrayList();
            foreach (DataRow child in childDt.Rows)
            {
                if (!childArray.Contains(child["RoleAndCategorizeId"].ToString()))
                {
                    childArray.Add(child["RoleAndCategorizeId"].ToString());
                }
            }
            return childArray;
        }

        public static ArrayList GetDirectParentNodes(int RoleAndCategorizeId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int)
                };
            parameter[0].Value = RoleAndCategorizeId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_RoleAndCategorizeGetDirectParentNodes]", parameter).Tables[0];
            ArrayList converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                converted.Add(row["RoleAndCategorizeOwnerId"].ToString());
            }
            return converted;
        }

        public static DataTable GetDirectChildNodes(int RoleAndCategorizeId, int isRole)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("IsRole", SqlDbType.Int)
                };
            parameter[0].Value = RoleAndCategorizeId;
            parameter[1].Value = isRole;
            return intranetDb.RunProcedureDS("[acs].[p_RoleAndCategorizeGetDirectChildNodes]", parameter).Tables[0];

        }

        public static ArrayList GetDirectChildNodesArray(int RoleAndCategorizeId, int isRole)
        {
            DataTable dataTable = GetDirectChildNodes(RoleAndCategorizeId, isRole);
            var converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                if (!converted.Contains(row["RoleAndCategorizeId"].ToString()))
                {
                    converted.Add(row["RoleAndCategorizeId"].ToString());
                }
            }
            return converted;
        }

        public static ArrayList GetAllNodeForRoleAndCategorize(int RoleAndCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int)
                };
            parameters[0].Value = RoleAndCategorizeId;
            var ds = _intranetDB.RunProcedureDS("[acs].p_GetAllNodeForRoleAndCategorize", parameters);
            ArrayList converted = new ArrayList(ds.Tables[0].Rows.Count);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                converted.Add(row["RoleAndCategorizeTreeId"].ToString());
            }
            return converted;
        }

        public static DataTable GetAllNodeExceptDirectChild(int RoleAndCategorizeId, int isRole)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("IsRole", SqlDbType.Int)
                };
            parameter[0].Value = RoleAndCategorizeId;
            parameter[1].Value = isRole;
            return intranetDb.RunProcedureDS("[acs].[p_RoleAndCategorizeGetAllNodeExceptDirectChild]", parameter).Tables[0];

        }

        public static ArrayList GetAllNodeExceptDirectChildArray(int RoleAndCategorizeId, int isRole)
        {
            DataTable dataTable = GetAllNodeExceptDirectChild(RoleAndCategorizeId, isRole);
            var converted = new ArrayList(dataTable.Rows.Count);
            foreach (DataRow row in dataTable.Rows)
            {
                if (!converted.Contains(row["RoleAndCategorizeId"].ToString()))
                {
                    converted.Add(row["RoleAndCategorizeId"].ToString());
                }
            }
            return converted;
        }

        public static void ChangeRoleCategorizeTreeOwner(int RoleAndCategorizeTreeId, int roleAndCategorizeOwnerId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("RoleAndCategorizeTreeId", SqlDbType.Int),
                    new SqlParameter("RoleAndCategorizeOwnerId", SqlDbType.Int),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RoleAndCategorizeTreeId;
            parameters[1].Value = (roleAndCategorizeOwnerId == -1) ? DBNull.Value : (object) roleAndCategorizeOwnerId;
            parameters[2].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_ChangeRoleCategorizeTreeOwner", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static void RoleOrCategorizeUpdateCategorizeMembership(int RoleAndCategorizeId, string CategorizeMemberShip)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("CategorizeMemberShip", SqlDbType.NVarChar, -1),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RoleAndCategorizeId;
            parameters[1].Value = CategorizeMemberShip;
            parameters[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_RoleOrCategorizeUpdateCategorizeMembership", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }               
           
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public static void RoleOrCategorizeUpdateCategorizeMembers(int RoleAndCategorizeId, string CategorizeMembers, int isRole)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("CategorizeMembers", SqlDbType.NVarChar, -1),
                    new SqlParameter("IsRole", SqlDbType.Int),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = RoleAndCategorizeId;
            parameters[1].Value = CategorizeMembers;
            parameters[2].Value = isRole;
            parameters[3].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_RoleOrCategorizeUpdateCategorizeMembers", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }               
           
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception("خطا در ثبت"));
        }

        public static DataTable GetAllNodes(int isRole, string RoleAndCategorizeTilte, int systemId)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameter =
                {
                    new SqlParameter("IsRole", SqlDbType.Int),
                    new SqlParameter("RoleAndCategorizeTilte", SqlDbType.NVarChar, 300),
                    new SqlParameter("SystemId", SqlDbType.Int),
                };
            parameter[0].Value = isRole;
            parameter[1].Value = GeneralDB.ConvertTo256(RoleAndCategorizeTilte);
            parameter[2].Value = systemId;
            DataTable dataTable = intranetDb.RunProcedureDS("[acs].[p_RoleAndCategorizeGetAll]", parameter).Tables[0];
            return dataTable;
        }

        public static ArrayList GetAllNodesArrayList(int isRole, string RoleAndCategorizeTilte, int systemId)
        {
            var searchResultArrayList = new ArrayList();
            var nodeDt = GetAllNodes(isRole, RoleAndCategorizeTilte, systemId);
            foreach (DataRow dataRow in nodeDt.Rows)
            {
                searchResultArrayList.Add(dataRow["RoleAndCategorizeId"].ToString());
            }
            return searchResultArrayList;
        }

        //------------------------- end own function ----------------

        public static int AddAcsRoleAndCategorizeTreeDB(int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeOwnerId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime)
                };
            parameters[0].Value = RoleAndCategorizeOwnerId;
            parameters[1].Value = RoleAndCategorizeId;
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;


            SqlParameter paramOut = new SqlParameter("@RoleAndCategorizeTreeId", SqlDbType.Int, 4);
            paramOut.Direction = ParameterDirection.Output;
            return _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeTreeAdd", parameters, paramOut);
        }

        public static bool UpdateAcsRoleAndCategorizeTreeDB(int RoleAndCategorizeTreeId, int? RoleAndCategorizeOwnerId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeTreeId", SqlDbType.Int, 4),
                    new SqlParameter("@RoleAndCategorizeOwnerId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime)
                };

            parameters[0].Value = RoleAndCategorizeTreeId;

            parameters[1].Value = RoleAndCategorizeOwnerId;
            parameters[2].Value = RoleAndCategorizeId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;


            return _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeTreeUpdate", parameters);
        }

        public static bool DeleteAcsRoleAndCategorizeTreeDB(int RoleAndCategorizeTreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleAndCategorizeTreeId", SqlDbType.Int, 4)
                };
            parameters[0].Value = RoleAndCategorizeTreeId;
            return _intranetDB.RunProcedure("acs.p_AcsRoleAndCategorizeTreeDel", parameters);
        }

        public static AcsRoleAndCategorizeTreeDB GetSingleAcsRoleAndCategorizeTreeDB(int RoleAndCategorizeTreeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@RoleAndCategorizeTreeId", DataTypes.integer, 4, RoleAndCategorizeTreeId)
                    };

                using (reader = _intranetDB.RunProcedureReader("acs.p_AcsRoleAndCategorizeTreeGetSingle", parameters))
                {
                    if (reader != null)
                        if (reader.Read())
                            return GetAcsRoleAndCategorizeTreeDBFromDataReader(reader);
                }

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsRoleAndCategorizeTreeDB> GetAllAcsRoleAndCategorizeTreeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsRoleAndCategorizeTreeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsRoleAndCategorizeTreeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsRoleAndCategorizeTreeDB> GetPageAcsRoleAndCategorizeTreeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsRoleAndCategorizeTree";
            parameters[5].Value = "RoleAndCategorizeTreeId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsRoleAndCategorizeTreeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsRoleAndCategorizeTreeDB GetAcsRoleAndCategorizeTreeDBFromDataReader(IDataReader reader)
        {
            return new AcsRoleAndCategorizeTreeDB(int.Parse(reader["RoleAndCategorizeTreeId"].ToString()),
                                                  Convert.IsDBNull(reader["RoleAndCategorizeOwnerId"]) ? null : (int?) reader["RoleAndCategorizeOwnerId"],
                                                  int.Parse(reader["RoleAndCategorizeId"].ToString()),
                                                  bool.Parse(reader["IsActive"].ToString()),
                                                  reader["CreationDate"].ToString(),
                                                  reader["ModificationDate"].ToString());
        }

        public static List<AcsRoleAndCategorizeTreeDB> GetAcsRoleAndCategorizeTreeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsRoleAndCategorizeTreeDB> lst = new List<AcsRoleAndCategorizeTreeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsRoleAndCategorizeTreeDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsRoleAndCategorizeTreeDB> GetAcsRoleAndCategorizeTreeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsRoleAndCategorizeTreeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsRoleAndCategorizeTreeDB> GetAcsRoleAndCategorizeTreeDBCollectionByAcsRoleAndRoleCategorizeDB(int? RoleAndCategorizeOwnerId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RoleAndCategorizeOwnerId", SqlDbType.Int);
            parameter.Value = RoleAndCategorizeOwnerId;
            return GetAcsRoleAndCategorizeTreeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsRoleAndCategorizeTreeGetByAcsRoleAndRoleCategorize", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}