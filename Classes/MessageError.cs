﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class MessageError
    {
        private static string[] arr;

        public static string GetMessage(Enum ErrorMessageCode)
        {
            return GetMessage(Convert.ToString(ErrorMessageCode));
        }

        public static string GetMessage(int ErrorMessageCode)
        {
            return GetMessage(Convert.ToString(ErrorMessageCode));
        }

        public static string GetMessage(string MessageError)
        {
            arr = MessageError.Split(',');
            var assembly = Assembly.GetExecutingAssembly();
            var resourcename = Path.GetFileNameWithoutExtension(assembly.CodeBase) + ".ErrorMessages.xml";
            var xmlreader = new StreamReader(assembly.GetManifestResourceStream(resourcename));

            //  var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), "ErrorMessages.xml");

            var messageText = (from c in XElement.Load(xmlreader).Elements("ErrorMessage")
                               where Convert.ToInt32(c.Attribute("MessageId").Value) == Convert.ToInt32(arr[0])
                               select c.Attribute("MessageText").Value).First();

            if (arr.Length > 1)
                messageText = String.Format(messageText, arr[1]);



            return messageText;

        }
    }
}
