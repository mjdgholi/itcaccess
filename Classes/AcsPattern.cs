﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsPattern"

    /// <summary>
    /// 
    /// </summary>
    public class AcsPattern : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int PatternId { get; set; }
        public string PatternTitle { get; set; }

        public string PatternTemplate { get; set; }

        public int PatternGroupId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsPattern()
        {
        }

        public AcsPattern(int _PatternId, string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            PatternId = _PatternId;
            this.PatternTitle = PatternTitle;
            this.PatternTemplate = PatternTemplate;
            this.PatternGroupId = PatternGroupId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :       

        #region Static Methods :

        //----- START OWN fUNCTION ------------
        public static DataSet GetAllPatternDs(int patternGroupId)
        {
          return  AcsPatternDB.GetAllPatternDs(patternGroupId);
        }

        //----- eND oWN fUNCTION --------------

        public static void Add(string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
            AcsPatternDB.AddAcsPatternDB(PatternTitle, PatternTemplate, PatternGroupId, IsActive, CreationDate, ModificationDate,out _AddedId);
           
        }

        public static void Update(int _PatternId, string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsPatternDB.UpdateAcsPatternDB(_PatternId, PatternTitle, PatternTemplate, PatternGroupId, IsActive, CreationDate, ModificationDate);
           
        }

        public static void Delete(int PatternId)
        {
            AcsPatternDB.DeleteAcsPatternDB(PatternId);            
        }

        public static AcsPattern GetSingleById(int _PatternId)
        {
            AcsPattern o = GetAcsPatternFromAcsPatternDB(
            AcsPatternDB.GetSingleAcsPatternDB(_PatternId));

            return o;
        }

        public static List<AcsPattern> GetAll()
        {
            List<AcsPattern> lst = new List<AcsPattern>();
            //string key = "AcsPattern_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsPattern>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsPatternCollectionFromAcsPatternDBList(
            AcsPatternDB.GetAllAcsPatternDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsPattern> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsPattern_List_Page_" + currentPage ;
            //string countKey = "AcsPattern_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsPattern> lst = new List<AcsPattern>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsPattern>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsPatternCollectionFromAcsPatternDBList(
            AcsPatternDB.GetPageAcsPatternDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsPattern GetAcsPatternFromAcsPatternDB(AcsPatternDB o)
        {
            if (o == null)
                return null;
            AcsPattern ret = new AcsPattern(o.PatternId, o.PatternTitle, o.PatternTemplate, o.PatternGroupId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsPattern> GetAcsPatternCollectionFromAcsPatternDBList(List<AcsPatternDB> lst)
        {
            List<AcsPattern> RetLst = new List<AcsPattern>();
            foreach (AcsPatternDB o in lst)
            {
                RetLst.Add(GetAcsPatternFromAcsPatternDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsPattern> GetAcsPatternCollectionByAcsPatternGroup(int PatternGroupId)
        {
            return GetAcsPatternCollectionFromAcsPatternDBList(AcsPatternDB.GetAcsPatternDBCollectionByAcsPatternGroupDB(PatternGroupId));
        }


        #endregion



        #endregion


    }
    #endregion
}