﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsObjectTypeDB"

    public class AcsObjectTypeDB
    {

        #region Properties :

        public int ObjectTypeId { get; set; }
        public string ObjectTypeTitle { get; set; }

        public string ObjectTypeEnglieshTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsObjectTypeDB()
        {
        }

        public AcsObjectTypeDB(int _ObjectTypeId, string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            ObjectTypeId = _ObjectTypeId;
            this.ObjectTypeTitle = ObjectTypeTitle;
            this.ObjectTypeEnglieshTitle = ObjectTypeEnglieshTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsObjectTypeDB(string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectTypeTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@ObjectTypeEnglieshTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ObjectTypeId", SqlDbType.Int),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = ObjectTypeTitle;
            parameters[1].Value = ObjectTypeEnglieshTitle;
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsObjectTypeAdd", parameters);
            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsObjectTypeDB(int ObjectTypeId, string ObjectTypeTitle, string ObjectTypeEnglieshTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectTypeTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@ObjectTypeEnglieshTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };

            parameters[0].Value = ObjectTypeId;
            parameters[1].Value = ObjectTypeTitle;
            parameters[2].Value = ObjectTypeEnglieshTitle;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsObjectTypeUpdate", parameters);
            string messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsObjectTypeDB(int ObjectTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                };
            parameters[0].Value = ObjectTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsObjectTypeDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsObjectTypeDB GetSingleAcsObjectTypeDB(int ObjectTypeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@ObjectTypeId", DataTypes.integer, 4, ObjectTypeId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsObjectTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsObjectTypeDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsObjectTypeDB> GetAllAcsObjectTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsObjectTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsObjectTypeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsObjectTypeDB> GetPageAcsObjectTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsObjectType";
            parameters[5].Value = "ObjectTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsObjectTypeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsObjectTypeDB GetAcsObjectTypeDBFromDataReader(IDataReader reader)
        {
            return new AcsObjectTypeDB(int.Parse(reader["ObjectTypeId"].ToString()),
                                       reader["ObjectTypeTitle"].ToString(),
                                       reader["ObjectTypeEnglieshTitle"].ToString(),
                                       bool.Parse(reader["IsActive"].ToString()),
                                       reader["CreationDate"].ToString(),
                                       reader["ModificationDate"].ToString());
        }

        public static List<AcsObjectTypeDB> GetAcsObjectTypeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsObjectTypeDB> lst = new List<AcsObjectTypeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsObjectTypeDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsObjectTypeDB> GetAcsObjectTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsObjectTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}