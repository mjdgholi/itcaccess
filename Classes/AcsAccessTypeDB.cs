﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsAccessTypeDB"

    public class AcsAccessTypeDB
    {

        #region Properties :

        public int AccessTypeId { get; set; }
        public string AccessTypeTitle { get; set; }

        public bool HasAccess { get; set; }

        public bool IsDeniedAccess { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsAccessTypeDB()
        {
        }

        public AcsAccessTypeDB(int _AccessTypeId, string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate)
        {
            AccessTypeId = _AccessTypeId;
            this.AccessTypeTitle = AccessTypeTitle;
            this.HasAccess = HasAccess;
            this.IsDeniedAccess = IsDeniedAccess;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsAccessTypeDB(string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@AccessTypeTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@HasAccess", SqlDbType.Bit),
                    new SqlParameter("@IsDeniedAccess", SqlDbType.Bit),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@AccessTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(AccessTypeTitle);
            parameters[1].Value = HasAccess;
            parameters[2].Value = IsDeniedAccess;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;

            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsAccessTypeAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsAccessTypeDB(int AccessTypeId, string AccessTypeTitle, bool HasAccess, bool IsDeniedAccess, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@AccessTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@AccessTypeTitle", SqlDbType.NVarChar, 350),
                    new SqlParameter("@HasAccess", SqlDbType.Bit),
                    new SqlParameter("@IsDeniedAccess", SqlDbType.Bit),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = AccessTypeId;

            parameters[1].Value = GeneralDB.ConvertTo256(AccessTypeTitle);
            parameters[2].Value = HasAccess;
            parameters[3].Value = IsDeniedAccess;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsAccessTypeUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsAccessTypeDB(int AccessTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@AccessTypeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = AccessTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsAccessTypeDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsAccessTypeDB GetSingleAcsAccessTypeDB(int AccessTypeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    _intranetDB.GetParameter("@AccessTypeId", DataTypes.integer, 4, AccessTypeId)
                };

            reader = _intranetDB.RunProcedureReader
                ("acs.p_AcsAccessTypeGetSingle", parameters);
            if (reader != null)
                if (reader.Read())
                    return GetAcsAccessTypeDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }
            
            return null;
        }

        public static List<AcsAccessTypeDB> GetAllAcsAccessTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsAccessTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsAccessTypeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsAccessTypeDB> GetPageAcsAccessTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsAccessType";
            parameters[5].Value = "AccessTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsAccessTypeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsAccessTypeDB GetAcsAccessTypeDBFromDataReader(IDataReader reader)
        {
            return new AcsAccessTypeDB(int.Parse(reader["AccessTypeId"].ToString()),
                                       reader["AccessTypeTitle"].ToString(),
                                       bool.Parse(reader["HasAccess"].ToString()),
                                       bool.Parse(reader["IsDeniedAccess"].ToString()),
                                       bool.Parse(reader["IsActive"].ToString()),
                                       reader["CreationDate"].ToString(),
                                       reader["ModificationDate"].ToString());
        }

        public static List<AcsAccessTypeDB> GetAcsAccessTypeDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsAccessTypeDB> lst = new List<AcsAccessTypeDB>();
            try
            {
               while (reader.Read())
                lst.Add(GetAcsAccessTypeDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

          
           
            return lst;
        }

        public static List<AcsAccessTypeDB> GetAcsAccessTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsAccessTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}