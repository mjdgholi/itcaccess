﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsUserAndGroup_RoleAndCategorizeDB"

    public class AcsUserAndGroup_RoleAndCategorizeDB
    {

        #region Properties :

        public int UserAndGroup_RoleAndCategorizeId { get; set; }
        public int UserAndGroupId { get; set; }

        public int RoleAndCategorizeId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        //-------------- start own function ----------------
        public static DataSet GetMemberOfRole(int roleAndCategorizeId, int isUser, int userGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsUser", SqlDbType.Int),
                    new SqlParameter("@UserGroupId", SqlDbType.Int),
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = isUser;
            parameter[2].Value = userGroupId;
            return _intranetDB.RunProcedureDS("acs.p_AcsUserAndGroup_RoleAndCategorizeGetByAcsRoleAndCategorizeDS", parameter);
        }

        public static DataSet GetNotMemberOfRole(int roleAndCategorizeId, int isUser, int userGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsUser", SqlDbType.Int),
                    new SqlParameter("@UserGroupId", SqlDbType.Int),
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = isUser;
            parameter[2].Value = userGroupId;
            return _intranetDB.RunProcedureDS("acs.p_AcsUserAndGroup_RoleAndCategorize_GetNotMemberOfRole", parameter);
        }

        public static void AddMembersForRoleAndCategorize(int roleAndCategorizeId, string userAndGroupIdStr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@UserAndGroupIdStr", SqlDbType.NVarChar, -1),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = userAndGroupIdStr;
            parameter[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AddMembersForRoleAndCategorize", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameter);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }                      
            var messageError = parameter[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void UpdateMembersForRoleAndCategorize(int userAndGroupId, string roleAndCategorizeIdStr)
        {           
            IDataParameter[] parameter =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeIdStr", SqlDbType.NVarChar, -1),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameter[0].Value = userAndGroupId;
            parameter[1].Value = roleAndCategorizeIdStr;
            parameter[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;            
            using (var sqlCommand = new SqlCommand("acs.p_UpdateMembersForRoleAndCategorize", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameter);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();                
                sqlConnection.Close();
            }           
            var messageError = parameter[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }


        public static void DeleteMembersForRoleAndCategorize(int roleAndCategorizeId, string userAndGroupIdStr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameter =
                {
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@UserAndGroupIdStr", SqlDbType.NVarChar, -1),
                    new SqlParameter("MessageError", SqlDbType.NVarChar, -1)
                };
            parameter[0].Value = roleAndCategorizeId;
            parameter[1].Value = userAndGroupIdStr;
            parameter[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_DeleteMembersForRoleAndCategorize", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameter);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }                      
          
            var messageError = parameter[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        //-------------- end own function ------------------

        public AcsUserAndGroup_RoleAndCategorizeDB()
        {
        }

        public AcsUserAndGroup_RoleAndCategorizeDB(int _UserAndGroup_RoleAndCategorizeId, int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroup_RoleAndCategorizeId = _UserAndGroup_RoleAndCategorizeId;
            this.UserAndGroupId = UserAndGroupId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsUserAndGroup_RoleAndCategorizeDB(int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@UserAndGroup_RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = UserAndGroupId;
            parameters[1].Value = RoleAndCategorizeId;
            parameters[2].Value = IsActive;
            parameters[3].Value = CreationDate;
            parameters[4].Value = ModificationDate;
            parameters[5].Direction = ParameterDirection.Output;
            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsUserAndGroup_RoleAndCategorizeAdd", parameters);

            if (parameters[5].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[5].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsUserAndGroup_RoleAndCategorizeDB(int UserAndGroup_RoleAndCategorizeId, int UserAndGroupId, int RoleAndCategorizeId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroup_RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = UserAndGroup_RoleAndCategorizeId;

            parameters[1].Value = UserAndGroupId;
            parameters[2].Value = RoleAndCategorizeId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsUserAndGroup_RoleAndCategorizeUpdate", parameters);
            string messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsUserAndGroup_RoleAndCategorizeDB(int UserAndGroup_RoleAndCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroup_RoleAndCategorizeId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = UserAndGroup_RoleAndCategorizeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsUserAndGroup_RoleAndCategorizeDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsUserAndGroup_RoleAndCategorizeDB GetSingleAcsUserAndGroup_RoleAndCategorizeDB(int UserAndGroup_RoleAndCategorizeId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@UserAndGroup_RoleAndCategorizeId", DataTypes.integer, 4, UserAndGroup_RoleAndCategorizeId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsUserAndGroup_RoleAndCategorizeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsUserAndGroup_RoleAndCategorizeDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                      reader.Close();
                }
              
            }



            return null;
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetAllAcsUserAndGroup_RoleAndCategorizeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsUserAndGroup_RoleAndCategorizeGetAll", new IDataParameter[] {}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetPageAcsUserAndGroup_RoleAndCategorizeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsUserAndGroup_RoleAndCategorize";
            parameters[5].Value = "UserAndGroup_RoleAndCategorizeId";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataSet(ds, out count);
        }

        public static AcsUserAndGroup_RoleAndCategorizeDB GetAcsUserAndGroup_RoleAndCategorizeDBFromDataReader(IDataReader reader)
        {
            return new AcsUserAndGroup_RoleAndCategorizeDB(int.Parse(reader["UserAndGroup_RoleAndCategorizeId"].ToString()),
                                                           int.Parse(reader["UserAndGroupId"].ToString()),
                                                           int.Parse(reader["RoleAndCategorizeId"].ToString()),
                                                           bool.Parse(reader["IsActive"].ToString()),
                                                           reader["CreationDate"].ToString(),
                                                           reader["ModificationDate"].ToString());
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataReader(IDataReader reader)
        {

            List<AcsUserAndGroup_RoleAndCategorizeDB> lst = new List<AcsUserAndGroup_RoleAndCategorizeDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsUserAndGroup_RoleAndCategorizeDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }
            return lst;
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetAcsUserAndGroup_RoleAndCategorizeDBCollectionByAcsRoleAndCategorizeDB(int RoleAndCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int);
            parameter.Value = RoleAndCategorizeId;
            return GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsUserAndGroup_RoleAndCategorizeGetByAcsRoleAndCategorize", new[] {parameter}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorizeDB> GetAcsUserAndGroup_RoleAndCategorizeDBCollectionByAcsUserAndGroupDB(int UserAndGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@UserAndGroupId", SqlDbType.Int);
            parameter.Value = UserAndGroupId;
            return GetAcsUserAndGroup_RoleAndCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsUserAndGroup_RoleAndCategorizeGetByAcsUserAndGroup", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}