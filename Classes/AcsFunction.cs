﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsFunction"

    /// <summary>
    /// 
    /// </summary>
    public class AcsFunction : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int FunctionId { get; set; }
        public int ObjectFieldId { get; set; }

        public string FunctionTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsFunction()
        {
        }

        public AcsFunction(int _FunctionId, int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            FunctionId = _FunctionId;
            this.ObjectFieldId = ObjectFieldId;
            this.FunctionTitle = FunctionTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :       

        //---------------- start own function -------------

        public static DataSet GetAllSqlFunction()
        {
            return AcsFunctionDB.GetAllSqlFunction();
        }    

        //---------------- end own function ---------------

        #region Static Methods :

        public static void Add(int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
             AcsFunctionDB.AddAcsFunctionDB(ObjectFieldId, FunctionTitle, IsActive, CreationDate, ModificationDate,out _AddedId);
        
        }

        public static void Update(int _FunctionId, int ObjectFieldId, string FunctionTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
           AcsFunctionDB.UpdateAcsFunctionDB(_FunctionId, ObjectFieldId, FunctionTitle, IsActive, CreationDate, ModificationDate);
           
        }

        public static void Delete(int FunctionId)
        {
            AcsFunctionDB.DeleteAcsFunctionDB(FunctionId);
           
        }

        public static AcsFunction GetSingleById(int _FunctionId)
        {
            AcsFunction o = GetAcsFunctionFromAcsFunctionDB(
            AcsFunctionDB.GetSingleAcsFunctionDB(_FunctionId));

            return o;
        }

        public static List<AcsFunction> GetAll()
        {
            List<AcsFunction> lst = new List<AcsFunction>();
            //string key = "AcsFunction_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsFunction>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsFunctionCollectionFromAcsFunctionDBList(
            AcsFunctionDB.GetAllAcsFunctionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsFunction> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsFunction_List_Page_" + currentPage ;
            //string countKey = "AcsFunction_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsFunction> lst = new List<AcsFunction>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsFunction>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsFunctionCollectionFromAcsFunctionDBList(
            AcsFunctionDB.GetPageAcsFunctionDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsFunction GetAcsFunctionFromAcsFunctionDB(AcsFunctionDB o)
        {
            if (o == null)
                return null;
            AcsFunction ret = new AcsFunction(o.FunctionId, o.ObjectFieldId, o.FunctionTitle, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsFunction> GetAcsFunctionCollectionFromAcsFunctionDBList(List<AcsFunctionDB> lst)
        {
            List<AcsFunction> RetLst = new List<AcsFunction>();
            foreach (AcsFunctionDB o in lst)
            {
                RetLst.Add(GetAcsFunctionFromAcsFunctionDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsFunction> GetAcsFunctionCollectionByAcsObjectField(int ObjectFieldId)
        {
            return GetAcsFunctionCollectionFromAcsFunctionDBList(AcsFunctionDB.GetAcsFunctionDBCollectionByAcsObjectFieldDB(ObjectFieldId));
        }


        #endregion



        #endregion


    }
    #endregion
}