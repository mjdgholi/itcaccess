﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsDataType"

    /// <summary>
    /// 
    /// </summary>
    public class AcsDataType : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int DataTypeId { get; set; }
        public string DataTypeTitle { get; set; }

        public string DataTypeSqlTitle { get; set; }

        public int DataTypeSqlId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsDataType()
        {
        }

        public AcsDataType(int _DataTypeId, string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            DataTypeId = _DataTypeId;
            this.DataTypeTitle = DataTypeTitle;
            this.DataTypeSqlTitle = DataTypeSqlTitle;
            this.DataTypeSqlId = DataTypeSqlId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :

        #region Static Methods :

        //------------ start own function -------------
        public static DataSet GetAllDataTypeDs()
        {
            return AcsDataTypeDB.GetAllDataTypeDs();
        }

        //------------ end own function ---------------

        public static void Add(string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsDataTypeDB.AddAcsDataTypeDB(DataTypeTitle, DataTypeSqlTitle, DataTypeSqlId, IsActive, CreationDate, ModificationDate, out _AddedId);

        }

        public static void Update(int _DataTypeId, string DataTypeTitle, string DataTypeSqlTitle, int DataTypeSqlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsDataTypeDB.UpdateAcsDataTypeDB(_DataTypeId, DataTypeTitle, DataTypeSqlTitle, DataTypeSqlId, IsActive, CreationDate, ModificationDate);
        }

        public static void Delete(int DataTypeId)
        {
            AcsDataTypeDB.DeleteAcsDataTypeDB(DataTypeId);
        }

        public static AcsDataType GetSingleById(int _DataTypeId)
        {
            AcsDataType o = GetAcsDataTypeFromAcsDataTypeDB(
                AcsDataTypeDB.GetSingleAcsDataTypeDB(_DataTypeId));
            return o;
        }

        public static List<AcsDataType> GetAll()
        {
            List<AcsDataType> lst = new List<AcsDataType>();
            //string key = "AcsDataType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsDataType>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsDataTypeCollectionFromAcsDataTypeDBList(
                AcsDataTypeDB.GetAllAcsDataTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsDataType> GetAllPaging(int currentPage, int pageSize,
                                                     string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsDataType_List_Page_" + currentPage ;
            //string countKey = "AcsDataType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsDataType> lst = new List<AcsDataType>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsDataType>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsDataTypeCollectionFromAcsDataTypeDBList(
                AcsDataTypeDB.GetPageAcsDataTypeDB(pageSize, currentPage,
                                                   whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsDataType GetAcsDataTypeFromAcsDataTypeDB(AcsDataTypeDB o)
        {
            if (o == null)
                return null;
            AcsDataType ret = new AcsDataType(o.DataTypeId, o.DataTypeTitle, o.DataTypeSqlTitle, o.DataTypeSqlId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsDataType> GetAcsDataTypeCollectionFromAcsDataTypeDBList(List<AcsDataTypeDB> lst)
        {
            List<AcsDataType> RetLst = new List<AcsDataType>();
            foreach (AcsDataTypeDB o in lst)
            {
                RetLst.Add(GetAcsDataTypeFromAcsDataTypeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }

    #endregion
}