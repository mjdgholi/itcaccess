﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsRowAccessDetail"

    /// <summary>
    /// 
    /// </summary>
    public class AcsRowAccessDetail : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int RowAccessDetailId { get; set; }
        public int RowAccessId { get; set; }

        public int? ObjectFieldId { get; set; }

        public int? OperatorId { get; set; }

        public int? FunctionId { get; set; }

        public int? ConditionId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int? OwnerId { get; set; }

        public string ValuesOrFunction { get; set; }

        public int? SystemRowAccessDetailOwnerId { get; set; }

        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsRowAccessDetail()
        {
        }

        public AcsRowAccessDetail(int _RowAccessDetailId, int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, string valuesOrFunction, int? SystemRowAccessDetailOwnerId)
        {
            RowAccessDetailId = _RowAccessDetailId;
            this.RowAccessId = RowAccessId;
            this.ObjectFieldId = ObjectFieldId;
            this.OperatorId = OperatorId;
            this.FunctionId = FunctionId;
            this.ConditionId = ConditionId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.OwnerId = OwnerId;
            this.ValuesOrFunction = valuesOrFunction;
            this.SystemRowAccessDetailOwnerId = SystemRowAccessDetailOwnerId;
        }

        #endregion

        #region Mehtods :

        #region Static Methods :

        // -------------- start own function ---------------
        public static DataSet GetSqlDataResultForRowAccess(int objectId, string userAndGroupRoleAccesstr)
        {
          return  AcsRowAccessDetailDB.GetSqlDataResultForRowAccess(objectId,userAndGroupRoleAccesstr);
        }

        public static DataSet GetSqlDataResultForRowAccessFromRowAccessStr(int objectId, string rowAccessStr)
        {
           return AcsRowAccessDetailDB.GetSqlDataResultForRowAccessFromRowAccessStr(objectId, rowAccessStr);
        }

        public static DataSet RunSqlCommand(int rowAccessId, string strSql)
        {
            return AcsRowAccessDetailDB.RunSqlCommand(rowAccessId, strSql);
        }        

        public static DataSet RowAccessDetailGetAll(int rowAccessId,bool loadUserData)
        {
            return AcsRowAccessDetailDB.RowAccessDetailGetAll(rowAccessId,loadUserData);
        }

        public static void p_AcsRowAccessDetailAddInTree(int RowAccessId, int? UserObjectFieldId, int? OperatorId, int? FunctionId, string Values, string userValue, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, out int _AddedId)
        {
            AcsRowAccessDetailDB.p_AcsRowAccessDetailAddInTree(RowAccessId,  UserObjectFieldId, OperatorId, FunctionId, Values, userValue, ConditionId, CreationDate, ModificationDate, OwnerId, out _AddedId);
        }

        public static void AcsRowAccessDetailUpdateInTree(int RowAccessDetailId, int? ObjectFieldId, int? OperatorId, int? FunctionId, string values,string userValues, int? ConditionId)
        {
            AcsRowAccessDetailDB.AcsRowAccessDetailUpdateInTree(RowAccessDetailId, ObjectFieldId, OperatorId, FunctionId, values,userValues, ConditionId);
        }

        public static void DeleteAcsRowAccessDetailFromTree(int userRowAccessDetailId)
        {
            AcsRowAccessDetailDB.DeleteAcsRowAccessDetailFromTree(userRowAccessDetailId);
        }

        public static DataSet RowAccessGetChildTree(int RowAccessId)
        {
            return AcsRowAccessDetailDB.RowAccessGetChildTree(RowAccessId);
        }

        // -------------- end own function -----------------

        public static void Add(int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId, out int _AddedId)
        {
            AcsRowAccessDetailDB.AddAcsRowAccessDetailDB(RowAccessId, ObjectFieldId, OperatorId, FunctionId, ConditionId, CreationDate, ModificationDate, OwnerId, out _AddedId);

        }

        public static void Update(int _RowAccessDetailId, int RowAccessId, int? ObjectFieldId, int? OperatorId, int? FunctionId, int? ConditionId, string CreationDate, string ModificationDate, int? OwnerId)
        {
            AcsRowAccessDetailDB.UpdateAcsRowAccessDetailDB(_RowAccessDetailId, RowAccessId, ObjectFieldId, OperatorId, FunctionId, ConditionId, CreationDate, ModificationDate, OwnerId);

        }

        public static void Delete(int RowAccessDetailId)
        {
            AcsRowAccessDetailDB.DeleteAcsRowAccessDetailDB(RowAccessDetailId);
        }

        public static AcsRowAccessDetail GetSingleById(int _RowAccessDetailId)
        {
            AcsRowAccessDetail o = GetAcsRowAccessDetailFromAcsRowAccessDetailDB(
                AcsRowAccessDetailDB.GetSingleAcsRowAccessDetailDB(_RowAccessDetailId));

            return o;
        }

        public static List<AcsRowAccessDetail> GetAll()
        {
            List<AcsRowAccessDetail> lst = new List<AcsRowAccessDetail>();
            //string key = "AcsRowAccessDetail_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRowAccessDetail>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(
                AcsRowAccessDetailDB.GetAllAcsRowAccessDetailDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsRowAccessDetail> GetAllPaging(int currentPage, int pageSize,
                                                            string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsRowAccessDetail_List_Page_" + currentPage ;
            //string countKey = "AcsRowAccessDetail_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsRowAccessDetail> lst = new List<AcsRowAccessDetail>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRowAccessDetail>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(
                AcsRowAccessDetailDB.GetPageAcsRowAccessDetailDB(pageSize, currentPage,
                                                                 whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsRowAccessDetail GetAcsRowAccessDetailFromAcsRowAccessDetailDB(AcsRowAccessDetailDB o)
        {
            if (o == null)
                return null;
            AcsRowAccessDetail ret = new AcsRowAccessDetail(o.RowAccessDetailId, o.RowAccessId, o.ObjectFieldId, o.OperatorId, o.FunctionId, o.ConditionId, o.CreationDate, o.ModificationDate, o.OwnerId, o.ValuesOrFunction, o.SystemRowAccessDetailOwnerId);
            return ret;
        }

        private static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(List<AcsRowAccessDetailDB> lst)
        {
            List<AcsRowAccessDetail> RetLst = new List<AcsRowAccessDetail>();
            foreach (AcsRowAccessDetailDB o in lst)
            {
                RetLst.Add(GetAcsRowAccessDetailFromAcsRowAccessDetailDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsCondition(int? ConditionId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsConditionDB(ConditionId));
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsFunction(int? FunctionId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsFunctionDB(FunctionId));
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsObjectField(int? ObjectFieldId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsObjectFieldDB(ObjectFieldId));
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsOperator(int? OperatorId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsOperatorDB(OperatorId));
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsRowAccess(int RowAccessId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsRowAccessDB(RowAccessId));
        }

        public static List<AcsRowAccessDetail> GetAcsRowAccessDetailCollectionByAcsRowAccessDetail(int? OwnerId)
        {
            return GetAcsRowAccessDetailCollectionFromAcsRowAccessDetailDBList(AcsRowAccessDetailDB.GetAcsRowAccessDetailDBCollectionByAcsRowAccessDetailDB(OwnerId));
        }


        #endregion



        #endregion


    }

    #endregion
}