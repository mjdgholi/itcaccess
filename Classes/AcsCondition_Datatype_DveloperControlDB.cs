﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsCondition_Datatype_DveloperControlDB"

    public class AcsCondition_Datatype_DveloperControlDB
    {

        #region Properties :

        public int Condition_Datatype_DveloperControlId { get; set; }
        public int ConditionId { get; set; }

        public int DataTypeId { get; set; }

        public bool IsHierarchy { get; set; }

        public int DveloperControlId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsCondition_Datatype_DveloperControlDB()
        {
        }

        public AcsCondition_Datatype_DveloperControlDB(int _Condition_Datatype_DveloperControlId, int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            Condition_Datatype_DveloperControlId = _Condition_Datatype_DveloperControlId;
            this.ConditionId = ConditionId;
            this.DataTypeId = DataTypeId;
            this.IsHierarchy = IsHierarchy;
            this.DveloperControlId = DveloperControlId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //-------------- start own function ---------------
        public static string GetDeveloperControlPerConditionAndField(int conditionId,int fieldId)
        {
            var intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters=
                {
                    new SqlParameter("ConditionId",SqlDbType.Int), 
                    new SqlParameter("FieldId",SqlDbType.Int), 
                };
            parameters[0].Value = conditionId;
            parameters[1].Value = fieldId;
            var datarow =intranetDb.RunProcedureDR("acs.p_AcsGetDeveloperControlPerConditionAndField",parameters);
            return datarow != null ? datarow["DeveloperControlEnglishTitle"].ToString() : "";
        }

        //-------------- end own function -----------------

        public static void AddAcsCondition_Datatype_DveloperControlDB(int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@DataTypeId", SqlDbType.Int),
                    new SqlParameter("@IsHierarchy",SqlDbType.Bit), 
                    new SqlParameter("@DveloperControlId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@Condition_Datatype_DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = ConditionId;
            parameters[1].Value = DataTypeId;
            parameters[2].Value = IsHierarchy;
            parameters[3].Value = DveloperControlId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_DveloperControlAdd", parameters);
            if (parameters[7].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[7].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsCondition_Datatype_DveloperControlDB(int Condition_Datatype_DveloperControlId, int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@Condition_Datatype_DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@ConditionId", SqlDbType.Int),
                    new SqlParameter("@DataTypeId", SqlDbType.Int),
                    new SqlParameter("@IsHierarchy",SqlDbType.Bit), 
                    new SqlParameter("@DveloperControlId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = Condition_Datatype_DveloperControlId;

            parameters[1].Value = ConditionId;
            parameters[2].Value = DataTypeId;
            parameters[3].Value = IsHierarchy;
            parameters[4].Value = DveloperControlId;
            parameters[5].Value = IsActive;
            parameters[6].Value = CreationDate;
            parameters[7].Value = ModificationDate;
            parameters[8].Direction = ParameterDirection.Output;

             _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_DveloperControlUpdate", parameters);
             string messageError = parameters[8].Value.ToString();
             if (messageError != "")
                 throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsCondition_Datatype_DveloperControlDB(int Condition_Datatype_DveloperControlId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@Condition_Datatype_DveloperControlId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = Condition_Datatype_DveloperControlId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsCondition_Datatype_DveloperControlDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsCondition_Datatype_DveloperControlDB GetSingleAcsCondition_Datatype_DveloperControlDB(int Condition_Datatype_DveloperControlId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@Condition_Datatype_DveloperControlId", DataTypes.integer, 4, Condition_Datatype_DveloperControlId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsCondition_Datatype_DveloperControlGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsCondition_Datatype_DveloperControlDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAllAcsCondition_Datatype_DveloperControlDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsCondition_Datatype_DveloperControlGetAll", new IDataParameter[] {}));
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetPageAcsCondition_Datatype_DveloperControlDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsCondition_Datatype_DveloperControl";
            parameters[5].Value = "Condition_Datatype_DveloperControlId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataSet(ds, out count);
        }

        public static AcsCondition_Datatype_DveloperControlDB GetAcsCondition_Datatype_DveloperControlDBFromDataReader(IDataReader reader)
        {
            return new AcsCondition_Datatype_DveloperControlDB(int.Parse(reader["Condition_Datatype_DveloperControlId"].ToString()),
                                                               int.Parse(reader["ConditionId"].ToString()),
                                                               int.Parse(reader["DataTypeId"].ToString()),
                                                               bool.Parse(reader["IsHierarchy"].ToString()),
                                                               int.Parse(reader["DveloperControlId"].ToString()),
                                                               bool.Parse(reader["IsActive"].ToString()),
                                                               reader["CreationDate"].ToString(),
                                                               reader["ModificationDate"].ToString());
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsCondition_Datatype_DveloperControlDB> lst = new List<AcsCondition_Datatype_DveloperControlDB>();

            try
            {
                while (reader.Read())
                    lst.Add(GetAcsCondition_Datatype_DveloperControlDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsConditionDB(int ConditionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ConditionId", SqlDbType.Int);
            parameter.Value = ConditionId;
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_DveloperControlGetByAcsCondition", new[] {parameter}));
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsDataTypeDB(int DataTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DataTypeId", SqlDbType.Int);
            parameter.Value = DataTypeId;
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_DveloperControlGetByAcsDataType", new[] {parameter}));
        }

        public static List<AcsCondition_Datatype_DveloperControlDB> GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsDveloperControlDB(int DveloperControlId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DveloperControlId", SqlDbType.Int);
            parameter.Value = DveloperControlId;
            return GetAcsCondition_Datatype_DveloperControlDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsCondition_Datatype_DveloperControlGetByAcsDveloperControl", new[] {parameter}));
        }


        #endregion

    }

    #endregion
}