﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsPatternDB"

    public class AcsPatternDB
    {

        #region Properties :

        public int PatternId { get; set; }
        public string PatternTitle { get; set; }

        public string PatternTemplate { get; set; }

        public int PatternGroupId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public AcsPatternDB()
        {
        }



        public AcsPatternDB(int _PatternId, string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            PatternId = _PatternId;
            this.PatternTitle = PatternTitle;
            this.PatternTemplate = PatternTemplate;
            this.PatternGroupId = PatternGroupId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        //----- START OWN fUNCTION ------------
        public static DataSet GetAllPatternDs(int patternGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {new SqlParameter("PatternGroupId", SqlDbType.Int)};
            parameters[0].Value = patternGroupId;
            return _intranetDB.RunProcedureDS("acs.p_AcsPatternGetAllDs", parameters);
        }

        //----- eND oWN fUNCTION --------------

        public static void AddAcsPatternDB(string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@PatternTemplate", SqlDbType.NVarChar, 100),
                    new SqlParameter("@PatternGroupId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = GeneralDB.ConvertTo256(PatternTitle);
            parameters[1].Value = PatternTemplate;
            parameters[2].Value = PatternGroupId;
            parameters[3].Value = IsActive;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Direction = ParameterDirection.Output;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsPatternAdd", parameters);
            if (parameters[6].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[6].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsPatternDB(int PatternId, string PatternTitle, string PatternTemplate, int PatternGroupId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@PatternTitle", SqlDbType.NVarChar, 100),
                    new SqlParameter("@PatternTemplate", SqlDbType.NVarChar, 100),
                    new SqlParameter("@PatternGroupId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = PatternId;

            parameters[1].Value = GeneralDB.ConvertTo256(PatternTitle);
            parameters[2].Value = PatternTemplate;
            parameters[3].Value = PatternGroupId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsPatternUpdate", parameters);
            string messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsPatternDB(int PatternId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@PatternId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = PatternId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("acs.p_AcsPatternDel", parameters);

        }

        public static AcsPatternDB GetSingleAcsPatternDB(int PatternId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@PatternId", DataTypes.integer, 4, PatternId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsPatternGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsPatternDBFromDataReader(reader);
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsPatternDB> GetAllAcsPatternDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsPatternDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("acs.p_AcsPatternGetAll", new IDataParameter[] {}));
        }

        public static List<AcsPatternDB> GetPageAcsPatternDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsPattern";
            parameters[5].Value = "PatternId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsPatternDBCollectionFromDataSet(ds, out count);
        }

        public static AcsPatternDB GetAcsPatternDBFromDataReader(IDataReader reader)
        {
            return new AcsPatternDB(int.Parse(reader["PatternId"].ToString()),
                                    reader["PatternTitle"].ToString(),
                                    reader["PatternTemplate"].ToString(),
                                    int.Parse(reader["PatternGroupId"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public static List<AcsPatternDB> GetAcsPatternDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsPatternDB> lst = new List<AcsPatternDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsPatternDBFromDataReader(reader));

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }


            return lst;
        }

        public static List<AcsPatternDB> GetAcsPatternDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsPatternDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsPatternDB> GetAcsPatternDBCollectionByAcsPatternGroupDB(int PatternGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PatternGroupId", SqlDbType.Int);
            parameter.Value = PatternGroupId;
            return GetAcsPatternDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsPatternGetByAcsPatternGroup", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}