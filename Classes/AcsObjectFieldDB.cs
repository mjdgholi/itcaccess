﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------
    #region Class "AcsObjectFieldDB"

    public class AcsObjectFieldDB
    {

        #region Properties :

        public int ObjectFieldId { get; set; }
        public int ObjectId { get; set; }

        public string FieldTitle { get; set; }

        public bool IsKey { get; set; }

        public bool IsOwner { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public AcsObjectFieldDB()
        {
        }

        public AcsObjectFieldDB(int _ObjectFieldId, int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive)
        {
            ObjectFieldId = _ObjectFieldId;
            this.ObjectId = ObjectId;
            this.FieldTitle = FieldTitle;
            this.IsKey = IsKey;
            this.IsOwner = IsOwner;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

        #region Methods :

        public static void AddAcsObjectFieldDB(int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive,out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ObjectId", SqlDbType.Int) ,
											  new SqlParameter("@FieldTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@IsKey", SqlDbType.Bit) ,
											  new SqlParameter("@IsOwner", SqlDbType.Bit) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
                                               new SqlParameter("@ObjectFieldId", SqlDbType.Int, 4),
                                               new SqlParameter("@MessageError",SqlDbType.NVarChar,-1), 
							};
            parameters[0].Value = ObjectId;
            parameters[1].Value = FieldTitle;
            parameters[2].Value = IsKey;
            parameters[3].Value = IsOwner;
            parameters[4].Value = CreationDate;
            parameters[5].Value = ModificationDate;
            parameters[6].Value = IsActive;
            parameters[7].Direction=ParameterDirection.Output;
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("acs.p_AcsObjectFieldAdd", parameters);
            if (parameters[7].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[7].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsObjectFieldDB(int ObjectFieldId, int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ObjectFieldId", SqlDbType.Int, 4),
											 new SqlParameter("@ObjectId", SqlDbType.Int) ,
											  new SqlParameter("@FieldTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@IsKey", SqlDbType.Bit) ,
											  new SqlParameter("@IsOwner", SqlDbType.Bit) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
                                              new SqlParameter("@MessageError",SqlDbType.NVarChar,-1), 
										};

            parameters[0].Value = ObjectFieldId;

            parameters[1].Value = ObjectId;
            parameters[2].Value = FieldTitle;
            parameters[3].Value = IsKey;
            parameters[4].Value = IsOwner;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Value = IsActive;
            parameters[8].Direction=ParameterDirection.Output;

             _intranetDB.RunProcedure("acs.p_AcsObjectFieldUpdate", parameters);
             string messageError = parameters[8].Value.ToString();
             if (messageError != "")
                 throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsObjectFieldDB(int ObjectFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ObjectFieldId", SqlDbType.Int, 4),
                                            new SqlParameter("@MessageError",SqlDbType.NVarChar,-1), 
										};
            parameters[0].Value = ObjectFieldId;
            parameters[1].Direction=ParameterDirection.Output;
             _intranetDB.RunProcedure("acs.p_AcsObjectFieldDel", parameters);
             string messageError = parameters[1].Value.ToString();
             if (messageError != "")
                 throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsObjectFieldDB GetSingleAcsObjectFieldDB(int ObjectFieldId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@ObjectFieldId", DataTypes.integer, 4, ObjectFieldId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("acs.p_AcsObjectFieldGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsObjectFieldDBFromDataReader(reader);

            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsObjectFieldDB> GetAllAcsObjectFieldDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsObjectFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("acs.p_AcsObjectFieldGetAll", new IDataParameter[] { }));
        }

        public static List<AcsObjectFieldDB> GetPageAcsObjectFieldDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsObjectField";
            parameters[5].Value = "ObjectFieldId";
            DataSet ds = _intranetDB.RunProcedureDS("acs.p_TablesGetPage", parameters);
            return GetAcsObjectFieldDBCollectionFromDataSet(ds, out count);
        }

        public static AcsObjectFieldDB GetAcsObjectFieldDBFromDataReader(IDataReader reader)
        {
            return new AcsObjectFieldDB(int.Parse(reader["ObjectFieldId"].ToString()),
                                    int.Parse(reader["ObjectId"].ToString()),
                                    reader["FieldTitle"].ToString(),
                                    bool.Parse(reader["IsKey"].ToString()),
                                    bool.Parse(reader["IsOwner"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public static List<AcsObjectFieldDB> GetAcsObjectFieldDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsObjectFieldDB> lst = new List<AcsObjectFieldDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsObjectFieldDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsObjectFieldDB> GetAcsObjectFieldDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsObjectFieldDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsObjectFieldDB> GetAcsObjectFieldDBCollectionByAcsObjectDB(int ObjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectId", SqlDbType.Int);
            parameter.Value = ObjectId;
            return GetAcsObjectFieldDBCollectionFromDataReader(_intranetDB.RunProcedureReader("acs.p_AcsObjectFieldGetByAcsObject", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}