﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsSystem"

    /// <summary>
    /// 
    /// </summary>
    public class AcsSystem : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int SystemId { get; set; }
        public string SystemTitle { get; set; }

        public string SustemEnglishTitle { get; set; }
        public string SystemSchema { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsSystem()
        {
        }

        public AcsSystem(int _SystemId, string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate)
        {
            SystemId = _SystemId;
            this.SystemTitle = SystemTitle;
            this.SustemEnglishTitle = SustemEnglishTitle;
            this.SystemSchema = SystemSchema;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :      

        #region Static Methods :

        public static void Add(string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsSystemDB.AddAcsSystemDB(SystemTitle, SustemEnglishTitle, SystemSchema, IsActive, CreationDate, ModificationDate, out _AddedId);            
        }

        public static void Update(int _SystemId, string SystemTitle, string SustemEnglishTitle, string SystemSchema, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsSystemDB.UpdateAcsSystemDB(_SystemId, SystemTitle, SustemEnglishTitle, SystemSchema, IsActive, CreationDate, ModificationDate);
          
        }

        public static void Delete(int SystemId)
        {
            AcsSystemDB.DeleteAcsSystemDB(SystemId);
           
        }

        public static AcsSystem GetSingleById(int _SystemId)
        {
            AcsSystem o = GetAcsSystemFromAcsSystemDB(
            AcsSystemDB.GetSingleAcsSystemDB(_SystemId));

            return o;
        }

        public static List<AcsSystem> GetAll()
        {
            List<AcsSystem> lst = new List<AcsSystem>();
            //string key = "AcsSystem_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsSystem>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsSystemCollectionFromAcsSystemDBList(
            AcsSystemDB.GetAllAcsSystemDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsSystem> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsSystem_List_Page_" + currentPage ;
            //string countKey = "AcsSystem_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsSystem> lst = new List<AcsSystem>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsSystem>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsSystemCollectionFromAcsSystemDBList(
            AcsSystemDB.GetPageAcsSystemDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsSystem GetAcsSystemFromAcsSystemDB(AcsSystemDB o)
        {
            if (o == null)
                return null;
            AcsSystem ret = new AcsSystem(o.SystemId, o.SystemTitle, o.SustemEnglishTitle, o.SystemSchema, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsSystem> GetAcsSystemCollectionFromAcsSystemDBList(List<AcsSystemDB> lst)
        {
            List<AcsSystem> RetLst = new List<AcsSystem>();
            foreach (AcsSystemDB o in lst)
            {
                RetLst.Add(GetAcsSystemFromAcsSystemDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}