﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{


    //-------------------------------------------------------
    #region "Class AcsObjectField"

    /// <summary>
    /// 
    /// </summary>
    public class AcsObjectField : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int ObjectFieldId { get; set; }
        public int ObjectId { get; set; }

        public string FieldTitle { get; set; }

        public bool IsKey { get; set; }

        public bool IsOwner { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsObjectField()
        {
        }

        public AcsObjectField(int _ObjectFieldId, int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive)
        {
            ObjectFieldId = _ObjectFieldId;
            this.ObjectId = ObjectId;
            this.FieldTitle = FieldTitle;
            this.IsKey = IsKey;
            this.IsOwner = IsOwner;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

        #region Mehtods :      

        #region Static Methods :

        public static void Add(int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive, out int _AddedId)
        {
           AcsObjectFieldDB.AddAcsObjectFieldDB(ObjectId, FieldTitle, IsKey, IsOwner, CreationDate, ModificationDate, IsActive,out _AddedId);
          
        }

        public static void Update(int _ObjectFieldId, int ObjectId, string FieldTitle, bool IsKey, bool IsOwner, string CreationDate, string ModificationDate, bool IsActive)
        {
            AcsObjectFieldDB.UpdateAcsObjectFieldDB(_ObjectFieldId, ObjectId, FieldTitle, IsKey, IsOwner, CreationDate, ModificationDate, IsActive);
            
        }

        public static void Delete(int ObjectFieldId)
        {
            AcsObjectFieldDB.DeleteAcsObjectFieldDB(ObjectFieldId);
          
        }

        public static AcsObjectField GetSingleById(int _ObjectFieldId)
        {
            AcsObjectField o = GetAcsObjectFieldFromAcsObjectFieldDB(
            AcsObjectFieldDB.GetSingleAcsObjectFieldDB(_ObjectFieldId));

            return o;
        }

        public static List<AcsObjectField> GetAll()
        {
            List<AcsObjectField> lst = new List<AcsObjectField>();
            //string key = "AcsObjectField_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObjectField>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsObjectFieldCollectionFromAcsObjectFieldDBList(
            AcsObjectFieldDB.GetAllAcsObjectFieldDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsObjectField> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsObjectField_List_Page_" + currentPage ;
            //string countKey = "AcsObjectField_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsObjectField> lst = new List<AcsObjectField>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsObjectField>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsObjectFieldCollectionFromAcsObjectFieldDBList(
            AcsObjectFieldDB.GetPageAcsObjectFieldDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsObjectField GetAcsObjectFieldFromAcsObjectFieldDB(AcsObjectFieldDB o)
        {
            if (o == null)
                return null;
            AcsObjectField ret = new AcsObjectField(o.ObjectFieldId, o.ObjectId, o.FieldTitle, o.IsKey, o.IsOwner, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private static List<AcsObjectField> GetAcsObjectFieldCollectionFromAcsObjectFieldDBList(List<AcsObjectFieldDB> lst)
        {
            List<AcsObjectField> RetLst = new List<AcsObjectField>();
            foreach (AcsObjectFieldDB o in lst)
            {
                RetLst.Add(GetAcsObjectFieldFromAcsObjectFieldDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsObjectField> GetAcsObjectFieldCollectionByAcsObject(int ObjectId)
        {
            return GetAcsObjectFieldCollectionFromAcsObjectFieldDBList(AcsObjectFieldDB.GetAcsObjectFieldDBCollectionByAcsObjectDB(ObjectId));
        }


        #endregion



        #endregion


    }
    #endregion
}