﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-----------------------------------------------------

    #region Class "AcsUserAndGroup_RoleAndCategorize_RowAccessDB"

    public class AcsUserAndGroup_RoleAndCategorize_RowAccessDB
    {

        #region Properties :

        public int UserAndGroup_Role_AccessId { get; set; }
        public int? UserAndGroupId { get; set; }

        public int? RoleAndCategorizeId { get; set; }

        public int AccessTypeId { get; set; }

        public int RowAccessId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        //-------------- start own function ---------------
        public static DataTable GetRowAccessObjectsPerRole(int roleId, int objectid)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                };
            parameters[0].Value = roleId;
            parameters[1].Value = objectid;
            return _intranetDB.RunProcedureDS("Acs.p_GetRowAccessObjectsPerRole", parameters).Tables[0];
        }

        public static DataTable GetRowAccessObjectsPerUser(int userId, int objectid)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                };
            parameters[0].Value = userId;
            parameters[1].Value = objectid;
            return _intranetDB.RunProcedureDS("Acs.p_GetRowAccessObjectsPerUser", parameters).Tables[0];
        }

        public static DataTable GetRowAccessObjectsPerRolePerObject(int roleId, int objectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RoleId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                };
            parameters[0].Value = roleId;
            parameters[1].Value = objectId;
            return _intranetDB.RunProcedureDS("Acs.p_GetRowAccessPerRolePerObject", parameters).Tables[0];
        }

        public static DataTable GetRowAccessObjectsPerUserPerObject(int userId, int objectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserId", SqlDbType.Int, 4),
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                };
            parameters[0].Value = userId;
            parameters[1].Value = objectId;
            return _intranetDB.RunProcedureDS("Acs.p_GetRowAccessPerUserPerObject", parameters).Tables[0];
        }

        public static int GetObjectPerRowAccess(int rowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@RowAccessId", SqlDbType.Int, 4),
                };
            parameters[0].Value = rowAccessId;

            var datatable = _intranetDB.RunProcedureDS("Acs.p_AcsGetObjectPerRowAccess", parameters).Tables[0];
            if (datatable.Rows.Count > 0) return Int32.Parse(datatable.Rows[0]["ObjectId"].ToString());
            return -1;
        }

        public static void DeleteAcsUserAndGroup_RowAccessPerObjectPerRole(int objectId, int roleId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                    new SqlParameter("@RoleId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = roleId;
            parameters[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsRowAccessDelPerObjectPerRole", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }          
          
            string messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsUserAndGroup_RowAccessPerObjectPerUser(int objectId, int userId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@ObjectId", SqlDbType.Int, 4),
                    new SqlParameter("@UserId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = objectId;
            parameters[1].Value = userId;
            parameters[2].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsRowAccessDelPerObjectPerUser", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }          
            
            string messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void AddAcsUserAndGroup_RoleAndCategorize_RowAccessGroupDB(int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, string RowAccessId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@AccessTypeId", SqlDbType.Int),
                    new SqlParameter("@RowAccessId", SqlDbType.NVarChar, -1),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@UserAndGroup_Role_AccessId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = (UserAndGroupId == -1) ? DBNull.Value : (object) UserAndGroupId;
            parameters[1].Value = (RoleAndCategorizeId == -1) ? DBNull.Value : (object) RoleAndCategorizeId;
            parameters[2].Value = AccessTypeId;
            parameters[3].Value = RowAccessId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            parameters[8].Direction = ParameterDirection.Output;

            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGroupAdd", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }              
           
            if (parameters[7].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[7].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        //-------------- end own function -----------------

        public AcsUserAndGroup_RoleAndCategorize_RowAccessDB()
        {
        }

        public AcsUserAndGroup_RoleAndCategorize_RowAccessDB(int _UserAndGroup_Role_AccessId, int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate)
        {
            UserAndGroup_Role_AccessId = _UserAndGroup_Role_AccessId;
            this.UserAndGroupId = UserAndGroupId;
            this.RoleAndCategorizeId = RoleAndCategorizeId;
            this.AccessTypeId = AccessTypeId;
            this.RowAccessId = RowAccessId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Methods :

        public static void AddAcsUserAndGroup_RoleAndCategorize_RowAccessDB(int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@AccessTypeId", SqlDbType.Int),
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@UserAndGroup_Role_AccessId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = (UserAndGroupId == -1) ? DBNull.Value : (object) UserAndGroupId;
            parameters[1].Value = (RoleAndCategorizeId == -1) ? DBNull.Value : (object) RoleAndCategorizeId;
            parameters[2].Value = AccessTypeId;
            parameters[3].Value = RowAccessId;
            parameters[4].Value = IsActive;
            parameters[5].Value = CreationDate;
            parameters[6].Value = ModificationDate;
            parameters[7].Direction = ParameterDirection.Output;
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessAdd", parameters);
            if (parameters[7].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[7].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
            {
                throw new Exception(MessageError.GetMessage(messageError));
            }
        }

        public static void UpdateAcsUserAndGroup_RoleAndCategorize_RowAccessDB(int UserAndGroup_Role_AccessId, int? UserAndGroupId, int? RoleAndCategorizeId, int AccessTypeId, int RowAccessId, bool IsActive, string CreationDate, string ModificationDate)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroup_Role_AccessId", SqlDbType.Int, 4),
                    new SqlParameter("@UserAndGroupId", SqlDbType.Int),
                    new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int),
                    new SqlParameter("@AccessTypeId", SqlDbType.Int),
                    new SqlParameter("@RowAccessId", SqlDbType.Int),
                    new SqlParameter("@IsActive", SqlDbType.Bit),
                    new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };

            parameters[0].Value = UserAndGroup_Role_AccessId;

            parameters[1].Value = (UserAndGroupId == -1) ? DBNull.Value : (object) UserAndGroupId;
            parameters[2].Value = (RoleAndCategorizeId == -1) ? DBNull.Value : (object) RoleAndCategorizeId;
            parameters[3].Value = AccessTypeId;
            parameters[4].Value = RowAccessId;
            parameters[5].Value = IsActive;
            parameters[6].Value = CreationDate;
            parameters[7].Value = ModificationDate;
            parameters[8].Direction = ParameterDirection.Output;


            SqlConnection sqlConnection = Intranet.Configuration.Settings.PortalSettings.SqlconstrSelf;
            using (var sqlCommand = new SqlCommand("acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessUpdate", sqlConnection))
            {
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddRange(parameters);
                sqlCommand.CommandTimeout = 0;
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }              
       
            string messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static void DeleteAcsUserAndGroup_RoleAndCategorize_RowAccessDB(int UserAndGroup_Role_AccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserAndGroup_Role_AccessId", SqlDbType.Int, 4),
                    new SqlParameter("@MessageError", SqlDbType.NVarChar, -1)
                };
            parameters[0].Value = UserAndGroup_Role_AccessId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessDel", parameters);
            string messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(Classes.MessageError.GetMessage(messageError)));
        }

        public static AcsUserAndGroup_RoleAndCategorize_RowAccessDB GetSingleAcsUserAndGroup_RoleAndCategorize_RowAccessDB(int UserAndGroup_Role_AccessId)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                    {
                        _intranetDB.GetParameter("@UserAndGroup_Role_AccessId", DataTypes.integer, 4, UserAndGroup_Role_AccessId)
                    };

                reader = _intranetDB.RunProcedureReader
                    ("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBFromDataReader(reader);


            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return null;
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAllAcsUserAndGroup_RoleAndCategorize_RowAccessDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetAll", new IDataParameter[] {}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetPageAcsUserAndGroup_RoleAndCategorize_RowAccessDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
                {
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@CurrentPage", SqlDbType.Int),
                    new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                    new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                    new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                    new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AcsUserAndGroup_RoleAndCategorize_RowAccess";
            parameters[5].Value = "UserAndGroup_Role_AccessId";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataSet(ds, out count);
        }

        public static AcsUserAndGroup_RoleAndCategorize_RowAccessDB GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBFromDataReader(IDataReader reader)
        {
            return new AcsUserAndGroup_RoleAndCategorize_RowAccessDB(int.Parse(reader["UserAndGroup_Role_AccessId"].ToString()),
                                                                     Convert.IsDBNull(reader["UserAndGroupId"]) ? null : (int?) reader["UserAndGroupId"],
                                                                     Convert.IsDBNull(reader["RoleAndCategorizeId"]) ? null : (int?) reader["RoleAndCategorizeId"],
                                                                     int.Parse(reader["AccessTypeId"].ToString()),
                                                                     int.Parse(reader["RowAccessId"].ToString()),
                                                                     bool.Parse(reader["IsActive"].ToString()),
                                                                     reader["CreationDate"].ToString(),
                                                                     reader["ModificationDate"].ToString());
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(IDataReader reader)
        {
            List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> lst = new List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB>();
            try
            {
                while (reader.Read())
                    lst.Add(GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBFromDataReader(reader));
            }
            finally
            {
                if (!reader.IsClosed)
                {
                    reader.Close();
                }
            }

            return lst;
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsAccessTypeDB(int AccessTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@AccessTypeId", SqlDbType.Int);
            parameter.Value = AccessTypeId;
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetByAcsAccessType", new[] {parameter}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsRoleAndCategorizeDB(int? RoleAndCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RoleAndCategorizeId", SqlDbType.Int);
            parameter.Value = RoleAndCategorizeId;
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetByAcsRoleAndCategorize", new[] {parameter}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsRowAccessDB(int RowAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RowAccessId", SqlDbType.Int);
            parameter.Value = RowAccessId;
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetByAcsRowAccess", new[] {parameter}));
        }

        public static List<AcsUserAndGroup_RoleAndCategorize_RowAccessDB> GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionByAcsUserAndGroupDB(int? UserAndGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@UserAndGroupId", SqlDbType.Int);
            parameter.Value = UserAndGroupId;
            return GetAcsUserAndGroup_RoleAndCategorize_RowAccessDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Acs.p_AcsUserAndGroup_RoleAndCategorize_RowAccessGetByAcsUserAndGroup", new[] {parameter}));
        }


        #endregion



    }

    #endregion
}