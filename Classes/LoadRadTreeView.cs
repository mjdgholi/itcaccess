﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.Classes
{
    public class LoadRadTreeView
    {
        public static void LoadRootNodes(Telerik.Web.UI.RadTreeView treeView, TreeNodeExpandMode expandMode)
        {
            DataSet ds = Classes.AcsUserAndGroupTreeDB.AcsUserAndGroupTreeGetGetChildNode(-1,-1);
            DataTable data = ds.Tables[0];

            foreach (DataRow row in data.Rows)
            {
                var node = new RadTreeNode
                               {
                                   Text = row["UserAndGroupName"].ToString(),
                                   Value = row["UserAndGroupTreeId"].ToString(),
                                   ExpandMode = expandMode,
                                   ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png",

                               };
                node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;
                treeView.Nodes.Add(node);
            }
        }

        public static void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode,int isUser,ArrayList selectedNode)
        {
            var userAndGroupTree = Classes.AcsUserAndGroupTree.GetSingleById(Int32.Parse(e.Node.Value));
            if (userAndGroupTree != null)
            {
                var userAndGroupId = userAndGroupTree.UserAndGroupId;
                DataSet ds = Classes.AcsUserAndGroupTreeDB.AcsUserAndGroupTreeGetGetChildNode(userAndGroupId,isUser);
                DataTable data = ds.Tables[0];

                foreach (DataRow row in data.Rows)
                {
                    var node = new RadTreeNode
                                   {
                                       Text = row["UserAndGroupName"].ToString(),
                                       Value = row["UserAndGroupTreeId"].ToString(),
                                       ExpandMode = expandMode,
                                       ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png"
                                   };
                    node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;

                   
                    e.Node.Nodes.Add(node);
                    if (selectedNode.Contains(node.Value))
                    {
                        node.Selected = true;
                    }
                    else
                    {
                        node.Selected = false;
                    }
                }
                e.Node.Expanded = true;
                e.Node.ExpandMode = TreeNodeExpandMode.ClientSide;
            }
        }

        public static List<RadTreeNode> FindNodeExpandByValue(RadTreeNodeCollection nodes, string nodeValue, Action<RadTreeNode> action)
        {
            List<RadTreeNode> foundNodes = null;
           
            foreach (RadTreeNode node in nodes)
            {
                if (node.Value == nodeValue)
                {
                    action(node);
                    return new List<RadTreeNode>() { node };
                }
                else
                {
                    foundNodes = FindNodeExpandByValue(node.Nodes, nodeValue, action);
                }
                if (foundNodes != null)
                {

                    return foundNodes;
                }
            }
            return null;
        }
    }
}