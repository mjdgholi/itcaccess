﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsCondition_Datatype_DveloperControl"

    /// <summary>
    /// 
    /// </summary>
    public class AcsCondition_Datatype_DveloperControl : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int Condition_Datatype_DveloperControlId { get; set; }
        public int ConditionId { get; set; }

        public int DataTypeId { get; set; }

        public bool IsHierarchy { get; set; }

        public int DveloperControlId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsCondition_Datatype_DveloperControl()
        {
        }

        public AcsCondition_Datatype_DveloperControl(int _Condition_Datatype_DveloperControlId, int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            Condition_Datatype_DveloperControlId = _Condition_Datatype_DveloperControlId;
            this.ConditionId = ConditionId;
            this.DataTypeId = DataTypeId;
            this.IsHierarchy = IsHierarchy;
            this.DveloperControlId = DveloperControlId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        //-------------- start own function ---------------
        public static string GetDeveloperControlPerConditionAndField(int conditionId, int fieldId)
        {
            return AcsCondition_Datatype_DveloperControlDB.GetDeveloperControlPerConditionAndField(conditionId, fieldId);
        }
        //-------------- end own function -----------------

        #region Mehtods :

        #region Static Methods :

        public static void Add(int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate, out int _AddedId)
        {
            AcsCondition_Datatype_DveloperControlDB.AddAcsCondition_Datatype_DveloperControlDB(ConditionId, DataTypeId, IsHierarchy, DveloperControlId, IsActive, CreationDate, ModificationDate, out _AddedId);
        }

        public static void Update(int _Condition_Datatype_DveloperControlId, int ConditionId, int DataTypeId, bool IsHierarchy, int DveloperControlId, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsCondition_Datatype_DveloperControlDB.UpdateAcsCondition_Datatype_DveloperControlDB(_Condition_Datatype_DveloperControlId, ConditionId, DataTypeId, IsHierarchy, DveloperControlId, IsActive, CreationDate, ModificationDate);
        }

        public static void Delete(int Condition_Datatype_DveloperControlId)
        {
            AcsCondition_Datatype_DveloperControlDB.DeleteAcsCondition_Datatype_DveloperControlDB(Condition_Datatype_DveloperControlId);

        }

        public static AcsCondition_Datatype_DveloperControl GetSingleById(int _Condition_Datatype_DveloperControlId)
        {
            AcsCondition_Datatype_DveloperControl o = GetAcsCondition_Datatype_DveloperControlFromAcsCondition_Datatype_DveloperControlDB(
                AcsCondition_Datatype_DveloperControlDB.GetSingleAcsCondition_Datatype_DveloperControlDB(_Condition_Datatype_DveloperControlId));

            return o;
        }

        public static List<AcsCondition_Datatype_DveloperControl> GetAll()
        {
            List<AcsCondition_Datatype_DveloperControl> lst = new List<AcsCondition_Datatype_DveloperControl>();
            //string key = "AcsCondition_Datatype_DveloperControl_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition_Datatype_DveloperControl>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(
                AcsCondition_Datatype_DveloperControlDB.GetAllAcsCondition_Datatype_DveloperControlDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsCondition_Datatype_DveloperControl> GetAllPaging(int currentPage, int pageSize,
                                                                               string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsCondition_Datatype_DveloperControl_List_Page_" + currentPage ;
            //string countKey = "AcsCondition_Datatype_DveloperControl_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsCondition_Datatype_DveloperControl> lst = new List<AcsCondition_Datatype_DveloperControl>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition_Datatype_DveloperControl>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(
                AcsCondition_Datatype_DveloperControlDB.GetPageAcsCondition_Datatype_DveloperControlDB(pageSize, currentPage,
                                                                                                       whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsCondition_Datatype_DveloperControl GetAcsCondition_Datatype_DveloperControlFromAcsCondition_Datatype_DveloperControlDB(AcsCondition_Datatype_DveloperControlDB o)
        {
            if (o == null)
                return null;
            AcsCondition_Datatype_DveloperControl ret = new AcsCondition_Datatype_DveloperControl(o.Condition_Datatype_DveloperControlId, o.ConditionId, o.DataTypeId, o.IsHierarchy, o.DveloperControlId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsCondition_Datatype_DveloperControl> GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(List<AcsCondition_Datatype_DveloperControlDB> lst)
        {
            List<AcsCondition_Datatype_DveloperControl> RetLst = new List<AcsCondition_Datatype_DveloperControl>();
            foreach (AcsCondition_Datatype_DveloperControlDB o in lst)
            {
                RetLst.Add(GetAcsCondition_Datatype_DveloperControlFromAcsCondition_Datatype_DveloperControlDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsCondition_Datatype_DveloperControl> GetAcsCondition_Datatype_DveloperControlCollectionByAcsCondition(int ConditionId)
        {
            return GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(AcsCondition_Datatype_DveloperControlDB.GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsConditionDB(ConditionId));
        }

        public static List<AcsCondition_Datatype_DveloperControl> GetAcsCondition_Datatype_DveloperControlCollectionByAcsDataType(int DataTypeId)
        {
            return GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(AcsCondition_Datatype_DveloperControlDB.GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsDataTypeDB(DataTypeId));
        }

        public static List<AcsCondition_Datatype_DveloperControl> GetAcsCondition_Datatype_DveloperControlCollectionByAcsDveloperControl(int DveloperControlId)
        {
            return GetAcsCondition_Datatype_DveloperControlCollectionFromAcsCondition_Datatype_DveloperControlDBList(AcsCondition_Datatype_DveloperControlDB.GetAcsCondition_Datatype_DveloperControlDBCollectionByAcsDveloperControlDB(DveloperControlId));
        }


        #endregion



        #endregion


    }

    #endregion
}