﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsUserAndGroup"

    /// <summary>
    /// 
    /// </summary>
    public class AcsUserAndGroup : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int UserAndGroupId { get; set; }
        public string UserAndGroupName { get; set; }

        public Guid UserAndGroupDomainId { get; set; }

        public bool IsUser { get; set; }

        public string UserPass { get; set; }

        public bool IsBuiltin { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int WebUserId { get; set; }

        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsUserAndGroup()
        {
        }

        public AcsUserAndGroup(int _UserAndGroupId, string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId)
        {
            UserAndGroupId = _UserAndGroupId;
            this.UserAndGroupName = UserAndGroupName;
            this.UserAndGroupDomainId = UserAndGroupDomainId;
            this.IsUser = IsUser;
            this.UserPass = UserPass;
            this.IsBuiltin = IsBuiltin;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.WebUserId = WebUserId;
        }

        #endregion

        #region Mehtods :

        #region Static Methods :

        //------------ start own function --------------
        public static void AddAcsUserAndGroupWithOwnerDB(string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, int userAndGroupOwnerId, string CreationDate, string ModificationDate, int WebUserId, out int _AddedId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserAndGroupName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@UserAndGroupDomainId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsUser", SqlDbType.Bit),
                                            new SqlParameter("@UserPass", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@IsBuiltin", SqlDbType.Bit),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@UserAndGroupOwnerId", SqlDbType.Int),
                                            new SqlParameter("@CreationDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@WebUserId",SqlDbType.Int), 
                                            new SqlParameter("@UserAndGroupId", SqlDbType.Int, 4),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, -1),
                                        };
            parameters[0].Value = UserAndGroupName;
            parameters[1].Value = UserAndGroupDomainId;
            parameters[2].Value = IsUser;
            parameters[3].Value = UserPass;
            parameters[4].Value = IsBuiltin;
            parameters[5].Value = IsActive;
            parameters[6].Value = (userAndGroupOwnerId != -1) ? (object)userAndGroupOwnerId : DBNull.Value;
            parameters[7].Value = WebUserId;
            parameters[8].Value = CreationDate;
            parameters[9].Value = ModificationDate;

            parameters[10].Direction = ParameterDirection.Output;
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[acs].p_AcsUserAndGroupAddWithOwner", parameters);
            if (parameters[10].Value.ToString() != "")
            {
                _AddedId = Int32.Parse(parameters[10].Value.ToString());
            }
            else
            {
                _AddedId = -1;
            }
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(MessageError.GetMessage(messageError)));
        }

        //------------ end own function ----------------

        public static void Add(string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId, out int _AddedId)
        {
            AcsUserAndGroupDB.AddAcsUserAndGroupDB(UserAndGroupName, UserAndGroupDomainId, IsUser, UserPass, IsBuiltin, IsActive, CreationDate, ModificationDate, WebUserId,out _AddedId);            
        }

        public static void Update(int _UserAndGroupId, string UserAndGroupName, Guid UserAndGroupDomainId, bool IsUser, string UserPass, bool IsBuiltin, bool IsActive, string CreationDate, string ModificationDate, int WebUserId)
        {
            AcsUserAndGroupDB.UpdateAcsUserAndGroupDB(_UserAndGroupId, UserAndGroupName, UserAndGroupDomainId, IsUser, UserPass, IsBuiltin, IsActive, CreationDate, ModificationDate, WebUserId);           
        }

        public static void Delete(int UserAndGroupId)
        {
            AcsUserAndGroupDB.DeleteAcsUserAndGroupDB(UserAndGroupId);           
        }

        public static AcsUserAndGroup GetSingleById(int _UserAndGroupId)
        {
            AcsUserAndGroup o = GetAcsUserAndGroupFromAcsUserAndGroupDB(
            AcsUserAndGroupDB.GetSingleAcsUserAndGroupDB(_UserAndGroupId));

            return o;
        }

        public static List<AcsUserAndGroup> GetAll()
        {
            List<AcsUserAndGroup> lst = new List<AcsUserAndGroup>();
            //string key = "AcsUserAndGroup_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsUserAndGroupCollectionFromAcsUserAndGroupDBList(
            AcsUserAndGroupDB.GetAllAcsUserAndGroupDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsUserAndGroup> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsUserAndGroup_List_Page_" + currentPage ;
            //string countKey = "AcsUserAndGroup_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsUserAndGroup> lst = new List<AcsUserAndGroup>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsUserAndGroup>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsUserAndGroupCollectionFromAcsUserAndGroupDBList(
            AcsUserAndGroupDB.GetPageAcsUserAndGroupDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsUserAndGroup GetAcsUserAndGroupFromAcsUserAndGroupDB(AcsUserAndGroupDB o)
        {
            if (o == null)
                return null;
            AcsUserAndGroup ret = new AcsUserAndGroup(o.UserAndGroupId, o.UserAndGroupName, o.UserAndGroupDomainId, o.IsUser, o.UserPass, o.IsBuiltin, o.IsActive, o.CreationDate, o.ModificationDate, o.WebUserId);
            return ret;
        }

        private static List<AcsUserAndGroup> GetAcsUserAndGroupCollectionFromAcsUserAndGroupDBList(List<AcsUserAndGroupDB> lst)
        {
            List<AcsUserAndGroup> RetLst = new List<AcsUserAndGroup>();
            foreach (AcsUserAndGroupDB o in lst)
            {
                RetLst.Add(GetAcsUserAndGroupFromAcsUserAndGroupDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }
    #endregion
}