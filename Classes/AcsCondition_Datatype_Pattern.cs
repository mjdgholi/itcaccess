﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsCondition_Datatype_Pattern"

    /// <summary>
    /// 
    /// </summary>
    public class AcsCondition_Datatype_Pattern : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int Condition_Datatype_PatternId { get; set; }
        public int ConditionId { get; set; }

        public int DataTypeId { get; set; }

        public int PatternId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsCondition_Datatype_Pattern()
        {
        }

        public AcsCondition_Datatype_Pattern(int _Condition_Datatype_PatternId, int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate)
        {
            Condition_Datatype_PatternId = _Condition_Datatype_PatternId;
            this.ConditionId = ConditionId;
            this.DataTypeId = DataTypeId;
            this.PatternId = PatternId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :
        
        #region Static Methods :

        public static void Add(int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
            AcsCondition_Datatype_PatternDB.AddAcsCondition_Datatype_PatternDB(ConditionId, DataTypeId, PatternId, IsActive, CreationDate, ModificationDate,out _AddedId);
        
        }

        public static void Update(int _Condition_Datatype_PatternId, int ConditionId, int DataTypeId, int PatternId, bool IsActive, string CreationDate, string ModificationDate)
        {
        AcsCondition_Datatype_PatternDB.UpdateAcsCondition_Datatype_PatternDB(_Condition_Datatype_PatternId, ConditionId, DataTypeId, PatternId, IsActive, CreationDate, ModificationDate);
           
        }

        public static void Delete(int Condition_Datatype_PatternId)
        {
             AcsCondition_Datatype_PatternDB.DeleteAcsCondition_Datatype_PatternDB(Condition_Datatype_PatternId);
           
        }

        public static AcsCondition_Datatype_Pattern GetSingleById(int _Condition_Datatype_PatternId)
        {
            AcsCondition_Datatype_Pattern o = GetAcsCondition_Datatype_PatternFromAcsCondition_Datatype_PatternDB(
            AcsCondition_Datatype_PatternDB.GetSingleAcsCondition_Datatype_PatternDB(_Condition_Datatype_PatternId));

            return o;
        }

        public static List<AcsCondition_Datatype_Pattern> GetAll()
        {
            List<AcsCondition_Datatype_Pattern> lst = new List<AcsCondition_Datatype_Pattern>();
            //string key = "AcsCondition_Datatype_Pattern_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition_Datatype_Pattern>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(
            AcsCondition_Datatype_PatternDB.GetAllAcsCondition_Datatype_PatternDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsCondition_Datatype_Pattern> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsCondition_Datatype_Pattern_List_Page_" + currentPage ;
            //string countKey = "AcsCondition_Datatype_Pattern_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsCondition_Datatype_Pattern> lst = new List<AcsCondition_Datatype_Pattern>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsCondition_Datatype_Pattern>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(
            AcsCondition_Datatype_PatternDB.GetPageAcsCondition_Datatype_PatternDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsCondition_Datatype_Pattern GetAcsCondition_Datatype_PatternFromAcsCondition_Datatype_PatternDB(AcsCondition_Datatype_PatternDB o)
        {
            if (o == null)
                return null;
            AcsCondition_Datatype_Pattern ret = new AcsCondition_Datatype_Pattern(o.Condition_Datatype_PatternId, o.ConditionId, o.DataTypeId, o.PatternId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsCondition_Datatype_Pattern> GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(List<AcsCondition_Datatype_PatternDB> lst)
        {
            List<AcsCondition_Datatype_Pattern> RetLst = new List<AcsCondition_Datatype_Pattern>();
            foreach (AcsCondition_Datatype_PatternDB o in lst)
            {
                RetLst.Add(GetAcsCondition_Datatype_PatternFromAcsCondition_Datatype_PatternDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsCondition_Datatype_Pattern> GetAcsCondition_Datatype_PatternCollectionByAcsCondition(int ConditionId)
        {
            return GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(AcsCondition_Datatype_PatternDB.GetAcsCondition_Datatype_PatternDBCollectionByAcsConditionDB(ConditionId));
        }

        public static List<AcsCondition_Datatype_Pattern> GetAcsCondition_Datatype_PatternCollectionByAcsDataType(int DataTypeId)
        {
            return GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(AcsCondition_Datatype_PatternDB.GetAcsCondition_Datatype_PatternDBCollectionByAcsDataTypeDB(DataTypeId));
        }

        public static List<AcsCondition_Datatype_Pattern> GetAcsCondition_Datatype_PatternCollectionByAcsPattern(int PatternId)
        {
            return GetAcsCondition_Datatype_PatternCollectionFromAcsCondition_Datatype_PatternDBList(AcsCondition_Datatype_PatternDB.GetAcsCondition_Datatype_PatternDBCollectionByAcsPatternDB(PatternId));
        }


        #endregion



        #endregion


    }
    #endregion
}