﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------

    #region "Class AcsRowAccess"

    /// <summary>
    /// 
    /// </summary>
    public class AcsRowAccess : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int RowAccessId { get; set; }

        public int ObjectId { get; set; }
        public string AccessTitle { get; set; }

        public string ConditionalExpression { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsRowAccess()
        {
        }

        public AcsRowAccess(int _RowAccessId,int objectId, string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate)
        {
            RowAccessId = _RowAccessId;
            this.ObjectId = objectId;
            this.AccessTitle = AccessTitle;
            this.ConditionalExpression = ConditionalExpression;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :


        #region Static Methods :

        //------------------- start own function ----------------
        public static void UpdateRowAccessConditionalExpression(int rowAccessId, string ConditionalExpression)
        {
            AcsRowAccessDB.UpdateRowAccessConditionalExpression(rowAccessId, ConditionalExpression);
        }

        public static DataTable GetRowAccessPerObject(int objectId)
        {
            return AcsRowAccessDB.GetRowAccessPerObject(objectId);
        }

        public static DataTable GetRowAccessPerObjectRoleHasNotIt(int objectId, int roleId)
        {
            return AcsRowAccessDB.GetRowAccessPerObjectRoleHasNotIt(objectId, roleId);
        }

        public static DataTable GetRowAccessPerObjectUserHasNotIt(int objectId, int userId)
        {
            return AcsRowAccessDB.GetRowAccessPerObjectUserHasNotIt(objectId, userId);
        }

        public static int GetObjectPerRowAccess(int rowAccessId)
        {
            return AcsRowAccessDB.GetObjectPerRowAccess(rowAccessId);
        }

        //------------------- end own function ------------------

        public static void Add(int objectId,string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate, out int _addedId)
        {
            AcsRowAccessDB.AddAcsRowAccessDB(objectId,AccessTitle, ConditionalExpression, IsActive, CreationDate, ModificationDate, out _addedId);
        }

        public static void Update(int _RowAccessId,int objectId, string AccessTitle, string ConditionalExpression, bool IsActive, string CreationDate, string ModificationDate)
        {
            AcsRowAccessDB.UpdateAcsRowAccessDB(_RowAccessId,objectId, AccessTitle, ConditionalExpression, IsActive, CreationDate, ModificationDate);

        }

        public static void Delete(int RowAccessId)
        {
            AcsRowAccessDB.DeleteAcsRowAccessDB(RowAccessId);
        }

        public static AcsRowAccess GetSingleById(int _RowAccessId)
        {
            AcsRowAccess o = GetAcsRowAccessFromAcsRowAccessDB(
                AcsRowAccessDB.GetSingleAcsRowAccessDB(_RowAccessId));

            return o;
        }

        public static List<AcsRowAccess> GetAll()
        {
            List<AcsRowAccess> lst = new List<AcsRowAccess>();
            //string key = "AcsRowAccess_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRowAccess>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsRowAccessCollectionFromAcsRowAccessDBList(
                AcsRowAccessDB.GetAllAcsRowAccessDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsRowAccess> GetAllPaging(int currentPage, int pageSize,
                                                      string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsRowAccess_List_Page_" + currentPage ;
            //string countKey = "AcsRowAccess_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsRowAccess> lst = new List<AcsRowAccess>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRowAccess>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsRowAccessCollectionFromAcsRowAccessDBList(
                AcsRowAccessDB.GetPageAcsRowAccessDB(pageSize, currentPage,
                                                     whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsRowAccess GetAcsRowAccessFromAcsRowAccessDB(AcsRowAccessDB o)
        {
            if (o == null)
                return null;
            AcsRowAccess ret = new AcsRowAccess(o.RowAccessId,o.ObjectId, o.AccessTitle, o.ConditionalExpression, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsRowAccess> GetAcsRowAccessCollectionFromAcsRowAccessDBList(List<AcsRowAccessDB> lst)
        {
            List<AcsRowAccess> RetLst = new List<AcsRowAccess>();
            foreach (AcsRowAccessDB o in lst)
            {
                RetLst.Add(GetAcsRowAccessFromAcsRowAccessDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
                                             DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }


        #endregion



        #endregion


    }

    #endregion
}