﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.Configuration;

namespace Intranet.DesktopModules.ItcAccess.Classes
{

    //-------------------------------------------------------
    #region "Class AcsRoleAndCategorize"

    /// <summary>
    /// 
    /// </summary>
    public class AcsRoleAndCategorize : DeskTopObj
    {

        #region Properties :

        #region Base :

        public int RoleAndCategorizeId { get; set; }
        public string RoleAndCategorizeTilte { get; set; }

        public bool IsRole { get; set; }

        public int SystemId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Related :

        #endregion

        #endregion

        #region Constrauctors :

        public AcsRoleAndCategorize()
        {
        }

        public AcsRoleAndCategorize(int _RoleAndCategorizeId, string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate)
        {
            RoleAndCategorizeId = _RoleAndCategorizeId;
            this.RoleAndCategorizeTilte = RoleAndCategorizeTilte;
            this.IsRole = IsRole;
            this.SystemId = SystemId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

        #region Mehtods :
        
        #region Static Methods :

        public static void Add(string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate,out int _AddedId)
        {
            AcsRoleAndCategorizeDB.AddAcsRoleAndCategorizeDB(RoleAndCategorizeTilte, IsRole, SystemId, IsActive, CreationDate, ModificationDate,out _AddedId);
           
        }

        public static void Update(int _RoleAndCategorizeId, string RoleAndCategorizeTilte, bool IsRole, int SystemId, bool IsActive, string CreationDate, string ModificationDate)
        {
             AcsRoleAndCategorizeDB.UpdateAcsRoleAndCategorizeDB(_RoleAndCategorizeId, RoleAndCategorizeTilte, IsRole, SystemId, IsActive, CreationDate, ModificationDate);
      
        }

        public static void Delete(int RoleAndCategorizeId)
        {
            AcsRoleAndCategorizeDB.DeleteAcsRoleAndCategorizeDB(RoleAndCategorizeId);          
        }

        public static AcsRoleAndCategorize GetSingleById(int _RoleAndCategorizeId)
        {
            AcsRoleAndCategorize o = GetAcsRoleAndCategorizeFromAcsRoleAndCategorizeDB(
            AcsRoleAndCategorizeDB.GetSingleAcsRoleAndCategorizeDB(_RoleAndCategorizeId));

            return o;
        }

        public static List<AcsRoleAndCategorize> GetAll()
        {
            List<AcsRoleAndCategorize> lst = new List<AcsRoleAndCategorize>();
            //string key = "AcsRoleAndCategorize_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRoleAndCategorize>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAcsRoleAndCategorizeCollectionFromAcsRoleAndCategorizeDBList(
            AcsRoleAndCategorizeDB.GetAllAcsRoleAndCategorizeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public static List<AcsRoleAndCategorize> GetAllPaging(int currentPage, int pageSize,
                                                        string sortExpression, out int count, string whereClause)
        {
            //string key = "AcsRoleAndCategorize_List_Page_" + currentPage ;
            //string countKey = "AcsRoleAndCategorize_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AcsRoleAndCategorize> lst = new List<AcsRoleAndCategorize>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AcsRoleAndCategorize>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAcsRoleAndCategorizeCollectionFromAcsRoleAndCategorizeDBList(
            AcsRoleAndCategorizeDB.GetPageAcsRoleAndCategorizeDB(pageSize, currentPage,
             whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static AcsRoleAndCategorize GetAcsRoleAndCategorizeFromAcsRoleAndCategorizeDB(AcsRoleAndCategorizeDB o)
        {
            if (o == null)
                return null;
            AcsRoleAndCategorize ret = new AcsRoleAndCategorize(o.RoleAndCategorizeId, o.RoleAndCategorizeTilte, o.IsRole, o.SystemId, o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<AcsRoleAndCategorize> GetAcsRoleAndCategorizeCollectionFromAcsRoleAndCategorizeDBList(List<AcsRoleAndCategorizeDB> lst)
        {
            List<AcsRoleAndCategorize> RetLst = new List<AcsRoleAndCategorize>();
            foreach (AcsRoleAndCategorizeDB o in lst)
            {
                RetLst.Add(GetAcsRoleAndCategorizeFromAcsRoleAndCategorizeDB(o));
            }
            return RetLst;
        }

        private static void PurgeCacheItem(string prefix)
        {
            prefix = prefix.ToLower();
            List<string> itemsToRemove = new List<string>();

            System.Collections.IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().ToLower().StartsWith(prefix))
                    itemsToRemove.Add(enumerator.Key.ToString());
            }

            foreach (string itemToRemove in itemsToRemove)
                HttpContext.Current.Cache.Remove(itemToRemove);
        }

        private static void InsertIntoCache(object o, string key, double second)
        {
            HttpContext.Current.Cache.Insert(key, o, null,
               DateTime.Now.AddSeconds(second), TimeSpan.Zero);
        }

        public static List<AcsRoleAndCategorize> GetAcsRoleAndCategorizeCollectionByAcsSystem(int SystemId)
        {
            return GetAcsRoleAndCategorizeCollectionFromAcsRoleAndCategorizeDBList(AcsRoleAndCategorizeDB.GetAcsRoleAndCategorizeDBCollectionByAcsSystemDB(SystemId));
        }


        #endregion



        #endregion


    }
    #endregion
}