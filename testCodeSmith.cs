﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CSharpCodeGeneratorSample;

namespace BusinessObjects
{
    #region Ostan
    /// <summary>
    /// This object represents the properties and methods of a Ostan.
    /// </summary>
    public class Ostan
    {
        private int _id;
        private string _nameOstan = String.Empty;

        public Ostan()
        {
        }

        public Ostan(int id)
        {
            // NOTE: If this reference doesn't exist then add SqlService.cs from the template directory to your solution.
            SqlService sql = new SqlService();
            sql.AddParameter("@CodOstan", SqlDbType.Int, id);
            SqlDataReader reader = sql.ExecuteSqlReader("SELECT * FROM Ostan WHERE CodOstan = @CodOstan");

            if (reader.Read())
            {
                this.LoadFromReader(reader);
                reader.Close();
            }
            else
            {
                if (!reader.IsClosed) reader.Close();
                throw new ApplicationException("Ostan does not exist.");
            }
        }

        public Ostan(SqlDataReader reader)
        {
            this.LoadFromReader(reader);
        }

        protected void LoadFromReader(SqlDataReader reader)
        {
            if (reader != null && !reader.IsClosed)
            {
                _id = reader.GetInt32(0);
                if (!reader.IsDBNull(1)) _nameOstan = reader.GetString(1);
            }
        }

        public void Delete()
        {
            Ostan.Delete(_id);
        }

        public void Update()
        {
            SqlService sql = new SqlService();
            StringBuilder queryParameters = new StringBuilder();

            sql.AddParameter("@CodOstan", SqlDbType.Int, Id);
            queryParameters.Append("CodOstan = @CodOstan");

            sql.AddParameter("@NameOstan", SqlDbType.NVarChar, NameOstan);
            queryParameters.Append(", NameOstan = @NameOstan");

            string query = String.Format("Update Ostan Set {0} Where CodOstan = @CodOstan", queryParameters.ToString());
            SqlDataReader reader = sql.ExecuteSqlReader(query);
        }

        public void Create()
        {
            SqlService sql = new SqlService();
            StringBuilder queryParameters = new StringBuilder();

            sql.AddParameter("@CodOstan", SqlDbType.Int, Id);
            queryParameters.Append("@CodOstan");

            sql.AddParameter("@NameOstan", SqlDbType.NVarChar, NameOstan);
            queryParameters.Append(", @NameOstan");           
            string query = String.Format("Insert Into Ostan ({0}) Values ({1})", queryParameters.ToString().Replace("@", ""), queryParameters.ToString());
            SqlDataReader reader = sql.ExecuteSqlReader(query);
        }

        public static Ostan NewOstan(int id)
        {
            Ostan newEntity = new Ostan();
            newEntity._id = id;

            return newEntity;
        }

        #region Public Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string NameOstan
        {
            get { return _nameOstan; }
            set { _nameOstan = value; }
        }
        #endregion

        public static Ostan GetOstan(int id)
        {
            return new Ostan(id);
        }

        public static void Delete(int id)
        {
            SqlService sql = new SqlService();
            sql.AddParameter("@CodOstan", SqlDbType.Int, id);

            SqlDataReader reader = sql.ExecuteSqlReader("Delete Ostan Where CodOstan = @CodOstan");
        }
    }
    #endregion
}

