﻿using System;
using System.Drawing;
using System.Web.UI;

namespace Intranet.DesktopModules.ItcAccess
{
    public partial class MessageErrorControl : UserControl
    {
        public  void ShowErrorMessage(string messageText)
        {
            lblMessage.Visible = true;
            imgMessage.Visible = true;
            lblMessage.Text = messageText;
            imgMessage.ImageUrl = @"images\Error.png";
            lblMessage.ForeColor = Color.Red;
        }

        public void ShowSuccesMessage(string messageText)
        {
            lblMessage.Visible = true;
            imgMessage.Visible = true;
            lblMessage.Text = messageText;
            imgMessage.ImageUrl = @"images\Ok.png";
            lblMessage.ForeColor = Color.DarkGreen;
        }

        public void ShowWarningMessage(string messageText)
        {
            lblMessage.Visible = true;
            imgMessage.Visible = true;
            lblMessage.Text = messageText;
            imgMessage.ImageUrl = @"images\Warning.png";
            lblMessage.ForeColor = Color.DarkOrange;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            imgMessage.Visible = false;
        }
    }
}