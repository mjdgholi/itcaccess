﻿using System;
using System.Web.UI;

namespace Intranet.DesktopModules.ItcAccess
{
    public partial class MessageControl : UserControl
    {
        private string SaveSuccessfully = "اطلاعات با موفقیت ثبت شد.";
        private string SaveUnSuccessfully = "اطلاعات با موفقیت ثبت نشد.";
        private string DeleteSuccessfully = "اطلاعات با موفقیت حذف شد.";
        private string DeleteUnSuccessfully = "این رکورد قابل حذف نمی باشد ";
        private string DeleteUnSuccessfullyRelatedInformation = "رکورد انتخاب شده دارای اطلاعات وابسته است و قابل حذف نمی باشد.";
        private string EditUnSuccessfullyRelatedInformation = "رکورد انتخاب شده دارای اطلاعات وابسته است و قابل ویرایش نمی باشد.";
        private string HasDependentData = "این رکورد اطلاعات وابسته دارد .";
        private string NoItemSearch = "موردی یافت نشد.";
        private string DuplicateCode = "کد وارد شده تکراری است.";
        private string ChangeParentNode = "واحدهای تحت نظارت و خود واحد نمی توانند به عنوان ناظر همان واحد انتخاب شوند";
        private string SelectNode = "یک واحد باید انتخاب شود";
        private string DuplicatePersonnelyCode = "کد پرسنلی تکراری است";
        private string DuplicateRecord = "رکورد تکراری می باشد";
        private string DuplicateRecordDate = "با اين تاريخ وضيعت همساني براي پرسنل موجود مي باشد";
        private string ConfirmedStatus = "رکوردی برای این پرسنل وجود دارد که بلاتکلیف است";
        private string EmploymentStatusNo = "اين مشخصات با وضيعت استخدام پرسنل همساني ندارد";
        private string OccupationStatusNo = "اين مشخصات با وضيعت  اشتغال پرسنل همساني ندارد";
        private string PostPersonelStatusNo = "برای این شخص پستی تعریف نشده است";
        private string Beforestartingemploymen = "تاريخ اجراي حکم نظام وظيفه قبل از شروع استخدام است ";
        private string Afterstartingemployment = "تاريخ پايان وضيعت نظام وظيفه پرسنل بعد از شروع استخدام است ";
        private string ChangeModeEdit = "لطفا تغييرات در قسمت ويرايش انجام شود";
        private string InformationIncomplete = "اطلاعات وارد شده ناقص می باشد.";
        private string JobStatusNo = "این مشخصات با وضیعت شغل پرسنل همسانی ندارد";
        private string EducationStatusNo = "این مشخصات با مدرک تحصیلی پرسنل همسانی ندارد";
        private string ValidateDate = "تاریخ وارد شده معتبر نمی باشد";
        private string Duplicate = "مقدار وارد شده تکراری می باشد";
        private string FromDate = "تاریخ آغاز را مشخص کنید";
        private string Job = "در اين تاريخ شغل مربوطه فعال نمي باشد";
        private string Post = "در اين تاريخ پست مربوطه فعال نمي باشد";
        private string DuplicateTitle = "عنوان واردشده تکراری است";
        private string Employmentproblems = "عنوان واردشده تکراری است";
        private string NewExperience = "سابقه جديدي براي فرد درج نشده است";
        private string SamePerson = "لطفا افراد وابسته را انتخاب کنید.";
        private string DependentPerson = "حکم در جريان براي اين فرد وجود دارد.";
        private string SacrificePerson = "از این نوع ایثارگری حکم در جریان برای فرد وجود دارد.";
        private string SelectPost = "پستی از جدول بالا باید انتخاب شود.";
        private string SelectPostNotIsActive = "پست انتخاب شده غیر فعال می باشد.";
        private string SelectNodeParentNoIsActive = "واحد ناظر انتخاب شده غیر فعال می باشد.";
        private string IndexCode = "کد تشکیلات تکراری است";
        private string IndexDosCode = "کد اداری تکراری است";
        private string SameInfo = "اطلاعات هيج تغييري نکرده است";
        private string Confirmed = "اين اطلاعات تاييد شده است قابل ويرايش نيست";
        private string NoChangeInfo = "این اطلاعات هیچ تغییری نکرده است";


        public enum MessageStatuses
        {
            SaveSuccessfully, SaveUnSuccessfully, DeleteSuccessfully, DeleteUnSuccessfully, DeleteUnSuccessfullyRelatedInformation, HasDependentData, NoItemSearch, DuplicateCode, None, ChangeParentNode, SelectNode, DuplicatePersonnelyCode, DuplicateRecord, ConfirmedStatus, EmploymentStatusNo, OccupationStatusNo, Beforestartingemploymen, Afterstartingemployment, ChangeModeEdit, InformationIncomplete, JobStatusNo, EducationStatusNo, DuplicateRecordDate, PostPersonelStatusNo, Duplicate, FromDate, ValidateDate, Job, Post, DuplicateTitle, EditUnSuccessfullyRelatedInformation, Employmentproblems, NewExperience, SamePerson, DependentPerson, SacrificePerson, SelectPost, SelectPostNotIsActive, SelectNodeParentNoIsActive, IndexCode, IndexDosCode, SameInfo, Confirmed, NoChangeInfo
        }

        public MessageStatuses MessageStatus
        {
            get
            {
                return (MessageStatuses)ViewState["MessageStatus"];
            }
            set
            {
                ViewState["MessageStatus"] = value;
            }
        }

        //public MessageStatuses MessageStatus;

        protected override void OnPreRender(EventArgs e)
        {
            switch (MessageStatus)
            {
                case MessageStatuses.None:

                    imgMessage.Visible = false;
                    lblMessage.Text = "";
                    break;
                case MessageStatuses.SaveSuccessfully:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Ok.png";
                    lblMessage.Text = SaveSuccessfully;
                    break;
                case MessageStatuses.SaveUnSuccessfully:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = SaveUnSuccessfully;
                    break;
                case MessageStatuses.DeleteSuccessfully:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Ok.png";
                    lblMessage.Text = DeleteSuccessfully;
                    break;
                case MessageStatuses.DeleteUnSuccessfully:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = DeleteUnSuccessfully;
                    break;
                case MessageStatuses.DeleteUnSuccessfullyRelatedInformation:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = DeleteUnSuccessfullyRelatedInformation;
                    break;
                case MessageStatuses.HasDependentData:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = HasDependentData;
                    break;
                case MessageStatuses.NoItemSearch:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = NoItemSearch;
                    break;
                case MessageStatuses.DuplicateCode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = DuplicateCode;
                    break;
                case MessageStatuses.ChangeParentNode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = ChangeParentNode;
                    break;
                case MessageStatuses.SelectNode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = SelectNode;
                    break;
                case MessageStatuses.DuplicatePersonnelyCode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = DuplicatePersonnelyCode;
                    break;
                case MessageStatuses.DuplicateRecord:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = DuplicateRecord;
                    break;
                case MessageStatuses.ConfirmedStatus:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = ConfirmedStatus;
                    break;
                case MessageStatuses.EmploymentStatusNo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = EmploymentStatusNo;
                    break;
                case MessageStatuses.OccupationStatusNo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = OccupationStatusNo;
                    break;
                case MessageStatuses.Beforestartingemploymen:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Beforestartingemploymen;
                    break;
                case MessageStatuses.Afterstartingemployment:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Afterstartingemployment;
                    break;
                case MessageStatuses.ChangeModeEdit:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = ChangeModeEdit;
                    break;
                case MessageStatuses.InformationIncomplete:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = InformationIncomplete;
                    break;
                case MessageStatuses.JobStatusNo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = JobStatusNo;
                    break;
                case MessageStatuses.EducationStatusNo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = EducationStatusNo;
                    break;
                case MessageStatuses.DuplicateRecordDate:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = DuplicateRecordDate;
                    break;
                case MessageStatuses.PostPersonelStatusNo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = PostPersonelStatusNo;
                    break;
                case MessageStatuses.Duplicate:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Duplicate;
                    break;
                case MessageStatuses.FromDate:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = FromDate;
                    break;
                case MessageStatuses.ValidateDate:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = ValidateDate;
                    break;
                case MessageStatuses.Job:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Job;
                    break;
                case MessageStatuses.Post:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Post;
                    break;
                case MessageStatuses.DuplicateTitle:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = DuplicateTitle;
                    break;
                case MessageStatuses.EditUnSuccessfullyRelatedInformation:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = EditUnSuccessfullyRelatedInformation;
                    break;
                case MessageStatuses.Employmentproblems:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = Employmentproblems;
                    break;
                case MessageStatuses.NewExperience:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = NewExperience;
                    break;
                case MessageStatuses.SamePerson:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = SamePerson;
                    break;
                case MessageStatuses.DependentPerson:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = DependentPerson;
                    break;
                case MessageStatuses.SacrificePerson:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png"; ;
                    lblMessage.Text = SacrificePerson;
                    break;
                case MessageStatuses.SelectPost:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = SelectPost;

                    break;
                case MessageStatuses.SelectPostNotIsActive:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = SelectPostNotIsActive;
                    break;
                case MessageStatuses.SelectNodeParentNoIsActive:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = SelectNodeParentNoIsActive;
                    break;
                case MessageStatuses.IndexCode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = IndexCode;
                    break;

                case MessageStatuses.IndexDosCode:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = IndexDosCode;
                    break;

                case MessageStatuses.SameInfo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = SameInfo;
                    break;
                case MessageStatuses.Confirmed:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = Confirmed;
                    break;

                case MessageStatuses.NoChangeInfo:
                    imgMessage.Visible = true;
                    imgMessage.ImageUrl = @"images\Error.png";
                    lblMessage.Text = NoChangeInfo;
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["MessageStatus"] = MessageStatuses.None;
        }

    }
}