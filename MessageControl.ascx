﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageControl.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.MessageControl" %>
<table>
    <tr>
        <td align="center" valign="middle">
            <asp:Image ID="imgMessage" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Text="Label" Font-Bold="False" Font-Names="Arial"
                Font-Size="12pt"></asp:Label>
        </td>
    </tr>
</table>
