﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.ItcAccess.Classes;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmRunSqlQuery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void SetGrvSqlResult()
        {
            if (Request["WhereClause"].Trim() != "")
            {
                int delimiter = Request["WhereClause"].IndexOf('_');
                int len = Request["WhereClause"].Length;
                var objectId = Request["WhereClause"].Substring(0, delimiter);
                var userAndGroupRoleAccesstr = Request["WhereClause"].Substring(delimiter + 1, (len - 1) - delimiter);

                DataSet dataSet = new DataSet();
                dataSet = AcsRowAccessDetail.GetSqlDataResultForRowAccess(Int32.Parse(objectId), userAndGroupRoleAccesstr);
                GrvSqlResult.DataSource = dataSet;
            }
        }

        protected void GrvSqlResult_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            SetGrvSqlResult();
        }
    }
}