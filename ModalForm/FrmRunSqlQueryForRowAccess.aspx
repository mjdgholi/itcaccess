﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmRunSqlQueryForRowAccess.aspx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmRunSqlQueryForRowAccess" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title></title>
        <style type="text/css">
            .style1
            {
                color: #660033;
            }
            .style2
            {
                font-weight: normal;
            }
        </style>
    </head>
    <body dir="rtl">
        <form id="form1" runat="server">
            <div>
                <table dir="rtl" align="right" width="100%">
                    <tr>
                        <td align="center">
                            <h3 class="style2">
                                <strong>نتیجه دسترسی های تعیین شده</strong></h3>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp; 
            <telerik:RadScriptManager ID="RadScriptManager1" Runat="server"></telerik:RadScriptManager>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="GrvSqlResult" runat="server" AllowPaging="True" AllowSorting="True"
                                PageSize="10" Skin="Office2007" 
                                onneeddatasource="GrvSqlResult_NeedDataSource" style="direction: rtl">
                                <pagerstyle firstpagetooltip="صفحه اول" lastpagetooltip="صفحه آخر" nextpagestooltip="صفحات بعدی"
                                    nextpagetooltip="صفحه بعدی" pagertextformat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                    pagesizelabeltext="اندازه صفحه" prevpagestooltip="صفحات قبلی" prevpagetooltip="صفحه قبلی"
                                    verticalalign="Middle" horizontalalign="Center" />
                                <clientsettings></clientsettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
</html>
