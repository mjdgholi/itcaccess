﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmUserGroupManage.aspx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmUserGroupManage" %>

<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 10px;
        }
    </style>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
 fnUserAndGroupSearch:
       {
          
           var myValue = new Array(<%=Session["ItemSearch"].ToString() %>); 
           var index= -1;
           
           function NextNode() {              
               if (index <  myValue.length - 1) {                      
                   index = index + 1;  
                   expandNode(myValue[index]); 
                   return true;
                }
                else
                {
//                alert('end Next');
                  return false;
                }
           }

           function PreviousNode() {    
               
               if (index > 0)
               {
                index = index - 1;  
                expandNode(myValue[index]);
               return true;
               }
               else
               {
//               alert ('end Previous');
               return false;
               }
           }
           

           function expandNode(nodeid) {
               var treeView = $find("<%= trvUserAndGroup.ClientID %>");
               var node = treeView.findNodeByValue(nodeid);
               if (node) {
                   node.expand();
                   node.select();
                   scrollToNode(treeView, node);
                   return true;
               }
               return false;
           }


           function scrollToNode(treeview, node) {
               var nodeElement = node.get_contentElement();
               var treeViewElement = treeview.get_element();

               var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
               var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
               var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

               if (relativeOffsetTop < treeViewElement.scrollTop) {
                   treeViewElement.scrollTop = relativeOffsetTop;
               }

               var height = nodeElement.offsetHeight;

               if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                   treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
               }
           }
       }          
        </script>
    </telerik:RadCodeBlock>
</head>
<body dir="rtl" background='#F0F0F0'>
    <script type="text/javascript">
        function RowSelected(sender, eventArgs) {
            var oArg = new Object();
            oArg.Title = eventArgs._dataKeyValues.UserAndGroupName;
            oArg.KeyId = eventArgs._dataKeyValues.UserAndGroupId;
            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <form id="form2" runat="server">
        
    <table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" colspan="5" valign="middle">
    <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
    </telerik:RadScriptManager>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="5" valign="middle">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="9pt"
                    ForeColor="#003399" Text="لیست گروههایی که کاربر در آن عضو است"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="5" valign="middle">
                &nbsp;
                <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                <table>
                    <tr>
                        <td>
                            <cc1:CustomRadButton ID="BtnAdd" runat="server" CustomeButtonType="Edit" OnClick="BtnAdd_Click"
                                Skin="Simple" Width="100px">
                            </cc1:CustomRadButton>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" Width="180px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" OnClick="imgbtnSearch_Click"
                                ToolTip="جستجو" Width="25px" />
                        </td>
                        <td class="style1">
                            <img alt="جلو" height="25" onclick="NextNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Next.png"%>'
                                width="25" />
                        </td>
                        <td>
                            <img alt="عقب" height="25" onclick="PreviousNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Previous.png"%>'
                                width="25" />
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right" width="50%">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;.
            </td>
        </tr>
        <tr>
            <td align="right" width="10%">
                &nbsp;
            </td>
            <td align="right" width="50%">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" colspan="5" height="350" valign="top" width="10%">
                <telerik:RadTreeView ID="trvUserAndGroup" runat="server" BorderColor="#999999" BorderStyle="Solid"
                    BorderWidth="1px" CheckBoxes="True" dir="rtl" OnNodeDataBound="trvUserAndGroup_NodeDataBound"
                    OnNodeExpand="trvUserAndGroup_NodeExpand" Skin="Office2007" Width="99%">
                </telerik:RadTreeView>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
