﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmSetRowAccessForUser.aspx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmSetRowAccessForUser" %>

<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body dir="rtl" style="font-weight: 700">
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="PnlAll" runat="server">
            <table dir="rtl" width="100%">
                <tr>
                    <td align="center" bgcolor="#DDE8FF">
                        <asp:Label ID="LblTitle" runat="server" Font-Names="B Titr" Font-Size="14pt" ForeColor="#000099"
                            Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100%" valign="top">
                        <asp:Panel runat="server" ID="PnlItem">
                            <table dir="rtl" width="100%">
                                <tr>
                                    <td colspan="5">
                                        <table dir="rtl">
                                            <tr>
                                                <td align="right">
                                                    <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                                                    </cc1:CustomRadButton>
                                                </td>
                                                <td align="right">
                                                    <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                                                        <Icon PrimaryIconCssClass="rbEdit" />
                                                    </cc1:CustomRadButton>
                                                </td>
                                                <td align="right">
                                                    <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                                                        OnClick="BtnSearch_Click">
                                                        <Icon PrimaryIconCssClass="rbSearch" />
                                                    </cc1:CustomRadButton>
                                                </td>
                                                <td align="right">
                                                    <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                                                        Empty="" OnClick="BtnShowAll_Click" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                                                        <Icon PrimaryIconCssClass="rbRefresh" />
                                                    </cc1:CustomRadButton>
                                                </td>
                                                <td align="right">
                                                    <uc1:MessageErrorControl ID="MessageErrorControl2" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10%" style="margin-right: 40px">
                                        <asp:Label ID="LblSystem" runat="server" Text="عنوان سیستم:"></asp:Label>
                                    </td>
                                    <td width="40%" style="margin-right: 40px">
                                        <cc1:CustomRadComboBox ID="CmbSystem" runat="server" AppendDataBoundItems="True"
                                            AutoPostBack="True" CausesValidation="False" Filter="Contains" MarkFirstMatch="True"
                                            NoWrap="True" OnSelectedIndexChanged="CmbSystem_SelectedIndexChanged" Skin="Office2007"
                                            Width="400px">
                                        </cc1:CustomRadComboBox>
                                    </td>
                                    <td width="10%" colspan="2" nowrap="nowrap">
                                        &nbsp;
                                    </td>
                                    <td width="40%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <asp:Label ID="LblObject" runat="server" Text="عنوان آبجکت:"></asp:Label>
                                    </td>
                                    <td width="40%">
                                        <cc1:CustomRadComboBox ID="CmbObject" runat="server" AppendDataBoundItems="True"
                                            AutoPostBack="True" CausesValidation="False" Filter="Contains" MarkFirstMatch="True"
                                            NoWrap="True" OnSelectedIndexChanged="CmbObject_SelectedIndexChanged" Skin="Office2007"
                                            Width="400px">
                                        </cc1:CustomRadComboBox>
                                    </td>
                                    <td colspan="2" nowrap="nowrap" width="10%">
                                        &nbsp;
                                    </td>
                                    <td width="40%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <asp:Label ID="LblObjectRowAccess" runat="server" Text="دسترسی:"></asp:Label>
                                    </td>
                                    <td width="40%">
                                        <telerik:RadComboBox ID="CmbObjectRowAccess" runat="server" CheckBoxes="True" 
                                            Height="200px" Skin="Office2007" Width="400px">
                                            <Localization AllItemsCheckedString="تمامی موارد انتخاب گردید" CheckAllString="انتخاب همه" />
                                        </telerik:RadComboBox>
                                    </td>
                                    <td colspan="2" nowrap="nowrap" width="10%">
                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" OnClick="Button1_Click"
                                            Text="مشاهده نتیجه دسترسی" />
                                    </td>
                                    <td width="40%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" nowrap="nowrap">
                                        <asp:Label ID="LblAccessType" runat="server" Text="نوع دسترسی:"></asp:Label>
                                    </td>
                                    <td width="40%" dir="rtl">
                                        <telerik:RadComboBox ID="CmbAccessType" runat="server" AppendDataBoundItems="True"
                                            CausesValidation="False" KeyId="" MarkFirstMatch="True" Skin="Office2007" Width="100px">
                                            <Localization AllItemsCheckedString="تمامی موارد انتخاب گردید" CheckAllString="انتخاب خمه" />
                                        </telerik:RadComboBox>
                                    </td>
                                    <td align="right">
                                        <asp:RequiredFieldValidator ID="rfvObjectRowAccess" runat="server" ControlToValidate="CmbObjectRowAccess"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfvAccessType" runat="server" ControlToValidate="CmbAccessType"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="right" colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap" width="10%">
                                        <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                                    </td>
                                    <td width="40%">
                                        <telerik:RadComboBox ID="CmbIsActive" runat="server" Filter="Contains" KeyId="" MarkFirstMatch="True"
                                            Skin="Office2007" Width="100px">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Selected="True" />
                                                <telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" Selected="True" />
                                                <telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="50%" colspan="3">
                                        <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap" width="10%">
                                        &nbsp;
                                    </td>
                                    <td width="40%">
                                        &nbsp;
                                    </td>
                                    <td colspan="3" width="50%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 100%" dir="ltr">
                        <telerik:RadGrid ID="GrvRowAccess" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnDetailTableDataBind="GrvRowAccess_DetailTableDataBind"
                            OnItemCommand="GrvObjectAccess_ItemCommand" OnItemDataBound="GrvObjectAccess_ItemDataBound"
                            OnNeedDataSource="GrvRowAccess_NeedDataSource" Skin="Office2007" Width="100%">
                            <ClientSettings AllowColumnsReorder="True" EnableRowHoverStyle="true" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView ClientDataKeyNames="ObjectId" DataKeyNames="ObjectId" Dir="RTL"
                                Name="MasterTable" NoDetailRecordsText="اطلاعاتی برای نمایش یافت نشد" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <DetailTables>
                                    <telerik:GridTableView runat="server" DataKeyNames="UserAndGroup_Role_AccessId" Name="RowAccessDetail">
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="AccessTitle" FilterControlAltText="Filter AccessTitle column"
                                                HeaderText="عنوان دسترسی" SortExpression="AccessTitle" UniqueName="AccessTitle">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AccessTypeTitle" FilterControlAltText="Filter AccessTypeTitle column"
                                                HeaderText="نوع دسترسی" SortExpression="AccessTypeTitle" UniqueName="AccessTypeTitle">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DirectOrFromRole" FilterControlAltText="Filter DirectOrFromRole column"
                                                HeaderText="نحوه احراز دسترسی" SortExpression="DirectOrFromRole" UniqueName="DirectOrFromRole">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RoleAndCategorizeTilte" FilterControlAltText="Filter RoleAndCategorizeTilte column"
                                                HeaderText="عنوان نقش" SortExpression="RoleAndCategorizeTilte" UniqueName="RoleAndCategorizeTilte">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
                                                UniqueName="Edit">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>'
                                                        CommandName="_MyEditUserAndGroup_Role_Access" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                                                UniqueName="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>'
                                                        CommandName="_MyDeleteUserAndGroup_Role_Access" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                                        OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="مشاهده نتیجه دسترسی"
                                                UniqueName="ResultPerRolePerRowAccess">
                                                <ItemTemplate>
                                                    <cc1:SelectControl ID="SelectControlResultPerRolePerRowAccess" runat="server" CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>'
                                                        imageName="RunQuery" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQuery.aspx"
                                                        RadWindowHeight="500" RadWindowWidth="700" Width="1px" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                    </telerik:GridTableView></DetailTables>
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="ObjectTitle" FilterControlAltText="Filter ObjectTitle column"
                                        HeaderText="عنوان آبجکت" SortExpression="ObjectTitle" UniqueName="ObjectTitle">
                                        <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                                        UniqueName="DeleteAllRowAccessPerObjectPerRole">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgButtonDeleteAllRowAccessPerObjectPerRole" runat="server"
                                                CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                CommandName="_MyDeleteAllRowAccessPerObjectPerUser" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                                OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="مشاهده نتیجه دسترسی"
                                        UniqueName="ResultPerObjectPerRole">
                                        <ItemTemplate>
                                            <cc1:SelectControl ID="SelectControlResultPerObjectPerRole" runat="server" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                imageName="RunQuery" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQuery.aspx"
                                                RadWindowHeight="500" RadWindowWidth="700" Width="1px" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                    NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                    PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                    VerticalAlign="Middle" />
                            </MasterTableView><HeaderStyle Height="40px" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="BtnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="BtnEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="BtnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="BtnShowAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CmbObject">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CmbObjectRowAccess" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CmbSystem">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CmbObject" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="GrvRowAccess">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadWindowManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlAll" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <p style="direction: ltr">
        &nbsp;</p>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" DestroyOnClose="True"
        InitialBehavior="Maximize" InitialBehaviors="Maximize" Modal="True">
    </telerik:RadWindowManager>
    </form>
</body>
</html>
