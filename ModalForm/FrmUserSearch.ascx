﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmUserSearch.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmUserSearch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="MessageErrorControl" TagPrefix="uc2" %>
<asp:Panel ID="pnlbtn" runat="server">
    <table style="width: 100%;" dir="rtl">
        <tr>
            <td width="5%">
                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" OnClick="btnSearch_Click">
                    <Icon PrimaryIconCssClass="rbSearch" />
                </cc1:CustomRadButton>
            </td>
            <td width="5%">
                <cc1:CustomRadButton ID="cmbexportExcel" runat="server" CustomeButtonType="ExcelExport"
                    OnClick="cmbexportExcel_Click">
                    <Icon PrimaryIconUrl="mvwres://ITC.Library, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null/ITC.Library.Images.RadButtonImage.Excel.png" />
                </cc1:CustomRadButton>
            </td>
            <td width="5%">
                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" OnClick="btnShowAll_Click">
                    <Icon PrimaryIconCssClass="rbRefresh" />
                </cc1:CustomRadButton>
            </td>
            <td>
                <cc1:CustomRadButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" CustomeButtonType="Cancel">
                    <Icon PrimaryIconCssClass="rbCancel" />
                </cc1:CustomRadButton>
            </td>
            <td width="5%">
                <cc1:CustomRadButton ID="btnSelectAll" runat="server" CustomeButtonType="SelectAll"
                    OnClick="btnSelectAll_Click">
                    <Icon PrimaryIconUrl="mvwres://ITC.Library, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null/ITC.Library.Images.SelectAll.Selectall.png" />
                </cc1:CustomRadButton>
            </td>
            <td width="5%">
                <cc1:CustomRadButton ID="btnSelectPersonnelAndBack" runat="server" OnClientClicked="CloseRadWindow"
                    CustomeButtonType="Back" Width="110px" Skin="Windows7">
                    <Icon PrimaryIconUrl="mvwres://ITC.Library, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null/ITC.Library.Images.SelectPersonnelAndBack.SelectPersonnelAndBack.png"
                        PrimaryIconCssClass="rbPrevious" />
                </cc1:CustomRadButton>
            </td>
            <td width="60%">
                <asp:Panel ID="pnlMessageError" runat="server">
                    <uc2:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadPanelBar runat="server" ID="radpanelbarSearch" Width="100%" dir="rtl"
    Skin="Outlook">
    <Items>
        <telerik:RadPanelItem Text="مشخصات فردی" Expanded="True" Font-Bold="True" Font-Size="11"
            Font-Names="Arial" ForeColor="White" ImagePosition="Right" ImageUrl="Images/MenuPubPerson.png">
            <Items>
                <telerik:RadPanelItem Value="PersonalInformation">
                    <ItemTemplate>
                        <table style="width: 100%; background-color: #F0F0F0;" dir="rtl">
                            <tr>
                                <td>
                                    <label>
                                        نام:</label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtFirstName" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <label>
                                        نام خانوادگي:</label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtlastName" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                                <td dir="ltr" align="left">
                                    <cc1:SelectControl ID="SelectControl1" runat="server" Width="0px" ActiveCloseRadWindow="True"
                                        imageName="PersonAdd" PortalPathUrl="person/PersonPage.aspx" HasNationalNo="True"
                                        ToolTip="ثبت پرسنل جدید" PrsOrganizationPhysicalChartFullAccess="True" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        کد ملی:</label>
                                </td>
                                <td dir="rtl">
                                    <telerik:RadTextBox ID="txtNationalNo" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <label>
                                        شماره پرسنلی:</label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtPersonnelNo" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        شماره شناسنامه:</label>
                                </td>
                                <td dir="rtl">
                                    <telerik:RadTextBox ID="txtCertificateNo" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <label>
                                        نام پدر:</label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtFatherName" runat="server" Width="150px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        جنسیت:</label>
                                </td>
                                <td dir="rtl">
                                    <cc1:CustomRadComboBox ID="cmbGender" runat="server" Width="150px" AppendDataBoundItems="True">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        وضیعت تاهل:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbMaritalStatus" runat="server" Width="150px" AppendDataBoundItems="True">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Value="0" />
                                            <telerik:RadComboBoxItem runat="server" Text="مجرد" Value="1" />
                                            <telerik:RadComboBoxItem runat="server" Text="متاهل" Value="2" />
                                        </Items>
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        مقطع تحصیلی:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbEducationDegreeTitle" runat="server" CheckBoxes="True"
                                        AllowCustomText="True" Filter="Contains" Height="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        مرکزتحصیلی:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbEducationCenterTitle" runat="server" AppendDataBoundItems="True"
                                        AllowCustomText="True" Filter="Contains" Height="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        رشته تحصیلی:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbEducationCourseTitle" runat="server" AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="drpEducationCourseTitle_SelectedIndexChanged" AutoPostBack="True"
                                        AllowCustomText="True" Filter="Contains" CausesValidation="False" Height="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        گرایش تحصیلی:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbEducationOrientationTitle" runat="server" AppendDataBoundItems="True"
                                        AllowCustomText="True" Filter="Contains" Height="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
        <telerik:RadPanelItem Text="مشخصات حکمی" Expanded="True" Font-Bold="True" Font-Size="11"
            Font-Names="Arial" ForeColor="White" ImagePosition="Right" ImageUrl="Images/Commandmen.png">
            <Items>
                <telerik:RadPanelItem Value="CommandmentInformation">
                    <ItemTemplate>
                        <table style="width: 100%; background-color: #F0F0F0;" dir="rtl">
                            <tr>
                                <td>
                                    <label>
                                        پست:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbPrsPost" runat="server" EnableLoadOnDemand="True" EnableVirtualScrolling="True"
                                        OnItemsRequested="radcomboPrsPost_ItemsRequested" ShowMoreResultsBox="True" AppendDataBoundItems="True"
                                        Height="210px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        سطح پست:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbPostLevel" runat="server" Width="200px" AllowCustomText="True"
                                        Filter="StartsWith" Height="200px" CheckBoxes="True">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        شغل:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbPrsJOb" runat="server" EnableLoadOnDemand="True" EnableVirtualScrolling="True"
                                        OnItemsRequested="radcomboboxJOb_ItemsRequested" ShowMoreResultsBox="True" AppendDataBoundItems="True"
                                        Height="210px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        سطح شغل:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbJobLevel" runat="server" Width="200px" AllowCustomText="True"
                                        Filter="StartsWith" Height="200px" CheckBoxes="True">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        انواع استخدام:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbEmploymentType" runat="server" CheckBoxes="True" Width="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        انواع اشتغال:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbOccupationStatus" runat="server" CheckBoxes="True"
                                        Width="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        انواع ایثارگری:</label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbPrsSacrificeType" runat="server" CheckBoxes="True"
                                        Width="200px">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        گروه:
                                    </label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbGroupNo" runat="server" CheckBoxes="True" Width="200px">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Text="1" Value="1" />
                                            <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                                            <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                                            <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                                            <telerik:RadComboBoxItem runat="server" Text="5" Value="5" />
                                            <telerik:RadComboBoxItem runat="server" Text="6" Value="6" />
                                            <telerik:RadComboBoxItem runat="server" Text="7" Value="7" />
                                            <telerik:RadComboBoxItem runat="server" Text="8" Value="8" />
                                            <telerik:RadComboBoxItem runat="server" Text="9" Value="9" />
                                            <telerik:RadComboBoxItem runat="server" Text="10" Value="10" />
                                            <telerik:RadComboBoxItem runat="server" Text="11" Value="11" />
                                            <telerik:RadComboBoxItem runat="server" Text="12" Value="12" />
                                            <telerik:RadComboBoxItem runat="server" Text="13" Value="13" />
                                            <telerik:RadComboBoxItem runat="server" Text="14" Value="14" />
                                            <telerik:RadComboBoxItem runat="server" Text="15" Value="15" />
                                            <telerik:RadComboBoxItem runat="server" Text="16" Value="16" />
                                            <telerik:RadComboBoxItem runat="server" Text="17" Value="17" />
                                            <telerik:RadComboBoxItem runat="server" Text="18" Value="18" />
                                            <telerik:RadComboBoxItem runat="server" Text="19" Value="19" />
                                            <telerik:RadComboBoxItem runat="server" Text="20" Value="20" />
                                        </Items>
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        درصدحق جذب:
                                    </label>
                                </td>
                                <td>
                                    <cc1:NumericTextBox ID="txtAttracctionBenfit" runat="server" Text=""></cc1:NumericTextBox>
                                    <cc1:CustomRadComboBox ID="cmbAttracctionBenfitOperation" runat="server" KeyId=""
                                        Width="50px">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Text="=" Value="=" />
                                            <telerik:RadComboBoxItem runat="server" Text="&gt;=" Value="&gt;=" />
                                            <telerik:RadComboBoxItem runat="server" Text="&lt;=" Value="&lt;=" />
                                        </Items>
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <label>
                                        سوابق:
                                    </label>
                                </td>
                                <td>
                                    <cc1:NumericTextBox ID="txtExperience" runat="server" Text=""></cc1:NumericTextBox>
                                    <cc1:CustomRadComboBox ID="cmbExperienceOperation" runat="server" KeyId="" Width="50px">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Text="=" Value="=" />
                                            <telerik:RadComboBoxItem runat="server" Text="&gt;=" Value="&gt;=" />
                                            <telerik:RadComboBoxItem runat="server" Text="&lt;=" Value="&lt;=" />
                                        </Items>
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        ساختار نمودار فیزیکی:
                                    </label>
                                </td>
                                <td>
                                    <cc1:SelectControl ID="selectOrganizationPhysicalChart" runat="server" PortalPathUrl="Person/PubOrganizationPhysicalChartMultiSelectPage.aspx"
                                        imageName="OrganizationPhysicalChart" SelectControlClear="True" />
                                    <asp:CheckBox ID="checkUnOrganizationPhysicalChart" runat="server" Text="فاقد محل خدمت"
                                        Checked="True" />
                                </td>
                                <td>
                                    <label>
                                        وضیعت نظام وظیفه:
                                    </label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="drpdownMilitaryStatusTitle" runat="server" AppendDataBoundItems="True">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <asp:Label runat="server" ID="lblDependentTypeTitle">عنوان نوع وابستگی:</asp:Label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbDependentTypeTitle" runat="server" AppendDataBoundItems="true">
                                    </cc1:CustomRadComboBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblDependentStatusTitle">عنوان نوع وضیعت وابستگی:</asp:Label>
                                </td>
                                <td>
                                    <cc1:CustomRadComboBox ID="cmbDependentStatusTitle" runat="server" AppendDataBoundItems="true">
                                    </cc1:CustomRadComboBox>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelItem>
    </Items>
    <ExpandAnimation Type="None"></ExpandAnimation>
</telerik:RadPanelBar>
<table style="width: 100%;" dir="rtl">
    <tr>
        <td align="center" valign="top" bgcolor="White">
            <telerik:RadGrid ID="grdPerson" runat="server" AllowCustomPaging="True" AllowPaging="True"
                AllowSorting="True" ShowFooter="True" DataKeyNames="PersonId" AutoGenerateColumns="False"
                CellSpacing="0" GridLines="None" HorizontalAlign="Center" Skin="Outlook" OnPageIndexChanged="grdPerson_PageIndexChanged"
                OnPageSizeChanged="grdPerson_PageSizeChanged" OnSortCommand="grdPerson_SortCommand"
                AllowMultiRowSelection="True">
                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                </HeaderContextMenu>
                <MasterTableView Dir="RTL" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد" DataKeyNames="PersonId"
                    ClientDataKeyNames="PersonId,FirstName,LastName,NationalNo" GroupsDefaultExpanded="False">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="صفحه اول" HorizontalAlign="Center"
                        LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی"
                        PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                        PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                        VerticalAlign="Middle" />
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"
                        Display="False">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام" SortExpression="FirstName">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی" SortExpression="LastName">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CertificateNo" HeaderText="شماره شناسنامه" SortExpression="CertificateNo">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NationalNo" HeaderText="کد ملی" SortExpression="NationalNo">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PersonnelNo" HeaderText="شماره پرسنلی" SortExpression="PersonnelNo">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EmploymentTypeTitle" HeaderText="استخدام" SortExpression="EmploymentTypeTitle"
                            UniqueName="EmploymentTypeTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OccupationStatusTitle" HeaderText="اشتغال" SortExpression="OccupationStatusTitle"
                            UniqueName="OccupationStatusTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PostTitle" HeaderText="‍پست" SortExpression="PostTitle"
                            UniqueName="PostTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PostLevelTitle" HeaderText="‍سطح پست" SortExpression="PostLevelTitle"
                            UniqueName="PostLevelTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="JobTitle" HeaderText="عنوان شغل" SortExpression="JobTitle"
                            UniqueName="JobTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="JobLevelTitle" HeaderText="سطح شغل" SortExpression="JobLevelTitle"
                            UniqueName="JobLevelTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OrganizationPhysicalChartTitle" HeaderText="محل خدمت"
                            SortExpression="OrganizationPhysicalChartTitle" UniqueName="OrganizationPhysicalChartTitle"
                            FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EducationDegreeTitle" HeaderText="مدرک تحصیلی"
                            SortExpression="EducationDegreeTitle" UniqueName="EducationDegreeTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EducationCourseTitle" HeaderText="رشته تحصیلی"
                            SortExpression="EducationCourseTitle" UniqueName="EducationCourseTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PersonnelAttractionBenifitPercent" HeaderText="درصد حق جذب"
                            SortExpression="PersonnelAttractionBenifitPercent" UniqueName="PersonnelAttractionBenifitPercent"
                            FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MilitaryStatusTitle" HeaderText="وضعت های نظام وظیفه"
                            SortExpression="MilitaryStatusTitle" UniqueName="MilitaryStatusTitle" FilterControlAltText="Filter column column">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PersonnelExperience" FilterControlAltText="Filter PersonnelExperience column"
                            HeaderText="سابقه روز,ماه,سال" SortExpression="PersonnelExperience" UniqueName="PersonnelExperience">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PersonnelDayExperience" FilterControlAltText="Filter PersonnelExperience column"
                            HeaderText="سوابق به تعداد روز" SortExpression="PersonnelDayExperience" UniqueName="PersonnelDayExperience">
                        </telerik:GridBoundColumn>
                        <telerik:GridClientSelectColumn UniqueName="Select">
                        </telerik:GridClientSelectColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column" UniqueName="EditCommandColumn1">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True"></PagerStyle>
                </MasterTableView>
                <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                    <ClientEvents OnRowSelected="RowSelected" OnRowDeselected="RowDeselected" OnMasterTableViewCreated="MasterTableViewCreated">
                    </ClientEvents>
                    <Selecting AllowRowSelect="True"></Selecting>
                </ClientSettings>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
                <StatusBarSettings LoadingText="بارگذاری..." ReadyText="آماده" />
            </telerik:RadGrid>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
<asp:HiddenField ID="hiddenfieldTitle" runat="server" />
<asp:HiddenField ID="hiddenfieldNationalCode" runat="server" />
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var myArrayKey = new Array();
        var myArrayTitle = new Array();
        var hiddenfieldvalue = '';
        var hiddenfieldtitle = '';
        document.getElementById('<%=hiddenfieldNationalCode.ClientID %>').value = '';
        var str = '';
        function RowSelected(sender, eventArgs) {

            KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];

            hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            hiddenfieldtitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
            document.getElementById('<%=hiddenfieldNationalCode.ClientID %>').value = eventArgs._dataKeyValues.NationalNo;
            var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
            var txtTitlevalue = (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim();
            if (include(myArrayKey, textKeyvalue) != true) {
                if (myArrayKey.length > 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                hiddenfieldtitle = hiddenfieldtitle + str + (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim();
                myArrayKey.push(textKeyvalue);
                myArrayTitle.push(txtTitlevalue);
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
        }

        function RowDeselected(sender, eventArgs) {

            str = '';
            hiddenfieldvalue = '';
            hiddenfieldtitle = '';
            KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];
            for (var i = 0; i < myArrayKey.length; i++) {
                if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                    myArrayKey.splice(i, 1);
            }
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (myArrayTitle[j] == (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim())
                    myArrayTitle.splice(j, 1);
            }

            for (var i = 0; i < myArrayKey.length; i++) {
                if (i != 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
            }
            str = '';
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (j != 0)
                    str = ',';
                hiddenfieldtitle = hiddenfieldtitle + str + myArrayTitle[j];
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
        }

        function MasterTableViewCreated(sender, eventArgs) {
            if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
            if (document.getElementById('<%=hiddenfieldTitle.ClientID %>').value != '')
                myArrayTitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value.split(',');
            KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
            for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                        sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                }
        }


        function include(arr, obj) {
            if (arr.length == 0)
                return false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj)
                    return true;
            }
        }

        function CloseRadWindow() {

            if (document.getElementById('<%=hiddenfieldNationalCode.ClientID %>').value == '') {
                alert('کد ملی اجباری می باشد');
            } else {
                var oArg = new Object();
                oArg.Title = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
                oArg.KeyId = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
                var oWnd = GetRadWindow();
                oWnd.close(oArg);
            }

        }


        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</telerik:RadScriptBlock>
<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="cmbEducationCourseTitle">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbEducationOrientationTitle" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbPrsPost">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbPrsPost" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbPrsJOb">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbPrsJOb" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdPerson">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdPerson" 
                        LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radpanelbarSearch" />
                    <telerik:AjaxUpdatedControl ControlID="grdPerson" 
                        LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="pnlbtn" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnShowAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radpanelbarSearch" />
                    <telerik:AjaxUpdatedControl ControlID="grdPerson" 
                        LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="pnlMessageError" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radpanelbarSearch" />
                    <telerik:AjaxUpdatedControl ControlID="grdPerson" 
                        LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="pnlbtn" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSelectAll">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdPerson" 
                        LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="hiddenfieldKeyId" />
                    <telerik:AjaxUpdatedControl ControlID="hiddenfieldTitle" />
                    <telerik:AjaxUpdatedControl ControlID="pnlMessageError" />
                </UpdatedControls>
            </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
