﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmUserGroupManage : Page
    {
        private ArrayList _directParentNodeList = new ArrayList();
        private ArrayList _searchGroupIdList = new ArrayList();
        private ArrayList _allChildNodeList=new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var userAndGroup = AcsUserAndGroup.GetSingleById(userAndGroupId);
                if (userAndGroup.IsUser)
                {
                    lblTitle.Text = "گروه هایی که کاربر < " + userAndGroup.UserAndGroupName + " > در آن عضویت دارد";
                }
                else
                {
                    lblTitle.Text = "گروه هایی که گروه < " + userAndGroup.UserAndGroupName + " > در آن عضویت دارد";
                }
                _allChildNodeList = AcsUserAndGroupTreeDB.GetChildNodesArray(userAndGroupId);
                ViewState["AllChildNode"] = _allChildNodeList;
                Session["ItemSearch"] = "";
                LoadTree();
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            try
            {
                string selectedNodes = "";
                foreach (RadTreeNode node in trvUserAndGroup.GetAllNodes())
                {
                    if (node.Checked)
                    {
                        selectedNodes += (selectedNodes == "") ? node.Attributes["UserAndGroupId"] : "," + node.Attributes["UserAndGroupId"];
                    }
                }
                AcsUserAndGroupTree.UserOrGroupUpdateGroupMembership(userAndGroupId, selectedNodes);                

                LoadTree();
                MessageErrorControl1.ShowSuccesMessage("بروز رسانی عضویت انجام گردید");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void LoadTree()
        {
            var userAndGroupIdCurrent = Int32.Parse(Request["WhereClause"]);
            _directParentNodeList = AcsUserAndGroupTree.GetDirectParentNodes(userAndGroupIdCurrent);
            trvUserAndGroup.Nodes.Clear();
            LoadRootNodes(trvUserAndGroup, TreeNodeExpandMode.ServerSideCallBack);

            var dataTable = AcsUserAndGroupTree.GetParentNodes(userAndGroupIdCurrent);
            ExpandFoundedNode(dataTable);

            foreach (int groupId in _searchGroupIdList)
            {
                dataTable = AcsUserAndGroupTree.GetParentNodes(groupId);
                ExpandFoundedNode(dataTable);
            }
        }

        protected void trvUserAndGroup_NodeDataBound(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            //var parentNodeList = (ArrayList) ViewState["parentGroupList"];
            //var curNodee = e.Node;
            //if (parentNodeList.Contains(e.Node.Value))
            //{
            //    curNodee.Checked = true;
            //    curNodee.Expanded = true;
            //    curNodee.Selected = true;
            //}
        }

        protected void trvUserAndGroup_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            //e.Node.Nodes.Clear();
            PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack, 0);
        }

        protected void LoadRootNodes(Telerik.Web.UI.RadTreeView treeView, TreeNodeExpandMode expandMode)
        {
            var userAndGroupIdCurrent = Int32.Parse(Request["WhereClause"]);
            var ds = Classes.AcsUserAndGroupTreeDB.AcsUserAndGroupTreeGetGetChildNode(-1, 0); // لود نودهای ریشه
            var data = ds.Tables[0];

            foreach (DataRow row in data.Rows)
            {
                var node = new RadTreeNode
                               {
                                   Text = row["UserAndGroupName"].ToString(),
                                   Value = row["UserAndGroupTreeId"].ToString(),
                                   ExpandMode = expandMode,
                                   //ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png",

                               };
                node.Attributes.Add("UserAndGroupId", row["UserAndGroupId"].ToString());
                node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;
                var userAndGroupId = row["UserAndGroupId"].ToString();
                if (_directParentNodeList.Contains(userAndGroupId))
                {
                   // node.Selected = true;
                    node.Checked = true;
                }
                if (_searchGroupIdList.Contains(Int32.Parse(userAndGroupId)))
                {
                    node.Text = node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                   // node.Selected = true;
                }
                _allChildNodeList = (ArrayList) ViewState["AllChildNode"];
                if (_allChildNodeList.Contains(row["UserAndGroupId"].ToString()))//  نود فرزند نمی تواند به عنوان پدر انتخاب گردد
                {
                    node.Checkable = false;
                }
                if (row["UserAndGroupId"].ToString() == userAndGroupIdCurrent.ToString()) // خود نود نمی تواند به عنوان پدر خود انتخاب گردد
                {
                    node.Checkable = false;
                    node.BackColor = Color.LightSkyBlue;
                }
                treeView.Nodes.Add(node);
            }
        }

        protected void ExpandFoundedNode(DataTable dataTable)
        {
            foreach (DataRow dr in dataTable.Rows)
            {
                var foundNode = trvUserAndGroup.FindNodeByValue(dr["UserAndGroupTreeId"].ToString());
                if (foundNode != null)
                {
                    var radTreeNodeEventArgs = new RadTreeNodeEventArgs(foundNode);
                    trvUserAndGroup_NodeExpand(trvUserAndGroup, radTreeNodeEventArgs);
                    foundNode.Expanded = true;

                    //string userAndGroupId = foundNode.Attributes["UserAndGroupId"];
                    //if (_directParentNodeList.Contains(userAndGroupId))
                    //{
                    //   // foundNode.Selected = true;
                    //    foundNode.Checked = true;
                    //}

                    //if (_searchGroupIdList.Contains(Int32.Parse(userAndGroupId)))
                    //{
                    //    foundNode.Text = foundNode.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                    //   // foundNode.Selected = true;
                    //}

                }
            }
        }

        public void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode, int isUser)
        {
            var userAndGroupId = Int32.Parse(e.Node.Attributes["UserAndGroupId"]);
            DataSet ds = Classes.AcsUserAndGroupTreeDB.AcsUserAndGroupTreeGetGetChildNode(userAndGroupId, isUser);
            DataTable data = ds.Tables[0];
            ArrayList curNodeChildValues = new ArrayList();
            foreach (RadTreeNode node in e.Node.Nodes)
            {
                curNodeChildValues.Add(node.Attributes["UserAndGroupId"]);
            }
            foreach (DataRow row in data.Rows)
            {
                if (!curNodeChildValues.Contains(row["UserAndGroupId"].ToString()))
                {
                    var node = new RadTreeNode
                                   {
                                       Text = row["UserAndGroupName"].ToString(),
                                       Value = row["UserAndGroupTreeId"].ToString(),
                                       ExpandMode = expandMode,
                                     //  ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png"
                                   };
                    node.Attributes.Add("UserAndGroupId", row["UserAndGroupId"].ToString());
                    node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;
                    
                    if (_searchGroupIdList.Contains(Int32.Parse(row["UserAndGroupId"].ToString())))
                    {
                        node.Text = node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                       // node.Selected = true;
                    }
                    _allChildNodeList = (ArrayList) ViewState["AllChildNode"];
                    if (_allChildNodeList.Contains(row["UserAndGroupId"].ToString()))
                    {
                        node.Checkable = false;
                    }                   
                    node.Checked = _directParentNodeList.Contains(row["UserAndGroupId"].ToString());
                    if (row["UserAndGroupId"].ToString() == Request["WhereClause"]) // خود نود نمی تواند به عنوان پدر خود انتخاب گردد
                    {
                        node.Checkable = false;
                        node.BackColor = Color.LightSkyBlue;
                    }                   

                    e.Node.Nodes.Add(node);
                }
            }
            e.Node.Expanded = true;
            e.Node.ExpandMode = TreeNodeExpandMode.ClientSide;
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text!="")
            {
                var searchNodes = AcsUserAndGroupTree.GetAllNodes(0, TxtGroupName.Text);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["UserAndGroupId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["UserAndGroupTreeId"].ToString() + "'" : "," + "'" + datarow["UserAndGroupTreeId"].ToString() + "'";
                }
                LoadTree();
            }
           
        }
    }
}