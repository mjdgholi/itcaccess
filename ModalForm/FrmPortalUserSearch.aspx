﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmPortalUserSearch.aspx.cs" EnableSessionState="true"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmPortalUserSearch" %>

<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
    <div>
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function RowSelected(sender, eventArgs) {
                    var oArg = new Object();
                    oArg.Title = eventArgs._dataKeyValues.PersonFullName;
                    oArg.KeyId = eventArgs._dataKeyValues.UserID;
                    var oWnd = GetRadWindow();
                    oWnd.close(oArg);
                }
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }
                function CloseRadWindow() {
                    var oWnd = GetRadWindow();
                    oWnd.close();
                }
        
            </script>
        </telerik:RadScriptBlock>
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" HeaderText="موارد زیر باید پر شود:"
            ShowMessageBox="True" ShowSummary="False" />
        <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
            <tr>
                <td dir="rtl" style="width: 100%; height: 40px">
                    <asp:Panel ID="Panel2" runat="server">
                        <table>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadButton ID="radbtnSearch" runat="server" Text="جستجو" OnClick="radbtnSearch_Click"
                                        Width="70px" CausesValidation="False">
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    <telerik:RadButton ID="radbtnShowAll" runat="server" Text="نمایش همگی" OnClick="radbtnShowAll_Click"
                                        Width="70px" CausesValidation="False">
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadButton ID="radbtnBack" runat="server" Text="انصراف" OnClick="radbtnBack_Click"
                                        Width="70px" CausesValidation="False">
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    <telerik:RadButton ID="radbtnClose" runat="server" Text="بازگشت" Width="70px" CausesValidation="False"
                                        OnClientClicked="CloseRadWindow">
                                    </telerik:RadButton>
                                </td>
                                <td>
                                    <cc2:CustomMessageErrorControl ID="MessageErrorControl" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top" style="width: 100%">
                    <table style="width: 100%;" dir="rtl">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel1" runat="server">
                                    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
                                        <tr>
                                            <td class="style1">
                                                نام
                                                <label>
                                                    :</label>
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="txtFirstName" runat="server" Width="150px" Style="margin-right: 0px"
                                                    MaxLength="50">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td class="style1">
                                                <label>
                                                    نام خانوادگی:</label>
                                            </td>
                                            <td dir="rtl">
                                                <telerik:RadTextBox ID="txtLastName" runat="server" Style="margin-right: 0px" Width="150px"
                                                    MaxLength="50">
                                                </telerik:RadTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">
                                                نام کاربری:
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="txtUserName" runat="server" MaxLength="50" Style="margin-right: 0px"
                                                    Width="150px">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td class="style1">
                                                &nbsp;
                                            </td>
                                            <td dir="rtl">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td class="style1">
                                                &nbsp;
                                            </td>
                                            <td dir="rtl">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top" bgcolor="White">
                                <telerik:RadGrid ID="grdPortalUser" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" ShowFooter="True" AutoGenerateColumns="False" CellSpacing="0"
                                    GridLines="None" HorizontalAlign="Center" Skin="Office2007" OnItemCommand="grdPortalUser_ItemCommand"
                                    OnPageIndexChanged="grdPortalUser_PageIndexChanged" OnPageSizeChanged="grdPortalUser_PageSizeChanged"
                                    OnSortCommand="grdPortalUser_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView Dir="RTL" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد" DataKeyNames="UserID"
                                        ClientDataKeyNames="PersonFullName,UserID" GroupsDefaultExpanded="False">
                                        <PagerStyle AlwaysVisible="True"></PagerStyle>
                                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"
                                            Display="False">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام" SortExpression="FirstName">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی"
                                                SortExpression="LastName">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="UserName" HeaderText="نام کاربری" SortExpression="UserName">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column" UniqueName="EditCommandColumn1">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="صفحه اول" HorizontalAlign="Center"
                                            LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی"
                                            PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents OnRowSelected="RowSelected"></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                    <StatusBarSettings LoadingText="بارگذاری..." ReadyText="آماده" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="radbtnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel2" />
                        <telerik:AjaxUpdatedControl ControlID="Panel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdPortalUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radbtnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel2" />
                        <telerik:AjaxUpdatedControl ControlID="Panel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdPortalUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radbtnShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPortalUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radbtnEditSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel2" />
                        <telerik:AjaxUpdatedControl ControlID="Panel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdPortalUser" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radbtnBack">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel2" />
                        <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="cmbCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="cmbCity" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdPortalUser">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="Panel2" />
                        <telerik:AjaxUpdatedControl ControlID="Panel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdPortalUser" />
                        <telerik:AjaxUpdatedControl ControlID="hiddenfieldKeyId" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManagerProxy>
        <asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
        <telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
            <script type="text/javascript">
                function fnPicName() {

                    var ctrl = document.getElementById('<%=hiddenfieldKeyId.ClientID%>');
                    return ctrl.value;
                }
            </script>
        </telerik:RadScriptBlock>
    </div>
    </form>
</body>
</html>
 