﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmPortalUserSearch : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["pagestate"] == null)
            {
                ViewState["pagestate"] = 1;
                ViewState["WhereClause"] = "";
                ViewState["SortExpression"] = "";
                ViewState["SortType"] = "";
                SetGridView(0);
                SetPanelFirst();
                radbtnShowAll.Visible = false;
                ViewState["pagestate"] = "IsPostBack";
            }
        }

        protected void radbtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtFirstName.Text.Trim() != "")
                {
                    ViewState["WhereClause"] += " and FirstName like N'%" + txtFirstName.Text.Trim() + "%' ";
                }
                if (txtLastName.Text.Trim() != "")
                {
                    ViewState["WhereClause"] += " and LastName like N'%" + txtLastName.Text.Trim() + "%' ";
                }
                if (txtUserName.Text.Trim() != "")
                {
                    ViewState["WhereClause"] += " and UserName like N'%" + txtUserName.Text.Trim() + "%' ";
                }

                SetGridView(grdPortalUser.MasterTableView.CurrentPageIndex);
            }
            catch (Exception ex)
            {
                MessageErrorControl.ShowErrorMessage(ex.Message);
            }
        }

        protected void radbtnBack_Click(object sender, EventArgs e)
        {
            SetClearTextBox();
            SetPanelFirst();
        }

        protected void radbtnShowAll_Click(object sender, EventArgs e)
        {
            radbtnShowAll.Visible = false;
            SetClearTextBox();
            SetPanelFirst();
            ViewState["WhereClause"] = "";
            SetGridView(grdPortalUser.MasterTableView.CurrentPageIndex);
        }

        public static string WriteStringForSpecificLength(string str, int length)
        {
            int len = str.Length;
            if (len > length)
                len = length;
            return str.Substring(0, len);
        }

        #region Procedure:

        private void SetGridView(int pageIndex)
        {
            int count;
            grdPortalUser.MasterTableView.CurrentPageIndex = pageIndex;
            DataSet ds = GeneralDB.GetPageDB(grdPortalUser.MasterTableView.PageSize,
                                             grdPortalUser.MasterTableView.CurrentPageIndex, ViewState["WhereClause"].ToString(), ViewState["SortExpression"].ToString(),
                                             "acs.v_PortalUser", "UserId", out count, ViewState["SortType"].ToString());
            grdPortalUser.DataSource = ds.Tables[0];
            grdPortalUser.VirtualItemCount = count;
            grdPortalUser.DataBind();
        }

        public void SetPanelFirst()
        {
            radbtnSearch.Visible = true;
            radbtnBack.Visible = false;
        }

        private void SetClearTextBox()
        {
            txtLastName.Text = "";
            txtFirstName.Text = "";
            txtUserName.Text = "";
        }

        #endregion

        protected void grdPortalUser_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {

        }

        protected void grdPortalUser_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGridView(e.NewPageIndex);
        }

        protected void grdPortalUser_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGridView(grdPortalUser.CurrentPageIndex);
        }

        protected void grdPortalUser_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            ViewState["SortExpression"] = e.SortExpression;
            SetGridView(grdPortalUser.MasterTableView.CurrentPageIndex);
        }
    }
}