﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmSetUserRoles : System.Web.UI.Page
    {

        private ArrayList _searchRoleAndCategorizeIdList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var userAndGroup = AcsUserAndGroup.GetSingleById(userAndGroupId);
                if (userAndGroup.IsUser)
                {
                    lblTitle.Text = "نقش های کاربر < " + userAndGroup.UserAndGroupName + " > ";
                }
                else
                {
                    lblTitle.Text = "نقش های گروه < " + userAndGroup.UserAndGroupName + " > ";
                }
                Session["ItemSearch"] = "";
                LoadTree();
            }
        }

        protected ArrayList GetUserRoleAndCategorizes()
        {
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            var roleAndCategorizeLst = AcsUserAndGroup_RoleAndCategorize.GetAcsUserAndGroup_RoleAndCategorizeCollectionByAcsUserAndGroup(userAndGroupId);
            var roleAndCategorizeArr = new ArrayList();
            foreach (var roleAndCategorize in roleAndCategorizeLst)
            {
                roleAndCategorizeArr.Add(roleAndCategorize.RoleAndCategorizeId);
            }
            return roleAndCategorizeArr;
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            try
            {
                string selectedNodes = "";
                foreach (RadTreeNode node in trvRoleAndCategorize.GetAllNodes())
                {
                    if (node.Checked)
                    {
                        var roleAndCategorizeId = Int32.Parse(node.Value);
                        selectedNodes += (selectedNodes == "") ? roleAndCategorizeId.ToString() : "," + roleAndCategorizeId;
                    }
                }
                AcsUserAndGroup_RoleAndCategorize.UpdateMembersForRoleAndCategorize(userAndGroupId, selectedNodes);

                LoadTree();
                MessageErrorControl1.ShowSuccesMessage("بروز رسانی عضویت انجام گردید");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void LoadTree()
        {
            SetTree();
            SetUserRolesInTree();
        }

        protected void SetUserRolesInTree()
        {
            //--------------- اگر یک کاربر یا گروه در گروه دیگر عضو باشند تمامی نقشهای گروههای بالاتر را دارا میباشند
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            var allParentGroupForCurUserOrGroupArr = new ArrayList();
            var allParentGroupForCurUserOrGroupDt = AcsUserAndGroupTree.GetParentNodes(userAndGroupId);
            foreach (DataRow group in allParentGroupForCurUserOrGroupDt.Rows)
            {
                allParentGroupForCurUserOrGroupArr.Add(Int32.Parse(group["UserAndGroupId"].ToString()));
            }
            Dictionary<int, int> userOrGroupIndirectRoles = new Dictionary<int, int>(); // <RoleAndCategorize,UserAndGroup>  نقش و گروه کاربری دارای این نقش
            foreach (int userAndGroupIdItem in allParentGroupForCurUserOrGroupArr)
            {
                var rolePerUserOrGroupLst = AcsUserAndGroup_RoleAndCategorize.GetAcsUserAndGroup_RoleAndCategorizeCollectionByAcsUserAndGroup(userAndGroupIdItem);
                foreach (var role in rolePerUserOrGroupLst)
                {
                    if (!userOrGroupIndirectRoles.ContainsKey(role.RoleAndCategorizeId))
                    {
                        userOrGroupIndirectRoles.Add(role.RoleAndCategorizeId, userAndGroupIdItem);
                    }
                    
                }
            }

            //--------------- اگر یک کاربر یا گروه در گروه دیگر عضو باشند تمامی نقشهای گروههای بالاتر را دارا میباشند

            var userOrGroupdirectRoles = GetUserRoleAndCategorizes();
            var userOrGroupDirectAndIndirectRolesArr = new ArrayList();
            userOrGroupDirectAndIndirectRolesArr.AddRange(userOrGroupdirectRoles);
            userOrGroupDirectAndIndirectRolesArr.AddRange(userOrGroupIndirectRoles.Keys);
            var allParentRoleForDirectAndIndirectUserOrGroupRole = GetAllParentNodeForUserRoles(userOrGroupDirectAndIndirectRolesArr);
            foreach (RadTreeNode node in trvRoleAndCategorize.GetAllNodes())
            {
                var roleAndCategorizeId = Int32.Parse(node.Value);
                if (allParentRoleForDirectAndIndirectUserOrGroupRole.Contains(roleAndCategorizeId))
                {
                    node.Expanded = true;
                }
                if (userOrGroupIndirectRoles.Keys.Contains(roleAndCategorizeId))
                {
                    var myUserGroup = AcsUserAndGroup.GetSingleById(userOrGroupIndirectRoles[roleAndCategorizeId]);
                    var groupTitle = myUserGroup.UserAndGroupName;
                    node.BackColor = Color.Orange;
                    node.ToolTip = string.Format("گروه کاربری ({0}) این نقش را دارا میباشد", groupTitle);
                }

                if (userOrGroupdirectRoles.Contains(roleAndCategorizeId))
                {
                    var curNodee = node;
                    {
                        curNodee.Checked = true;

                        curNodee.BackColor = Color.SkyBlue;
                    }
                }
            }


        }

        protected void SetTree()
        {
            trvRoleAndCategorize.DataTextField = "RoleAndCategorizeTilte";
            trvRoleAndCategorize.DataValueField = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldID = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldParentID = "RoleAndCategorizeOwnerId";
            trvRoleAndCategorize.DataSource = AcsRoleAndCategorizeTree.GetAllNodes(-1, "", -1);
            trvRoleAndCategorize.DataBind();
            if (this.trvRoleAndCategorize.Nodes.Count != 0 && this.trvRoleAndCategorize.Nodes[0] != null)
            {
                this.trvRoleAndCategorize.Nodes[0].Expanded = true;
            }
        }

        protected ArrayList GetAllParentNodeForUserRoles(ArrayList userRoleAndCategorizesArr)
        {
            var allParentNodes = new ArrayList();
            foreach (int roleAndCategorize in userRoleAndCategorizesArr)
            {
                var parentNodeDt = AcsRoleAndCategorizeTree.GetParentNodes(roleAndCategorize);
                foreach (DataRow dataRow in parentNodeDt.Rows)
                {
                    if (!allParentNodes.Contains(Int32.Parse(dataRow["RoleAndCategorizeId"].ToString())))
                    {
                        allParentNodes.Add(Int32.Parse(dataRow["RoleAndCategorizeId"].ToString()));
                    }
                }
            }
            return allParentNodes;
        }

        protected void trvRoleAndCategorize_NodeDataBound(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            var roleAndCategorizeId = Int32.Parse(e.Node.Value);
            var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleAndCategorizeId);

            if (!myRoleAndCategorize.IsRole)
            {
                e.Node.Checkable = false;
            }
            if (_searchRoleAndCategorizeIdList.Contains(roleAndCategorizeId))
            {
                e.Node.ExpandParentNodes();
                e.Node.Text = e.Node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                // node.Selected = true;
            }

        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = AcsRoleAndCategorizeTree.GetAllNodes(-1, TxtGroupName.Text,-1);
                _searchRoleAndCategorizeIdList = new ArrayList();
                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchRoleAndCategorizeIdList.Add(Int32.Parse(datarow["RoleAndCategorizeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeId"].ToString() + "'";
                }
                LoadTree();

            }
        }
    }
}