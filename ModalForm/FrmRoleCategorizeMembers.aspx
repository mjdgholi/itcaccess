﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmRoleCategorizeMembers.aspx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmRoleCategorizeMembers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="MessageErrorControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title></title>
    </head>
    <body>
        <form id="form1" runat="server">
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
            <table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" colspan="5" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="5" valign="middle">
                        <asp:Label ID="lblTitle" runat="server" Font-Names="Tahoma" Font-Size="9pt" ForeColor="#003399"
                            Text="لیست گروههایی که نقش در آن عضو است" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="10%">
                        <table>
                            <tr>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td>&nbsp; </td>
                                <td class="style1">&nbsp; </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" width="50%">
                        <telerik:RadTreeView ID="trvRoleAndCategorize" runat="server" BorderColor="#999999"
                            BorderStyle="None" CheckBoxes="True" dir="rtl" OnNodeExpand="trvRoleAndCategorize_NodeExpand"
                            Skin="Office2007" Width="99%" TriStateCheckBoxes="False"></telerik:RadTreeView>
                    </td>
                    <td align="right">&nbsp; </td>
                    <td align="right">&nbsp; </td>
                    <td align="right">&nbsp; </td>
                </tr>
                <tr>
                    <td align="right" width="10%" colspan="5">
                        <table width="100%" dir="rtl">
                            <tr>
                                <td width="5%" dir="rtl" valign="top" align="center">&nbsp;</td>
                                <td width="40%" dir="rtl" valign="top" align="right" nowrap="nowrap">
                                    <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                                </td>
                                <td width="4%" align="center">&nbsp;</td>
                                <td width="40%" align="center" valign="top" dir="rtl">&nbsp;</td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="5%" dir="rtl" valign="top" align="center">&nbsp; </td>
                                <td width="40%" dir="rtl" valign="top" align="center">
                                    <asp:Label ID="LblMembers" runat="server" Font-Bold="True" Text="نقش های گروه"></asp:Label>
                                </td>
                                <td width="4%" align="center">&nbsp;</td>
                                <td width="40%" align="center" valign="top" dir="rtl">
                                    <asp:Label ID="LblNotMembers" runat="server" Font-Bold="True" Text="نقش های خارج از گروه"></asp:Label>
                                </td>
                                <td width="5%"></td>
                            </tr>
                            <tr>
                                <td width="5%" rowspan="3" dir="rtl" valign="top" align="center">&nbsp; </td>
                                <td width="40%" rowspan="3" dir="rtl" valign="top" align="center" style="border: 1px solid #000000;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LstMembers"
                                        ErrorMessage="نقش را انخاب نمایید" Font-Names="Tahoma" Font-Size="9pt" ValidationGroup="Member"></asp:RequiredFieldValidator>
                                    <cc1:CustomeRadListBox ID="LstMembers" runat="server" dir="rtl" Width="200px" Skin="Office2007"
                                        ToolTip="Member"></cc1:CustomeRadListBox>
                                    <br />
                                </td>
                                <td width="4%" align="center" valign="top" height="30">
                                    <asp:ImageButton ID="ImgbAddMembers" runat="server" Height="30px" ImageUrl="../Images/Right.png"
                                        ToolTip="افزودن اعضای انتخابی" Width="30px" OnClick="ImgbAddMembers_Click" 
                                        ValidationGroup="NotMember" BorderWidth="2px" />
                                    <br />
                                    <br />
                                    <asp:ImageButton ID="ImgbRemoveMembers" runat="server" Height="30px" ImageUrl="../Images/Left.png"
                                        ToolTip="حذف اعضای انتخابی" Width="30px" OnClick="ImgbRemoveMembers_Click" 
                                        ValidationGroup="Member" BorderWidth="2px" />
                                </td>
                                <td width="40%" rowspan="3" align="center" valign="top" dir="rtl" style="border: 1px solid #000000;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LstNotMembers"
                                        ErrorMessage="نقش را انخاب نمایید" Font-Names="Tahoma" Font-Size="9pt" ValidationGroup="NotMember"></asp:RequiredFieldValidator>
                                    <cc1:CustomeRadListBox ID="LstNotMembers" runat="server" dir="rtl" Width="200px"
                                        Skin="Office2007" OnItemDataBound="LstNotMembers_ItemDataBound" 
                                        ValidationGroup="NotMember" CheckBoxes="True">
                                    </cc1:CustomeRadListBox>
                                </td>
                                <td width="5%" rowspan="3"></td>
                            </tr>
                            <tr>
                                <td width="4%" align="center" valign="top" height="20">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td width="4%" align="center">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="10%">&nbsp; </td>
                    <td align="right" width="50%">&nbsp; </td>
                    <td align="right">&nbsp; </td>
                    <td align="right">&nbsp; </td>
                    <td align="right">&nbsp; </td>
                </tr>
                <tr>
                    <td align="right" colspan="5" valign="top" width="10%">&nbsp;</td>
                </tr>
            </table>
        </form>
    </body>
