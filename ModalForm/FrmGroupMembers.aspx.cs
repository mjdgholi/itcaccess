﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmGroupMembers : Page
    {
        private ArrayList _directChildList = new ArrayList();
        private ArrayList _allChildList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var userAndGroup = AcsUserAndGroup.GetSingleById(userAndGroupId);
                lblTitle.Text = "اعضای گروه < " + userAndGroup.UserAndGroupName + " >";

                _directChildList = AcsUserAndGroupTree.GetDirectChildNodesArray(userAndGroupId, 1);
                _allChildList = AcsUserAndGroupTree.GetChildNodesArray(userAndGroupId);
                ViewState["DirectChildList"] = _directChildList;
                ViewState["AllChildList"] = _allChildList;

                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                SetLstMembers(userAndGroupId);
                SetLstNotMembers(userAndGroupId);
            }
        }

        protected void SetLstMembers(int userAndGroupId)
        {
            var dtMembers = AcsUserAndGroupTree.GetDirectChildNodesTable(userAndGroupId, 1);
            LstMembers.DataTextField = "UserAndGroupName";
            LstMembers.DataValueField = "UserAndGroupId";
            LstMembers.DataSource = dtMembers;
            LstMembers.DataBind();
        }

        protected void SetLstNotMembers(int userAndGroupId)
        {
            var dtMembers = AcsUserAndGroupTree.GetAllNodeExceptDirectChild(userAndGroupId, 1);
            LstNotMembers.DataTextField = "UserAndGroupName";
            LstNotMembers.DataValueField = "UserAndGroupId";
            LstNotMembers.DataSource = dtMembers;
            LstNotMembers.DataBind();
        }    

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void trvUserAndGroup_NodeExpand(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        protected void ImgbAddMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstNotMembers.CheckedItems.Count>0)
                {
                    var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                    _directChildList = (ArrayList)ViewState["DirectChildList"];
                    var directChilds = "";
                    string[] checkedItemStr = LstNotMembers.KeyId.Split(',');
                    foreach (var notMembers in checkedItemStr)
                    {
                        if (!_directChildList.Contains(notMembers))
                        {
                            _directChildList.Add(notMembers);
                        }
                    }
                    
                    foreach (var directChild in _directChildList)
                    {
                        directChilds += (directChilds == "") ? directChild.ToString() : "," + directChild;
                    }
                    AcsUserAndGroupTree.UserOrGroupUpdateGroupMembers(userAndGroupId, directChilds, -1);
                    ViewState["DirectChildList"] = _directChildList;

                    SetLstMembers(userAndGroupId);
                    SetLstNotMembers(userAndGroupId);
                    MessageErrorControl1.ShowSuccesMessage("تخصیص کاربر به گروه با موفقیت انجام گردید");
                }               
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }

        }

        protected void ImgbRemoveMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstMembers.CheckedItems.Count>0)
                {
                    var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                    _directChildList = (ArrayList)ViewState["DirectChildList"];
                    var directChilds = "";
                    string[] checkedItemStr = LstMembers.KeyId.Split(',');
                    foreach (var  members in checkedItemStr)
                    {
                        if (_directChildList.Contains(members))
                        {
                            _directChildList.Remove(members);
                        }
                    }
                    foreach (var directChild in _directChildList)
                    {
                        directChilds += (directChilds == "") ? directChild.ToString() : "," + directChild;
                    }
                    AcsUserAndGroupTree.UserOrGroupUpdateGroupMembers(userAndGroupId, directChilds, -1);
                    ViewState["DirectChildList"] = _directChildList;

                    SetLstMembers(userAndGroupId);
                    SetLstNotMembers(userAndGroupId);
                    MessageErrorControl1.ShowSuccesMessage("حذف کاربر از گروه با موفقیت انجام گردید");
                }                
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void LstNotMembers_ItemDataBound(object sender, RadListBoxItemEventArgs e)
        {
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            _allChildList = AcsUserAndGroupTree.GetChildNodesArray(userAndGroupId);       
            if (_allChildList.Contains(e.Item.Value))
            {
                e.Item.BackColor = Color.Khaki;
                e.Item.ToolTip = "کاربر " + e.Item.Text + " به طور غیر مستقیم عضو گروه میباشد. ";
            }           
        }
    }
}