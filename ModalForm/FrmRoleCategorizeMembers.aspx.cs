﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmRoleCategorizeMembers : System.Web.UI.Page
    {
        private ArrayList _directChildList = new ArrayList();
        private ArrayList _allChildList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var roleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
                var roleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleAndCategorizeId);
                lblTitle.Text = "اعضای گروه < " + roleAndCategorize.RoleAndCategorizeTilte + " >";

                _directChildList = AcsRoleAndCategorizeTree.GetDirectChildNodesArray(roleAndCategorizeId, 1);
                _allChildList = AcsRoleAndCategorizeTree.GetChildNodesArray(roleAndCategorizeId);
                ViewState["DirectChildList"] = _directChildList;
                ViewState["AllChildList"] = _allChildList;

                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                SetLstMembers(roleAndCategorizeId);
                SetLstNotMembers(roleAndCategorizeId);
            }
        }

        protected void SetLstMembers(int roleAndCategorizeId)
        {
            var dtMembers = AcsRoleAndCategorizeTree.GetDirectChildNodesTable(roleAndCategorizeId, 1);
            LstMembers.DataTextField = "RoleAndCategorizeTilte";
            LstMembers.DataValueField = "RoleAndCategorizeId";
            LstMembers.DataSource = dtMembers;
            LstMembers.DataBind();
        }

        protected void SetLstNotMembers(int roleAndCategorizeId)
        {
            var dtMembers = AcsRoleAndCategorizeTree.GetAllNodeExceptDirectChild(roleAndCategorizeId, 1);
            LstNotMembers.DataTextField = "RoleAndCategorizeTilte";
            LstNotMembers.DataValueField = "RoleAndCategorizeId";
            LstNotMembers.DataSource = dtMembers;
            LstNotMembers.DataBind();
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void trvRoleAndCategorize_NodeExpand(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        protected void ImgbAddMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstNotMembers.SelectedItems.Count > 0)
                {
                    var roleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
                    _directChildList = (ArrayList)ViewState["DirectChildList"];
                    var directChilds = "";
                    foreach (RadListBoxItem notMembers in LstNotMembers.SelectedItems)
                    {
                        if (!_directChildList.Contains(notMembers.Value))
                        {
                            _directChildList.Add(notMembers.Value);
                        }
                    }
                    foreach (var directChild in _directChildList)
                    {
                        directChilds += (directChilds == "") ? directChild.ToString() : "," + directChild.ToString();
                    }
                    AcsRoleAndCategorizeTree.RoleOrCategorizeUpdateCategorizeMembers(roleAndCategorizeId, directChilds, -1);
                    ViewState["DirectChildList"] = _directChildList;

                    SetLstMembers(roleAndCategorizeId);
                    SetLstNotMembers(roleAndCategorizeId);
                    MessageErrorControl1.ShowSuccesMessage("تخصیص نقش به گروه با موفقیت انجام گردید");
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }

        }

        protected void ImgbRemoveMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstMembers.SelectedItems.Count > 0)
                {
                    var roleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
                    _directChildList = (ArrayList)ViewState["DirectChildList"];
                    var directChilds = "";
                    foreach (RadListBoxItem members in LstMembers.SelectedItems)
                    {
                        if (_directChildList.Contains(members.Value))
                        {
                            _directChildList.Remove(members.Value);
                        }
                    }
                    foreach (var directChild in _directChildList)
                    {
                        directChilds += (directChilds == "") ? directChild.ToString() : "," + directChild.ToString();
                    }
                    AcsRoleAndCategorizeTree.RoleOrCategorizeUpdateCategorizeMembers(roleAndCategorizeId, directChilds, -1);
                    ViewState["DirectChildList"] = _directChildList;

                    SetLstMembers(roleAndCategorizeId);
                    SetLstNotMembers(roleAndCategorizeId);
                    MessageErrorControl1.ShowSuccesMessage("حذف نقش از گروه با موفقیت انجام گردید");
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void LstNotMembers_ItemDataBound(object sender, RadListBoxItemEventArgs e)
        {
            var roleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
            _allChildList = AcsRoleAndCategorizeTree.GetChildNodesArray(roleAndCategorizeId);
            if (_allChildList.Contains(e.Item.Value))
            {
                e.Item.BackColor = Color.Khaki;
                e.Item.ToolTip = "نقش " + e.Item.Text + " به طور غیر مستقیم عضو گروه میباشد. ";
            }
        }
    }
}