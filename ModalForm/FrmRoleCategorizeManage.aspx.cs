﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmRoleCategorizeManage : System.Web.UI.Page
    {
        private ArrayList _directParentNodeList = new ArrayList();
        private ArrayList _searchGroupIdList = new ArrayList();
        private ArrayList _allChildNodeList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var RoleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
                var RoleAndCategorize = AcsRoleAndCategorize.GetSingleById(RoleAndCategorizeId);
                if (RoleAndCategorize.IsRole)
                {
                    lblTitle.Text = "گروه هایی که نقش < " + RoleAndCategorize.RoleAndCategorizeTilte + " > در آن عضویت دارد";
                }
                else
                {
                    lblTitle.Text = "گروه هایی که گروه < " + RoleAndCategorize.RoleAndCategorizeTilte + " > در آن عضویت دارد";
                }
                _allChildNodeList = AcsRoleAndCategorizeTreeDB.GetChildNodesArray(RoleAndCategorizeId);
                ViewState["AllChildNode"] = _allChildNodeList;
                Session["ItemSearch"] = "";
                LoadTree();
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            var RoleAndCategorizeId = Int32.Parse(Request["WhereClause"]);
            try
            {
                string selectedNodes = "";
                foreach (RadTreeNode node in trvRoleAndCategorize.GetAllNodes())
                {
                    if (node.Checked)
                    {
                        selectedNodes += (selectedNodes == "") ? node.Attributes["RoleAndCategorizeId"] : "," + node.Attributes["RoleAndCategorizeId"];
                    }
                }
                AcsRoleAndCategorizeTree.RoleOrCategorizeUpdateCategorizeMembership(RoleAndCategorizeId, selectedNodes);

                LoadTree();
                MessageErrorControl1.ShowSuccesMessage("بروز رسانی عضویت انجام گردید");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void LoadTree()
        {
            var RoleAndCategorizeIdCurrent = Int32.Parse(Request["WhereClause"]);
            _directParentNodeList = AcsRoleAndCategorizeTree.GetDirectParentNodes(RoleAndCategorizeIdCurrent);
            trvRoleAndCategorize.Nodes.Clear();
            LoadRootNodes(trvRoleAndCategorize, TreeNodeExpandMode.ServerSideCallBack);

            var dataTable = AcsRoleAndCategorizeTree.GetParentNodes(RoleAndCategorizeIdCurrent);
            ExpandFoundedNode(dataTable);

            foreach (int groupId in _searchGroupIdList)
            {
                dataTable = AcsRoleAndCategorizeTree.GetParentNodes(groupId);
                ExpandFoundedNode(dataTable);
            }
        }

        protected void trvRoleAndCategorize_NodeDataBound(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            //var parentNodeList = (ArrayList) ViewState["parentGroupList"];
            //var curNodee = e.Node;
            //if (parentNodeList.Contains(e.Node.Value))
            //{
            //    curNodee.Checked = true;
            //    curNodee.Expanded = true;
            //    curNodee.Selected = true;
            //}
        }

        protected void trvRoleAndCategorize_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            //e.Node.Nodes.Clear();
            PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack, 0);
        }

        protected void LoadRootNodes(Telerik.Web.UI.RadTreeView treeView, TreeNodeExpandMode expandMode)
        {
            var RoleAndCategorizeIdCurrent = Int32.Parse(Request["WhereClause"]);
            var ds = Classes.AcsRoleAndCategorizeTreeDB.AcsRoleAndCategorizeTreeGetGetChildNode(-1, 0);
            var data = ds.Tables[0];

            foreach (DataRow row in data.Rows)
            {
                var node = new RadTreeNode
                {
                    Text = row["RoleAndCategorizeTilte"].ToString(),
                    Value = row["RoleAndCategorizeTreeId"].ToString(),
                    ExpandMode = expandMode,
                    //ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png",

                };
                node.Attributes.Add("RoleAndCategorizeId", row["RoleAndCategorizeId"].ToString());
                node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;
                var RoleAndCategorizeId = row["RoleAndCategorizeId"].ToString();
                if (_directParentNodeList.Contains(RoleAndCategorizeId))
                {
                    // node.Selected = true;
                    node.Checked = true;
                }
                if (_searchGroupIdList.Contains(Int32.Parse(RoleAndCategorizeId)))
                {
                    node.Text = node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                    // node.Selected = true;
                }
                _allChildNodeList = (ArrayList)ViewState["AllChildNode"];
                if (_allChildNodeList.Contains(row["RoleAndCategorizeId"].ToString()))
                {
                    node.Checkable = false;
                }
                if (row["RoleAndCategorizeId"].ToString() == RoleAndCategorizeIdCurrent.ToString()) // خود نود نمی تواند به عنوان پدر خود انتخاب گردد
                {
                    node.Checkable = false;
                    node.BackColor = Color.LightSkyBlue;
                }
                treeView.Nodes.Add(node);
            }
        }

        protected void ExpandFoundedNode(DataTable dataTable)
        {
            foreach (DataRow dr in dataTable.Rows)
            {
                var foundNode = trvRoleAndCategorize.FindNodeByValue(dr["RoleAndCategorizeTreeId"].ToString());
                if (foundNode != null)
                {
                    var radTreeNodeEventArgs = new RadTreeNodeEventArgs(foundNode);
                    trvRoleAndCategorize_NodeExpand(trvRoleAndCategorize, radTreeNodeEventArgs);
                    foundNode.Expanded = true;

                    //string RoleAndCategorizeId = foundNode.Attributes["RoleAndCategorizeId"];
                    //if (_directParentNodeList.Contains(RoleAndCategorizeId))
                    //{
                    //   // foundNode.Selected = true;
                    //    foundNode.Checked = true;
                    //}

                    //if (_searchGroupIdList.Contains(Int32.Parse(RoleAndCategorizeId)))
                    //{
                    //    foundNode.Text = foundNode.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                    //   // foundNode.Selected = true;
                    //}

                }
            }
        }

        public void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode, int isUser)
        {
            var RoleAndCategorizeId = Int32.Parse(e.Node.Attributes["RoleAndCategorizeId"]);
            DataSet ds = Classes.AcsRoleAndCategorizeTreeDB.AcsRoleAndCategorizeTreeGetGetChildNode(RoleAndCategorizeId, isUser);
            DataTable data = ds.Tables[0];
            ArrayList curNodeChildValues = new ArrayList();
            foreach (RadTreeNode node in e.Node.Nodes)
            {
                curNodeChildValues.Add(node.Attributes["RoleAndCategorizeId"]);
            }
            foreach (DataRow row in data.Rows)
            {
                if (!curNodeChildValues.Contains(row["RoleAndCategorizeId"].ToString()))
                {
                    var node = new RadTreeNode
                    {
                        Text = row["RoleAndCategorizeTilte"].ToString(),
                        Value = row["RoleAndCategorizeTreeId"].ToString(),
                        ExpandMode = expandMode,
                        //  ImageUrl = bool.Parse(row["IsUser"].ToString()) ? "../Images/User1.png" : "../Images/Group1.png"
                    };
                    node.Attributes.Add("RoleAndCategorizeId", row["RoleAndCategorizeId"].ToString());
                    node.ExpandMode = Convert.ToInt32(row["ChildrenCount"]) > 0 ? TreeNodeExpandMode.ServerSideCallBack : TreeNodeExpandMode.ClientSide;

                    if (_searchGroupIdList.Contains(Int32.Parse(row["RoleAndCategorizeId"].ToString())))
                    {
                        node.Text = node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                        // node.Selected = true;
                    }
                    _allChildNodeList = (ArrayList)ViewState["AllChildNode"];
                    if (_allChildNodeList.Contains(row["RoleAndCategorizeId"].ToString()))
                    {
                        node.Checkable = false;
                    }
                    node.Checked = _directParentNodeList.Contains(row["RoleAndCategorizeId"].ToString());
                    if (row["RoleAndCategorizeId"].ToString() == Request["WhereClause"]) // خود نود نمی تواند به عنوان پدر خود انتخاب گردد
                    {
                        node.Checkable = false;
                        node.BackColor = Color.LightSkyBlue;
                    }

                    e.Node.Nodes.Add(node);
                }
            }
            e.Node.Expanded = true;
            e.Node.ExpandMode = TreeNodeExpandMode.ClientSide;
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = AcsRoleAndCategorizeTree.GetAllNodes(0, TxtGroupName.Text,-1);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["RoleAndCategorizeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'";
                }
                LoadTree();
            }

        }
    }
}