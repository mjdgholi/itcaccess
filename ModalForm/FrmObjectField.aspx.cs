﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmObjectField : Page
    {
        private int _objectId;

        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            _objectId = Int32.Parse(Request["WhereClause"]);
            var objectTitle = AcsObject.GetSingleById(_objectId).ObjectTitle;
            lblTitle.Text = AcsObject.GetSingleById(_objectId).ObjectTitle;
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbObjectFiledTitle(objectTitle);
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsObjectField.Add(_objectId, CmbObjectFieldTitle.SelectedValue, ChkIsKey.Checked, ChkIsOwner.Checked, DateTime.Now.ToString(), DateTime.Now.ToString(), bool.Parse(CmbIsActive.SelectedValue), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObjectField", "ObjectFieldId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "ObjectFieldId=" + addedId, "ObjectId=" + _objectId);
                var pageSize = GrvObjectField.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");
                GrvObjectField.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void SetCmbObjectFiledTitle(string objectName)
        {
            CmbObjectFieldTitle.Items.Clear();
            CmbObjectFieldTitle.Items.Add(new RadComboBoxItem("", "-1"));
            CmbObjectFieldTitle.DataTextField = "ColumnNameDesc";
            CmbObjectFieldTitle.DataValueField = "ColumnName";


            CmbObjectFieldTitle.DataSource = AcsObject.GetSqlObjectColumn(objectName);
            CmbObjectFieldTitle.DataBind();
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var objectFieldId = Convert.ToInt32(ViewState["ObjectFieldId"].ToString());
                Classes.AcsObjectField.Update(objectFieldId, _objectId, CmbObjectFieldTitle.SelectedValue, ChkIsKey.Checked, ChkIsOwner.Checked, DateTime.Now.ToString(),
                                              DateTime.Now.ToString(), bool.Parse(CmbIsActive.SelectedValue));

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObjectField", "ObjectFieldId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "ObjectFieldId=" + objectFieldId, "ObjectId=" + _objectId);
                var pageSize = GrvObjectField.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvObjectField.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(CmbObjectFieldTitle.SelectedValue))
            {
                ViewState["WhereClause"] += " and FieldTitle like N'%" + CmbObjectFieldTitle.Text + "%'";
            }
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;
            if (whereClause == "")

                whereClause = "ObjectId=" + _objectId;
            else
                whereClause += "and ObjectId=" + _objectId;

            GrvObjectField.MasterTableView.CurrentPageIndex = pageIndex;
            GrvObjectField.DataSource = GeneralDB.GetPageDB(GrvObjectField.MasterTableView.PageSize, pageIndex,
                                                            whereClause, ViewState["sortField"].ToString(),
                                                            "[acs].v_AcsObjectField", "ObjectFieldId", out count,
                                                            ViewState["SortType"].ToString());
            GrvObjectField.MasterTableView.VirtualItemCount = count;
            GrvObjectField.DataBind();
        }

        protected void SetPageControl()
        {
            CmbObjectFieldTitle.ClearSelection();
            ChkIsKey.Checked = false;
            ChkIsOwner.Checked = false;
        }

        protected void GrvObjectField_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var objectFieldId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["ObjectFieldId"] = objectFieldId;
                var mySystem = Classes.AcsObjectField.GetSingleById(objectFieldId);
                if (mySystem != null)
                {
                    CmbObjectFieldTitle.SelectedValue = mySystem.FieldTitle;
                    ChkIsKey.Checked = mySystem.IsKey;
                    ChkIsOwner.Checked = mySystem.IsOwner;
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsObjectField.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvObjectField.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvObjectField_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectField_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvObjectField.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectField_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvObjectField.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectField_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvObjectField_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}