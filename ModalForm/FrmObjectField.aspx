﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmObjectField.aspx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.ModalForm.FrmObjectField" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>
    </telerik:RadScriptManager>
    <div>
        <asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll">
            <table dir="rtl">
                <tr>
                    <td align="right">
                        <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                        </cc1:CustomRadButton>
                    </td>
                    <td align="right">
                        <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                            <Icon PrimaryIconCssClass="rbEdit" />
                        </cc1:CustomRadButton>
                    </td>
                    <td align="right">
                        <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                            OnClick="BtnSearch_Click">
                            <Icon PrimaryIconCssClass="rbSearch" />
                        </cc1:CustomRadButton>
                    </td>
                    <td align="right">
                        <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                            OnClick="BtnShowAll_Click" Empty="" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                            <Icon PrimaryIconCssClass="rbRefresh" />
                        </cc1:CustomRadButton>
                    </td>
                    <td align="right">
                        <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                    </td>
                </tr>
            </table>
            <table dir="rtl" width="100%">
                <tr>
                    <td nowrap="nowrap" width="10%">
                        &nbsp;<asp:Panel ID="pnlControls" runat="server">
                            <table style="text-align: right" width="100%">
                                <tr>
                                    <td align="center" colspan="6" bgcolor="#E8E8E8">
                                        <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Size="14pt" Text="Label"
                                            ForeColor="#000066"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="10%" nowrap="nowrap">
                                        <asp:Label ID="LblName" runat="server" Text="عنوان فیلد شئ:"></asp:Label>
                                    </td>
                                    <td width="30%">
                                        <telerik:RadComboBox ID="CmbObjectFieldTitle" runat="server" Width="500px" AppendDataBoundItems="True"
                                            Filter="Contains" MarkFirstMatch="True" Skin="Office2007">
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="10%">
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="CmbObjectFieldTitle"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                    <td width="10%">
                                        <asp:CheckBox ID="ChkIsKey" runat="server" Text="آیا کلید است؟" />
                                    </td>
                                    <td width="30%">
                                    </td>
                                    <td width="10%">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="10%" nowrap="nowrap">
                                        <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                                    </td>
                                    <td width="30%">
                                        <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" Width="100px"
                                            MarkFirstMatch="True" Filter="Contains">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Selected="True" Text="" Value="-1" />
                                                <telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" />
                                                <telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="10%">
                                        <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                            ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                    <td width="10%" nowrap="nowrap">
                                        <asp:CheckBox ID="ChkIsOwner" runat="server" Text="آیا فیلد در برگیرنده میباشد؟" />
                                    </td>
                                    <td width="30%">
                                        &nbsp;
                                    </td>
                                    <td width="10%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="GrvObjectField" runat="server" AllowPaging="True" AllowSorting="True" 
                AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Office2007"
                OnItemCommand="GrvObjectField_ItemCommand" OnPageIndexChanged="GrvObjectField_PageIndexChanged"
                OnPageSizeChanged="GrvObjectField_PageSizeChanged" OnSortCommand="GrvObjectField_SortCommand"
                Width="95%" OnNeedDataSource="GrvObjectField_NeedDataSource" AllowCustomPaging="True"
                OnItemDataBound="GrvObjectField_ItemDataBound">
                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                </HeaderContextMenu>
                <MasterTableView DataKeyNames="ObjectFieldId" Dir="RTL" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد"
                    ClientDataKeyNames="ObjectFieldId">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="ColumnNameDesc" FilterControlAltText="Filter ColumnNameDesc column"
                            HeaderText="عنوان فیلد شئ" SortExpression="ColumnNameDesc" UniqueName="ColumnNameDesc">
                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsKeyText" FilterControlAltText="Filter IsKeyText column"
                            HeaderText="آیا کلید است" SortExpression="IsKeyText" UniqueName="IsKeyText">
                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsOwnerText" FilterControlAltText="Filter IsOwnerText column"
                            HeaderText="آیا فیلد دربرگیرنده است" SortExpression="IsOwnerText" UniqueName="IsOwnerText">
                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsActiveText" FilterControlAltText="Filter IsActiveText column"
                            HeaderText="وضعیت رکورد" SortExpression="IsActiveText" UniqueName="IsActiveText">
                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
                            UniqueName="Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectFieldId").ToString() %>'
                                    CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                            UniqueName="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectFieldId").ToString() %>'
                                    CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                    OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                        NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                        PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                        VerticalAlign="Middle" HorizontalAlign="Center" />
                </MasterTableView><HeaderStyle Height="40px" />
                <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                    <Selecting AllowRowSelect="True"></Selecting>
                </ClientSettings>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
