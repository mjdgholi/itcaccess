﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.ModalForm
{
    public partial class FrmSetRowAccessForUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (ViewState["PageStatus"] == null)
            {
                //  SelectControlPerson.WhereClause = "";
                if (Request["WhereClause"] != null)
                {
                    var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                    var myUser = AcsUserAndGroup.GetSingleById(userAndGroupId);
                    LblTitle.Text = "مدیریت دسترسی های کاربر " + myUser.UserAndGroupName;
                    ViewState["PageStatus"] = "IsPostBack";
                    SetCmbSystem();
                    SetCmbObject(-1);
                    SetCmbAccessType();
                    BtnEdit.Visible = false;
                    ViewState["PageStatus"] = "IsPostBack";
                }
            }
            if (!IsPostBack)
            {

            }
        }

        protected void SetGrvRowAccess()
        {
            int objectId = -1;
            if (CmbObject.SelectedIndex > 0)
            {
                objectId = Int32.Parse(CmbObject.SelectedValue);
            }

            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            GrvRowAccess.DataSource = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerUser(userAndGroupId, objectId);
        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataBind();
        }

        protected void SetCmbObject(int systemId)
        {
            CmbObject.Items.Clear();
            CmbObject.Items.Add(new RadComboBoxItem(""));
            CmbObject.DataTextField = "ObjectTitle";
            CmbObject.DataValueField = "ObjectId";
            CmbObject.DataSource = AcsObject.GetAllPerSystemDs(systemId);
            CmbObject.DataBind();
        }

        protected void SetCmbAccessType()
        {
            CmbAccessType.Items.Clear();
            CmbAccessType.Items.Add(new RadComboBoxItem(""));
            CmbAccessType.DataTextField = "AccessTypeTitle";
            CmbAccessType.DataValueField = "AccessTypeId";
            CmbAccessType.DataSource = AcsAccessType.GetAll();
            CmbAccessType.DataBind();
        }

        protected void SetCmbObjectRowAccess(int objectId, int userId)
        {
            CmbObjectRowAccess.Items.Clear();
            CmbObjectRowAccess.DataTextField = "AccessTitle";
            CmbObjectRowAccess.DataValueField = "RowAccessId";
            CmbObjectRowAccess.DataSource = AcsRowAccess.GetRowAccessPerObjectUserHasNotIt(objectId, userId);
            CmbObjectRowAccess.DataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                int accessTypeId = Int32.Parse(CmbAccessType.SelectedValue);
                var rowAccessId = "";
                foreach (RadComboBoxItem radComboBoxItem in CmbObjectRowAccess.CheckedItems)
                {
                    rowAccessId += (rowAccessId == "") ? radComboBoxItem.Value : "," + radComboBoxItem.Value;
                }
                AcsUserAndGroup_RoleAndCategorize_RowAccess.AddAcsUserAndGroup_RoleAndCategorize_RowAccessGroupDB(userAndGroupId, -1, accessTypeId, rowAccessId,
                                                                                                                  bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                SetPageControl();
                GrvRowAccess.Rebind();
                MessageErrorControl2.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl2.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var acsUserAndGroupRoleAndCategorizeRowAccessId = Convert.ToInt32(ViewState["UserAndGroup_Role_AccessId"].ToString());
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var accessTypeId = Int32.Parse(CmbAccessType.SelectedValue);
                var rowAccessId = Int32.Parse(CmbObjectRowAccess.SelectedValue);
                Classes.AcsUserAndGroup_RoleAndCategorize_RowAccess.Update(acsUserAndGroupRoleAndCategorizeRowAccessId, userAndGroupId, -1, accessTypeId, rowAccessId,
                                                                           bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());
                GrvRowAccess.Rebind();
                SetPageControl();
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                MessageErrorControl2.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl2.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            GrvRowAccess.Rebind();
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            GrvRowAccess.Rebind();
            SetPageControl();
        }

        protected DataTable SetDetailTable(int userId, int objectId)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerUserPerObject(userId, objectId);
        }

        protected void GrvRowAccess_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

            GridDataItem dataItem = (GridDataItem) e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "RowAccessDetail":
                    {
                        string objectId = dataItem.GetDataKeyValue("ObjectId").ToString();
                        var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                        e.DetailTableView.DataSource = SetDetailTable(userAndGroupId, Int32.Parse(objectId));
                        break;
                    }
            }
        }

        protected void GrvObjectAccess_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyDeleteAllRowAccessPerObjectPerUser")
            {
                try
                { 
                    var objectId = Int32.Parse(e.CommandArgument.ToString());
                    var myObject = Classes.AcsObject.GetSingleById(objectId);
                    var systemId = myObject.SystemId;
                    CmbSystem.SelectedValue = systemId.ToString();
                    SetCmbObject(systemId);
                    CmbObject.SelectedValue = objectId.ToString();
                  
                    var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                    AcsUserAndGroup_RoleAndCategorize_RowAccess.DeleteAcsUserAndGroup_RowAccessPerObjectPerUser(objectId, userAndGroupId);
                    GrvRowAccess.Rebind();
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    SetPageControl();

                    MessageErrorControl2.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl2.ShowErrorMessage(ex.Message);
                }
            }
            if (e.CommandName == "_MyEditUserAndGroup_Role_Access")
            {
                if (e.Item.Cells[4].Text == "مستقيم")
                {
                    e.Item.Selected = true;
                    var userAndGroupRoleAccessId = Int32.Parse(e.CommandArgument.ToString());
                    ViewState["UserAndGroup_Role_AccessId"] = userAndGroupRoleAccessId;
                    var myAndGroupRoleAccess = Classes.AcsUserAndGroup_RoleAndCategorize_RowAccess.GetSingleById(userAndGroupRoleAccessId);
                    if (myAndGroupRoleAccess != null)
                    {
                        CmbAccessType.SelectedValue = myAndGroupRoleAccess.AccessTypeId.ToString();
                        CmbIsActive.SelectedValue = myAndGroupRoleAccess.IsActive.ToString();

                        var objectId = AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetObjectPerRowAccess(myAndGroupRoleAccess.RowAccessId);
                        CmbObject.SelectedValue = objectId.ToString();
                        SetCmbObjectRowAccess(objectId, -1);
                        CmbObjectRowAccess.CheckBoxes = false;
                        CmbObjectRowAccess.SelectedValue = myAndGroupRoleAccess.RowAccessId.ToString();

                        BtnEdit.Visible = true;
                        BtnAdd.Visible = false;
                    }
                }
                else
                {
                    MessageErrorControl2.ShowWarningMessage("امکان ویرایش دسترسی وجود ندارد - دسترسی از طریق نقش احراز گردیده است");
                }
            }
            if (e.CommandName == "_MyDeleteUserAndGroup_Role_Access")
            {
                try
                {
                    if (e.Item.Cells[4].Text == "مستقيم")
                    {
                        AcsUserAndGroup_RoleAndCategorize_RowAccess.Delete(Convert.ToInt32(e.CommandArgument));
                        GrvRowAccess.Rebind();
                        BtnEdit.Visible = false;
                        BtnAdd.Visible = true;
                        SetPageControl();

                        MessageErrorControl2.ShowSuccesMessage("حذف موفق");
                    }
                    else
                    {
                        MessageErrorControl2.ShowWarningMessage("امکان حذف دسترسی وجود ندارد - دسترسی از طریق نقش احراز گردیده است");
                    }
                }
                catch (Exception ex)
                {
                    MessageErrorControl2.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvObjectAccess_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "MasterTable")
            {

                var item = (GridDataItem) e.Item;
                var objectId = Int32.Parse(item.GetDataKeyValue("ObjectId").ToString());
                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var rowAccessDt = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerUserPerObject(userAndGroupId, objectId);
                string userAndGroupRoleAccessId = "";
                foreach (DataRow datarow in rowAccessDt.Rows)
                {
                    userAndGroupRoleAccessId += (userAndGroupRoleAccessId == "") ? datarow["UserAndGroup_Role_AccessId"].ToString() : "," + datarow["UserAndGroup_Role_AccessId"].ToString();
                }

                var mySelectControl = (ITC.Library.Controls.SelectControl) item["ResultPerObjectPerRole"].FindControl("SelectControlResultPerObjectPerRole");
                mySelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessId;

            }
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "RowAccessDetail")
            {
                var parentItem = e.Item.OwnerTableView.ParentItem;
                var objectId = Int32.Parse(parentItem.GetDataKeyValue("ObjectId").ToString());

                var userAndGroupId = Int32.Parse(Request["WhereClause"]);
                var rowAccessDt = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerUserPerObject(userAndGroupId, objectId);
                string userAndGroupRoleAccessStrId = "";
                foreach (DataRow datarow in rowAccessDt.Rows)
                {
                    userAndGroupRoleAccessStrId += (userAndGroupRoleAccessStrId == "") ? datarow["UserAndGroup_Role_AccessId"].ToString() : "," + datarow["UserAndGroup_Role_AccessId"].ToString();
                }
                var myParentSelectControl = (ITC.Library.Controls.SelectControl) parentItem["ResultPerObjectPerRole"].FindControl("SelectControlResultPerObjectPerRole");
                myParentSelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessStrId;



                var childItem = (GridDataItem) e.Item;
                var userAndGroupRoleAccessId = Int32.Parse(childItem.GetDataKeyValue("UserAndGroup_Role_AccessId").ToString());

                var mySelectControl = (ITC.Library.Controls.SelectControl) childItem["ResultPerRolePerRowAccess"].FindControl("SelectControlResultPerRolePerRowAccess");
                mySelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessId;
            }
        }

        protected void CmbObject_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var userAndGroupId = Int32.Parse(Request["WhereClause"]);
            if (CmbObject.SelectedValue != null && CmbObject.SelectedIndex > 0)
            {
                int objectId = Int32.Parse(CmbObject.SelectedValue);
                SetCmbObjectRowAccess(objectId, userAndGroupId);
            }
            // SelectControlPerson.WhereClause = "";
        }

        protected void GrvRowAccess_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                SetGrvRowAccess();
            }
        }

        protected void SetPageControl()
        {
            CmbObject.ClearSelection();
            CmbObjectRowAccess.ClearSelection();
            CmbIsActive.ClearSelection();
            CmbAccessType.ClearSelection();
            CmbObjectRowAccess.CheckBoxes = true;
            CmbObjectRowAccess.ClearCheckedItems();
            // SelectControlPerson.WhereClause = "";
        }

        protected void CmbObjectRowAccess_ItemChecked(object sender, RadComboBoxItemEventArgs e)
        {
            string rowAccessStr = "";
            foreach (var rowAccessItem in CmbObjectRowAccess.CheckedItems)
            {
                rowAccessStr += (rowAccessStr == "") ? rowAccessItem.Value : "," + rowAccessItem.Value;
            }
            int objectId = Int32.Parse(CmbObject.SelectedValue);
            // SelectControlPerson.WhereClause = objectId + "_" + rowAccessStr;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string rowAccessIdStr = "";
            if (CmbObjectRowAccess.CheckedItems.Count > 0)
            {
                foreach (var checkedItem in CmbObjectRowAccess.CheckedItems)
                {
                    rowAccessIdStr += (rowAccessIdStr == "") ? checkedItem.Value : "," + checkedItem.Value;
                }

                int objectId = AcsRowAccess.GetObjectPerRowAccess(Int32.Parse(CmbObjectRowAccess.CheckedItems[0].Value));
                var queryString = objectId + "_" + rowAccessIdStr;
                RadWindow newWindow = new RadWindow();
                string modalPath = Path.Combine(Intranet.Configuration.Settings.PortalSettings.FullPortalPath, @"DeskTopModules\ItcAccess\ModalForm\FrmRunSqlQueryForRowAccess.aspx" + "?WhereClause=" + queryString);
                newWindow.NavigateUrl = modalPath;
                //  newWindow.Top = Unit.Pixel(22);
                newWindow.VisibleOnPageLoad = true;
                // newWindow.Left = Unit.Pixel(0);
                newWindow.DestroyOnClose = true;
                newWindow.Modal = true;
                RadWindowManager1.Windows.Add(newWindow);
            }

        }
    
        protected void CmbSystem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int systemId = -1;
            if (CmbSystem.SelectedIndex > 0)
            {
                systemId = Int32.Parse(CmbSystem.SelectedValue);
            }
            SetCmbObject(systemId); 
        }
    }
}