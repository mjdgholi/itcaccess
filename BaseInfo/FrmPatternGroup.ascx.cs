﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmPatternGroup : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsPatternGroup.Add(TxtName.Text,bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsPatternGroup", "PatternGroupId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "PatternGroupId=" + addedId, "");
                var pageSize = GrvPatternGroup.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvPatternGroup.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var PatternGroupId = Convert.ToInt32(ViewState["PatternGroupId"].ToString());
                Classes.AcsPatternGroup.Update(PatternGroupId, TxtName.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsPatternGroup", "PatternGroupId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "PatternGroupId=" + PatternGroupId, "");
                var pageSize = GrvPatternGroup.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvPatternGroup.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and PatternGroupTitle like N'%" + TxtName.Text + "%'";
            }
           
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvPatternGroup.MasterTableView.CurrentPageIndex = pageIndex;
            GrvPatternGroup.DataSource = GeneralDB.GetPageDB(GrvPatternGroup.MasterTableView.PageSize, pageIndex,
                                                            whereClause, ViewState["sortField"].ToString(),
                                                            "[acs].v_AcsPatternGroup", "PatternGroupId", out count,
                                                            ViewState["SortType"].ToString());
            GrvPatternGroup.MasterTableView.VirtualItemCount = count;
            GrvPatternGroup.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvPatternGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var PatternGroupId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["PatternGroupId"] = PatternGroupId;
                var myPatternGroup = Classes.AcsPatternGroup.GetSingleById(PatternGroupId);
                if (myPatternGroup != null)
                {
                    TxtName.Text = myPatternGroup.PatternGroupTitle;                 
                    CmbIsActive.SelectedValue = myPatternGroup.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsPatternGroup.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvPatternGroup.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvPatternGroup_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPatternGroup_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvPatternGroup.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPatternGroup_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvPatternGroup.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPatternGroup_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvPatternGroup_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}