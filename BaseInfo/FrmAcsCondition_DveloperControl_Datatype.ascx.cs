﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmAcsCondition_Datatype_DveloperControl : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbDveloperControl();
                SetCmbDataType();
                SetCmbCondition();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsCondition_Datatype_DveloperControl.Add(Int32.Parse(CmbCondition.SelectedValue), Int32.Parse(CmbDataType.SelectedValue),ChkIsHierarchy.Checked,
                                                  Int32.Parse(CmbDveloperControl.SelectedValue), bool.Parse(CmbIsActive.SelectedValue),
                                                  DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition_Datatype_DveloperControl", "Condition_Datatype_DveloperControlId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "Condition_Datatype_DveloperControlId=" + addedId, "");
                var pageSize = GrvAcsCondition_Datatype_DveloperControl.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvAcsCondition_Datatype_DveloperControl.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var conditionDatatypeDveloperControlId = Convert.ToInt32(ViewState["Condition_Datatype_DveloperControlId"].ToString());
                Classes.AcsCondition_Datatype_DveloperControl.Update(conditionDatatypeDveloperControlId, Int32.Parse(CmbCondition.SelectedValue), Int32.Parse(CmbDataType.SelectedValue),ChkIsHierarchy.Checked,
                                                             Int32.Parse(CmbDveloperControl.SelectedValue), bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition_Datatype_DveloperControl", "Condition_Datatype_DveloperControlId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "Condition_Datatype_DveloperControlId=" + conditionDatatypeDveloperControlId, "");
                var pageSize = GrvAcsCondition_Datatype_DveloperControl.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvAcsCondition_Datatype_DveloperControl.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (CmbCondition.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and ConditionId  =" + CmbCondition.SelectedValue;
            }

            if (CmbDataType.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and DataTypeId  =" + CmbDataType.SelectedValue;
            }

            if (CmbDveloperControl.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and DveloperControlId =" + CmbDveloperControl.SelectedValue;
            }

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            if (ChkIsHierarchy.Checked)
            {
                ViewState["WhereClause"] += " and IsHierarchy=1 ";
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetCmbCondition()
        {
            CmbCondition.Items.Clear();
            CmbCondition.Items.Add(new RadComboBoxItem("", "-1"));
            CmbCondition.DataTextField = "FullTitle";
            CmbCondition.DataValueField = "ConditionId";
            CmbCondition.DataSource = AcsCondition.GetAllAcsCondition();
            CmbCondition.DataBind();
        }

        protected void SetCmbDataType()
        {
            CmbDataType.Items.Clear();
            CmbDataType.Items.Add(new RadComboBoxItem("", "-1"));
            CmbDataType.DataTextField = "DataTypeTitleFullText";
            CmbDataType.DataValueField = "DataTypeId";
            CmbDataType.DataSource = AcsDataType.GetAllDataTypeDs();
            CmbDataType.DataBind();
        }

        protected void SetCmbDveloperControl()
        {            
            CmbDveloperControl.Items.Clear();
            CmbDveloperControl.Items.Add(new RadComboBoxItem(""));
            CmbDveloperControl.DataTextField = "DveloperControlTitle";
            CmbDveloperControl.DataValueField = "DveloperControlId";
            CmbDveloperControl.DataSource = AcsDveloperControl.GetAll();
            CmbDveloperControl.DataBind();
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvAcsCondition_Datatype_DveloperControl.MasterTableView.CurrentPageIndex = pageIndex;
            GrvAcsCondition_Datatype_DveloperControl.DataSource = GeneralDB.GetPageDB(GrvAcsCondition_Datatype_DveloperControl.MasterTableView.PageSize, pageIndex,
                                                                           whereClause, ViewState["sortField"].ToString(),
                                                                           "[acs].v_AcsCondition_Datatype_DveloperControl", "Condition_Datatype_DveloperControlId", out count,
                                                                           ViewState["SortType"].ToString());
            GrvAcsCondition_Datatype_DveloperControl.MasterTableView.VirtualItemCount = count;
            GrvAcsCondition_Datatype_DveloperControl.DataBind();
        }

        protected void SetPageControl()
        {
            CmbCondition.ClearSelection();
            CmbDveloperControl.ClearSelection();
            CmbDataType.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
            ChkIsHierarchy.Checked = false;
        }

        protected void GrvAcsCondition_Datatype_DveloperControl_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var conditionDatatypeDveloperControlId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["Condition_Datatype_DveloperControlId"] = conditionDatatypeDveloperControlId;
                var mySystem = Classes.AcsCondition_Datatype_DveloperControl.GetSingleById(conditionDatatypeDveloperControlId);
                if (mySystem != null)
                {

                    CmbCondition.SelectedValue = mySystem.ConditionId.ToString();
                    CmbDveloperControl.SelectedValue = mySystem.DveloperControlId.ToString();
                    CmbDataType.SelectedValue = mySystem.DataTypeId.ToString();
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();
                    ChkIsHierarchy.Checked = mySystem.IsHierarchy;
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsCondition_Datatype_DveloperControl.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvAcsCondition_Datatype_DveloperControl.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvAcsCondition_Datatype_DveloperControl_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAcsCondition_Datatype_DveloperControl_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvAcsCondition_Datatype_DveloperControl.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAcsCondition_Datatype_DveloperControl_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvAcsCondition_Datatype_DveloperControl.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAcsCondition_Datatype_DveloperControl_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvAcsCondition_Datatype_DveloperControl_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}