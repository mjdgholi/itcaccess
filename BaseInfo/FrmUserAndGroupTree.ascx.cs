﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmUserAndGroupTree : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["PageStatus"] == null)
            {
                LoadRadTreeView.LoadRootNodes(this.trvUserAndGroup, TreeNodeExpandMode.ServerSideCallBack);
                ExpandFoundedNode(27);
                ViewState["PageStatus"] = "IsPostBack";
                RadTreeView tree = (RadTreeView) CmbGroupOwner.Items[0].FindControl("trvCmbUserAndGroup");
                LoadRadTreeView.LoadRootNodes(tree, TreeNodeExpandMode.ServerSideCallBack);
                SetPageControlForAdd();

            }
        }

        protected void trvUserAndGroup_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.Nodes.Clear();
            var selectedNodes = new ArrayList();
            if (Session["SelectedNodes"] != null) selectedNodes = (ArrayList) Session["SelectedNodes"];
            LoadRadTreeView.PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack, -1, selectedNodes);
        }

        protected void trvCmbUserAndGroup_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.Nodes.Clear();
            var selectedNodes = new ArrayList();
            if (Session["SelectedNodes"] != null) selectedNodes = (ArrayList) Session["SelectedNodes"];
            LoadRadTreeView.PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack, 0, selectedNodes); // فقط گروهها آورده شود
        }

        protected void trvUserAndGroup_NodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            try
            {
                RadTreeNode nodeEdited = e.Node;
                var newText = e.Text;
                var userAndGroupTreeId = Int32.Parse(e.Node.Value);

                var userAndGroupTree = AcsUserAndGroupTree.GetSingleById(userAndGroupTreeId);
                if (userAndGroupTree != null)
                {
                    var userAndGroupId = userAndGroupTree.UserAndGroupId;
                    AcsUserAndGroupTree.UpdateUserOrGroupName(userAndGroupId, newText);
                    nodeEdited.Text = newText;
                }
            }
            catch (Exception)
            {
                MessageErrorControl1.ShowErrorMessage("خطا در ثبت");
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int _AddedId;
                var userAndGroupOwnerId = -1;
                int userAndGroupOwnerTreeId = (String.IsNullOrEmpty(CmbGroupOwner.SelectedValue)) ? -1 : Int32.Parse(CmbGroupOwner.SelectedValue);
                int userAndGroupId = Int32.Parse(TxtUserAndGroupName.KeyId);               
              
                if (userAndGroupOwnerTreeId != -1)
                {
                    var userAndGroupTree = AcsUserAndGroupTree.GetSingleById(userAndGroupOwnerTreeId);
                    userAndGroupOwnerId = userAndGroupTree.UserAndGroupId;
                }
                Classes.AcsUserAndGroupTree.Add(userAndGroupOwnerId,userAndGroupId,true,DateTime.Now.ToString(),DateTime.Now.ToString(),out _AddedId);
                ExpandFoundedNode(userAndGroupId);
                

                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void ExpandFoundedNode(int userAndGroupId)
        {
            DataTable dataTable = AcsUserAndGroupTreeDB.GetParentNodes(userAndGroupId);
            ArrayList allNodeForUserAndGroupList = AcsUserAndGroupTree.GetAllNodeForUserAndGroup(userAndGroupId);
            Session["SelectedNodes"] = allNodeForUserAndGroupList;

            trvUserAndGroup.Nodes.Clear();
            LoadRadTreeView.LoadRootNodes(trvUserAndGroup, TreeNodeExpandMode.ServerSideCallBack);

            foreach (DataRow dr in dataTable.Rows)
            {
                RadTreeNode foundNode = trvUserAndGroup.FindNodeByValue(dr["UserAndGroupTreeId"].ToString());
                if (foundNode != null)
                {
                    RadTreeNodeEventArgs radTreeNodeEventArgs = new RadTreeNodeEventArgs(foundNode);
                    trvUserAndGroup_NodeExpand(trvUserAndGroup, radTreeNodeEventArgs);
                    foundNode.Expanded = true;
                }                                                                   
            }
        }

        protected void SetPageControlForEdit()
        {
            BtnEdit.Visible = true;
            BtnDelete.Visible = true;
            BtnAdd.Visible = false;
        }

        protected void SetPageControlForAdd()
        {
            BtnEdit.Visible = false;
            BtnDelete.Visible = false;
            BtnAdd.Visible = true;

        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                SetPageControlForAdd();
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void trvUserAndGroup_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value == "remove")
                {
                    var userAndGroupTreeId = Int32.Parse(e.Node.Value);
                    var userAndGroupTree = AcsUserAndGroupTree.GetSingleById(userAndGroupTreeId);
                    var userAndGroupOwnerId = userAndGroupTree.UserAndGroupOwnerId;
                    AcsUserAndGroupTree.Delete(userAndGroupTreeId);
                    clickedNode.Visible = false;
                }
            }
            catch (Exception)
            {

            }
        }

        protected void trvUserAndGroup_NodeDrop(object sender, RadTreeNodeDragDropEventArgs e)
        {
            try
            {
                RadTreeNode sourceNode = e.SourceDragNode;
                var userAndGroupTreeSource = AcsUserAndGroupTree.GetSingleById(Int32.Parse(sourceNode.Value));
                int userAndGroupIdSource = userAndGroupTreeSource.UserAndGroupId;
                int userAndGroupIdDes;
                if (e.DestDragNode != null)
                {
                    RadTreeNode destNode = e.DestDragNode;
                    var userAndGroupTreeDes = AcsUserAndGroupTree.GetSingleById(Int32.Parse(destNode.Value));
                    userAndGroupIdDes = userAndGroupTreeDes.UserAndGroupId;
                }
                else
                {
                    userAndGroupIdDes = -1;
                }
                Classes.AcsUserAndGroupTree.ChangeUserAndGroupTreeOwner(Convert.ToInt32(e.SourceDragNode.Value), userAndGroupIdDes);
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                trvUserAndGroup.Nodes.Clear();
                ExpandFoundedNode(userAndGroupIdSource);
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);

            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            if (TxtUserAndGroupName.KeyId!="")
            {
                
            }
        }
    }
}