﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRowAccess.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRowAccess" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll" BorderColor="White"
    BorderWidth="2px">
    <table dir="rtl">
        <tr>
            <td align="right">
                <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" ValidationGroup="RowAccess">
                </cc1:CustomRadButton>
            </td>
            <td align="right">
                <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click"
                    ValidationGroup="RowAccess">
                    <Icon PrimaryIconCssClass="rbEdit" />
                </cc1:CustomRadButton>
            </td>
            <td align="right">
                <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                    OnClick="BtnSearch_Click">
                    <Icon PrimaryIconCssClass="rbSearch" />
                </cc1:CustomRadButton>
            </td>
            <td align="right">
                <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                    OnClick="BtnShowAll_Click" Empty="" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                    <Icon PrimaryIconCssClass="rbRefresh" />
                </cc1:CustomRadButton>
            </td>
            <td align="right">
                <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <table dir="rtl" width="100%">
        <tr>
            <td nowrap="nowrap" width="10%">
                <asp:Panel ID="pnlControls" runat="server">
                    <table style="text-align: right" width="100%">
                        <tr>
                            <td align="right" nowrap="nowrap" width="10%">
                                <asp:Label ID="LblSystem" runat="server" Text="عنوان سیستم:"></asp:Label>
                            </td>
                            <td width="30%">
                                <cc1:CustomRadComboBox ID="CmbSystem" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    Filter="Contains" MarkFirstMatch="True" NoWrap="True" 
                                    onselectedindexchanged="CmbSystem_SelectedIndexChanged" Skin="Office2007" 
                                    Width="500px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="60%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap" width="10%">
                                <asp:Label ID="LblObject" runat="server" Text="عنوان آبجکت:"></asp:Label>
                            </td>
                            <td width="30%">
                                <cc1:CustomRadComboBox ID="CmbObject" runat="server" 
                                    AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True" 
                                    NoWrap="True" Skin="Office2007" Width="500px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="60%">
                                <asp:RequiredFieldValidator ID="rfvObject" runat="server" 
                                    ControlToValidate="CmbObject" ErrorMessage="*" ValidationGroup="RowAccess"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="10%" nowrap="nowrap">
                                <asp:Label ID="LblAccessTitle" runat="server" Text="عنوان دسترسی موضوعی:"></asp:Label>
                            </td>
                            <td width="30%">
                                <telerik:RadTextBox ID="TxtName" runat="server" Skin="Office2007" 
                                    ValidationGroup="RowAccess" Width="300px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="60%">
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                                    ControlToValidate="TxtName" ErrorMessage="*" ValidationGroup="RowAccess"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap" width="10%">
                                <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                            </td>
                            <td width="30%">
                                <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" Width="300px"
                                    ValidationGroup="RowAccess" Filter="Contains" MarkFirstMatch="True">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Selected="True" Text="" Value="-1" />
                                        <telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                    ErrorMessage="*" InitialValue="" ValidationGroup="RowAccess"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap" valign="top" width="10%">
                                <asp:Label ID="LblSqlRowAccess" runat="server" Text="عبارت شرطی:"></asp:Label>
                            </td>
                            <td colspan="2" width="30%" dir="ltr" align="right">
                                <telerik:RadTextBox ID="TxtSqlRowAccess" runat="server" Enabled="True" Height="50px"
                                    ReadOnly="True" TextMode="MultiLine" Width="500">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="GrvRowAccess" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Office2007" OnItemCommand="GrvRowAccess_ItemCommand"
        OnPageIndexChanged="GrvRowAccess_PageIndexChanged" OnPageSizeChanged="GrvRowAccess_PageSizeChanged"
        OnSortCommand="GrvRowAccess_SortCommand" Width="100%" OnNeedDataSource="GrvRowAccess_NeedDataSource"
        AllowCustomPaging="True" OnItemDataBound="GrvRowAccess_ItemDataBound">
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
        <MasterTableView DataKeyNames="RowAccessId" Dir="RTL" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد"
            ClientDataKeyNames="RowAccessId">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="AccessTitle" FilterControlAltText="Filter AccessTitle column"
                    HeaderText="عنوان دسترسی موضوعی" SortExpression="AccessTitle" UniqueName="AccessTitle">
                    <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="عبارت شرطی"
                    UniqueName="ConditionalExpression">
                    <ItemTemplate>
                        <asp:TextBox ID="ImgButton3" Style="direction: ltr" BackColor="#C9E8F8" TextMode="MultiLine"
                            Height="30" Enabled="True" ReadOnly="True" runat="server" Width="300" CausesValidation="false"
                            Text='<%#Eval("ConditionalExpression").ToString() %>' /></ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" BorderWidth="1px" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="IsActiveText" FilterControlAltText="Filter IsActiveText column"
                    HeaderText="وضعیت رکورد" SortExpression="AccessTitle" UniqueName="IsActiveText">
                    <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ثبت جزئیات دسترسی"
                    UniqueName="Other1">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgButt232on3" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RowAccessId").ToString() %>'
                            CommandName="Other" ForeColor="#000066" ImageUrl="../Images/access2.png" /></ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
                    UniqueName="Edit">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RowAccessId").ToString() %>'
                            CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                    UniqueName="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RowAccessId").ToString() %>'
                            CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                            OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه"
                    UniqueName="ShowAll">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RowAccessId").ToString() %>'
                            CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../Images/ShowAllInGrid.png" /></ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                VerticalAlign="Middle" HorizontalAlign="Center" />
        </MasterTableView><HeaderStyle Height="40px" />
        <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>
</asp:Panel>
<asp:Panel runat="server" ID="pnlAccessDetail">
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" Height="100%" OnPageViewCreated="RadMultiPage1_PageViewCreated1"
        Width="100%" />
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAccessDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAccessDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAccessDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAccessDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvRowAccess">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAccessDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
