﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmAccessType : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsAccessType.Add(TxtAccessTypeTitle.Text, bool.Parse(CmbHasAccess.SelectedValue), bool.Parse(CmbIsDeniedAccess.SelectedValue),
                                  bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(),
                                  out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsAccessType", "AccessTypeId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "AccessTypeId=" + addedId, "");
                var pageSize = GrvAccessType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvAccessType.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var accessTypeId = Convert.ToInt32(ViewState["AccessTypeId"].ToString());
                Classes.AcsAccessType.Update(accessTypeId, TxtAccessTypeTitle.Text, bool.Parse(CmbHasAccess.SelectedValue), bool.Parse(CmbIsDeniedAccess.SelectedValue),
                                             bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsAccessType", "AccessTypeId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "AccessTypeId=" + accessTypeId, "");
                var pageSize = GrvAccessType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvAccessType.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtAccessTypeTitle.Text))
            {
                ViewState["WhereClause"] += " and AccessTypeTitle like N'%" + TxtAccessTypeTitle.Text + "%'";
            }
           
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            switch (CmbHasAccess.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and HasAccess=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and HasAccess=0";
                    break;
            }

            switch (CmbIsDeniedAccess.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsDeniedAccess=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsDeniedAccess=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvAccessType.MasterTableView.CurrentPageIndex = pageIndex;
            GrvAccessType.DataSource = GeneralDB.GetPageDB(GrvAccessType.MasterTableView.PageSize, pageIndex,
                                                           whereClause, ViewState["sortField"].ToString(),
                                                           "[acs].v_AcsAccessType", "AccessTypeId", out count,
                                                           ViewState["SortType"].ToString());
            GrvAccessType.MasterTableView.VirtualItemCount = count;
            GrvAccessType.DataBind();
        }

        protected void SetPageControl()
        {
            TxtAccessTypeTitle.Text = "";
            CmbIsDeniedAccess.ClearSelection();
            CmbHasAccess.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvAccessType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var accessTypeId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["AccessTypeId"] = accessTypeId;
                var mySystem = Classes.AcsAccessType.GetSingleById(accessTypeId);
                if (mySystem != null)
                {
                    TxtAccessTypeTitle.Text = mySystem.AccessTypeTitle;
                    CmbHasAccess.SelectedValue = mySystem.HasAccess.ToString();
                    CmbIsDeniedAccess.SelectedValue = mySystem.IsDeniedAccess.ToString();
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsAccessType.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvAccessType.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvAccessType_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAccessType_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvAccessType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAccessType_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvAccessType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvAccessType_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvAccessType_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}