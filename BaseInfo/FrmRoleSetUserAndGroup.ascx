﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRoleSetUserAndGroup.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRoleSetUserAndGroup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="MessageErrorControl" TagPrefix="uc1" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
 fnRoleAndCateGorizeSearch2:
       {
          
           var myValue = new Array(<%=Session["ItemSearch"].ToString() %>); 
           var index= -1;
           
           function NextNode() {    
               //alert(index);
               if (index <  myValue.length - 1) {                      
                   index = index + 1;                 
                   expandNode(myValue[index]);
                   
                   return true;
                }
                else
                {

                  return false;
                }
           }

           function PreviousNode() {
              // alert(index);
               if (index > 0)
               {
                index = index - 1;  
                expandNode(myValue[index]);               
               return true;
               }
               else
               {
               return false;
               }
           }
           

           function expandNode(nodeid) {
               var treeView = $find("<%= trvRoleAndCategorize.ClientID %>");
               var node = treeView.findNodeByValue(nodeid);
               if (node) {
                   node.get_parent().expand();
                   node.expand();
                   //node.select();
                   node.set_selected(true);                   
                   scrollToNode(treeView, node);
                   return true;
               }
               return false;
           }


           function scrollToNode(treeview, node) {
               var nodeElement = node.get_contentElement();
               var treeViewElement = treeview.get_element();

               var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
               var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
               var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

               if (relativeOffsetTop < treeViewElement.scrollTop) {
                   treeViewElement.scrollTop = relativeOffsetTop;
               }

               var height = nodeElement.offsetHeight;

               if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                   treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
               }
           }
       }          
    </script>
</telerik:RadScriptBlock>
<asp:Panel runat="server" ID="pnlAll">
    <table width="100%" dir="rtl" align="right">
        <tr>
            <td></td>
            <td align="center" colspan="3">
                <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" Width="120px" LabelWidth="75px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" OnClick="imgbtnSearch_Click"
                                ToolTip="جستجو" Width="25px" />
                        </td>
                        <td>
                            <img alt="جلو" height="25" onclick="NextNode();"  src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Next.png"%>'
                                width="25" />
                        </td>
                        <td>
                            <img alt="عقب" height="25" onclick="PreviousNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Previous.png"%>'
                                width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <telerik:RadTreeView ID="trvRoleAndCategorize" runat="server" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" dir="rtl" Skin="Office2007" onnodeclick="trvRoleAndCategorize_NodeClick"
                                Height="400px" onnodedatabound="trvRoleAndCategorize_NodeDataBound"></telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="3" valign="top">
                <table dir="rtl" width="100%">
                    <tr>
                        <td align="center" dir="rtl" valign="top" width="40%">
                            <asp:Label ID="LblMembers" runat="server" Font-Bold="True" Text="اعضای نقش"></asp:Label>
                        </td>
                        <td align="center" width="4%">&nbsp; </td>
                        <td align="center" dir="rtl" valign="top" width="40%">
                            <asp:Label ID="LblNotMembers" runat="server" Font-Bold="True" Text="اعضای فاقد نقش"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" rowspan="2" style="border: 1px solid #000000;" valign="top" width="40%">
                            <table>
                                <tr>
                                    <td align="right" dir="rtl">
                                        <cc1:CustomeRadListBox ID="LstMembers" runat="server" ActiveSelectAll="True" CheckBoxes="True"
                                            dir="rtl" SelectionMode="Multiple" Skin="Outlook" ToolTip="Member" ValidationGroup="Member"
                                            Width="200px"></cc1:CustomeRadListBox>                                      
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="top" width="4%">
                            <table>
                                <tr>
                                    <td >
                                        <asp:ImageButton ID="ImgbAddMembers" runat="server" style="border: 2px solid #000000;" Height="30px" ImageUrl="../Images/Right.png"
                                            OnClick="ImgbAddMembers_Click" ToolTip="افزودن اعضای انتخابی" ValidationGroup="NotMember"
                                            Width="30px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <asp:ImageButton ID="ImgbRemoveMembers" runat="server" BorderWidth="2px" 
                                            Height="30px" ImageUrl="../Images/Left.png" OnClick="ImgbRemoveMembers_Click" 
                                            ToolTip="حذف اعضای انتخابی" ValidationGroup="Member" Width="30px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" rowspan="2" style="border: 1px solid #000000;" valign="top" width="40%">
                            <table>
                                <tr>
                                    <td align="right" dir="rtl">
                                        <cc1:CustomeRadListBox ID="LstNotMembers" runat="server" ActiveSelectAll="True" CheckBoxes="True"
                                            dir="rtl" SelectionMode="Multiple" Skin="Outlook" ValidationGroup="NotMember"
                                            Width="200px"></cc1:CustomeRadListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="4%">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td align="right"></td>
        </tr>
        <tr>
            <td colspan="4">
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="Office2007">
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
                    <ajaxsettings>
                        <telerik:AjaxSetting 
                    AjaxControlID="imgbtnSearch"><updatedcontrols>
                                <telerik:AjaxUpdatedControl ControlID="RadScriptBlock1" />
                                <telerik:AjaxUpdatedControl 
                        ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" /></updatedcontrols></telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="trvRoleAndCategorize">
                            <updatedcontrols>
                                <telerik:AjaxUpdatedControl ControlID="RadScriptBlock1" />
                                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                                    LoadingPanelID="RadAjaxLoadingPanel1" />
                            </updatedcontrols>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting 
                    AjaxControlID="ImgbAddMembers"><updatedcontrols>
                                <telerik:AjaxUpdatedControl 
                            ControlID="RadScriptBlock1" />
                                <telerik:AjaxUpdatedControl 
                            ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" /></updatedcontrols></telerik:AjaxSetting><telerik:AjaxSetting 
                    AjaxControlID="ImgbRemoveMembers"><updatedcontrols>
                                <telerik:AjaxUpdatedControl 
                            ControlID="RadScriptBlock1" />
                                <telerik:AjaxUpdatedControl 
                            ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" /></updatedcontrols></telerik:AjaxSetting></ajaxsettings>
                </telerik:RadAjaxManagerProxy>
            </td>
        </tr>
    </table>
</asp:Panel>
