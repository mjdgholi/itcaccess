﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRowAccess : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                HttpContext.Current.Response.Redirect(Intranet.Configuration.Settings.PortalSettings.PortalPath);
            }
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetGrvObjectData(0, "");
                SetCmbSystem();
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                GrvRowAccess.MasterTableView.Columns[6].Visible = false;
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                var objectId = Int32.Parse(CmbObject.SelectedValue);
                AcsRowAccess.Add(objectId,TxtName.Text, TxtSqlRowAccess.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsRowAccess", "RowAccessId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "RowAccessId=" + addedId, "");
                var pageSize = GrvRowAccess.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvRowAccess.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var rowAccessId = Convert.ToInt32(ViewState["RowAccessId"].ToString());
                int objectId = Int32.Parse(CmbObject.SelectedValue);
                Classes.AcsRowAccess.Update(rowAccessId,objectId, TxtName.Text, TxtSqlRowAccess.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsRowAccess", "RowAccessId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "RowAccessId=" + rowAccessId, "");
                var pageSize = GrvRowAccess.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvRowAccess.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                CmbSystem.Enabled = true;
                CmbObject.Enabled = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (CmbSystem.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and SystemId = " + CmbSystem.SelectedValue;
            }
            if (CmbObject.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and ObjectId = " + CmbObject.SelectedValue;
            }
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and AccessTitle like N'%" + TxtName.Text + "%'";
            }
            if (!string.IsNullOrEmpty(TxtSqlRowAccess.Text))
            {
                ViewState["WhereClause"] += " and ConditionalExpression like N'%" + TxtSqlRowAccess.Text + "%'";
            }
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");
            pnlAccessDetail.Visible = false;
            pnlControls.Visible = true;
            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
            CmbSystem.Enabled = true;
            CmbObject.Enabled = true;
            GrvRowAccess.MasterTableView.Columns[6].Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvRowAccess.MasterTableView.CurrentPageIndex = pageIndex;
            GrvRowAccess.DataSource = GeneralDB.GetPageDB(GrvRowAccess.MasterTableView.PageSize, pageIndex,
                                                          whereClause, ViewState["sortField"].ToString(),
                                                          "[acs].v_AcsRowAccess", "RowAccessId", out count,
                                                          ViewState["SortType"].ToString());
            GrvRowAccess.MasterTableView.VirtualItemCount = count;
            GrvRowAccess.DataBind();
        }

        protected void SetPageControl()
        {
            CmbSystem.ClearSelection();
            CmbObject.ClearSelection();
            TxtName.Text = "";
            TxtSqlRowAccess.Text = "";
            CmbIsActive.SelectedIndex = 1;
            GrvRowAccess.MasterTableView.Columns[6].Visible = false;
            pnlAccessDetail.Visible = false;
        }

        protected void GrvRowAccess_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var rowAccessId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["RowAccessId"] = rowAccessId;
                var myRowAccess = Classes.AcsRowAccess.GetSingleById(rowAccessId);
                if (myRowAccess != null)
                {
                    var objectId = myRowAccess.ObjectId;
                    var myObject = Classes.AcsObject.GetSingleById(objectId);
                    var systemId = myObject.SystemId;
                    CmbSystem.SelectedValue = systemId.ToString();
                    SetCmbObject(systemId);
                    CmbObject.SelectedValue = objectId.ToString();
                    TxtName.Text = myRowAccess.AccessTitle;
                    TxtSqlRowAccess.Text = myRowAccess.ConditionalExpression;
                    CmbIsActive.SelectedValue = myRowAccess.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                    pnlControls.Visible = true;
                    if (myRowAccess.ConditionalExpression.Trim() != "")
                    {
                        CmbSystem.Enabled = false;
                        CmbObject.Enabled = false;
                    }
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsRowAccess.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvRowAccess.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    CmbSystem.Enabled = true;
                    CmbObject.Enabled = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();
                    pnlControls.Visible = true;
                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
            if (e.CommandName == "Other")
            {
                RadMultiPage1.PageViews.Clear();
                var rowAccessId = Int32.Parse(e.CommandArgument.ToString());
                Session["RowAccessId"] = rowAccessId;
                var pageView = new RadPageView {ID = "FrmRowAccessDetailPv"};
                RadMultiPage1.PageViews.Add(pageView);
                SetGrvObjectData(0, "RowAccessId=" + rowAccessId);
                GrvRowAccess.MasterTableView.Columns[6].Visible = true;
                pnlAccessDetail.Visible = true;
                pnlControls.Visible = false;
            }
            if (e.CommandName == "_ShowAll")
            {
                try
                {
                    SetGrvObjectData(GrvRowAccess.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    GrvRowAccess.MasterTableView.Columns[6].Visible = false;
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    CmbSystem.Enabled = true;
                    CmbObject.Enabled = true;
                    SetPageControl();
                    pnlAccessDetail.Visible = false;
                    pnlControls.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvRowAccess_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRowAccess_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvRowAccess.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRowAccess_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvRowAccess.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRowAccess_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvRowAccess_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }

        protected void RadMultiPage1_PageViewCreated1(object sender, RadMultiPageEventArgs e)
        {
            const string userControlName = @"DesktopModules\ItcAccess\BaseInfo\FrmRowAccessDetail.ascx";
            var userControl = (Page.LoadControl(userControlName));
            if (userControl != null)
            {
                userControl.ID = "FrmRowAccessDetail" + "_userControl";
            }

            e.PageView.Controls.Add(userControl);
            e.PageView.Selected = true;

        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataBind();
        }

        protected void SetCmbObject(int systemId)
        {
            CmbObject.Items.Clear();
            CmbObject.Items.Add(new RadComboBoxItem(""));
            CmbObject.DataTextField = "TableDesc";
            CmbObject.DataValueField = "ObjectId";
            CmbObject.DataSource = AcsObject.GetAllPerSystemDs(systemId);
            CmbObject.DataBind();
        }

        protected void CmbSystem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int systemId = -1;
            if (CmbSystem.SelectedIndex>0)
            {
                 systemId = Int32.Parse(CmbSystem.SelectedValue);
            }
            SetCmbObject(systemId);
        }
    }
}