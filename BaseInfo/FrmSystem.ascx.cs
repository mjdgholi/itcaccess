﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmSystem : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);

            if (ViewState["PageStatus"] == null)
            {

                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetgrvSystemData(0, "");
                SetCmbSchema();
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }

            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsSystem.Add(TxtName.Text,TxtLatinName.Text,CmbSchema.SelectedItem.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsSystem", "SystemId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "SystemId=" + addedId,"");
                var pageSize = GrvSystem.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetgrvSystemData(curRecPage, "");

                GrvSystem.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {                               
                var systemId = Convert.ToInt32(ViewState["SystemId"].ToString());
                Classes.AcsSystem.Update(systemId, TxtName.Text,TxtLatinName.Text,CmbSchema.SelectedItem.Text,bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsSystem", "SystemId", ViewState["sortField"].ToString(),
                                                   ViewState["SortType"].ToString(), "SystemId=" + systemId, "");
                var pageSize = GrvSystem.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetgrvSystemData(curRecPage, "");

                GrvSystem.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and SystemTitle like N'%" + TxtName.Text + "%'";
            }

            if (!string.IsNullOrEmpty(TxtLatinName.Text))
            {
                ViewState["WhereClause"] += " and SustemEnglishTitle like N'%" + TxtLatinName.Text + "%'";
            }

            if (CmbSchema.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and SystemSchema =" + CmbSchema.SelectedItem.Text + " ";
            }

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetgrvSystemData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetgrvSystemData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetgrvSystemData(int pageIndex, string whereClause)
        {
            int count;           

            GrvSystem.MasterTableView.CurrentPageIndex = pageIndex;
            GrvSystem.DataSource = GeneralDB.GetPageDB(GrvSystem.MasterTableView.PageSize, pageIndex,
                                                                    whereClause, ViewState["sortField"].ToString(),
                                                                    "[acs].v_AcsSystem", "SystemId", out count,
                                                                    ViewState["SortType"].ToString());
            GrvSystem.MasterTableView.VirtualItemCount = count;
            GrvSystem.DataBind();
        }

        protected void SetCmbSchema()
        {
            CmbSchema.Items.Clear();
            CmbSchema.Items.Add(new RadComboBoxItem(""));
            CmbSchema.DataTextField = "name";
            CmbSchema.DataValueField = "name";
            CmbSchema.DataSource = SqlClass.GetAllSqlSchema();
            CmbSchema.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvSystem_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var systemId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["SystemId"] = systemId;
                var mySystem = Classes.AcsSystem.GetSingleById(systemId);
                if (mySystem != null)
                {
                    TxtName.Text = mySystem.SystemTitle;
                    TxtLatinName.Text = mySystem.SustemEnglishTitle;
                    CmbSchema.SelectedValue = mySystem.SystemSchema;
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();

                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsSystem.Delete(Convert.ToInt32(e.CommandArgument));
                    SetgrvSystemData(GrvSystem.MasterTableView.CurrentPageIndex,
                                              ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvSystem_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetgrvSystemData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvSystem_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetgrvSystemData(GrvSystem.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvSystem_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetgrvSystemData(GrvSystem.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvSystem_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvSystem_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }        
    }
}