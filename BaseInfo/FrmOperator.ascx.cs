﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmOperator : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsOperator.Add(TxtName.Text, TxtSqlOperator.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsOperator", "OperatorId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "OperatorId=" + addedId, "");
                var pageSize = GrvOperator.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvOperator.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var OperatorId = Convert.ToInt32(ViewState["OperatorId"].ToString());
                Classes.AcsOperator.Update(OperatorId, TxtName.Text, TxtSqlOperator.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsOperator", "OperatorId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "OperatorId=" + OperatorId, "");
                var pageSize = GrvOperator.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvOperator.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and OperatorTitle like N'%" + TxtName.Text + "%'";
            }

            if (!string.IsNullOrEmpty(TxtSqlOperator.Text))
            {
                ViewState["WhereClause"] += " and SqlOperator like N'%" + TxtSqlOperator.Text + "%'";
            }
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvOperator.MasterTableView.CurrentPageIndex = pageIndex;
            GrvOperator.DataSource = GeneralDB.GetPageDB(GrvOperator.MasterTableView.PageSize, pageIndex,
                                                            whereClause, ViewState["sortField"].ToString(),
                                                            "[acs].v_AcsOperator", "OperatorId", out count,
                                                            ViewState["SortType"].ToString());
            GrvOperator.MasterTableView.VirtualItemCount = count;
            GrvOperator.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            TxtSqlOperator.Text = "";
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvOperator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var operatorId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["OperatorId"] = operatorId;
                var myOperator = Classes.AcsOperator.GetSingleById(operatorId);
                if (myOperator != null)
                {
                    TxtName.Text = myOperator.OperatorTitle;
                    TxtSqlOperator.Text = myOperator.SqlOperator;
                    CmbIsActive.SelectedValue = myOperator.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsOperator.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvOperator.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvOperator_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvOperator_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvOperator.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvOperator_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvOperator.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvOperator_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvOperator_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}