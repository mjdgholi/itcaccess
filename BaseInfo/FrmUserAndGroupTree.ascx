﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmUserAndGroupTree.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmUserAndGroupTree" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="MessageErrorControl" TagPrefix="uc1" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<table dir="rtl" align="right" width="100%" bgcolor="White" border="2" cellpadding="0"
    cellspacing="0">
    <tr>
        <td>
            <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="BtnAdd">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                            <telerik:AjaxUpdatedControl ControlID="pnlControls" />
                            <telerik:AjaxUpdatedControl ControlID="pnlTree" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="trvUserAndGroup">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                            <telerik:AjaxUpdatedControl ControlID="pnlControls" />
                            <telerik:AjaxUpdatedControl ControlID="trvUserAndGroup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManagerProxy>
        </td>
        <td>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" KeepInScreenBounds="True">
                <Windows>
                    <telerik:RadWindow ID="RadWindow1" runat="server" KeepInScreenBounds="True">
                    </telerik:RadWindow>
                </Windows>
            </telerik:RadWindowManager>
        </td>
        <td>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
            </telerik:RadAjaxLoadingPanel>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table dir="rtl" width="100%">
                <tr>
                    <td colspan="6" dir="rtl">
                        <asp:Panel runat="server" ID="pnlButton">
                            <table runat="server" id="tblButton">
                                <tr>
                                    <td>
                                        <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" Text="افزودن به گروه">
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit">
                                            <Icon PrimaryIconCssClass="rbEdit" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <cc1:CustomRadButton ID="BtnSearch" runat="server" CustomeButtonType="Search" OnClick="BtnSearch_Click">
                                            <Icon PrimaryIconCssClass="rbSearch" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <cc1:CustomRadButton ID="BtnDelete" runat="server" CustomeButtonType="Delete">
                                            <Icon PrimaryIconCssClass="rbRemove" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Panel runat="server" ID="pnlControls">
                            <table>
                                <tr>
                                    <td width="10%" nowrap="nowrap">
                                        <asp:Label ID="LblName" runat="server" Text="نام کاربری یا گروه:"></asp:Label>
                                    </td>
                                    <td width="30%">
                                        <cc1:SelectControl ID="TxtUserAndGroupName" runat="server" imageName="PersonAdd"
                                            PortalPathUrl="ItcAccess/ModalForm/FrmUserAndGroupManagement.aspx" PrsOrganizationPhysicalChartFullAccess="False" />
                                    </td>
                                    <td width="10%">
                                        &nbsp;
                                    </td>
                                    <td width="10%">
                                    </td>
                                    <td width="30%">
                                    </td>
                                    <td width="10%">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <asp:Label ID="LblIsBuiltin0" runat="server" Text="گروه در بر گیرنده"></asp:Label>
                                    </td>
                                    <td width="30%">
                                        <telerik:RadComboBox ID="CmbGroupOwner" runat="server" CollapseAnimation-Type="None"
                                            EmptyMessage="" ExpandAnimation-Type="None" OnClientDropDownOpened="OnClientDropDownOpenedHandler"
                                            ShowToggleImage="True" Skin="Office2007" Style="vertical-align: middle;" Width="300px"
                                            Filter="Contains" MarkFirstMatch="True">
                                            <ItemTemplate>
                                                <div>
                                                    <telerik:RadTreeView ID="trvCmbUserAndGroup" runat="server" Height="140px" OnClientNodeClicking="nodeClicking" Skin="Office2007"
                                                        OnNodeExpand="trvCmbUserAndGroup_NodeExpand" Skin="Hay" Width="100%">
                                                    </telerik:RadTreeView>
                                                </div>
                                            </ItemTemplate>
                                            <Items>
                                                <telerik:RadComboBoxItem Text="" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </td>
                                    <td width="10%">
                                        &nbsp;
                                    </td>
                                    <td width="10%" style="direction: ltr">
                                        &nbsp;
                                    </td>
                                    <td width="30%">
                                        &nbsp;
                                    </td>
                                    <td width="10%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#E1F0FF" colspan="3">
            <asp:Panel runat="server" ID="pnlTree">
                <telerik:RadTreeView ID="trvUserAndGroup" runat="server" OnClientDoubleClick="OnClientDoubleClick"
                    dir="rtl" OnNodeExpand="trvUserAndGroup_NodeExpand" AllowNodeEditing="True" OnNodeEdit="trvUserAndGroup_NodeEdit"
                    Skin="Office2007" OnContextMenuItemClick="trvUserAndGroup_ContextMenuItemClick"
                    EnableDragAndDrop="True" OnNodeDrop="trvUserAndGroup_NodeDrop">
                    <ContextMenus>
                        <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Vista">
                            <Items>
                                <telerik:RadMenuItem Value="add" Text="افزودن گروه یا کاربر" ImageUrl="../Images/addUser.png">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem Value="edit" Text="ویرایش" ImageUrl="../Images/editUser.png"
                                    PostBack="false">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem Value="remove" Text="حذف" ImageUrl="../Images/deleteUser.png">
                                </telerik:RadMenuItem>
                            </Items>
                            <CollapseAnimation Type="OutQuint"></CollapseAnimation>
                        </telerik:RadTreeViewContextMenu>
                    </ContextMenus>
                </telerik:RadTreeView>
            </asp:Panel>
        </td>
    </tr>
</table>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function OnClientDoubleClick(sender, args) {
            var node = args.get_node();
            var nodeElement = node.get_contentElement();
            var treeViewElement = sender.get_element();
            var nodeOffsetTop = sender._getTotalOffsetTop(nodeElement);
            var treeOffsetTop = sender._getTotalOffsetTop(treeViewElement);
            var relativeOffsetTop = nodeOffsetTop - treeOffsetTop + 5;
            var nodeValue = args.get_node().get_value();
            window.alert(nodeValue);
            var oWnd = radopen(null, "RadWindow1");
            // oWnd.MoveTo(100, relativeOffsetTop);
        }



        function nodeClicking(sender, args) {
            var comboBox = $find("<%= CmbGroupOwner.ClientID %>");

            var node = args.get_node();

            comboBox.set_text(node.get_text());

            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
            comboBox.commitChanges();

            comboBox.hideDropDown();

            // Call comboBox.attachDropDown if:
            // 1) The RadComboBox is inside an AJAX panel.
            // 2) The RadTreeView has a server-side event handler for the NodeClick event, i.e. it initiates a postback when clicking on a Node.
            // Otherwise the AJAX postback becomes a normal postback regardless of the outer AJAX panel.

            //comboBox.attachDropDown();
        }

        function StopPropagation(e) {
            if (!e) {
                e = window.event;
            }

            e.cancelBubble = true;
        }

        function OnClientDropDownOpenedHandler(sender, eventArgs) {
            var tree = sender.get_items().getItem(0).findControl("trvCmbUserAndGroup");
            var selectedNode = tree.get_selectedNode();
            if (selectedNode) {
                selectedNode.scrollIntoView();
            }
        }
    </script>
</telerik:RadScriptBlock>
