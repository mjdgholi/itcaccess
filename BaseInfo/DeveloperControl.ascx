﻿<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<table>
    <tr>
        <td dir="ltr">
            <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeveloperControl.ascx.cs"
                Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.DeveloperControl" %>
            <%@ Register Assembly="Telerik.Web.UI, Version=2012.3.1016.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4"
                Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
            <%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
            <cc1:CustomRadComboBox ID="MultiSelectComboBox" runat="server" Width="300" EnableCheckAllItemsCheckBox="True"
                EnableTextSelection="False" Filter="Contains" MarkFirstMatch="True" >
                <Localization AllItemsCheckedString="تمامی موارد انتخاب گردید" CheckAllString="انتخاب همه"
                    ItemsCheckedString="گزینه انتخاب گردید" NoMatches="عدم تطابق" ShowMoreFormatString="ار" />
            </cc1:CustomRadComboBox>
            <cc1:NumericTextBox ID="NumericTextBox" runat="server" Text=""></cc1:NumericTextBox>
            <telerik:RadTextBox ID="TextBox" runat="server"></telerik:RadTextBox>
            <cc1:JalaliCalendar ID="JalaliCalendar1" runat="server"> </cc1:JalaliCalendar>
            <telerik:RadTreeView ID="TreeViewControl" runat="server" CheckBoxes="True" CheckChildNodes="True" Height="300px" Skin="Office2007">
            </telerik:RadTreeView>
        </td>
    </tr>
</table>
