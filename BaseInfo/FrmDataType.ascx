﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmDataType.ascx.cs" Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmDataType" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<div>
    <asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll">
        <table dir="rtl">
            <tr>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click"></cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                        <Icon PrimaryIconCssClass="rbEdit" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                        OnClick="BtnSearch_Click">
                        <Icon PrimaryIconCssClass="rbSearch" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                        OnClick="BtnShowAll_Click" Empty="" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                        <Icon PrimaryIconCssClass="rbRefresh" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table dir="rtl" width="100%">
            <tr>
                <td nowrap="nowrap" width="10%">
                    <asp:Panel ID="pnlControls" runat="server" BorderColor="White" BorderWidth="2px">
                        <table style="text-align: right" width="100%">
                            <tr>
                                <td align="right" width="10%" nowrap="nowrap">
                                    <asp:Label ID="LblDataTypeTitle" runat="server" Text="عنوان نوع داده:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadTextBox ID="TxtDataTypeTitle" Runat="server"></telerik:RadTextBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="TxtDataTypeTitle"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap="nowrap" width="10%">
                                    <asp:Label ID="LblDataTypeSqlTitle" runat="server" 
                                        Text="عنوان نوع داده در سی کیو ال:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbDataTypeSqlTitle" runat="server" KeyId="" 
                                        Skin="Office2007" Width="300px"
                                        AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True"></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvHasAccess" runat="server" ControlToValidate="CmbDataTypeSqlTitle"
                                        ErrorMessage="*" InitialValue=""></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                                <td width="30%">
                                    &nbsp;
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" width="10%" nowrap="nowrap">
                                    <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" 
                                        Width="300px" MarkFirstMatch="True"><items><telerik:RadComboBoxItem runat="server" Selected="True" 
                                                    Text="" Value="-1" /><telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" /><telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" /></items></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                        ErrorMessage="*" InitialValue=""></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                                <td width="30%">
                                    &nbsp;
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="6" nowrap="nowrap">
                                    <telerik:RadGrid ID="GrvDataType" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                        AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
                                        OnItemCommand="GrvDataType_ItemCommand" OnItemDataBound="GrvDataType_ItemDataBound"
                                        OnNeedDataSource="GrvDataType_NeedDataSource" OnPageIndexChanged="GrvDataType_PageIndexChanged"
                                        OnPageSizeChanged="GrvDataType_PageSizeChanged" OnSortCommand="GrvDataType_SortCommand"
                                        Skin="Office2007" Width="95%"><headercontextmenu cssclass="GridContextMenu GridContextMenu_Default"></headercontextmenu><mastertableview clientdatakeynames="DataTypeId" datakeynames="DataTypeId" dir="RTL"
                                            nomasterrecordstext="اطلاعاتی برای نمایش یافت نشد"><CommandItemSettings ExportToPdfText="Export to PDF" /><commanditemsettings exporttopdftext="Export to PDF" /><rowindicatorcolumn filtercontrolalttext="Filter RowIndicator column"><HeaderStyle Width="20px" /></rowindicatorcolumn><expandcollapsecolumn filtercontrolalttext="Filter ExpandColumn column"><HeaderStyle Width="20px" /></expandcollapsecolumn><Columns><telerik:GridBoundColumn DataField="DataTypeTitle" 
                                                    FilterControlAltText="Filter DataTypeTitle column" HeaderText="عنوان نوع داده" 
                                                    SortExpression="DataTypeTitle" UniqueName="DataTypeTitle"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="DataTypeSqlTitle" 
                                                    FilterControlAltText="Filter HasAccessText column" HeaderText="عنوان نوع داده در سی کیو ال" 
                                                    SortExpression="DataTypeSqlTitle" UniqueName="DataTypeSqlTitle"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="IsActiveText" 
                                                    FilterControlAltText="Filter IsActiveText column" HeaderText="وضعیت رکورد" 
                                                    SortExpression="DataTypeTitle" UniqueName="IsActiveText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                                    HeaderText="ویرایش" UniqueName="Edit"><ItemTemplate><asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" 
                                                            CommandArgument='<%#Eval("DataTypeId").ToString() %>' CommandName="_MyِEdit" 
                                                            ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                                    HeaderText="حذف" UniqueName="Delete"><ItemTemplate><asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" 
                                                            CommandArgument='<%#Eval("DataTypeId").ToString() %>' CommandName="_MyِDelete" 
                                                            ForeColor="#000066" ImageUrl="../Images/Delete.bmp" 
                                                            OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn></Columns><editformsettings><editcolumn filtercontrolalttext="Filter EditCommandColumn column"></editcolumn></editformsettings><PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" 
                                                LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" 
                                                NextPageToolTip="صفحه بعدی" 
                                                PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;." 
                                                PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" 
                                                PrevPageToolTip="صفحه قبلی" VerticalAlign="Middle" /></mastertableview><headerstyle height="40px" /><clientsettings allowcolumnsreorder="True" enablerowhoverstyle="true" reordercolumnsonclient="True"><selecting allowrowselect="True" /><selecting allowrowselect="True" /></clientsettings><filtermenu enableimagesprites="False"></filtermenu></telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvDataType">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
