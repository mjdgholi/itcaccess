﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmObject.ascx.cs" Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmObject" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<div>
    <asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll" BorderColor="White"
        BorderWidth="2px">
        <table dir="rtl">
            <tr>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click"></cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                        <Icon PrimaryIconCssClass="rbEdit" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                        OnClick="BtnSearch_Click">
                        <Icon PrimaryIconCssClass="rbSearch" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                        OnClick="BtnShowAll_Click">
                        <Icon PrimaryIconCssClass="rbRefresh" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table dir="rtl" width="100%">
            <tr>
                <td nowrap="nowrap" width="10%">&nbsp;<asp:Panel ID="pnlControls" runat="server">
                    <table>
                        <tr>
                            <td width="10%" align="right" nowrap="nowrap">
                                <asp:Label ID="LblObjectType" runat="server" Text="عنوان نوع آبجکت:"></asp:Label>
                            </td>
                            <td width="50%">
                                <cc1:CustomRadComboBox ID="CmbObjectType" runat="server" 
                                    AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True" 
                                    NoWrap="True" Skin="Office2007" Width="500px" 
                                    onselectedindexchanged="CmbObjectType_SelectedIndexChanged"></cc1:CustomRadComboBox>
                            </td>
                            <td width="10%">
                                <asp:RequiredFieldValidator ID="rfvSystem0" runat="server" 
                                    ControlToValidate="CmbObjectType" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="10%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap" width="10%">
                                <asp:Label ID="LblSystem" runat="server" Text="عنوان سیستم:"></asp:Label>
                            </td>
                            <td width="50%">
                                <cc1:CustomRadComboBox ID="CmbSystem" runat="server" 
                                    AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True" 
                                    NoWrap="True" Skin="Office2007" Width="500px"></cc1:CustomRadComboBox>
                            </td>
                            <td width="10%">
                                <asp:RequiredFieldValidator ID="rfvSystem" runat="server" 
                                    ControlToValidate="CmbSystem" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%"></td>
                            <td width="30%"></td>
                            <td width="10%"></td>
                        </tr>
                        <tr>
                            <td align="right" width="10%">
                                <asp:Label ID="LblName" runat="server" Text="عنوان شیئ:"></asp:Label>
                            </td>
                            <td width="30%">
                                <cc1:CustomRadComboBox ID="CmbObjectTitle" runat="server" AppendDataBoundItems="True"
                                    Filter="Contains" MarkFirstMatch="True" NoWrap="True" Skin="Office2007" 
                                    Width="500px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="10%">
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="CmbObjectTitle"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%">&nbsp; </td>
                            <td width="30%">&nbsp; </td>
                            <td width="10%">&nbsp; </td>
                        </tr>
                        <tr>
                            <td align="right" width="10%">&nbsp;</td>
                            <td width="30%">
                                <asp:CheckBox ID="ChkIsHierachy" runat="server" Text="آیا سلسله مراتبی است؟" />
                            </td>
                            <td width="10%">&nbsp;</td>
                            <td width="10%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="10%">&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="GrvObject" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Office2007" OnItemCommand="GrvObject_ItemCommand"
            OnPageIndexChanged="GrvObject_PageIndexChanged" OnPageSizeChanged="GrvObject_PageSizeChanged"
            OnSortCommand="GrvObject_SortCommand" Width="95%" OnNeedDataSource="GrvObject_NeedDataSource"
            AllowCustomPaging="True" OnItemDataBound="GrvObject_ItemDataBound"><headercontextmenu cssclass="GridContextMenu GridContextMenu_Default"></headercontextmenu><mastertableview datakeynames="ObjectId" dir="RTL" nomasterrecordstext="اطلاعاتی برای نمایش یافت نشد"
                clientdatakeynames="ObjectId"><CommandItemSettings ExportToPdfText="Export to PDF" /><RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"><HeaderStyle Width="20px" /></RowIndicatorColumn><ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"><HeaderStyle Width="20px" /></ExpandCollapseColumn><Columns><telerik:GridBoundColumn DataField="ObjectTitleDesc" FilterControlAltText="Filter ObjectTitleDesc column"
                                             HeaderText="عنوان شیئ" SortExpression="ObjectTitleDesc" UniqueName="ObjectTitleDesc"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="ObjectTypeTitle" FilterControlAltText="Filter ObjectTypeTitle column"
                                             HeaderText="نوع شیئ" SortExpression="ObjectTypeTitle" UniqueName="ObjectTypeTitle"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="SystemTitle" FilterControlAltText="Filter SystemTitle column"
                                             HeaderText="عنوان سیستم" SortExpression="SystemTitle" UniqueName="SystemTitle"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="IsHierarchyFullText" FilterControlAltText="Filter IsHierarchyFullText column"
                                             HeaderText="سلسله مراتبی است؟" SortExpression="IsHierarchyFullText" UniqueName="IsHierarchyFullText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="تعیین فیلدها" UniqueName="Field"><ItemTemplate><cc1:SelectControl ID="SelectControlPerson" runat="server" WhereClause='<%#Eval("ObjectId").ToString() %>'
    RadWindowHeight="700" RadWindowWidth="850" imageName="Organization" Width="1px"
    PortalPathUrl="ItcAccess/ModalForm/FrmObjectField.aspx" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
        UniqueName="Edit"><ItemTemplate><asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                            CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
            UniqueName="Delete"><ItemTemplate><asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                                                CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                                                OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn></Columns><EditFormSettings><EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn></EditFormSettings><PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                                                                                                                                                 NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                                                                                                                 PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                                                                                                                 VerticalAlign="Middle" HorizontalAlign="Center" /></mastertableview><headerstyle height="40px" /><clientsettings reordercolumnsonclient="True" allowcolumnsreorder="True" enablerowhoverstyle="true"><Selecting AllowRowSelect="True"></Selecting></clientsettings><filtermenu enableimagesprites="False"></filtermenu></telerik:RadGrid>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Windows7">
    </telerik:RadAjaxLoadingPanel>
</div>
<telerik:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvObject">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>
