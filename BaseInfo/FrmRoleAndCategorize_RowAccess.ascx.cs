﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRoleAndCategorize_RowAccess : ModuleControls
    {
        private ArrayList _searchGroupIdList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["PageStatus"] == null)
            {
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
                SetCmbSystem();
                SetCmbObject(-1);
                SetCmbAccessType();
                SetRoleAndCategorizeTree();
                BtnEdit.Visible = false;
                Session["ItemSearch"] = "";
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
                MessageErrorControl2.ShowWarningMessage("ابتدا نقش مورد نظر را نتخاب نمایید");
                
            }
            if (!IsPostBack)
            {

            }
        }

        protected void SetRoleAndCategorizeTree()
        {
            trvRoleAndCategorize.DataTextField = "RoleAndCategorizeTilte";
            trvRoleAndCategorize.DataValueField = "RoleAndCategorizeTreeId";
            trvRoleAndCategorize.DataFieldID = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldParentID = "RoleAndCategorizeOwnerId";
            trvRoleAndCategorize.DataSource = AcsRoleAndCategorizeTree.GetAllNodes(-1, "", -1);
            trvRoleAndCategorize.DataBind();
        }

        protected void SetGrvRowAccess()
        {
            if (trvRoleAndCategorize.SelectedNode != null)
            {
                int objectId = -1;
                if (CmbObject.SelectedIndex > 0)
                {
                    objectId = Int32.Parse(CmbObject.SelectedValue);
                }

                int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                int roleId = myRole.RoleAndCategorizeId;
                GrvRowAccess.DataSource = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerRole(roleId, objectId);
            }

        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataBind();
        }

        protected void SetCmbObject(int systemId)
        {
            CmbObject.Items.Clear();
            CmbObject.Items.Add(new RadComboBoxItem(""));
            CmbObject.DataTextField = "ObjectTitle";
            CmbObject.DataValueField = "ObjectId";
            CmbObject.DataSource = AcsObject.GetAllPerSystemDs(systemId);
            CmbObject.DataBind();
        }

        protected void SetCmbAccessType()
        {
            CmbAccessType.Items.Clear();
            CmbAccessType.Items.Add(new RadComboBoxItem(""));
            CmbAccessType.DataTextField = "AccessTypeTitle";
            CmbAccessType.DataValueField = "AccessTypeId";
            CmbAccessType.DataSource = AcsAccessType.GetAll();
            CmbAccessType.DataBind();
        }

        protected void SetCmbObjectRowAccess(int objectId, int roleId)
        {
            CmbObjectRowAccess.Items.Clear();
            CmbObjectRowAccess.DataTextField = "AccessTitle";
            CmbObjectRowAccess.DataValueField = "RowAccessId";
            CmbObjectRowAccess.DataSource = AcsRowAccess.GetRowAccessPerObjectRoleHasNotIt(objectId, roleId);
            CmbObjectRowAccess.DataBind();
        }

        protected DataTable SetDetailTable(int roleId, int objectId)
        {
            return AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerRolePerObject(roleId, objectId);
        }

        protected void GrvObjectAccess_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyEditUserAndGroup_Role_Access")
            {
                e.Item.Selected = true;
                var userAndGroupRoleAccessId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["UserAndGroup_Role_AccessId"] = userAndGroupRoleAccessId;
                var myAndGroupRoleAccess = Classes.AcsUserAndGroup_RoleAndCategorize_RowAccess.GetSingleById(userAndGroupRoleAccessId);
                if (myAndGroupRoleAccess != null)
                {
                    CmbAccessType.SelectedValue = myAndGroupRoleAccess.AccessTypeId.ToString();
                    CmbIsActive.SelectedValue = myAndGroupRoleAccess.IsActive.ToString();

                    var objectId = AcsUserAndGroup_RoleAndCategorize_RowAccessDB.GetObjectPerRowAccess(myAndGroupRoleAccess.RowAccessId);
                    CmbObject.SelectedValue = objectId.ToString();
                    SetCmbObjectRowAccess(objectId, -1);
                    CmbObjectRowAccess.CheckBoxes = false;
                    CmbObjectRowAccess.SelectedValue = myAndGroupRoleAccess.RowAccessId.ToString();
                    //CmbObjectRowAccess.FindItemByValue(myAndGroupRoleAccess.RowAccessId.ToString()).Checked = true;

                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                }
            }

            if (e.CommandName == "_MyDeleteAllRowAccessPerObjectPerRole")
            {
                try
                {
                    var objectId = Int32.Parse(e.CommandArgument.ToString());
                    int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                    var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                    int roleId = myRole.RoleAndCategorizeId;
                    AcsUserAndGroup_RoleAndCategorize_RowAccess.DeleteAcsUserAndGroup_RowAccessPerObjectPerRole(objectId, roleId);
                    GrvRowAccess.Rebind();
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    SetPageControl();

                    MessageErrorControl2.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl2.ShowErrorMessage(ex.Message);
                }
            }

            if (e.CommandName == "_MyDeleteUserAndGroup_Role_Access")
            {
                try
                {
                    AcsUserAndGroup_RoleAndCategorize_RowAccess.Delete(Convert.ToInt32(e.CommandArgument));
                    GrvRowAccess.Rebind();
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    SetPageControl();

                    MessageErrorControl2.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl2.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvObjectAccess_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "MasterTable")
            {
                if (trvRoleAndCategorize.SelectedNode != null)
                {
                    var item = (GridDataItem) e.Item;
                    var objectId = Int32.Parse(item.GetDataKeyValue("ObjectId").ToString());
                    int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                    var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                    int roleId = myRole.RoleAndCategorizeId;
                    var rowAccessDt = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerRolePerObject(roleId, objectId);
                    string userAndGroupRoleAccessId = "";
                    foreach (DataRow datarow in rowAccessDt.Rows)
                    {
                        userAndGroupRoleAccessId += (userAndGroupRoleAccessId == "") ? datarow["UserAndGroup_Role_AccessId"].ToString() : "," + datarow["UserAndGroup_Role_AccessId"].ToString();
                    }

                    var mySelectControl = (ITC.Library.Controls.SelectControl) item["ResultPerObjectPerRole"].FindControl("SelectControlResultPerObjectPerRole");
                    mySelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessId;
                }
            }
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "RowAccessDetail")
            {
                var parentItem = e.Item.OwnerTableView.ParentItem;
                var objectId = Int32.Parse(parentItem.GetDataKeyValue("ObjectId").ToString());

                int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                int roleId = myRole.RoleAndCategorizeId;
                var rowAccessDt = AcsUserAndGroup_RoleAndCategorize_RowAccess.GetRowAccessObjectsPerRolePerObject(roleId, objectId);
                string userAndGroupRoleAccessStrId = "";
                foreach (DataRow datarow in rowAccessDt.Rows)
                {
                    userAndGroupRoleAccessStrId += (userAndGroupRoleAccessStrId == "") ? datarow["UserAndGroup_Role_AccessId"].ToString() : "," + datarow["UserAndGroup_Role_AccessId"].ToString();
                }
                var myParentSelectControl = (ITC.Library.Controls.SelectControl) parentItem["ResultPerObjectPerRole"].FindControl("SelectControlResultPerObjectPerRole");
                myParentSelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessStrId;



                var childItem = (GridDataItem) e.Item;
                var userAndGroupRoleAccessId = Int32.Parse(childItem.GetDataKeyValue("UserAndGroup_Role_AccessId").ToString());

                var mySelectControl = (ITC.Library.Controls.SelectControl) childItem["ResultPerRolePerRowAccess"].FindControl("SelectControlResultPerRolePerRowAccess");
                mySelectControl.WhereClause = objectId + "_" + userAndGroupRoleAccessId;
            }
        }

        protected void GrvObjectAccess_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

        }

        protected void GrvObjectAccess_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {

        }

        protected void GrvObjectAccess_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {

        }

        protected void GrvRowAccess_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                SetGrvRowAccess();
            }
        }

        protected void GrvRowAccess_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem) e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "RowAccessDetail":
                    {
                        string objectId = dataItem.GetDataKeyValue("ObjectId").ToString();
                        int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                        var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                        int roleId = myRole.RoleAndCategorizeId;
                        e.DetailTableView.DataSource = SetDetailTable(roleId, Int32.Parse(objectId));
                        break;
                    }
            }
        }

        protected void trvRoleAndCategorize_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            SetPageControl();
            GrvRowAccess.Rebind();

            int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
            var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
            int roleId = myRole.RoleAndCategorizeId;
            var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleId);
            var systemId = myRoleAndCategorize.SystemId;
            CmbSystem.SelectedValue = systemId.ToString();
            SetCmbObject(systemId);
            BtnEdit.Visible = false;
            BtnAdd.Visible = true;
        }

        protected void CmbObject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (trvRoleAndCategorize.SelectedNode != null)
            {
                if (CmbObject.SelectedValue != null && CmbObject.SelectedIndex > 0)
                {
                    int objectId = Int32.Parse(CmbObject.SelectedValue);
                    int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                    var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                    int roleId = myRole.RoleAndCategorizeId;
                    SetCmbObjectRowAccess(objectId, roleId);
                }
                SelectControlPerson.WhereClause = "";
            }
            else
            {
                MessageErrorControl2.ShowWarningMessage("ابتدا نقش را انتخاب نمایید");
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                int roleId = myRole.RoleAndCategorizeId;
                var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleId);
                if (myRoleAndCategorize.IsRole)
                {
                    int accessTypeId = Int32.Parse(CmbAccessType.SelectedValue);
                    var rowAccessId = "";
                    foreach (RadComboBoxItem radComboBoxItem in CmbObjectRowAccess.CheckedItems)
                    {
                        rowAccessId += (rowAccessId == "") ? radComboBoxItem.Value : "," + radComboBoxItem.Value;
                    }
                    AcsUserAndGroup_RoleAndCategorize_RowAccess.AddAcsUserAndGroup_RoleAndCategorize_RowAccessGroupDB(-1, roleId, accessTypeId, rowAccessId,
                                                                                                                      bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                    SetPageControl();
                    GrvRowAccess.Rebind();
                    MessageErrorControl2.ShowSuccesMessage("ثبت موفق");
                }
                else
                {
                    MessageErrorControl2.ShowErrorMessage("برای گروه نقش نمی توان دسترسی تعیین نمود");
                }

            }
            catch (Exception ex)
            {
                MessageErrorControl2.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var acsUserAndGroupRoleAndCategorizeRowAccessId = Convert.ToInt32(ViewState["UserAndGroup_Role_AccessId"].ToString());
                int roleTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                var myRole = AcsRoleAndCategorizeTree.GetSingleById(roleTreeId);
                int roleId = myRole.RoleAndCategorizeId;
                var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleId);
                if (myRoleAndCategorize.IsRole)
                {
                    int accessTypeId = Int32.Parse(CmbAccessType.SelectedValue);
                    int rowAccessId = Int32.Parse(CmbObjectRowAccess.SelectedValue);
                    Classes.AcsUserAndGroup_RoleAndCategorize_RowAccess.Update(acsUserAndGroupRoleAndCategorizeRowAccessId, -1, roleId, accessTypeId, rowAccessId,
                                                                               bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());
                    SetPageControl();
                    GrvRowAccess.Rebind();
                    BtnAdd.Visible = true;
                    BtnEdit.Visible = false;
                    MessageErrorControl2.ShowSuccesMessage("ویرایش موفق");
                    SetPageControl();
                }
                else
                {
                    MessageErrorControl2.ShowErrorMessage("برای گروه نقش نمی توان دسترسی تعیین نمود");
                }
            }
            catch (Exception ex)
            {
                MessageErrorControl2.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            GrvRowAccess.Rebind();
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            GrvRowAccess.Rebind();
        }

        protected void SetPageControl()
        {
           // CmbObject.ClearSelection();
            CmbObjectRowAccess.ClearCheckedItems();
            CmbIsActive.ClearSelection();
            CmbAccessType.ClearSelection();
            SelectControlPerson.WhereClause = "";
            CmbObjectRowAccess.ClearCheckedItems();
            CmbObjectRowAccess.KeyId = "";
            CmbAccessType.SelectedIndex = 1;
            CmbIsActive.SelectedIndex = 1;
        }

        protected void CmbObjectRowAccess_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (CmbObject.SelectedIndex > 0 && CmbObjectRowAccess.SelectedIndex > 0)
            {
                int objectId = Int32.Parse(CmbObject.SelectedValue);
                int rowAccessId = Int32.Parse(CmbObjectRowAccess.SelectedValue);
                SelectControlPerson.WhereClause = objectId + "_" + rowAccessId;
            }
            else
            {
                SelectControlPerson.WhereClause = "";
            }
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = AcsRoleAndCategorizeTree.GetAllNodes(-1, TxtGroupName.Text,-1);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["RoleAndCategorizeTreeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'";
                }
                SetRoleAndCategorizeTree();
            }
           
        }

        protected void trvRoleAndCategorize_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (_searchGroupIdList.Contains(Int32.Parse(e.Node.Value)))
            {
                e.Node.ExpandParentNodes();
                e.Node.Text = e.Node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                // node.Selected = true;
            }
        }

        protected void CmbSystem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            int systemId = -1;
            if (CmbSystem.SelectedIndex > 0)
            {
                systemId = Int32.Parse(CmbSystem.SelectedValue);
            }
            SetCmbObject(systemId);
        }
    }
}