﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmCondition : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbPattern();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsCondition.Add(TxtName.Text, TxtSqlCondition.Text, Int32.Parse(CmbPattern.SelectedValue), bool.Parse(CmbIsMultiValue.SelectedValue),
                                 bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition", "ConditionId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "ConditionId=" + addedId, "");
                var pageSize = GrvCondition.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvCondition.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var conditionId = Convert.ToInt32(ViewState["ConditionId"].ToString());
                Classes.AcsCondition.Update(conditionId, TxtName.Text, TxtSqlCondition.Text, Int32.Parse(CmbPattern.SelectedValue), bool.Parse(CmbIsMultiValue.SelectedValue),
                                            bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition", "ConditionId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "ConditionId=" + conditionId, "");
                var pageSize = GrvCondition.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvCondition.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);

            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and ConditionTitle like N'%" + TxtName.Text + "%'";
            }
            if (!string.IsNullOrEmpty(TxtSqlCondition.Text))
            {
                ViewState["WhereClause"] += " and SqlCondition like N'%" + TxtSqlCondition.Text + "%'";
            }

            if (CmbPattern.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and PatternId =" + CmbPattern.SelectedValue;
            }
           
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }
            if (CmbPattern.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and PatternId =" + TxtSqlCondition.Text;
            }

            switch (CmbIsMultiValue.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsMultiValue=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsMultiValue=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetCmbPattern()
        {
            const int conditionPatternGroup = 1; // الگو های شرط ها
            CmbPattern.Items.Clear();
            CmbPattern.Items.Add(new RadComboBoxItem(""));
            CmbPattern.DataTextField = "PatternFullText";
            CmbPattern.DataValueField = "PatternId";
            CmbPattern.DataSource = AcsPattern.GetAllPatternDs(conditionPatternGroup);
            CmbPattern.DataBind();
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvCondition.MasterTableView.CurrentPageIndex = pageIndex;
            GrvCondition.DataSource = GeneralDB.GetPageDB(GrvCondition.MasterTableView.PageSize, pageIndex,
                                                          whereClause, ViewState["sortField"].ToString(),
                                                          "[acs].v_AcsCondition", "ConditionId", out count,
                                                          ViewState["SortType"].ToString());
            GrvCondition.MasterTableView.VirtualItemCount = count;
            GrvCondition.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            TxtSqlCondition.Text = "";
            CmbIsMultiValue.ClearSelection();
            CmbPattern.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvCondition_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var conditionId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["ConditionId"] = conditionId;
                var myCondition = Classes.AcsCondition.GetSingleById(conditionId);
                if (myCondition != null)
                {
                    TxtName.Text = myCondition.ConditionTitle;
                    TxtSqlCondition.Text = myCondition.SqlCondition;
                    CmbPattern.SelectedValue = myCondition.PatternId.ToString();
                    CmbIsMultiValue.SelectedValue = myCondition.IsMultiValue.ToString();
                    CmbIsActive.SelectedValue = myCondition.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsCondition.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvCondition.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvCondition_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvCondition.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvCondition.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvCondition_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}