﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRoleCategorizeManagement.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRoleCategorizeManagement" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<script type="text/javascript">

    function RowSelected(sender, eventArgs) {
        var oArg = new Object();
        oArg.Title = eventArgs._dataKeyValues.RoleAndCategorizeTilte;
        oArg.KeyId = eventArgs._dataKeyValues.RoleAndCategorizeId;
        var oWnd = GetRadWindow();
        oWnd.close(oArg);
    }
    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        return oWindow;
    }
</script>
<div>
    <asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll" 
        BorderColor="White" BorderWidth="2px">
        <table dir="rtl">
            <tr>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                        <Icon PrimaryIconCssClass="rbEdit" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                        OnClick="BtnSearch_Click">
                        <Icon PrimaryIconCssClass="rbSearch" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll" OnClick="BtnShowAll_Click">
                        <Icon PrimaryIconCssClass="rbRefresh" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table dir="rtl" width="100%">
            <tr>
                <td nowrap="nowrap" width="10%">
                    &nbsp;<asp:Panel ID="pnlControls" runat="server">
                        <table>
                            <tr>
                                <td width="10%" align="right" nowrap="nowrap">
                                    <asp:Label ID="LblName" runat="server" Text="عنوان گروه نقش:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadTextBox ID="TxtName" runat="server" Skin="Office2007" Width="300px"></telerik:RadTextBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="TxtName"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" width="10%">
                                    <asp:Label ID="lblSystemTitle" runat="server" Text="عنوان سیستم:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbSystem" runat="server" KeyId="" Skin="Office2007" Width="300px"
                                        AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True"></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvSystem" runat="server" ControlToValidate="CmbSystem"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                                <td width="30%">
                                    &nbsp;
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="10%" align="right" nowrap="nowrap">
                                    <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" 
                                        Width="300px" Filter="Contains" MarkFirstMatch="True"><items><telerik:RadComboBoxItem runat="server" Selected="True" Text=""
                                                Value="-1" /><telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" /><telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" /></items></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                        ErrorMessage="*" InitialValue=""></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                </td>
                                <td width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="GrvRoleAndCategorize" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Office2007" OnItemCommand="GrvRoleAndCategorize_ItemCommand"
            OnPageIndexChanged="GrvRoleAndCategorize_PageIndexChanged" OnPageSizeChanged="GrvRoleAndCategorize_PageSizeChanged"
            OnSortCommand="GrvRoleAndCategorize_SortCommand" Width="95%" OnNeedDataSource="GrvRoleAndCategorize_NeedDataSource"
            AllowCustomPaging="True" OnItemDataBound="GrvRoleAndCategorize_ItemDataBound"><mastertableview datakeynames="RoleAndCategorizeId" dir="RTL" nomasterrecordstext="اطلاعاتی برای نمایش یافت نشد"
                clientdatakeynames="RoleAndCategorizeTilte,RoleAndCategorizeId"><CommandItemSettings ExportToPdfText="Export to PDF" /><RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"><HeaderStyle Width="20px" /></RowIndicatorColumn><ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"><HeaderStyle Width="20px" /></ExpandCollapseColumn><Columns><telerik:GridBoundColumn DataField="RoleAndCategorizeTilte" FilterControlAltText="Filter RoleAndCategorizeTilte column"
                        HeaderText="عنوان گروه نقش" SortExpression="RoleAndCategorizeTilte" UniqueName="RoleAndCategorizeTilte"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="SystemTitle" FilterControlAltText="Filter SystemTitle column"
                        HeaderText="عنوان سیستم" SortExpression="SystemTitle" UniqueName="SystemTitle"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="IsActiveText" FilterControlAltText="Filter IsActiveText column"
                        HeaderText="وضعیت رکورد" SortExpression="IsActiveText" UniqueName="IsActiveText"><HeaderStyle HorizontalAlign="Center" Wrap="False" /><ItemStyle HorizontalAlign="Center" Wrap="False" /></telerik:GridBoundColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="عضویت در گروه"
                        UniqueName="_GroupManage"><ItemTemplate><cc1:SelectControl ID="SelectControlGroupManage" runat="server" WhereClause='<%#Eval("RoleAndCategorizeId").ToString() %>'
                                RadWindowHeight="600" RadWindowWidth="800" imageName="MemberShip" Width="1px"
                                PortalPathUrl="ItcAccess/ModalForm/FrmRoleCategorizeManage.aspx" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="اعضای گروه"
                                UniqueName="_GroupMembers"><ItemTemplate><cc1:SelectControl ID="SelectControl_GroupMember" runat="server" WhereClause='<%#Eval("RoleAndCategorizeId").ToString() %>'
                                                   RadWindowHeight="600" RadWindowWidth="800" imageName="Members" Width="1px" PortalPathUrl="ItcAccess/ModalForm/FrmRoleCategorizeMembers.aspx" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
                        UniqueName="Edit"><ItemTemplate><asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RoleAndCategorizeId").ToString() %>'
                                CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                        UniqueName="Delete"><ItemTemplate><asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RoleAndCategorizeId").ToString() %>'
                                CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn></Columns><EditFormSettings><EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn></EditFormSettings><PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                    NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                    PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                    VerticalAlign="Middle" HorizontalAlign="Center" /></mastertableview><headerstyle height="40px" /><clientsettings reordercolumnsonclient="True" allowcolumnsreorder="True" enablerowhoverstyle="true"><Selecting AllowRowSelect="True"></Selecting></clientsettings><filtermenu enableimagesprites="False"></filtermenu></telerik:RadGrid>
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Windows7">
    </telerik:RadAjaxLoadingPanel>
</div>
<telerik:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvRoleAndCategorize">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>
