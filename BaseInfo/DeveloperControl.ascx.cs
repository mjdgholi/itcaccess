﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class DeveloperControl : System.Web.UI.UserControl
    {
        public string QueryString { get; set; }

        public string RetValueUser
        {
            get
            {
                var values = "";
                switch (ViewState["ControlTitle"].ToString())
                {
                    case "MultiSelectComboBox":
                        {
                            foreach (RadComboBoxItem radComboBoxItem in this.MultiSelectComboBox.CheckedItems)
                            {
                                values += (values == "") ? radComboBoxItem.Text : "," + radComboBoxItem.Text;
                            }
                            break;
                        }
                    case "NumericTextBox":
                        {
                            values = this.NumericTextBox.Text;
                            break;
                        }
                    case "TextBox":
                        {
                            values = this.TextBox.Text;
                            break;
                        }
                    case "JalaliCalendar1":
                        {
                            values = this.JalaliCalendar1.Text;
                            break;
                        }
                    case "TreeViewControl":
                        {
                            foreach (RadTreeNode treeNode in TreeViewControl.CheckedNodes)
                            {
                                values += (values == "") ? treeNode.Text : "," + treeNode.Text;
                            }
                            break;
                        }
                }
                return values;
            }
        }

        public string RetValue
        {
            get
            {
                var values = "";
                switch (ViewState["ControlTitle"].ToString())
                {
                    case "MultiSelectComboBox":
                        {
                            foreach (RadComboBoxItem radComboBoxItem in this.MultiSelectComboBox.CheckedItems)
                            {
                                values += (values == "") ? radComboBoxItem.Value : "," + radComboBoxItem.Value;
                            }
                            break;
                        }
                    case "NumericTextBox":
                        {
                            values = this.NumericTextBox.Text;
                            break;
                        }
                    case "TextBox":
                        {
                            values = this.TextBox.Text;
                            break;
                        }
                    case "JalaliCalendar1":
                        {
                            values = this.JalaliCalendar1.Text;
                            break;
                        }
                    case "TreeViewControl":
                        {
                            foreach (RadTreeNode treeNode in TreeViewControl.CheckedNodes)
                            {
                                values += (values == "") ? treeNode.Value : "," + treeNode.Value;
                            }
                            break;
                        }
                }
                return values;
            }
        }

        public void SetValue(string id, string values)
        {
            string[] valueArray = values.Split(',');

            switch (ViewState["ControlTitle"].ToString())
            {
                case "MultiSelectComboBox":
                    {
                        foreach (RadComboBoxItem radComboBoxItem in this.MultiSelectComboBox.Items)
                        {
                            if (valueArray.Contains(radComboBoxItem.Value))
                            {
                                radComboBoxItem.Checked = true;
                            }
                        }
                        break;
                    }
                case "NumericTextBox":
                    {
                        NumericTextBox.Text = values;
                        break;
                    }
                case "TextBox":
                    {
                        TextBox.Text = values;
                        break;
                    }
                case "JalaliCalendar1":
                    {
                        JalaliCalendar1.Text = values;
                        break;
                    }
                case "TreeViewControl":
                    {
                        foreach (RadTreeNode treeNode in this.TreeViewControl.GetAllNodes())
                        {
                            if (valueArray.Contains(treeNode.Value))
                            {
                                treeNode.Checked = true;
                                treeNode.ExpandParentNodes();
                            }
                        }
                        break;
                    }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["IsPostBackControl"] == null)
            {
                ViewState["ControlTitle"] = "";
            }
            ViewState["IsPostBackControl"] = "True";
        }

        public void SetCmbMultiSelectComboBox(DataSet dataSet)
        {
            //MultiSelectComboBox.EnableCheckAllItemsCheckBox = true;
            //MultiSelectComboBox.EnableTextSelection = false;
            //MultiSelectComboBox.Localization.AllItemsCheckedString = "تمامی موارد انتخاب گردید";
            //MultiSelectComboBox.Localization.CheckAllString = "انتخاب همه";
            //MultiSelectComboBox.Localization.ItemsCheckedString = "گزینه انتخاب گردید";

            MultiSelectComboBox.Items.Clear();
            MultiSelectComboBox.DataSource = dataSet;
            MultiSelectComboBox.DataTextField = "TextField";
            MultiSelectComboBox.DataValueField = "ValueField";
            MultiSelectComboBox.DataBind();
            MultiSelectComboBox.CheckBoxes = true;
        }

        public void SetTrvMultiSelectTreeView(DataSet dataSet)
        {
            TreeViewControl.DataTextField = "TextField";
            TreeViewControl.DataValueField = "ValueField";
            TreeViewControl.DataFieldID = "ValueField";
            TreeViewControl.DataFieldParentID = "OwnerField";
            TreeViewControl.DataSource = dataSet;
            TreeViewControl.DataBind();
            if (TreeViewControl.Nodes.Count > 0)
            {
                TreeViewControl.Nodes[0].Expanded = true;
            }
        }

        public void SetControlVisibility(string id)
        {
            ViewState["ControlTitle"] = id;
            FindControlRecursive(this, id);
        }

        private Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id || root.ID == this.ID)
            {
                root.Visible = true;
            }
            else
            {
                root.Visible = false;
            }
            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null) return t;
            }
            return null;
        }

        public void TreeViewCollapseAllNode()
        {
            if (TreeViewControl.Visible)
            {
                TreeViewControl.CollapseAllNodes();
            }
        }
    }
}