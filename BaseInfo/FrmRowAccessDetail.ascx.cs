﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Controls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
//using Microsoft.SqlServer.TransactSql.ScriptDom;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRowAccessDetail : ModuleControls
    {
        private int _rowAccessId;
        private int _objectId;
        private string _objectTitle;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) || Session["RowAccessId"] == null)
            {
                Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" +
                                                                      ModuleConfiguration.ModuleID + "&page=ItcAccess/HomePage"));
            }
            Page.Title = "ثبت جزئیات دسترسی";
            //     var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            _rowAccessId = Int32.Parse(Session["RowAccessId"].ToString());
            _objectId = AcsRowAccess.GetObjectPerRowAccess(_rowAccessId);
            var myObject = AcsObject.GetSingleById(_objectId);
            _objectTitle = myObject.ObjectTitle;
            mdlRunQuery.WhereClause = _objectId + "_" + _rowAccessId;
            if (Session["RowAccessId"] != null)
            {

                if (ViewState["PageStatus"] == null)
                {
                    ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                    ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                    ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                                   ? "1=1 "
                                                   : ViewState["WhereClause"].ToString();
                    SetCmbObjectFiledTitle(_objectId);
                    SetCmbCondition();
                    SetCmbOperator();
                    SetTreeAccessDetail(_rowAccessId);
                    var myRowAccess = AcsRowAccess.GetSingleById(_rowAccessId);
                    TxtSqlExpersion.Text = myRowAccess.ConditionalExpression;
                    //  CheckSqlExperssionValidation(myRowAccess.ConditionalExpression);
                    SetPageControlForAdd();
                    DeveloperControl1.SetControlVisibility("TextBox");
                    BtnEdit.Visible = false;
                    ViewState["PageStatus"] = "IsPostBack";
                }
            }
            else
            {
                MessageErrorControl1.ShowWarningMessage("اعتبار صفحه به پایان رسیده است ، لطفا صفحه را دوباره بارگذاری نمایید");
            }
            if (!IsPostBack)
            {

            }
        }

        protected void Validating()
        {
            int count;
            GeneralDB.GetPageDB(10, 0, "RowAccessId=" + _rowAccessId, "", "acs.t_AcsRowAccessDetail", "RowAccessDetailId", out count, "");
            if (count > 0)
            {
                if (CmbOperator.SelectedIndex < 1)
                    throw new Exception("عملگر را انتخاب نمایید");
                if (treeRowAccessDetail.SelectedNode == null)
                    throw new Exception("شرط دربرگیرنده را انتخاب نمایید");
            }
            if (DeveloperControl1.RetValue == "" && CmbFunction.SelectedIndex < 1)
            {
                throw new Exception("حداقل یکی از موارد مقادیر یا تابع باید مقدار بگیرند");
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Validating();
                int addedId;
                int userObjectFieldId = (CmbObjectField.SelectedIndex > 0) ? Int32.Parse(CmbObjectField.SelectedValue) : -1;
                int functionId = (CmbFunction.SelectedIndex > 0) ? Int32.Parse(CmbFunction.SelectedValue) : -1;
                int conditionId = (CmbCondition.SelectedIndex > 0) ? Int32.Parse(CmbCondition.SelectedValue) : -1;
                int operatorId = (CmbOperator.SelectedIndex > 0) ? Int32.Parse(CmbOperator.SelectedValue) : -1;
                int userOwnerId = (string.IsNullOrEmpty(treeRowAccessDetail.SelectedValue)) ? -1 : Int32.Parse(treeRowAccessDetail.SelectedValue);
                var myUserOwner = AcsRowAccessDetail.GetSingleById(userOwnerId);
                int ownerId;
                if (myUserOwner != null && myUserOwner.OperatorId == null) // عملگر نمی باشد
                {
                    if (myUserOwner.SystemRowAccessDetailOwnerId != null)
                    {
                        ownerId = Int32.Parse(myUserOwner.SystemRowAccessDetailOwnerId.ToString());
                    }
                    else
                        ownerId = -1;
                }
                else
                {
                    ownerId = userOwnerId;
                }
                string values = DeveloperControl1.RetValue;
                string userValue = DeveloperControl1.RetValueUser;
                if (values != "")
                {
                    functionId = -1;
                }
                AcsRowAccessDetail.p_AcsRowAccessDetailAddInTree(_rowAccessId, userObjectFieldId, operatorId, functionId, values, userValue, conditionId, DateTime.Now.ToString(), DateTime.Now.ToString(), ownerId, out addedId);
                SetTreeAccessDetail(_rowAccessId);
                var myRowAccess = AcsRowAccess.GetSingleById(_rowAccessId);
                TxtSqlExpersion.Text = myRowAccess.ConditionalExpression;
                // CheckSqlExperssionValidation(myRowAccess.ConditionalExpression);
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                treeRowAccessDetail.FindNodeByValue(addedId.ToString()).Selected = true;
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowWarningMessage(exception.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            int rowAccessDetailId = Int32.Parse(ViewState["RowAccessDetailId"].ToString());
            int userRowAccessDetailId = Int32.Parse(ViewState["UserRowAccessDetailId"].ToString());
            UpdateAccessDetail(rowAccessDetailId, userRowAccessDetailId);
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (treeRowAccessDetail.SelectedValue != null)
                {
                    int userRowAccessDetailId = Int32.Parse(treeRowAccessDetail.SelectedValue);
                    DeleteAccessDetail(userRowAccessDetailId);
                    MessageErrorControl1.ShowSuccesMessage("حذف موفق");
                    SetPageControl();
                }
                else
                    throw new Exception("عبارت مورد نظر برای حذف را انتخاب نمایید");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void DeleteAccessDetail(int userRowAccessDetailId)
        {
            try
            {
                AcsRowAccessDetail.DeleteAcsRowAccessDetailFromTree(userRowAccessDetailId);
                SetTreeAccessDetail(_rowAccessId);
                //   AcsRowAccess.UpdateRowAccessConditionalExpression(rowAccessId, sqlExpr);
                var myRowAccess = AcsRowAccess.GetSingleById(_rowAccessId);
                TxtSqlExpersion.Text = myRowAccess.ConditionalExpression;
                CheckSqlExperssionValidation(myRowAccess.ConditionalExpression);
                MessageErrorControl1.ShowSuccesMessage("حذف موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void UpdateAccessDetail(int rowAccessDetailId, int userRowAccessDetailId)
        {
            try
            {
                int objectFieldId = (CmbObjectField.SelectedIndex > 0) ? Int32.Parse(CmbObjectField.SelectedValue) : -1;
                int functionId = (CmbFunction.SelectedIndex > 0) ? Int32.Parse(CmbFunction.SelectedValue) : -1;
                int conditionId = (CmbCondition.SelectedIndex > 0) ? Int32.Parse(CmbCondition.SelectedValue) : -1;
                int operatorId = (CmbOperator.SelectedIndex > 0) ? Int32.Parse(CmbOperator.SelectedValue) : -1;
                string values = DeveloperControl1.RetValue;
                string userValue = DeveloperControl1.RetValueUser;
                if (values != "")
                {
                    functionId = -1;
                }
                AcsRowAccessDetail.AcsRowAccessDetailUpdateInTree(rowAccessDetailId, objectFieldId, operatorId, functionId, values, userValue, conditionId);
                SetTreeAccessDetail(_rowAccessId);
                //   AcsRowAccess.UpdateRowAccessConditionalExpression(rowAccessId, sqlExpr);                
                var myRowAccess = AcsRowAccess.GetSingleById(_rowAccessId);
                TxtSqlExpersion.Text = myRowAccess.ConditionalExpression;
                //  CheckSqlExperssionValidation(myRowAccess.ConditionalExpression);

                SetPageControl();
                SetPageControlForAdd();
                treeRowAccessDetail.FindNodeByValue(userRowAccessDetailId.ToString()).Selected = true;
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void SetPageControl()
        {
            CmbObjectField.ClearSelection();           
            CmbFunction.ClearSelection();
            CmbCondition.ClearSelection();
            CmbOperator.ClearSelection();
            treeRowAccessDetail.ClearSelectedNodes();
            //  TxtSqlExpersion.Text = "";
            DeveloperControl1.SetControlVisibility("TextBox");
            DeveloperControl1.SetValue("TextBox", "");            
        }

        protected void SetPageControlForEdit(bool isOperator)
        {
            BtnAdd.Visible = false;
            BtnEdit.Visible = true;
            //   treeRowAccessDetail.Enabled = false;
            if (isOperator)
            {
                CmbObjectField.Enabled = false;
                CmbCondition.Enabled = false;            
                CmbFunction.Enabled = false;
                CmbOperator.Enabled = true;
                DeveloperControl1.SetControlVisibility("Disable");
            }
            else
            {
                CmbObjectField.Enabled = true;
                CmbCondition.Enabled = true;                
                CmbFunction.Enabled = true;
                CmbOperator.Enabled = false;
                //TxtValues.Enabled = true;
            }
        }

        protected void SetPageControlForAdd()
        {
            BtnAdd.Visible = true;
            BtnEdit.Visible = false;
            treeRowAccessDetail.Enabled = true;
            CmbObjectField.Enabled = true;
            CmbCondition.Enabled = true;          
            CmbFunction.Enabled = true;
            CmbOperator.Enabled = true;
        }       

        protected void SetCmbObjectFiledTitle(int objectId)
        {
            CmbObjectField.Items.Clear();
            CmbObjectField.Items.Add(new RadComboBoxItem("", "-1"));
            CmbObjectField.DataTextField = "FieldTitle";
            CmbObjectField.DataValueField = "ObjectFieldId";
            CmbObjectField.DataSource = AcsObjectField.GetAcsObjectFieldCollectionByAcsObject(objectId);
            CmbObjectField.DataBind();
        }

        protected void SetCmbFunction(int objectFieldId)
        {
            CmbFunction.Items.Clear();
            CmbFunction.Items.Add(new RadComboBoxItem("", "-1"));
            CmbFunction.DataTextField = "FunctionTitle";
            CmbFunction.DataValueField = "FunctionId";
            CmbFunction.DataSource = AcsFunction.GetAcsFunctionCollectionByAcsObjectField(objectFieldId);
            CmbFunction.DataBind();
        }

        protected void SetCmbCondition()
        {
            CmbCondition.Items.Clear();
            CmbCondition.Items.Add(new RadComboBoxItem("", "-1"));
            CmbCondition.DataTextField = "FullTitle";
            CmbCondition.DataValueField = "ConditionId";
            CmbCondition.DataSource = AcsCondition.GetAllAcsCondition();
            CmbCondition.DataBind();
        }

        protected void SetCmbOperator()
        {
           CmbOperator.Items.Clear();
            CmbOperator.Items.Add(new RadComboBoxItem("", "-1"));
            CmbOperator.DataTextField = "FullTitle";
            CmbOperator.DataValueField = "OperatorId";
            CmbOperator.DataSource = AcsOperator.GetAllOperator();
            CmbOperator.DataBind();
        }      

        protected void CmbObjectField_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SetCmbFunction(Int32.Parse(e.Value));
            if (CmbCondition.SelectedIndex > 0 && CmbObjectField.SelectedIndex > 0)
            {
                var conditionId = Int32.Parse(CmbCondition.SelectedValue);
                var fieldId = Int32.Parse(e.Value);
                SetDeveloperControl(conditionId, fieldId);
            }
            else
            {
                DeveloperControl1.SetControlVisibility("TextBox");
            }
        }

        protected void SetTreeAccessDetail(int rowAccessId)
        {
            const bool loadUserData = true; // تنها اطلاعات معادل کاربر نمایش داده شود
            treeRowAccessDetail.DataTextField = "ExperssionTitle";
            treeRowAccessDetail.DataValueField = "RowAccessDetailId";
            treeRowAccessDetail.DataFieldID = "RowAccessDetailId";
            treeRowAccessDetail.DataFieldParentID = "OwnerId";
            DataTable dtAccessDetail = AcsRowAccessDetail.RowAccessDetailGetAll(rowAccessId, loadUserData).Tables[0];
            treeRowAccessDetail.DataSource = dtAccessDetail;
            treeRowAccessDetail.DataBind();
            treeRowAccessDetail.ExpandAllNodes();

        }

        protected string SqlExperssionResult()
        {
            //DataTable dtTree = AcsRowAccessDetail.RowAccessGetChildTree(_rowAccessId).Tables[0];
            //ViewState["RowAccessDetail"] = dtTree;
            //foreach (DataRow datarow in dtTree.Rows)
            //{
            //    if (String.IsNullOrEmpty(datarow["OwnerId"].ToString()))
            //    {
            //        return TxtSqlExpersion.Text = GenerateSqlExperssion(datarow);                    
            //    }
            //}
            return "";

        }

        protected void treeRowAccessDetail_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                int userRowAccessDetailId = Int32.Parse(clickedNode.Value);
                int rowAccessDetailId;
                switch (e.MenuItem.Value)
                {
                    case "remove":
                        {
                            DeleteAccessDetail(userRowAccessDetailId);
                            break;
                        }
                    case "edit":
                        {
                            var myUserRowAccessDetail = AcsRowAccessDetail.GetSingleById(userRowAccessDetailId);
                            if (myUserRowAccessDetail.OperatorId != null) // عملگر است
                            {
                                rowAccessDetailId = userRowAccessDetailId;
                            }
                            else
                            {
                                rowAccessDetailId = Int32.Parse(myUserRowAccessDetail.SystemRowAccessDetailOwnerId.ToString());
                            }
                            var myRowAccessDetail = AcsRowAccessDetail.GetSingleById(Int32.Parse(rowAccessDetailId.ToString())); // با اطلاعات سیستمی پر میشود                            
                            SetPageControl();
                            if (myRowAccessDetail != null)
                            {
                                ViewState["RowAccessDetailId"] = rowAccessDetailId;
                                ViewState["UserRowAccessDetailId"] = userRowAccessDetailId;
                                SetPageControlForEdit(false);
                                int conditionId = -1;

                                if (myRowAccessDetail.OperatorId == null) // عملگر نمی باشد
                                {
                                                    
                                    var userFieldId = Int32.Parse(myUserRowAccessDetail.ObjectFieldId.ToString());                                  
                                    CmbObjectField.SelectedValue = userFieldId.ToString();
                                    if (myRowAccessDetail.ConditionId != null)
                                    {
                                        conditionId = Int32.Parse(myRowAccessDetail.ConditionId.ToString());
                                        CmbCondition.SelectedValue = myRowAccessDetail.ConditionId.ToString();
                                    }

                                    SetCmbFunction(Int32.Parse(myRowAccessDetail.ObjectFieldId.ToString()));

                                    if (myRowAccessDetail.FunctionId != null)
                                        CmbFunction.SelectedValue = myRowAccessDetail.FunctionId.ToString();
                                    else
                                    {
                                        var developerControlTitle = AcsCondition_Datatype_DveloperControl.GetDeveloperControlPerConditionAndField(conditionId, userFieldId);
                                        SetDeveloperControl(conditionId, userFieldId);
                                        DeveloperControl1.SetValue(developerControlTitle, myRowAccessDetail.ValuesOrFunction);
                                        //TxtValues.Text = rowAccessDetail.ValuesOrFunction;
                                    }
                                }
                                else
                                {
                                    SetPageControlForEdit(true);
                                    CmbOperator.SelectedValue = myRowAccessDetail.OperatorId.ToString();
                                }

                            }

                            break;
                        }
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnReturn_Click(object sender, EventArgs e)
        {
            //bool initialQuotedIdentifiers = false;
            //TSqlParser parser = new TSql110Parser(initialQuotedIdentifiers);
            //StreamReader sr = new StreamReader(@"c:\test\sqltest.sql");
            //IList<ParseError> errors;
            //TSqlFragment fragment = parser.Parse(sr, out errors);
            //StreamWriter sw = new StreamWriter(@"c:\test\sqltestResult.sql");
            //Sql110ScriptGenerator scriptGen = new Sql110ScriptGenerator();
            //string outPut = "";
            //scriptGen.GenerateScript(fragment, out outPut);
            //sw.Close();
            //sr.Close();
            //SetPageControl();
            //SetPageControlForAdd();
        }

        protected DataRow GetRightOrLeftChild(int rowAccessDetailId, bool rightChild)
        {
            int rightOrLeftChild = (rightChild) ? 1 : 2;
            DataTable rowAccessDetailDt = (DataTable) ViewState["RowAccessDetail"];
            foreach (DataRow datarow in rowAccessDetailDt.Rows)
            {
                if (!string.IsNullOrEmpty(datarow["OwnerId"].ToString()) && Int32.Parse(datarow["OwnerId"].ToString()) == rowAccessDetailId && Int32.Parse(datarow["ChildDirecttion"].ToString()) == rightOrLeftChild)
                {
                    return datarow;
                }
            }

            return null;
        }

        protected string GenerateSqlExperssion(DataRow dataRow)
        {
            //string leftChildStr = "", rightChildStr = "", parentStr = "";
            //int rowAccessDetailId = Int32.Parse(dataRow["RowAccessDetailId"].ToString());
            //var leftChild = GetRightOrLeftChild(rowAccessDetailId, false);
            //if (leftChild != null) // left Child
            //{
            //    leftChildStr = GenerateSqlExperssion(leftChild);
            //}

            //parentStr = dataRow["ExperssionTitle"].ToString();

            //var rightChild = GetRightOrLeftChild(rowAccessDetailId, true);
            //if (rightChild != null) // right Child
            //{
            //    rightChildStr = GenerateSqlExperssion(rightChild);
            //}

            //leftChildStr = (leftChildStr != "") ? "(" + leftChildStr : "";
            //rightChildStr = (rightChildStr != "") ? rightChildStr + ")" : "";
            //return leftChildStr + " " + parentStr + " " + rightChildStr;
            return "";
        }

        protected void CheckSqlExperssionValidation(string sqlEpr)
        {
            //if (string.IsNullOrEmpty(sqlEpr.Trim()))
            //    return;

            //sqlEpr = " select * from testTable where " + sqlEpr; // Extra Len  = 30
            //const bool initialQuotedIdentifiers = false;
            //TSqlParser parser = new TSql110Parser(initialQuotedIdentifiers);
            //TextReader textReader = new StringReader(sqlEpr);
            //IList<ParseError> errors;
            //TSqlFragment fragment = parser.Parse(textReader, out errors);
            //if (errors.Count > 0)
            //{
            //    foreach (var parseError in errors)
            //    {
            //        var errorCol = parseError.Column - 30;

            //        TxtSqlExpersion.Text = TxtSqlExpersion.Text.Substring(0, errorCol - 5) + "<span style='color:red'>" +
            //                               TxtSqlExpersion.Text.Substring(errorCol - 5, 6) + "</span>" +
            //                               TxtSqlExpersion.Text.Substring(errorCol + 1, TxtSqlExpersion.Text.Length - (errorCol + 1));
            //    }
            //}
        }

        protected void CmbCondition_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var conditionId = Int32.Parse(e.Value);
            var fieldId = Int32.Parse(CmbObjectField.SelectedValue);
            SetDeveloperControl(conditionId, fieldId);
        }

        protected void SetDeveloperControl(int conditionId, int fieldId)
        {
            try
            {
                var developerControlTitle = AcsCondition_Datatype_DveloperControl.GetDeveloperControlPerConditionAndField(conditionId, fieldId);
                this.DeveloperControl1.SetControlVisibility(developerControlTitle);               
                var myObject = AcsObject.GetSingleById(_objectId);
                if (myObject != null)
                {
                    bool isHierarchy = myObject.IsHierarchy;
                    var selectQuery = "";

                    var objectFieldList = AcsObjectField.GetAcsObjectFieldCollectionByAcsObject(_objectId);
                    if (objectFieldList == null) throw new Exception("فیلد های جدول تعریف نشده است");
                    var keyField = "";
                    var ownerField = "";
                    foreach (var acsObjectField in objectFieldList)
                    {
                        var myObjectFieldKey = AcsObjectField.GetSingleById(acsObjectField.ObjectFieldId);
                        if (acsObjectField.IsKey)
                        {                                                     
                            keyField = myObjectFieldKey.FieldTitle;
                        }
                        else if (acsObjectField.IsOwner)
                        {
                            ownerField = myObjectFieldKey.FieldTitle;
                        }
                    }
                    if (keyField.Trim() == "")
                    {
                        throw new Exception("فیلد کلید تعیین نشده است");
                    }
                    var objectFieldId = Int32.Parse(CmbObjectField.SelectedValue);
                    var myObjectField = AcsObjectField.GetSingleById(objectFieldId);
                    if (isHierarchy)
                    {
                      
                        if (ownerField.Trim() == "")
                        {
                            throw new Exception("فیلد دربرگیرنده تعیین نشده است");
                        }

                        selectQuery = string.Format("select {0} as TextField,{1} as ValueField,{2} as OwnerField from {3}", myObjectField.FieldTitle, keyField, ownerField, _objectTitle);
                    }
                    else
                    {
                        selectQuery = string.Format("select {0} as TextField,{1} as ValueField from {2}", myObjectField.FieldTitle, keyField, _objectTitle);
                    }

                    var dataset = SqlClass.ExecuteSqlQuery(selectQuery);
                    if (developerControlTitle == "MultiSelectComboBox")
                    {
                        this.DeveloperControl1.SetCmbMultiSelectComboBox(dataset);
                    }
                    if (developerControlTitle == "TreeViewControl")
                    {
                        this.DeveloperControl1.SetTrvMultiSelectTreeView(dataset);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void SetDeveloperControlVisibility(string id)
        {
            DeveloperControl1.SetControlVisibility(id);
        }

        protected void imgbRunSqlCommand_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void treeRowAccessDetail_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (e.Node.Text.Length > 100)
            {
                e.Node.ToolTip = e.Node.Text;
                e.Node.Text = e.Node.Text.Substring(0, 100) + " ...";
            }
        }
    }
}