﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmAcsCondition_Datatype_Pattern : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbPattern();
                SetCmbDataType();
                SetCmbCondition();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsCondition_Datatype_Pattern.Add(Int32.Parse(CmbCondition.SelectedValue), Int32.Parse(CmbDataType.SelectedValue),
                                                  Int32.Parse(CmbPattern.SelectedValue), bool.Parse(CmbIsActive.SelectedValue),
                                                  DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition_Datatype_Pattern", "Condition_Datatype_PatternId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "Condition_Datatype_PatternId=" + addedId, "");
                var pageSize = GrvCondition_Datatype_Pattern.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvCondition_Datatype_Pattern.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var conditionDatatypePatternId = Convert.ToInt32(ViewState["Condition_Datatype_PatternId"].ToString());
                Classes.AcsCondition_Datatype_Pattern.Update(conditionDatatypePatternId, Int32.Parse(CmbCondition.SelectedValue), Int32.Parse(CmbDataType.SelectedValue),
                                                             Int32.Parse(CmbPattern.SelectedValue), bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsCondition_Datatype_Pattern", "Condition_Datatype_PatternId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "Condition_Datatype_PatternId=" + conditionDatatypePatternId, "");
                var pageSize = GrvCondition_Datatype_Pattern.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvCondition_Datatype_Pattern.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (CmbCondition.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and ConditionId  =" + CmbCondition.SelectedValue;
            }

            if (CmbDataType.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and DataTypeId  =" + CmbDataType.SelectedValue;
            }

            if (CmbPattern.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and PatternId =" + CmbPattern.SelectedValue;
            }

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetCmbCondition()
        {
            CmbCondition.Items.Clear();
            CmbCondition.Items.Add(new RadComboBoxItem("", "-1"));
            CmbCondition.DataTextField = "FullTitle";
            CmbCondition.DataValueField = "ConditionId";
            CmbCondition.DataSource = AcsCondition.GetAllAcsCondition();
            CmbCondition.DataBind();
        }

        protected void SetCmbDataType()
        {
            CmbDataType.Items.Clear();
            CmbDataType.Items.Add(new RadComboBoxItem("", "-1"));
            CmbDataType.DataTextField = "DataTypeTitleFullText";
            CmbDataType.DataValueField = "DataTypeId";
            CmbDataType.DataSource = AcsDataType.GetAllDataTypeDs();
            CmbDataType.DataBind();
        }

        protected void SetCmbPattern()
        {
            const int conditionPatternGroup = 2; // الگو های مقادیر شرط های تکرارپذیر
            CmbPattern.Items.Clear();
            CmbPattern.Items.Add(new RadComboBoxItem(""));
            CmbPattern.DataTextField = "PatternFullText";
            CmbPattern.DataValueField = "PatternId";
            CmbPattern.DataSource = AcsPattern.GetAllPatternDs(conditionPatternGroup);
            CmbPattern.DataBind();
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvCondition_Datatype_Pattern.MasterTableView.CurrentPageIndex = pageIndex;
            GrvCondition_Datatype_Pattern.DataSource = GeneralDB.GetPageDB(GrvCondition_Datatype_Pattern.MasterTableView.PageSize, pageIndex,
                                                                           whereClause, ViewState["sortField"].ToString(),
                                                                           "[acs].v_AcsCondition_Datatype_Pattern", "Condition_Datatype_PatternId", out count,
                                                                           ViewState["SortType"].ToString());
            GrvCondition_Datatype_Pattern.MasterTableView.VirtualItemCount = count;
            GrvCondition_Datatype_Pattern.DataBind();
        }

        protected void SetPageControl()
        {
            CmbCondition.ClearSelection();
            CmbPattern.ClearSelection();
            CmbDataType.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvCondition_Datatype_Pattern_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var conditionDatatypePatternId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["Condition_Datatype_PatternId"] = conditionDatatypePatternId;
                var mySystem = Classes.AcsCondition_Datatype_Pattern.GetSingleById(conditionDatatypePatternId);
                if (mySystem != null)
                {

                    CmbCondition.SelectedValue = mySystem.ConditionId.ToString();
                    CmbPattern.SelectedValue = mySystem.PatternId.ToString();
                    CmbDataType.SelectedValue = mySystem.DataTypeId.ToString();
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsCondition_Datatype_Pattern.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvCondition_Datatype_Pattern.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvCondition_Datatype_Pattern_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_Datatype_Pattern_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvCondition_Datatype_Pattern.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_Datatype_Pattern_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvCondition_Datatype_Pattern.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvCondition_Datatype_Pattern_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvCondition_Datatype_Pattern_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}