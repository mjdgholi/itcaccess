﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmAcsCondition_Datatype_Pattern.ascx.cs" Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmAcsCondition_Datatype_Pattern" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<div>
    <asp:Panel Direction="RightToLeft" runat="server" ID="pnlAll">
        <table dir="rtl">
            <tr>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click"></cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                        <Icon PrimaryIconCssClass="rbEdit" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                        OnClick="BtnSearch_Click">
                        <Icon PrimaryIconCssClass="rbSearch" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                        OnClick="BtnShowAll_Click" Empty="" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                        <Icon PrimaryIconCssClass="rbRefresh" />
                    </cc1:CustomRadButton>
                </td>
                <td align="right">
                    <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <table dir="rtl" width="100%">
            <tr>
                <td nowrap="nowrap" width="10%">
                    <asp:Panel ID="pnlControls" runat="server" BorderColor="White" BorderWidth="2px">
                        <table style="text-align: right" width="100%">
                            <tr>
                                <td align="right" width="10%" nowrap="nowrap">
                                    <asp:Label ID="LblCondition_Datatype_PatternTitle" runat="server" 
                                        Text="عنوان شرط:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbCondition" runat="server" 
                                        AppendDataBoundItems="True" Filter="Contains" KeyId="" MarkFirstMatch="True" 
                                        Skin="Office2007" Width="300px"></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="CmbCondition"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                </td>
                                <td width="30%">
                                </td>
                                <td width="10%">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap="nowrap" width="10%">
                                    <asp:Label ID="LblHasAccess" runat="server" Text="نوع داده:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbDataType" runat="server" KeyId="" Skin="Office2007" Width="300px"
                                        AppendDataBoundItems="True" Filter="Contains" MarkFirstMatch="True"></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvHasAccess" runat="server" ControlToValidate="CmbDataType"
                                        ErrorMessage="*" ></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    
                                </td>
                                <td width="30%">
                                    
                                </td>
                                <td width="10%">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="right" nowrap="nowrap" width="10%">
                                    <asp:Label ID="LblPattern" runat="server" Text="الگو:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbPattern" runat="server" AppendDataBoundItems="True"
                                        KeyId="" Skin="Office2007" Width="300px" Filter="Contains" 
                                        MarkFirstMatch="True"></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvHasAccess0" runat="server" ControlToValidate="CmbPattern"
                                        ErrorMessage="*" ></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    </td>
                                <td width="30%">
                                    
                                </td>
                                <td width="10%">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="right" width="10%" nowrap="nowrap">
                                    <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                                </td>
                                <td width="30%">
                                    <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" 
                                        Width="300px" Filter="Contains" MarkFirstMatch="True"><items><telerik:RadComboBoxItem runat="server" Selected="True" 
                                                    /><telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" /><telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" /></items></telerik:RadComboBox>
                                </td>
                                <td width="10%">
                                    <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                        ErrorMessage="*" ></asp:RequiredFieldValidator>
                                </td>
                                <td width="10%">
                                    
                                </td>
                                <td width="30%">
                                    
                                </td>
                                <td width="10%">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="6" nowrap="nowrap">
                                    <telerik:RadGrid ID="GrvCondition_Datatype_Pattern" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                        AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
                                        OnItemCommand="GrvCondition_Datatype_Pattern_ItemCommand" OnItemDataBound="GrvCondition_Datatype_Pattern_ItemDataBound"
                                        OnNeedDataSource="GrvCondition_Datatype_Pattern_NeedDataSource" OnPageIndexChanged="GrvCondition_Datatype_Pattern_PageIndexChanged"
                                        OnPageSizeChanged="GrvCondition_Datatype_Pattern_PageSizeChanged" OnSortCommand="GrvCondition_Datatype_Pattern_SortCommand"
                                        Skin="Office2007" Width="95%"><headercontextmenu cssclass="GridContextMenu GridContextMenu_Default"></headercontextmenu><mastertableview clientdatakeynames="Condition_Datatype_PatternId" datakeynames="Condition_Datatype_PatternId" dir="RTL"
                                            nomasterrecordstext="اطلاعاتی برای نمایش یافت نشد"><CommandItemSettings ExportToPdfText="Export to PDF" /><commanditemsettings exporttopdftext="Export to PDF" /><rowindicatorcolumn filtercontrolalttext="Filter RowIndicator column"><HeaderStyle Width="20px" /></rowindicatorcolumn><expandcollapsecolumn filtercontrolalttext="Filter ExpandColumn column"><HeaderStyle Width="20px" /></expandcollapsecolumn><Columns><telerik:GridBoundColumn DataField="ConditionFullText" 
                                                    
                                            FilterControlAltText="Filter Condition_Datatype_PatternTitle column" HeaderText="عنوان شرط" 
                                                    SortExpression="ConditionFullText" UniqueName="ConditionFullText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="DataTypeFullText" 
                                                    FilterControlAltText="Filter HasAccessText column" HeaderText="نوع داده" 
                                                    SortExpression="DataTypeFullText" UniqueName="DataTypeFullText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="PatternFullText" 
                                                    FilterControlAltText="Filter IsDeniedAccessText column" HeaderText="الگو" 
                                                    SortExpression="PatternFullText" UniqueName="PatternFullText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridBoundColumn DataField="IsActiveText" 
                                                    FilterControlAltText="Filter IsActiveText column" HeaderText="وضعیت رکورد" 
                                                    SortExpression="Condition_Datatype_PatternTitle" UniqueName="IsActiveText"><HeaderStyle HorizontalAlign="Right" Wrap="False" /><ItemStyle HorizontalAlign="Right" Wrap="False" /></telerik:GridBoundColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                                    HeaderText="ویرایش" UniqueName="Edit"><ItemTemplate><asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" 
                                                            CommandArgument='<%#Eval("Condition_Datatype_PatternId").ToString() %>' CommandName="_MyِEdit" 
                                                            ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn><telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                                    HeaderText="حذف" UniqueName="Delete"><ItemTemplate><asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" 
                                                            CommandArgument='<%#Eval("Condition_Datatype_PatternId").ToString() %>' CommandName="_MyِDelete" 
                                                            ForeColor="#000066" ImageUrl="../Images/Delete.bmp" 
                                                            OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate><HeaderStyle HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></telerik:GridTemplateColumn></Columns><editformsettings><editcolumn filtercontrolalttext="Filter EditCommandColumn column"></editcolumn></editformsettings><PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" 
                                                LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" 
                                                NextPageToolTip="صفحه بعدی" 
                                                PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;." 
                                                PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" 
                                                PrevPageToolTip="صفحه قبلی" VerticalAlign="Middle" /></mastertableview><headerstyle height="40px" /><clientsettings allowcolumnsreorder="True" enablerowhoverstyle="true" reordercolumnsonclient="True"><selecting allowrowselect="True" /><selecting allowrowselect="True" /></clientsettings><filtermenu enableimagesprites="False"></filtermenu></telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvCondition_Datatype_Pattern">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
