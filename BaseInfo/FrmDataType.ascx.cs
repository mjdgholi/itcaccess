﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmDataType : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbSqlDataType();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsDataType.Add(TxtDataTypeTitle.Text,CmbDataTypeSqlTitle.SelectedItem.Text,Int32.Parse(CmbDataTypeSqlTitle.SelectedValue),
                                  bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(),out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsDataType", "DataTypeId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "DataTypeId=" + addedId, "");
                var pageSize = GrvDataType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvDataType.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var DataTypeId = Convert.ToInt32(ViewState["DataTypeId"].ToString());
                Classes.AcsDataType.Update(DataTypeId, TxtDataTypeTitle.Text, CmbDataTypeSqlTitle.SelectedItem.Text, Int32.Parse(CmbDataTypeSqlTitle.SelectedValue),
                                  bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsDataType", "DataTypeId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "DataTypeId=" + DataTypeId, "");
                var pageSize = GrvDataType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvDataType.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtDataTypeTitle.Text))
            {
                ViewState["WhereClause"] += " and DataTypeTitle like N'%" + TxtDataTypeTitle.Text + "%'";
            }

            if (CmbDataTypeSqlTitle.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and DataTypeSqlTitle like N'%" + CmbDataTypeSqlTitle.SelectedItem.Text + "%'";
            }

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }         

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvDataType.MasterTableView.CurrentPageIndex = pageIndex;
            GrvDataType.DataSource = GeneralDB.GetPageDB(GrvDataType.MasterTableView.PageSize, pageIndex,
                                                           whereClause, ViewState["sortField"].ToString(),
                                                           "[acs].v_AcsDataType", "DataTypeId", out count,
                                                           ViewState["SortType"].ToString());
            GrvDataType.MasterTableView.VirtualItemCount = count;
            GrvDataType.DataBind();
        }

        protected  void SetCmbSqlDataType()
        {
            CmbDataTypeSqlTitle.Items.Clear();
            CmbDataTypeSqlTitle.Items.Add(new RadComboBoxItem(""));
            CmbDataTypeSqlTitle.DataSource = SqlClass.GetAllDataType();
            CmbDataTypeSqlTitle.DataTextField = "NAME";
            CmbDataTypeSqlTitle.DataValueField = "system_type_id";
            CmbDataTypeSqlTitle.DataBind();
        }

        protected void SetPageControl()
        {
            TxtDataTypeTitle.Text = "";
            CmbDataTypeSqlTitle.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvDataType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var dataTypeId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["DataTypeId"] = dataTypeId;
                var mySystem = Classes.AcsDataType.GetSingleById(dataTypeId);
                if (mySystem != null)
                {
                    TxtDataTypeTitle.Text = mySystem.DataTypeTitle;
                    CmbDataTypeSqlTitle.SelectedValue = mySystem.DataTypeSqlId.ToString();                   
                    CmbIsActive.SelectedValue = mySystem.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsDataType.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvDataType.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvDataType_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvDataType_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvDataType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvDataType_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvDataType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvDataType_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvDataType_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}