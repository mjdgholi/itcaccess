﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmObjectType : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetGrvObjectData(0, "");
                SetCmbObjectType();
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsObjectType.Add(TxtName.Text, CmbObjectType.SelectedItem.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObjectType", "ObjectTypeId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "ObjectTypeId=" + addedId, "");
                var pageSize = GrvObjectType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvObjectType.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var objectTypeId = Convert.ToInt32(ViewState["ObjectTypeId"].ToString());
                Classes.AcsObjectType.Update(objectTypeId, TxtName.Text, CmbObjectType.SelectedItem.Text, bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObjectType", "ObjectTypeId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "ObjectTypeId=" + objectTypeId, "");
                var pageSize = GrvObjectType.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvObjectType.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and ObjectTypeTitle like N'%" + TxtName.Text + "%'";
            }

            if (!string.IsNullOrEmpty(CmbObjectType.SelectedItem.Text))
            {
                ViewState["WhereClause"] += " and ObjectTypeEnglieshTitle like N'%" + CmbObjectType.SelectedItem.Text + "%'";
            }
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvObjectType.MasterTableView.CurrentPageIndex = pageIndex;
            GrvObjectType.DataSource = GeneralDB.GetPageDB(GrvObjectType.MasterTableView.PageSize, pageIndex,
                                                            whereClause, ViewState["sortField"].ToString(),
                                                            "[acs].v_AcsObjectType", "ObjectTypeId", out count,
                                                            ViewState["SortType"].ToString());
            GrvObjectType.MasterTableView.VirtualItemCount = count;
            GrvObjectType.DataBind();
        }

        protected void SetCmbObjectType()
        {
            CmbObjectType.Items.Clear();
            CmbObjectType.Items.Add(new RadComboBoxItem(""));
            CmbObjectType.DataTextField = "type_desc";
            CmbObjectType.DataValueField = "type_desc";
            CmbObjectType.DataSource = SqlClass.GetAllSqlObjectType();
            CmbObjectType.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            CmbObjectType.SelectedItem.Text = "";
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvObjectType_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var objectTypeId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["ObjectTypeId"] = objectTypeId;
                var myObjectType = Classes.AcsObjectType.GetSingleById(objectTypeId);
                if (myObjectType != null)
                {
                    TxtName.Text = myObjectType.ObjectTypeTitle;
                    CmbObjectType.SelectedValue= myObjectType.ObjectTypeEnglieshTitle;
                    CmbIsActive.SelectedValue = myObjectType.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsObjectType.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvObjectType.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvObjectType_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectType_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvObjectType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectType_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvObjectType.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObjectType_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvObjectType_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}