﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRowAccessDetail.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRowAccessDetail" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../MessageErrorControl.ascx" TagName="messageerrorcontrol" TagPrefix="uc1" %>
<%@ Register Src="DeveloperControl.ascx" TagName="DeveloperControl" TagPrefix="uc2" %>
<asp:Panel runat="server" ID="PnlGenral">
    <table width="100%" align="right">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" dir="ltr" width="93%">
                                        <asp:TextBox ID="TxtSqlExpersion" runat="server" BackColor="#EAEAEA" BorderColor="Black"
                                            BorderStyle="Ridge" BorderWidth="1px" Font-Names="Tahoma" Font-Size="8pt" ForeColor="#000053"
                                            Height="150px" LabelWidth="" ReadOnly="True" TextMode="MultiLine" Width="900px"></asp:TextBox>
                                    </td>
                                    <td bgcolor="#EAEAEA" width="12%" align="center" style="border: 1px solid #000000;">
                                        <cc1:SelectControl ID="mdlRunQuery" runat="server" RadWindowHeight="800" RadWindowWidth="1000"
                                            imageName="RunQuery" Width="1px" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQueryForRowAccess.aspx" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table dir="rtl">
                    <tr>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" ValidationGroup="RowAccessDetail">
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click"
                                ValidationGroup="RowAccessDetail">
                                <Icon PrimaryIconCssClass="rbEdit" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnDelete" runat="server" CustomeButtonType="Delete" OnClick="BtnDelete_Click">
                                <Icon PrimaryIconCssClass="rbRemove" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnReturn" runat="server" CustomeButtonType="Back" OnClick="BtnReturn_Click">
                                <Icon PrimaryIconCssClass="rbPrevious" />
                            </cc1:CustomRadButton>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="right">
                            <uc1:messageerrorcontrol id="MessageErrorControl1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table align="right" dir="rtl">
                    <tr>
                        <td width="10%">
                            <asp:Label ID="LblObjectField" runat="server" Text="عنوان فیلد:"></asp:Label>
                        </td>
                        <td width="30%">
                            <telerik:RadComboBox ID="CmbObjectField" runat="server" AppendDataBoundItems="True"
                                Width="400px" AutoPostBack="True" OnSelectedIndexChanged="CmbObjectField_SelectedIndexChanged"
                                CausesValidation="False" Filter="Contains" MarkFirstMatch="True" Skin="Office2007">
                            </telerik:RadComboBox>
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                        <td width="10%" nowrap="nowrap">
                            &nbsp;
                        </td>
                        <td width="30%">
                            &nbsp;
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" valign="top" nowrap="nowrap">
                            <asp:Label ID="LblFunction0" runat="server" Text="عنوان شرط:"></asp:Label>
                        </td>
                        <td width="30%" valign="top">
                            <telerik:RadComboBox ID="CmbCondition" runat="server" AppendDataBoundItems="True"
                                keyid="" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="CmbCondition_SelectedIndexChanged"
                                Filter="Contains" MarkFirstMatch="True" Skin="Office2007">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbCondition" Selected="True" Text=""
                                        Value="-1" />
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbCondition" Text="فعال" Value="True" />
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbCondition" Text="غیر فعال" Value="False" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                        <td width="10%" nowrap="nowrap" nowrap="nowrap">
                            &nbsp;
                        </td>
                        <td width="30%">
                            &nbsp;
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" valign="top" nowrap="nowrap">
                            <asp:Label ID="LblValue" runat="server" Text="مقادیر:"></asp:Label>
                        </td>
                        <td valign="top" colspan="5" align="right" dir="rtl">
                            <uc2:developercontrol id="DeveloperControl1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" valign="top" nowrap="nowrap">
                            <asp:Label ID="LblFunction" runat="server" Text="عنوان تابع:"></asp:Label>
                        </td>
                        <td valign="top" colspan="5">
                            <telerik:RadComboBox ID="CmbFunction" runat="server" AppendDataBoundItems="True"
                                keyid="" Width="400px" MarkFirstMatch="True" Skin="Office2007">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbFunction" Selected="True" Text=""
                                        Value="-1" />
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbFunction" Text="فعال" Value="True" />
                                    <telerik:RadComboBoxItem runat="server" Owner="CmbFunction" Text="غیر فعال" Value="False" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" nowrap="nowrap">
                            <asp:Label ID="LblOperator" runat="server" Text="عنوان عملگر:"></asp:Label>
                        </td>
                        <td width="30%">
                            <telerik:RadComboBox ID="CmbOperator" runat="server" AppendDataBoundItems="True"
                                keyid="" Width="100px" Filter="Contains" MarkFirstMatch="True" Skin="Office2007">
                            </telerik:RadComboBox>
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                        <td width="10%" nowrap="nowrap">
                            &nbsp;
                        </td>
                        <td width="30%">
                            &nbsp;
                        </td>
                        <td width="10%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            <asp:Label ID="LblFunction1" runat="server" Text="نمودار درختی عبارت SQL:"></asp:Label>
                        </td>
                        <td colspan="5">
                            <table width="100%">
                                <tr>
                                    <td align="left" dir="ltr" width="90%">
                                        <telerik:RadTreeView ID="treeRowAccessDetail" runat="server" BorderColor="#003366" Skin="Office2007"
                                            BorderWidth="1px" CausesValidation="False" OnContextMenuItemClick="treeRowAccessDetail_ContextMenuItemClick"
                                            OnNodeDataBound="treeRowAccessDetail_NodeDataBound">
                                            <ExpandAnimation Type="InExpo" />
                                            <ContextMenus>
                                                <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
                                                    <Items>
                                                        <telerik:RadMenuItem ImageUrl="../Images/Edit.bmp" Text="ویرایش" Value="edit">
                                                        </telerik:RadMenuItem>
                                                        <telerik:RadMenuItem ImageUrl="../Images/Delete.bmp" Text="حذف" Value="remove">
                                                        </telerik:RadMenuItem>
                                                    </Items>
                                                    <CollapseAnimation Type="OutQuint" />
                                                </telerik:RadTreeViewContextMenu>
                                            </ContextMenus>
                                        </telerik:RadTreeView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
                            </telerik:RadAjaxLoadingPanel>
                            <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="BtnAdd">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="BtnEdit">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="BtnDelete">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="BtnReturn">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="CmbObject">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="CmbObjectField" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="CmbObjectField">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="CmbCondition">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="treeRowAccessDetail">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="PnlGenral" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                </AjaxSettings>
                            </telerik:RadAjaxManagerProxy>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
