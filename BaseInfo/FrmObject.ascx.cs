﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmObject : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                HttpContext.Current.Response.Redirect(Intranet.Configuration.Settings.PortalSettings.PortalPath);
            }
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbObjectTitle("", "");
                SetCmbSystem();
                SetCmbObjectType();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void SetCmbObjectTitle(string objectTitle, string schemaName)
        {
            CmbObjectTitle.Items.Clear();
            CmbObjectTitle.Items.Add(new RadComboBoxItem("", "-1"));
            CmbObjectTitle.DataTextField = "ObjectTitleDesc";
            CmbObjectTitle.DataValueField = "ObjectTitle";
            CmbObjectTitle.DataSource = AcsObject.GetAllSqlObject(objectTitle,schemaName);
            CmbObjectTitle.DataBind();
        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataBind();
        }

        protected void SetCmbObjectType()
        {
            CmbObjectType.Items.Clear();
            CmbObjectType.Items.Add(new RadComboBoxItem(""));
            CmbObjectType.DataTextField = "ObjectTypeTitle";
            CmbObjectType.DataValueField = "ObjectTypeId";
            CmbObjectType.DataSource = AcsObjectType.GetAll();
            CmbObjectType.DataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsObject.Add(CmbObjectTitle.SelectedValue,Int32.Parse(CmbSystem.SelectedValue),ChkIsHierachy.Checked,Int32.Parse(CmbObjectType.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObject", "ObjectId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "ObjectId=" + addedId, "");
                var pageSize = GrvObject.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvObject.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var ObjectId = Convert.ToInt32(ViewState["ObjectId"].ToString());
                Classes.AcsObject.Update(ObjectId, CmbObjectTitle.SelectedValue,Int32.Parse(CmbSystem.SelectedValue),ChkIsHierachy.Checked,Int32.Parse(CmbObjectType.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsObject", "ObjectId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "ObjectId=" + ObjectId, "");
                var pageSize = GrvObject.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1)/pageSize;
                var recIndex = (recRowNumber%pageSize == 0) ? pageSize - 1 : (recRowNumber%pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvObject.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (CmbObjectTitle.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and ObjectTitle like N'%" + CmbObjectTitle.SelectedValue + "%'";
            }
            if (CmbObjectType.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and ObjectTypeId =" + CmbObjectType.SelectedValue;
            }
            if (CmbSystem.SelectedIndex>0)
            {
                ViewState["WhereClause"] += " and SystemId =" + CmbSystem.SelectedValue;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;
            GrvObject.MasterTableView.CurrentPageIndex = pageIndex;
            GrvObject.DataSource = GeneralDB.GetPageDB(GrvObject.MasterTableView.PageSize, pageIndex,
                                                       whereClause, ViewState["sortField"].ToString(),
                                                       "[acs].v_AcsObject", "ObjectId", out count,
                                                       ViewState["SortType"].ToString());
            GrvObject.MasterTableView.VirtualItemCount = count;
            GrvObject.DataBind();
        }

        protected void SetPageControl()
        {
            CmbObjectTitle.ClearSelection();
            CmbSystem.ClearSelection();
            CmbObjectType.ClearSelection();
            ChkIsHierachy.Checked = false;
        }

        protected void GrvObject_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var objectId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["ObjectId"] = objectId;
                var mySystem = Classes.AcsObject.GetSingleById(objectId);
                if (mySystem != null)
                {
                    CmbObjectTitle.SelectedValue = mySystem.ObjectTitle;
                    CmbSystem.SelectedValue = mySystem.SystemId.ToString();
                    CmbObjectType.SelectedValue = mySystem.ObjectTypeId.ToString();
                    ChkIsHierachy.Checked = mySystem.IsHierarchy;
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsObject.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvObject.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvObject_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObject_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvObject.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObject_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvObject.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvObject_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvObject_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }

        protected void CmbObjectType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //e.Text
        }
    }
}