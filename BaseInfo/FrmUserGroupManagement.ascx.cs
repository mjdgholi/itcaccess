﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmUserGroupManagement : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);

            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetgrvUserAndGroupData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }


            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //var a = SelectControl1.Title + SelectControl1.KeyId;
                int addedId;
                const bool isUser = false;
                var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
                AcsUserAndGroup.Add(TxtName.Text, Guid.NewGuid(), isUser, "", bool.Parse(CmbIsBuiltin.SelectedValue), bool.Parse(CmbIsActive.SelectedValue),
                                    DateTime.Now.ToString(), DateTime.Now.ToString(),-1, out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsUserAndGroup", "UserAndGroupId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "UserAndGroupId=" + addedId, "IsUser=0");
                var pageSize = GrvUserAndGroup.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetgrvUserAndGroupData(curRecPage, "");

                GrvUserAndGroup.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
                const bool isUser = false;
                var UserAndGroupId = Convert.ToInt32(ViewState["UserAndGroupId"].ToString());
                Classes.AcsUserAndGroup.Update(UserAndGroupId, TxtName.Text, Guid.NewGuid(), isUser, "", bool.Parse(CmbIsBuiltin.SelectedValue), bool.Parse(CmbIsActive.SelectedValue),
                                              DateTime.Now.ToString(), DateTime.Now.ToString(),-1);

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsUserAndGroup", "UserAndGroupId", ViewState["sortField"].ToString(),
                                                   ViewState["SortType"].ToString(), "UserAndGroupId=" + UserAndGroupId, "IsUser=0 ");
                var pageSize = GrvUserAndGroup.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetgrvUserAndGroupData(curRecPage, "");

                GrvUserAndGroup.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and UserAndGroupName like N'%" + TxtName.Text + "%'";
            }          

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }
            switch (CmbIsBuiltin.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsBuiltin=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsBuiltin=0";
                    break;
            }

            SetgrvUserAndGroupData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetgrvUserAndGroupData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetgrvUserAndGroupData(int pageIndex, string whereClause)
        {
            int count;
            if (string.IsNullOrEmpty(whereClause))
                whereClause = " IsUser=0 ";
            else
                whereClause += " and IsUser=0 ";

            GrvUserAndGroup.MasterTableView.CurrentPageIndex = pageIndex;
            GrvUserAndGroup.DataSource = GeneralDB.GetPageDB(GrvUserAndGroup.MasterTableView.PageSize, pageIndex,
                                                                    whereClause, ViewState["sortField"].ToString(),
                                                                    "[acs].v_AcsUserAndGroup", "UserAndGroupId", out count,
                                                                    ViewState["SortType"].ToString());
            GrvUserAndGroup.MasterTableView.VirtualItemCount = count;
            GrvUserAndGroup.DataBind();
        }

        protected void SetPageControl()
        {           
            TxtName.Text = "";
            CmbIsActive.SelectedIndex = 1;
            CmbIsBuiltin.SelectedIndex= 2;

        }

        protected void GrvUserAndGroup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var userAndGroupId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["UserAndGroupId"] = userAndGroupId;
                var myUserAndGroup = Classes.AcsUserAndGroup.GetSingleById(userAndGroupId);
                if (myUserAndGroup != null)
                {
                    TxtName.Text = myUserAndGroup.UserAndGroupName;                   
                    //var user = Intranet.Security.UserDB.GetSingleUser(myUserAndGroup.WebUserId);
                    //if (user != null)
                    //{
                    //    txtPortalUser.Title = user.FirstName + " " + user.LastName;
                    //    txtPortalUser.KeyId = user.UserID.ToString();
                    //}

                    CmbIsActive.SelectedValue = myUserAndGroup.IsActive.ToString();
                    CmbIsBuiltin.SelectedValue = myUserAndGroup.IsBuiltin.ToString();

                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsUserAndGroup.Delete(Convert.ToInt32(e.CommandArgument));
                    SetgrvUserAndGroupData(GrvUserAndGroup.MasterTableView.CurrentPageIndex,
                                              ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvUserAndGroup_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetgrvUserAndGroupData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvUserAndGroup_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetgrvUserAndGroupData(GrvUserAndGroup.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvUserAndGroup_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetgrvUserAndGroupData(GrvUserAndGroup.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvUserAndGroup_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvUserAndGroup_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }        
    }
}