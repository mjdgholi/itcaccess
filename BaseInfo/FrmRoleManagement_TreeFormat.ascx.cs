﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRoleManagement_TreeFormat : ModuleControls
    {
        private ArrayList _searchGroupIdList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetCmbSystem();
                SetTrvRoleAndCategorize(-1, "", -1);
                BtnEdit.Visible = false;
                Session["ItemSearch"] = "";
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                int roleAndCategorizeTreeId;
                var isRole = bool.Parse(CmbIsRole.SelectedValue);
                var isActive = bool.Parse(CmbIsActive.SelectedValue);
                var systemId = Int32.Parse(CmbSystem.SelectedValue);
                var ownerId = -1;
                if (trvRoleAndCategorize.SelectedNode != null)
                    ownerId = Int32.Parse(trvRoleAndCategorize.SelectedValue);

                AcsRoleAndCategorizeTree.RoleAndCategorizeAddWithOwner(TxtName.Text, isRole, systemId, isActive, ownerId, DateTime.Now.ToString(), DateTime.Now.ToString(), out roleAndCategorizeTreeId);
                SetTrvRoleAndCategorize(-1, "", -1);
                var myRoleAndCategorizeTree = AcsRoleAndCategorizeTree.GetSingleById(roleAndCategorizeTreeId);
                SetSelectedNodeForTree(myRoleAndCategorizeTree.RoleAndCategorizeId.ToString());
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                var roleAndCategorizeId = Int32.Parse(ViewState["RoleAndCategorizeId"].ToString());
                var isRole = bool.Parse(CmbIsRole.SelectedValue);
                var isActive = bool.Parse(CmbIsActive.SelectedValue);
                var systemId = Int32.Parse(CmbSystem.SelectedValue);
                var ownerId = -1;
                if (trvRoleAndCategorize.SelectedNode != null)
                    ownerId = Int32.Parse(trvRoleAndCategorize.SelectedValue);

                AcsRoleAndCategorizeTree.RoleAndCategorizeUpdateWithOwner(roleAndCategorizeId, TxtName.Text, isRole, systemId, isActive, ownerId,
                                                                          DateTime.Now.ToString(), DateTime.Now.ToString());
                SetTrvRoleAndCategorize(-1, "", -1);
                SetPageControlForAdd();
                SetSelectedNodeForTree(roleAndCategorizeId.ToString());
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            SetTrvRoleAndCategorize(-1, "", -1);
            var roleAndCategorizeTitle = TxtName.Text;
            int isRole = -1;
            if (CmbIsRole.SelectedValue.ToLower() == "true")
            {
                isRole = 1;
            }
            else if (CmbIsRole.SelectedValue.ToLower() == "false")
            {
                isRole = 0;
            }

            var systemId = (CmbSystem.SelectedIndex > 0) ? Int32.Parse(CmbSystem.SelectedValue) : -1;
            var searchResultArrayList = AcsRoleAndCategorizeTree.GetAllNodesArrayList(isRole, roleAndCategorizeTitle, systemId);
            foreach (var node in trvRoleAndCategorize.GetAllNodes())
            {
                if (searchResultArrayList.Contains(node.Value))
                {
                    node.ExpandParentNodes();
                    if (TxtName.Text.Trim() != "")
                    {
                        node.Text = node.Text.Replace(GeneralDB.ConvertTo256(TxtName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtName.Text)));
                    }
                    else
                    {
                        node.BackColor = Color.LightSeaGreen;
                    }
                }
            }
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            SetPageControl();
            SetTrvRoleAndCategorize(-1, "", -1);
        }

        protected void SetTrvRoleAndCategorize(int isRole, string roleAndCategorizeTitle, int systemId)
        {
            trvRoleAndCategorize.DataTextField = "RoleAndCategorizeTilte";
            trvRoleAndCategorize.DataValueField = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldID = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldParentID = "RoleAndCategorizeOwnerId";
            trvRoleAndCategorize.DataSource = AcsRoleAndCategorizeTree.GetAllNodes(isRole, roleAndCategorizeTitle, -1);
            trvRoleAndCategorize.DataBind();
            if (trvRoleAndCategorize.Nodes.Count > 0)
            {
                trvRoleAndCategorize.Nodes[0].Expanded = true;
            }
        }

        protected void SetPageControlForEdit()
        {
            BtnAdd.Visible = false;
            BtnEdit.Visible = true;
        }

        protected void SetPageControlForAdd()
        {
            BtnAdd.Visible = true;
            BtnEdit.Visible = false;
        }

        protected void SetPageControl()
        {
            CmbSystem.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
            CmbIsRole.ClearSelection();
            TxtName.Text = "";
            trvRoleAndCategorize.ClearSelectedNodes();
        }

        protected void trvRoleAndCategorize_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    var roleAndCategorizeId = Int32.Parse(clickedNode.Value);
                    var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleAndCategorizeId);
                    var systemId = myRoleAndCategorize.SystemId;
                    var isActive = myRoleAndCategorize.IsActive.ToString();
                    var isRole = myRoleAndCategorize.IsRole.ToString();
                    TxtName.Text = myRoleAndCategorize.RoleAndCategorizeTilte;
                    CmbSystem.SelectedValue = systemId.ToString();
                    CmbIsActive.SelectedValue = isActive;
                    CmbIsRole.SelectedValue = isRole;
                    if (e.Node.ParentNode != null)
                    {
                        var ownerId = clickedNode.ParentNode.Value;
                        SetSelectedNodeForTree(ownerId);
                        clickedNode.ParentNode.Selected = true;
                    }
                    else
                    {
                        e.Node.Selected = false;
                    }

                    ViewState["RoleAndCategorizeId"] = roleAndCategorizeId;
                    SetPageControlForEdit();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {
                    var roleAndCategorizeId = Int32.Parse(clickedNode.Value);
                    var ownerId = clickedNode.ParentNode.Value;
                    AcsRoleAndCategorizeTree.DeleteWithAllChild(roleAndCategorizeId);
                    SetTrvRoleAndCategorize(-1, "", -1);
                    trvRoleAndCategorize.FindNodeByValue(ownerId).ExpandParentNodes();
                    trvRoleAndCategorize.FindNodeByValue(ownerId).Expanded = true;

                    MessageErrorControl1.ShowSuccesMessage("حذف موفق");
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void SetSelectedNodeForTree(string roleAndCategorizeId)
        {
            var parrentArr = new ArrayList();
            var parentDt = AcsRoleAndCategorizeTree.GetParentNodes(Int32.Parse(roleAndCategorizeId));
            foreach (DataRow parent in parentDt.Rows)
            {
                parrentArr.Add(parent["RoleAndCategorizeId"].ToString());
            }
            foreach (var radTreeNode in trvRoleAndCategorize.GetAllNodes())
            {
                if (parrentArr.Contains(radTreeNode.Value))
                {
                    radTreeNode.Expanded = true;
                }
                if (radTreeNode.Value == roleAndCategorizeId)
                {
                    radTreeNode.Selected = true;
                    return;
                }
            }
        }

        protected void CheckValidation()
        {
            if (CmbIsRole.SelectedValue.ToLower() == "true" && string.IsNullOrEmpty(trvRoleAndCategorize.SelectedValue))
            {
                throw new Exception("گروه نقش دربرگیرنده را انتخاب نمایید");
            }

            if (trvRoleAndCategorize.SelectedNode != null)
            {
                var roleAndCategorizeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleAndCategorizeId);
                if (myRoleAndCategorize.IsRole)
                {
                    throw new Exception("نود انتخاب شده باید گروه نقش باشد");
                }
            }
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = AcsRoleAndCategorizeTree.GetAllNodes(-1, TxtGroupName.Text, -1);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["RoleAndCategorizeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeId"].ToString() + "'";
                }
                SetTrvRoleAndCategorize(-1, "", -1);
            }
        }

        protected void trvRoleAndCategorize_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (_searchGroupIdList.Contains(Int32.Parse(e.Node.Value)))
            {
                e.Node.ExpandParentNodes();
                e.Node.Text = e.Node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                // node.Selected = true;
            }
        }
    }
}