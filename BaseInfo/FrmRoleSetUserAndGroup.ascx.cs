﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRoleSetUserAndGroup : ModuleControls
    {
        private ArrayList _searchGroupIdList = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                ViewState["PageStatus"] = "IsPostBack";
              //  LstMembers.KeyId = "";
                Session["ItemSearch"] = "";
                SetRoleAndCategorizeTree();         
     
            }
        }

        protected void SetRoleAndCategorizeTree()
        {
            trvRoleAndCategorize.DataTextField = "RoleAndCategorizeTilte";
            trvRoleAndCategorize.DataValueField = "RoleAndCategorizeTreeId";
            trvRoleAndCategorize.DataFieldID = "RoleAndCategorizeId";
            trvRoleAndCategorize.DataFieldParentID = "RoleAndCategorizeOwnerId";
            trvRoleAndCategorize.DataSource = AcsRoleAndCategorizeTree.GetAllNodes(-1, "", -1);
            trvRoleAndCategorize.DataBind();
        }

        protected void SetLstMembers(int roleAndCategorizeId, int isUser, int userGroupId)
        {
            var dtMembers = AcsUserAndGroup_RoleAndCategorize.GetMemberOfRole(roleAndCategorizeId, isUser, userGroupId);
            LstMembers.Items.Clear();
            LstMembers.KeyId = "";
            LstMembers.HiddenFieldTitle.Value = "";
            LstMembers.DataTextField = "UserAndGroupName";
            LstMembers.DataValueField = "UserAndGroupId";
            LstMembers.DataSource = dtMembers;
            LstMembers.DataBind();
        }

        protected void SetLstNotMembers(int roleAndCategorizeId, int isUser, int userGroupId)
        {
            var dtNotMembers = AcsUserAndGroup_RoleAndCategorize.GetNotMemberOfRole(roleAndCategorizeId, isUser, userGroupId);
            LstNotMembers.Items.Clear();
            LstNotMembers.KeyId = "";
            LstNotMembers.HiddenFieldTitle.Value = "";
            LstNotMembers.DataTextField = "UserAndGroupName";
            LstNotMembers.DataValueField = "UserAndGroupId";
            LstNotMembers.DataSource = (dtNotMembers.Tables[0].Rows.Count == 0) ? null : dtNotMembers;
            LstNotMembers.DataBind();
            SetLstNotMembersInDirectMembersColor();
        }

        protected void trvRoleAndCategorize_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            try
            {
                int roleAndCategorizeTreeId = Int32.Parse(e.Node.Value);
                var myRoleAndCategorizeTree = AcsRoleAndCategorizeTree.GetSingleById(roleAndCategorizeTreeId);
                if (myRoleAndCategorizeTree == null) throw new ArgumentNullException("myRoleAndCategorizeTree");
                var roleAndCategorizeId = myRoleAndCategorizeTree.RoleAndCategorizeId;
                var myRoleAndCategorize = AcsRoleAndCategorize.GetSingleById(roleAndCategorizeId);
                if (myRoleAndCategorize != null)
                {
                    if (myRoleAndCategorize.IsRole)
                    {
                        int isUser = -1;
                        int userAndGroupId = -1;
                        SetLstMembers(roleAndCategorizeId, isUser, userAndGroupId);
                        SetLstNotMembers(roleAndCategorizeId, isUser, userAndGroupId);
                    }
                    else
                    {
                        MessageErrorControl1.ShowErrorMessage("گروه نقش نمی تواند دارای عضو باشد");
                    }
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }

        }

        protected void ImgbAddMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstNotMembers.KeyId != "")
                {
                    if (!string.IsNullOrEmpty(trvRoleAndCategorize.SelectedValue))
                    {
                        var roleAndCategorizeTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                        var myRoleAndCategorizeTree = AcsRoleAndCategorizeTree.GetSingleById(roleAndCategorizeTreeId);
                        var roleAndCategorizeId = myRoleAndCategorizeTree.RoleAndCategorizeId;
                        //var newMembers = "";
                        //foreach (RadListBoxItem notMembers in LstNotMembers.SelectedItems)
                        //{
                        //    newMembers += (newMembers == "") ? notMembers.Value : "," + notMembers.Value;
                        //}
                        AcsUserAndGroup_RoleAndCategorize.AddMembersForRoleAndCategorize(roleAndCategorizeId, LstNotMembers.KeyId);
                        SetLstMembers(roleAndCategorizeId, -1, -1);
                        SetLstNotMembers(roleAndCategorizeId, -1, -1);
                        MessageErrorControl1.ShowSuccesMessage("تخصیص کاربر به نقش با موفقیت انجام گردید");
                        LstMembers.KeyId = "";
                    }
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void ImgbRemoveMembers_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (LstMembers.KeyId != "")
                {
                    if (!string.IsNullOrEmpty(trvRoleAndCategorize.SelectedValue))
                    {
                        var roleAndCategorizeTreeId = Int32.Parse(trvRoleAndCategorize.SelectedValue);
                        var myRoleAndCategorizeTree = AcsRoleAndCategorizeTree.GetSingleById(roleAndCategorizeTreeId);
                        var roleAndCategorizeId = myRoleAndCategorizeTree.RoleAndCategorizeId;
                        //var removedMembers = "";
                        //foreach (RadListBoxItem members in LstMembers.SelectedItems)
                        //{
                        //    removedMembers += (removedMembers == "") ? members.Value : "," + members.Value;
                        //}
                        AcsUserAndGroup_RoleAndCategorize.DeleteMembersForRoleAndCategorize(roleAndCategorizeId, LstMembers.KeyId);
                        SetLstMembers(roleAndCategorizeId, -1, -1);
                        SetLstNotMembers(roleAndCategorizeId, -1, -1);
                        MessageErrorControl1.ShowSuccesMessage("حذف اعضا از نقش با موفقیت انجام گردید");
                        LstMembers.KeyId = "";
                    }
                }
            }
            catch (Exception exception)
            {
                MessageErrorControl1.ShowErrorMessage(exception.Message);
            }
        }

        protected void SetLstNotMembersInDirectMembersColor()
        {
            var members = new ArrayList();
            foreach (RadListBoxItem item in LstMembers.Items)
            {
                members.Add(item.Value);
            }
            foreach (RadListBoxItem item in LstNotMembers.Items)
            {
                var notMemberParentArray = AcsUserAndGroupTree.GetParentNodesArrayList(Int32.Parse(item.Value));
                foreach (var notmemberParent in notMemberParentArray)
                {
                    if (members.Contains(notmemberParent.ToString()))
                    {
                        item.BackColor = Color.GreenYellow;
                        var userAndGroup = AcsUserAndGroup.GetSingleById(Int32.Parse(notmemberParent.ToString()));
                        item.ToolTip = "از طریق گروه <" + userAndGroup.UserAndGroupName + "> این نقش را دارا میباشد";
                    }
                }
            }
        }

        protected void imgbtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (TxtGroupName.Text != "")
            {
                var searchNodes = AcsRoleAndCategorizeTree.GetAllNodes(-1, TxtGroupName.Text, -1);

                _searchGroupIdList = new ArrayList();

                foreach (DataRow datarow in searchNodes.Rows)
                {
                    _searchGroupIdList.Add(Int32.Parse(datarow["RoleAndCategorizeTreeId"].ToString()));

                    Session["ItemSearch"] += (string.IsNullOrEmpty(Session["ItemSearch"].ToString())) ? "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'" : "," + "'" + datarow["RoleAndCategorizeTreeId"].ToString() + "'";
                }
                SetRoleAndCategorizeTree();
            }
           
        }

        protected void trvRoleAndCategorize_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (_searchGroupIdList.Contains(Int32.Parse(e.Node.Value)))
            {
                e.Node.ExpandParentNodes();
                e.Node.Text = e.Node.Text.Replace(GeneralDB.ConvertTo256(TxtGroupName.Text), string.Format("<font  color='blue'>{0}</font>", GeneralDB.ConvertTo256(TxtGroupName.Text)));
                // node.Selected = true;
            }
        }       
    }
}