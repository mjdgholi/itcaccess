﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRoleManagement_TreeFormat.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRoleManagement_TreeFormat" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register TagPrefix="uc1" TagName="MessageErrorControl" Src="../MessageErrorControl.ascx" %>
<telerik:RadScriptBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">
 fnRoleAndCateGorizeSearch:
       {
          
           var myValue = new Array(<%=Session["ItemSearch"].ToString() %>); 
           var index= -1;
           
           function NextNode() {    
               //alert(myValue.length);
               if (index <  myValue.length - 1) {                      
                   index = index + 1;                 
                   expandNode(myValue[index]);
                   
                   return true;
                }
                else
                {

                  return false;
                }
           }

           function PreviousNode() {
             //  alert(index);
               if (index > 0)
               {
                index = index - 1;  
                expandNode(myValue[index]);               
               return true;
               }
               else
               {
               return false;
               }
           }
           

           function expandNode(nodeid) {
               var treeView = $find("<%= trvRoleAndCategorize.ClientID %>");
               var node = treeView.findNodeByValue(nodeid);
               if (node) {
                   if (node.get_parent() != node.get_treeView()) {
                        node.get_parent().expand(); // root node must not expand 
                       
                   node.get_parent().expand();
                   node.expand();
                   //node.select();
                   node.set_selected(true);                   
                   scrollToNode(treeView, node);
                   return true;
               }
               return false;
           }


           function scrollToNode(treeview, node) {
               var nodeElement = node.get_contentElement();
               var treeViewElement = treeview.get_element();

               var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
               var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
               var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

               if (relativeOffsetTop < treeViewElement.scrollTop) {
                   treeViewElement.scrollTop = relativeOffsetTop;
               }

               var height = nodeElement.offsetHeight;

               if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                   treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
               }
           }
       }          
    </script>
</telerik:RadScriptBlock>
<asp:Panel ID="pnlAll" runat="server">
    <table dir="rtl" width="100%">
        <tr>
            <td colspan="6">
                <table dir="rtl">
                    <tr>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                                <Icon PrimaryIconCssClass="rbEdit" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnSearch" runat="server" CausesValidation="False" CustomeButtonType="Search"
                                OnClick="BtnSearch_Click">
                                <Icon PrimaryIconCssClass="rbSearch" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <cc1:CustomRadButton ID="BtnShowAll" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                                OnClick="BtnShowAll_Click">
                                <Icon PrimaryIconCssClass="rbRefresh" />
                            </cc1:CustomRadButton>
                        </td>
                        <td align="right">
                            <uc1:MessageErrorControl ID="MessageErrorControl1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap">
                <asp:Label ID="LblName" runat="server" Text="نام نقش یا گروه نقش:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadTextBox ID="TxtName" runat="server" Skin="Office2007" Width="300px">
                </telerik:RadTextBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="TxtName"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%">
            </td>
            <td width="30%">
            </td>
            <td width="10%">
            </td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap">
                <asp:Label ID="LblIsGroup" runat="server" Text="نوع نقش:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadComboBox ID="CmbIsRole" runat="server" KeyId="" Skin="Office2007" Width="300px"
                    Filter="Contains" MarkFirstMatch="True">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Owner="CmbIsRole" Selected="True" Value="-1" />
                        <telerik:RadComboBoxItem runat="server" Owner="CmbIsRole" Text="نقش" Value="True" />
                        <telerik:RadComboBoxItem runat="server" Owner="CmbIsRole" Text="گروه نقش" Value="False" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvSystem0" runat="server" ControlToValidate="CmbIsRole"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%">
                &nbsp;
            </td>
            <td width="30%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="10%">
                <asp:Label ID="lblSystemTitle" runat="server" Text="عنوان سیستم:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadComboBox ID="CmbSystem" runat="server" AppendDataBoundItems="True" KeyId=""
                    Skin="Office2007" Width="300px" Filter="Contains" MarkFirstMatch="True">
                </telerik:RadComboBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvSystem" runat="server" ControlToValidate="CmbSystem"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%">
                &nbsp;
            </td>
            <td width="30%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap">
                <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
            </td>
            <td width="30%">
                <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Skin="Office2007" Width="300px"
                    Filter="Contains" MarkFirstMatch="True">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Selected="True" Value="-1" />
                        <telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" />
                        <telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td width="10%">
                <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td width="10%">
                &nbsp;
            </td>
            <td width="30%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="10%" nowrap="nowrap" valign="top">
                <asp:Label ID="LblParentCategorize" runat="server" Text="گروه نقش دربرگیرنده:"></asp:Label>
            </td>
            <td width="30%" valign="top">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" LabelWidth="75px" Width="120px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" OnClick="imgbtnSearch_Click"
                                ToolTip="جستجو" Width="25px" />
                        </td>
                        <td>
                            <img alt="جلو" height="25" onclick="NextNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Next.png"%>'
                                width="25" />
                        </td>
                        <td>
                            <img alt="عقب" height="25" onclick="PreviousNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Previous.png"%>'
                                width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <telerik:RadTreeView ID="trvRoleAndCategorize" runat="server" BorderColor="#999999" Skin="Office2007"
                                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" dir="rtl" OnClientContextMenuItemClicking="ContextMenuClick"
                                OnContextMenuItemClick="trvRoleAndCategorize_ContextMenuItemClick" OnNodeDataBound="trvRoleAndCategorize_NodeDataBound"
                                Width="99%">
                                <ContextMenus>
                                    <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" Skin="Office2007">
                                        <Items>
                                            <telerik:RadMenuItem ImageUrl="../Images/editUser.png" PostBack="True" Text="ویرایش"
                                                Value="edit">
                                            </telerik:RadMenuItem>
                                            <telerik:RadMenuItem ImageUrl="../Images/deleteUser.png" Text="حذف" Value="remove">
                                            </telerik:RadMenuItem>
                                        </Items>
                                        <CollapseAnimation Type="OutQuint" />
                                    </telerik:RadTreeViewContextMenu>
                                </ContextMenus>
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="10%" valign="top">
                &nbsp;
            </td>
            <td width="10%" style="direction: ltr">
                &nbsp;
            </td>
            <td width="30%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top" width="10%">
                &nbsp;
            </td>
            <td valign="top" width="30%">
                &nbsp;
            </td>
            <td valign="top" width="10%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
            <td width="30%">
                &nbsp;
            </td>
            <td width="10%">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadScriptBlock runat="server">
    <script type="text/javascript">
        function ContextMenuClick(sender, eventArgs) {
            var node = eventArgs.get_node();
            var item = eventArgs.get_menuItem();
            if (item.get_value() == "remove") {
                eventArgs.set_cancel(!confirm('آیا مطمئن هستید؟'));
            }
        }  
    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="trvRoleAndCategorize">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="pnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
</telerik:RadAjaxLoadingPanel>
