﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmRoleCategorizeManagement : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);

            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null) ? "1=1 " : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";
                SetgrvRoleAndCategorizeData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetCmbSystem();
                SetPageControl();
            }

            if (!IsPostBack)
            {

            }
        }

        protected void SetCmbSystem()
        {
            CmbSystem.Items.Clear();
            CmbSystem.Items.Add(new RadComboBoxItem(""));
            CmbSystem.DataSource = AcsSystem.GetAll();
            CmbSystem.DataValueField = "SystemId";
            CmbSystem.DataTextField = "SystemTitle";
            CmbSystem.DataBind();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //var a = SelectControl1.Title + SelectControl1.KeyId;
                int addedId;
                const bool isRole = false;
                AcsRoleAndCategorize.Add(TxtName.Text,isRole,Int32.Parse(CmbSystem.SelectedValue), bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(), out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.t_AcsRoleAndCategorize", "RoleAndCategorizeId", ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(), "RoleAndCategorizeId=" + addedId, "IsRole=0");
                var pageSize = GrvRoleAndCategorize.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetgrvRoleAndCategorizeData(curRecPage, "");

                GrvRoleAndCategorize.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
                const bool isRole = false;
                var RoleAndCategorizeId = Convert.ToInt32(ViewState["RoleAndCategorizeId"].ToString());
                Classes.AcsRoleAndCategorize.Update(RoleAndCategorizeId, TxtName.Text, isRole,Int32.Parse(CmbSystem.SelectedValue), bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.t_AcsRoleAndCategorize", "RoleAndCategorizeId", ViewState["sortField"].ToString(),
                                                   ViewState["SortType"].ToString(), "RoleAndCategorizeId=" + RoleAndCategorizeId, "IsRole=0");
                var pageSize = GrvRoleAndCategorize.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetgrvRoleAndCategorizeData(curRecPage, "");

                GrvRoleAndCategorize.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }
        
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";
            if (!string.IsNullOrEmpty(TxtName.Text))
            {
                ViewState["WhereClause"] += " and RoleAndCategorizeTilte like N'%" + TxtName.Text + "%'";
            }

            if (CmbSystem.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and SystemId=" + CmbSystem.SelectedValue;
            }

            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetgrvRoleAndCategorizeData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetgrvRoleAndCategorizeData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetgrvRoleAndCategorizeData(int pageIndex, string whereClause)
        {
            int count;
            if (string.IsNullOrEmpty(whereClause))
                whereClause = " IsRole=0 ";
            else
                whereClause += " and IsRole=0 ";

            GrvRoleAndCategorize.MasterTableView.CurrentPageIndex = pageIndex;
            GrvRoleAndCategorize.DataSource = GeneralDB.GetPageDB(GrvRoleAndCategorize.MasterTableView.PageSize, pageIndex,
                                                                    whereClause, ViewState["sortField"].ToString(),
                                                                    "[acs].v_AcsRoleAndCategorize", "RoleAndCategorizeId", out count,
                                                                    ViewState["SortType"].ToString());
            GrvRoleAndCategorize.MasterTableView.VirtualItemCount = count;
            GrvRoleAndCategorize.DataBind();
        }

        protected void SetPageControl()
        {
            TxtName.Text = "";
            CmbIsActive.SelectedIndex = 1;
            CmbSystem.ClearSelection();
        }

        protected void GrvRoleAndCategorize_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var RoleAndCategorizeId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["RoleAndCategorizeId"] = RoleAndCategorizeId;
                var myRoleAndCategorize = Classes.AcsRoleAndCategorize.GetSingleById(RoleAndCategorizeId);
                if (myRoleAndCategorize != null)
                {
                    TxtName.Text = myRoleAndCategorize.RoleAndCategorizeTilte;
                    CmbSystem.SelectedValue = myRoleAndCategorize.SystemId.ToString();
                    CmbIsActive.SelectedValue = myRoleAndCategorize.IsActive.ToString();

                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsRoleAndCategorize.Delete(Convert.ToInt32(e.CommandArgument));
                    SetgrvRoleAndCategorizeData(GrvRoleAndCategorize.MasterTableView.CurrentPageIndex,
                                              ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvRoleAndCategorize_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetgrvRoleAndCategorizeData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRoleAndCategorize_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetgrvRoleAndCategorizeData(GrvRoleAndCategorize.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRoleAndCategorize_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetgrvRoleAndCategorizeData(GrvRoleAndCategorize.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvRoleAndCategorize_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvRoleAndCategorize_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }        
    }
}