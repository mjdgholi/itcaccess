﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.ItcAccess.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.ItcAccess.BaseInfo
{
    public partial class FrmPattern : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (ViewState["PageStatus"] == null)
            {
                ViewState["SortType"] = (ViewState["SortType"] == null) ? "" : ViewState["SortType"].ToString();
                ViewState["sortField"] = (ViewState["sortField"] == null) ? "" : ViewState["sortField"].ToString();
                ViewState["WhereClause"] = (ViewState["WhereClause"] == null)
                                               ? "1=1 "
                                               : ViewState["WhereClause"].ToString();
                pnlAll.DefaultButton = "BtnAdd";               
                SetCmbPatternGroup();
                SetGrvObjectData(0, "");
                BtnEdit.Visible = false;
                ViewState["PageStatus"] = "IsPostBack";
                SetPageControl();
            }
            if (!IsPostBack)
            {

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int addedId;
                AcsPattern.Add(TxtPatternTitle.Text,TxtPatternTemplate.Text,Int32.Parse(CmbPatternGroup.SelectedValue),
                                bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString(),
                                out addedId);
                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsPattern", "PatternId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "PatternId=" + addedId, "");
                var pageSize = GrvPattern.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvPattern.Items[recIndex].Selected = true;
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var PatternId = Convert.ToInt32(ViewState["PatternId"].ToString());
                Classes.AcsPattern.Update(PatternId, TxtPatternTitle.Text, TxtPatternTemplate.Text, Int32.Parse(CmbPatternGroup.SelectedValue),
                                bool.Parse(CmbIsActive.SelectedValue), DateTime.Now.ToString(), DateTime.Now.ToString());

                var recRowNumber = GeneralDB.GetRowNumberOfRecord("acs.v_AcsPattern", "PatternId",
                                                                  ViewState["sortField"].ToString(),
                                                                  ViewState["SortType"].ToString(),
                                                                  "PatternId=" + PatternId, "");
                var pageSize = GrvPattern.MasterTableView.PageSize;
                var curRecPage = (recRowNumber - 1) / pageSize;
                var recIndex = (recRowNumber % pageSize == 0) ? pageSize - 1 : (recRowNumber % pageSize) - 1;
                SetGrvObjectData(curRecPage, "");

                GrvPattern.Items[recIndex].Selected = true;
                BtnAdd.Visible = true;
                BtnEdit.Visible = false;
                pnlAll.DefaultButton = "BtnEdit";
                SetPageControl();
                MessageErrorControl1.ShowSuccesMessage("ویرایش موفق");
            }
            catch (Exception ex)
            {
                MessageErrorControl1.ShowErrorMessage(ex.Message);
            }
        }        

        protected void SetCmbPatternGroup()
        {
            CmbPatternGroup.Items.Clear();
            CmbPatternGroup.Items.Add(new RadComboBoxItem(""));
            CmbPatternGroup.DataTextField = "PatternGroupTitle";
            CmbPatternGroup.DataValueField = "PatternGroupId";
            CmbPatternGroup.DataSource = AcsPatternGroup.GetAll();
            CmbPatternGroup.DataBind();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "1 = 1";

            if (TxtPatternTitle.Text.Trim()!="")
            {
                ViewState["WhereClause"] += " and PatternTitle like N'%" + TxtPatternTitle.Text+"%' ";
            }
            if (TxtPatternTemplate.Text.Trim() != "")
            {
                ViewState["WhereClause"] += " and PatternTemplate like N'%" + TxtPatternTemplate.Text + "%' ";
            }
            if (CmbPatternGroup.SelectedIndex > 0)
            {
                ViewState["WhereClause"] += " and PatternGroupId =" + CmbPatternGroup.SelectedValue;
            }
            switch (CmbIsActive.SelectedValue)
            {
                case "True":
                    ViewState["WhereClause"] += " and IsActive=1";
                    break;
                case "False":
                    ViewState["WhereClause"] += " and IsActive=0";
                    break;
            }

            SetGrvObjectData(0, ViewState["WhereClause"].ToString());
        }

        protected void BtnShowAll_Click(object sender, EventArgs e)
        {
            ViewState["WhereClause"] = "";
            SetGrvObjectData(0, "");

            SetPageControl();
            BtnAdd.Visible = true;
            pnlAll.DefaultButton = "BtnAdd";
            BtnEdit.Visible = false;
        }

        protected void SetGrvObjectData(int pageIndex, string whereClause)
        {
            int count;

            GrvPattern.MasterTableView.CurrentPageIndex = pageIndex;
            GrvPattern.DataSource = GeneralDB.GetPageDB(GrvPattern.MasterTableView.PageSize, pageIndex,
                                                         whereClause, ViewState["sortField"].ToString(),
                                                         "[acs].v_AcsPattern", "PatternId", out count,
                                                         ViewState["SortType"].ToString());
            GrvPattern.MasterTableView.VirtualItemCount = count;
            GrvPattern.DataBind();
        }

        protected void SetPageControl()
        {
            TxtPatternTitle.Text = "";
            TxtPatternTemplate.Text = "";
            CmbPatternGroup.ClearSelection();
            CmbIsActive.SelectedIndex = 1;
        }

        protected void GrvPattern_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "_MyِEdit")
            {
                e.Item.Selected = true;
                var patternId = Int32.Parse(e.CommandArgument.ToString());
                ViewState["PatternId"] = patternId;
                var myPattern = Classes.AcsPattern.GetSingleById(patternId);
                if (myPattern != null)
                {
                    TxtPatternTitle.Text = myPattern.PatternTitle;
                    TxtPatternTemplate.Text = myPattern.PatternTemplate;
                    CmbPatternGroup.SelectedValue = myPattern.PatternGroupId.ToString();
                    CmbIsActive.SelectedValue = myPattern.IsActive.ToString();
                    BtnEdit.Visible = true;
                    BtnAdd.Visible = false;
                    pnlAll.DefaultButton = "BtnEdit";
                }
            }
            if (e.CommandName == "_MyِDelete")
            {
                try
                {
                    AcsPattern.Delete(Convert.ToInt32(e.CommandArgument));
                    SetGrvObjectData(GrvPattern.MasterTableView.CurrentPageIndex,
                                     ViewState["WhereClause"].ToString());
                    BtnEdit.Visible = false;
                    BtnAdd.Visible = true;
                    pnlAll.DefaultButton = "BtnAdd";
                    SetPageControl();

                    MessageErrorControl1.ShowSuccesMessage("ثبت موفق");
                }
                catch (Exception ex)
                {
                    MessageErrorControl1.ShowErrorMessage(ex.Message);
                }
            }
        }

        protected void GrvPattern_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SetGrvObjectData(e.NewPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPattern_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            var lastSortField = ViewState["sortField"].ToString();
            ViewState["sortField"] = e.SortExpression;
            if (lastSortField == e.SortExpression)
            {
                if (ViewState["SortType"].ToString() == "ASC")
                {
                    ViewState["SortType"] = "DESC";
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                }
            }
            else
            {
                ViewState["SortType"] = "ASC";
            }
            SetGrvObjectData(GrvPattern.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPattern_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            SetGrvObjectData(GrvPattern.CurrentPageIndex, ViewState["WhereClause"].ToString());
        }

        protected void GrvPattern_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {

        }

        protected void GrvPattern_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }
    }
}