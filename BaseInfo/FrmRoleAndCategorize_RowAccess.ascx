﻿.<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmRoleAndCategorize_RowAccess.ascx.cs"
    Inherits="Intranet.DesktopModules.ItcAccess.BaseInfo.FrmRoleAndCategorize_RowAccess" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %><%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %><%@ Register Src="../MessageErrorControl.ascx" TagName="MessageErrorControl" TagPrefix="uc1" %><telerik:RadScriptBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">
 fnRoleAndCateGorizeSearch:
       {
          
           var myValue = new Array(<%=Session["ItemSearch"].ToString() %>); 
           var index= -1;
           
           function NextNode() {    
               //alert(index);
               if (index <  myValue.length - 1) {                      
                   index = index + 1;                 
                   expandNode(myValue[index]);
                   
                   return true;
                }
                else
                {

                  return false;
                }
           }

           function PreviousNode() {
              // alert(index);
               if (index > 0)
               {
                index = index - 1;  
                expandNode(myValue[index]);               
               return true;
               }
               else
               {
               return false;
               }
           }
           

           function expandNode(nodeid) {
               var treeView = $find("<%= trvRoleAndCategorize.ClientID %>");
               var node = treeView.findNodeByValue(nodeid);
               if (node) {
                   node.get_parent().expand();
                   node.expand();
                   //node.select();
                   node.set_selected(true);                   
                   scrollToNode(treeView, node);
                   return true;
               }
               return false;
           }


           function scrollToNode(treeview, node) {
               var nodeElement = node.get_contentElement();
               var treeViewElement = treeview.get_element();

               var nodeOffsetTop = treeview._getTotalOffsetTop(nodeElement);
               var treeOffsetTop = treeview._getTotalOffsetTop(treeViewElement);
               var relativeOffsetTop = nodeOffsetTop - treeOffsetTop;

               if (relativeOffsetTop < treeViewElement.scrollTop) {
                   treeViewElement.scrollTop = relativeOffsetTop;
               }

               var height = nodeElement.offsetHeight;

               if (relativeOffsetTop + height > (treeViewElement.clientHeight + treeViewElement.scrollTop)) {
                   treeViewElement.scrollTop += ((relativeOffsetTop + height) - (treeViewElement.clientHeight + treeViewElement.scrollTop));
               }
           }
       }          
    </script>
</telerik:RadScriptBlock>
<asp:Panel ID="PnlAll" runat="server">
    <table dir="rtl" width="100%">
        <tr>
            <td width="100%" valign="top" colspan="2">
                <asp:Panel runat="server" ID="PnlItem">
                    <table dir="rtl" width="100%">
                        <tr>
                            <td colspan="5">
                                <table dir="rtl">
                                    <tr>
                                        <td align="right">
                                            <cc1:CustomRadButton ID="BtnAdd" runat="server" OnClick="BtnAdd_Click">
                                            </cc1:CustomRadButton>
                                        </td>
                                        <td align="right">
                                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" OnClick="BtnEdit_Click">
                                                <Icon PrimaryIconCssClass="rbEdit" />
                                            </cc1:CustomRadButton>
                                        </td>
                                        <td align="right">
                                            <cc1:CustomRadButton ID="BtnSearch0" runat="server" CausesValidation="False" CustomeButtonType="Search"
                                                OnClick="BtnSearch_Click">
                                                <Icon PrimaryIconCssClass="rbSearch" />
                                            </cc1:CustomRadButton>
                                        </td>
                                        <td align="right">
                                            <cc1:CustomRadButton ID="BtnShowAll0" runat="server" CausesValidation="False" CustomeButtonType="ShowAll"
                                                Empty="" OnClick="BtnShowAll_Click" Text="نمایش همگی" ToolTip="نمایش همگی" Width="100px">
                                                <Icon PrimaryIconCssClass="rbRefresh" />
                                            </cc1:CustomRadButton>
                                        </td>
                                        <td align="right">
                                            <uc1:MessageErrorControl ID="MessageErrorControl2" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label ID="LblSystem" runat="server" Text="عنوان سیستم:"></asp:Label>
                            </td>
                            <td width="40%">
                                <cc1:CustomRadComboBox ID="CmbSystem" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    Filter="Contains" MarkFirstMatch="True" NoWrap="True" 
                                    onselectedindexchanged="CmbSystem_SelectedIndexChanged" Skin="Office2007" 
                                    Width="400px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="10%" colspan="2">
                                &nbsp;</td>
                            <td width="40%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label ID="LblObject" runat="server" Text="عنوان آبجکت:"></asp:Label>
                            </td>
                            <td width="40%">
                                <cc1:CustomRadComboBox ID="CmbObject" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    Filter="Contains" MarkFirstMatch="True" NoWrap="True" 
                                    OnSelectedIndexChanged="CmbObject_SelectedIndexChanged" Skin="Office2007" 
                                    Width="400px">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td colspan="2" width="10%">
                            </td>
                            <td width="40%">
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" nowrap="nowrap">
                                <asp:Label ID="LblObject0" runat="server" Text="دسترسی های آبجکت:"></asp:Label>
                            </td>
                            <td width="40%" dir="rtl">
                                <cc1:CustomRadComboBox ID="CmbObjectRowAccess" runat="server" Filter="Contains" MarkFirstMatch="True"
                                    Width="400px" OnSelectedIndexChanged="CmbObjectRowAccess_SelectedIndexChanged"
                                    CheckBoxes="True" EnableCheckAllItemsCheckBox="True" Height="400px" Skin="Office2007">
                                    <Localization AllItemsCheckedString="تمامی موارد انتخاب گردید" CheckAllString="انتخاب همه" />
                                </cc1:CustomRadComboBox>
                            </td>
                            <td align="right">
                                <asp:RequiredFieldValidator ID="rfvObjectRowAccess" runat="server" ControlToValidate="CmbObjectRowAccess"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td align="right" colspan="2">
                                <cc1:SelectControl ID="SelectControlPerson" runat="server" RadWindowHeight="700"
                                    RadWindowWidth="850" imageName="RunQuery" Width="1px" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQueryForRowAccess.aspx" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label ID="LblAccessType" runat="server" Text="نوع دسترسی:"></asp:Label>
                            </td>
                            <td width="40%">
                                <telerik:RadComboBox ID="CmbAccessType" runat="server" AppendDataBoundItems="True"
                                    CausesValidation="False" KeyId="" Width="100px" MarkFirstMatch="True" Filter="Contains"
                                    Skin="Office2007">
                                    <Localization AllItemsCheckedString="تمامی موارد انتخاب گردید" CheckAllString="انتخاب خمه" />
                                </telerik:RadComboBox>
                            </td>
                            <td width="50%" colspan="3">
                                <asp:RequiredFieldValidator ID="rfvAccessType" runat="server" ControlToValidate="CmbAccessType"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label ID="LblIsActive" runat="server" Text="وضیعت رکورد:"></asp:Label>
                            </td>
                            <td width="40%">
                                <telerik:RadComboBox ID="CmbIsActive" runat="server" KeyId="" Width="100px" MarkFirstMatch="True"
                                    Filter="Contains" Skin="Office2007">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Selected="True" />
                                        <telerik:RadComboBoxItem runat="server" Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem runat="server" Text="غیر فعال" Value="False" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                            <td colspan="3" width="50%">
                                <asp:RequiredFieldValidator ID="rfvIsActive" runat="server" ControlToValidate="CmbIsActive"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" width="10%">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="TxtGroupName" runat="server" Width="120px" LabelWidth="75px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgbtnSearch" runat="server" CausesValidation="False" Height="25px"
                                ImageUrl="../Images/SearchOrganizationPhysicalChart.png" OnClick="imgbtnSearch_Click"
                                ToolTip="جستجو" Width="25px" />
                        </td>
                        <td>
                            <img alt="جلو" height="25" onclick="NextNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Next.png"%>'
                                width="25" />
                        </td>
                        <td>
                            <img alt="عقب" height="25" onclick="PreviousNode();" src='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath + "/DeskTopModules/ItcAccess/Images/Previous.png"%>'
                                width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <telerik:RadTreeView ID="trvRoleAndCategorize" runat="server" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" dir="rtl" Height="400px"
                                OnNodeClick="trvRoleAndCategorize_NodeClick" OnNodeDataBound="trvRoleAndCategorize_NodeDataBound"
                                Skin="Office2007">
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
                <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="trvRoleAndCategorize"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td valign="top" width="90%">
                <telerik:RadGrid ID="GrvRowAccess" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnDetailTableDataBind="GrvRowAccess_DetailTableDataBind"
                    OnItemCommand="GrvObjectAccess_ItemCommand" OnItemDataBound="GrvObjectAccess_ItemDataBound"
                    OnNeedDataSource="GrvRowAccess_NeedDataSource" OnPageIndexChanged="GrvObjectAccess_PageIndexChanged"
                    OnPageSizeChanged="GrvObjectAccess_PageSizeChanged" OnSortCommand="GrvObjectAccess_SortCommand"
                    Skin="Office2007" Width="100%">
                    <ClientSettings AllowColumnsReorder="True" EnableRowHoverStyle="true" ReorderColumnsOnClient="True">
                        <Selecting AllowRowSelect="True" />
                        <Selecting AllowRowSelect="True" />
                    </ClientSettings>
                    <MasterTableView ClientDataKeyNames="ObjectId" DataKeyNames="ObjectId" Dir="RTL"
                        NoDetailRecordsText="اطلاعاتی برای نمایش یافت نشد" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد"
                        Name="MasterTable">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <DetailTables>
                            <telerik:GridTableView runat="server" DataKeyNames="UserAndGroup_Role_AccessId" Name="RowAccessDetail">
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="AccessTitle" FilterControlAltText="Filter AccessTitle column"
                                        HeaderText="عنوان دسترسی" SortExpression="AccessTitle" UniqueName="AccessTitle">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccessTypeTitle" FilterControlAltText="Filter AccessTypeTitle column"
                                        HeaderText="نوع دسترسی" SortExpression="AccessTypeTitle" UniqueName="AccessTypeTitle">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="ویرایش"
                                        UniqueName="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>'
                                                CommandName="_MyEditUserAndGroup_Role_Access" ForeColor="#000066" ImageUrl="../Images/Edit.bmp" /></ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                                        UniqueName="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>'
                                                CommandName="_MyDeleteUserAndGroup_Role_Access" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                                OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="مشاهده نتیجه دسترسی"
                                        UniqueName="ResultPerRolePerRowAccess">
                                        <ItemTemplate>
                                            <cc1:SelectControl ID="SelectControlResultPerRolePerRowAccess" runat="server" RadWindowHeight="700"
                                                CommandArgument='<%#Eval("UserAndGroup_Role_AccessId").ToString() %>' RadWindowWidth="850"
                                                imageName="RunQuery" Width="1px" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQuery.aspx" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </telerik:GridTableView></DetailTables>
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="ObjectTitle" FilterControlAltText="Filter ObjectTitle column"
                                HeaderText="عنوان آبجکت" SortExpression="ObjectTitle" UniqueName="ObjectTitle">
                                <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                <ItemStyle HorizontalAlign="Right" Wrap="False" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="حذف"
                                UniqueName="DeleteAllRowAccessPerObjectPerRole">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgButtonDeleteAllRowAccessPerObjectPerRole" runat="server"
                                        CausesValidation="false" CommandArgument='<%#Eval("ObjectId").ToString() %>'
                                        CommandName="_MyDeleteAllRowAccessPerObjectPerRole" ForeColor="#000066" ImageUrl="../Images/Delete.bmp"
                                        OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;" /></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="مشاهده نتیجه دسترسی"
                                UniqueName="ResultPerObjectPerRole">
                                <ItemTemplate>
                                    <cc1:SelectControl ID="SelectControlResultPerObjectPerRole" runat="server" RadWindowHeight="700"
                                        CommandArgument='<%#Eval("ObjectId").ToString() %>' RadWindowWidth="850" imageName="RunQuery"
                                        Width="1px" PortalPathUrl="ItcAccess/ModalForm/FrmRunSqlQuery.aspx" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                            VerticalAlign="Middle" />
                    </MasterTableView>
                    <HeaderStyle Height="40px" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                    </HeaderContextMenu>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Office2007">
    </telerik:RadAjaxLoadingPanel>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadCodeBlock2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PnlAll" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnAdd">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnSearch0">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="BtnShowAll0">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbSystem">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CmbObject" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbObject">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="CmbObjectRowAccess" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="trvRoleAndCategorize">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="GrvRowAccess">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadCodeBlock2" />
                <telerik:AjaxUpdatedControl ControlID="PnlAll" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
